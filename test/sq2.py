import csv
import random
import sqlite3

path = "/home/kemal/.local/share/Kuyosis/kuyosis.db"

db = sqlite3.connect(path)
cur = db.cursor()



# sorgu = DbQuery()
# sorgu.cinsiyeteGoreOkumaSayisi()


cikis_saati = f"{random.randint(10, 15)}:{random.randint(10, 60)}:{random.randint(10, 60)}"
print(cikis_saati)


def kitapTransfer():
    sayac = 1
    for i in range(1,25000):
        kitap_id = random.randint(1,  4985)
        uye_id = random.randint(1, 496)
        cikis_tarihi = f"2023-0{random.randint(1, 9)}-{random.randint(10, 30)}"
        donus_tarihi = f"2024-0{random.randint(1, 3)}-{random.randint(10, 25)}"
        cikis_saati = f"{random.randint(10, 15)}:{random.randint(10, 60)}:{random.randint(10, 60)}"
        donus_saati = f"{random.randint(10, 15)}:{random.randint(10, 60)}:{random.randint(10, 60)}"
        # durum = True
        cur.execute("""INSERT INTO KitapTransfer
                            (kitap_id, uye_id, cikis_tarihi, donus_tarihi, cikis_saati, donus_saati, durum) 
                             VALUES(?, ?, ?, ?, ?, ?, ?)""",
                            (kitap_id, uye_id, cikis_tarihi, donus_tarihi, cikis_saati, donus_saati, True)
                ) 

        # print(sayac)
        # sayac+=1
        db.commit()

      

    db.close()
    print("bitti")

kitapTransfer()


def kitapTransferSil():
    for i in range(1000000, 1102030):
        try:
            cur.execute("DELETE FROM KitapTransfer WHERE rowID=?", (i,))
            db.commit()
        except: pass

    print("işlem bitti")
    db.close()




def kitap():
    karekod = 100008

    with open('/home/kemal/YazılımGelistirme/Python/küyösisQt6/test/kitaplar2.csv', 'r') as file:
        reader = csv.reader(file)
        for r in reader:
            if bool(r[0]) == True:
                print(r[0], ":", r[1], r[2])
                ad = r[0]
                yazar = r[1] if bool(r[1])  else "ANONİM"
                yayinevi = r[2] if bool(r[2])  else "MEB YAYINLARI"
                tur = "ROMAN"
                syf = random.randint(100, 1000)

                cur.execute("""INSERT INTO Kitaplar
                        (karekod_no, kitap_adi, yazar, yayinevi, tür, sayfa, bilgi, raf, eklenme_tarihi) 
                        VALUES(?,?,?,?,?,?,?,?, datetime('now', 'localtime'))""",  (karekod, ad, yazar, yayinevi, tur, syf, "", "GENEL")
                    )

                db.commit()
                karekod+=1
                print(karekod)

    db.close()
"""CREATE TABLE IF NOT EXISTS Uyeler(
                        id INTEGER NOT NULL PRIMARY KEY,
                        uye_tc INTEGER UNIQUE, 
                        uye_adi TEXT NOT NULL,
                        uye_soyadi TEXT NOT NULL,
                        d_tarihi TEXT,
                        cinsiyet TEXT,
                        ogrenci_no INTEGER UNIQUE, 
                        sinif INTEGER,
                        sube TEXT,
                        adres TEXT,
                        telefon TEXT,
                        kart_durumu TEXT,
                        bilgi TEXT,
                        eklenme_tarihi TEXT NOT NULL """
def uye():
    karekod = 100008

    with open('/home/kemal/YazılımGelistirme/Python/küyösisQt6/test/personel.csv', 'r') as file:
        reader = csv.reader(file)
        for r in reader:
            print(r)
            tc = r[0]
            ad = r[1]
            soyad = r[2]
            dogum = r[3]
            cinsiyet = None
            ogrenci_no = random.randint(100, 854789654)
            sinif = random.randint(1, 12)
            sube = random.choice(["A", "B", "C", "Ç","D","E","F"])
            adres = "KARS"
            tel = "5422967429"
            kart = False
            bilgi = ""

            print(sinif, sube, ogrenci_no)

            try:           
                cur.execute("""INSERT INTO Uyeler
                            (id, uye_tc, uye_adi, uye_soyadi, d_tarihi, cinsiyet, ogrenci_no, sinif, sube, adres, telefon, kart_durumu, bilgi, eklenme_tarihi) 
                             VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?, datetime('now', 'localtime'))""", 
                            (None, tc, ad, soyad, dogum, cinsiyet, ogrenci_no, sinif, sube, adres, tel, kart, bilgi)
                ) 

                db.commit()
            except:
                print("hata")

        db.close()



