#!/usr/bin/python

import sys

from PyQt6.QtCore import QRect, QPropertyAnimation
from PyQt6.QtWidgets import QWidget, QApplication, QFrame, QPushButton


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.button = QPushButton("Start", self)
        self.button.clicked.connect(self.doAnim)
        self.button.move(250,250)

        self.frame = QFrame(self)
        self.frame.setFrameStyle(QFrame.Shape.Panel | QFrame.Shadow.Raised)
        self.frame.setGeometry(0, 0, 40, 300)

        self.setGeometry(300, 300, 380, 300)
        self.setWindowTitle('Animation')
       

    def doAnim(self):

        self.anim = QPropertyAnimation(self.frame, b"geometry")
        self.anim.setDuration(400)
        self.anim.setStartValue(QRect(0, 0, 56, 1000))
        self.anim.setEndValue(QRect(0, 0, 200, 1000))
        self.anim.start()


def main():

    app = QApplication([])
    ex = Example()
    ex.show()
    sys.exit(app.exec())

main()