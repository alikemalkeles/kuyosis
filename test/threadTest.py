import sys
import time

from PyQt6.QtCore import QThread, pyqtSignal as Signal, pyqtSlot as Slot
from PyQt6.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QVBoxLayout, QProgressBar


class WorkerTaken(QThread):
    progress = Signal(int)
    completed = Signal(int)

    @Slot(int)
    def run(self):
        for i in range(1, 10):
            time.sleep(1)
            self.progress.emit(i)

        self.completed.emit(i)


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.setGeometry(100, 100, 300, 50)
        self.setWindowTitle('QThread Demo')

        # setup widget
        self.widget = QWidget()
        layout = QVBoxLayout()
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)

        self.progress_bar = QProgressBar(self)
        self.progress_bar.setValue(0)

        self.btn_start = QPushButton('Start', clicked=self.start)

        layout.addWidget(self.progress_bar)
        layout.addWidget(self.btn_start)

        self.worker = WorkerTaken()
        self.worker_thread = QThread()

        self.worker.progress.connect(self.update_progress)


      #  self.work_requested.connect(self.worker.do_work)

        # move worker to the worker thread
        self.worker.moveToThread(self.worker_thread)

        # start the thread


        # show the window
        self.show()

    def start(self):
        #self.btn_start.setEnabled(False)
      #  self.worker_thread.start()
        self.worker.start()

        n = 5
        self.progress_bar.setMaximum(n)

    def update_progress(self, v):
        self.progress_bar.setValue(v)




if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec())