import csv
import random
import sqlite3
import time

path = "/home/kemal/.local/share/Kuyosis/kuyosis.db"

db = sqlite3.connect(path)
cur = db.cursor()

start_time = time.time()

# sorgu = cur.execute("SELECT kitap_id FROM KitapTransfer WHERE donus_tarihi IS NULL;").fetchall()
sorgu = cur.execute("SELECT * FROM KitapTransfer WHERE kitap_id=2548 AND durum='0' ").fetchall()
if 245 in [i for i in sorgu]:
    print("içinde var", [i[0] for i in sorgu])
else:
    print("içinde yok: ", [i[0] for i in sorgu])

print(f"Sorgu süresi: : {(time.time() - start_time)}")



def kitapTransfer():
    sayac = 1
    print("YAzdırma başladı....")
    for i in range(1, 400000):
        kitap_id = random.randint(500,  20000)
        uye_id = random.randint(100, 4500)
        cikis_tarihi = f"2023-1{random.randint(1, 2)}-{random.randint(10, 30)}"

        try:
            cur.execute("""INSERT INTO KitapTransfer
                            (kitap_id, uye_id, cikis_tarihi, donus_tarihi, cikis_saati, donus_saati, durum) 
                             VALUES(?, ?,  ?, date('now', 'localtime'), time('now', 'localtime'), time('now', 'localtime'), ?)""", 
                            (kitap_id, uye_id, cikis_tarihi, True)
                ) 

        # print(sayac)
        # sayac+=1
            db.commit()
        except:
            pass
    

    db.close()
    print("bitti")


# kitapTransfer()


def kitap():
    karekod = 119704

    with open('/home/kemal/YazılımGelistirme/Python/küyösisQt6/test/kitaplar2.csv', 'r') as file:
        reader = csv.reader(file)
        for r in reader:
            if bool(r[0]) == True:
                print(r[0], ":", r[1], r[2])
                ad = r[0]
                yazar = r[1] if bool(r[1])  else "ANONİM"
                yayinevi = r[2] if bool(r[2])  else "MEB YAYINLARI"
                tur = "ROMAN"
                syf = random.randint(100, 1000)

                cur.execute("""INSERT INTO Kitaplar
                        (karekod_no, kitap_adi, yazar, yayinevi, tür, sayfa, bilgi, raf, eklenme_tarihi) 
                        VALUES(?,?,?,?,?,?,?,?, datetime('now', 'localtime'))""",  (karekod, ad, yazar, yayinevi, tur, syf, "", "GENEL")
                    )

                db.commit()
                karekod+=1
                print(karekod)

    db.close()



"""CREATE TABLE IF NOT EXISTS Uyeler(
                        id INTEGER NOT NULL PRIMARY KEY,
                        uye_tc INTEGER UNIQUE, 
                        uye_adi TEXT NOT NULL,
                        uye_soyadi TEXT NOT NULL,
                        d_tarihi TEXT,
                        cinsiyet TEXT,
                        ogrenci_no INTEGER UNIQUE, 
                        sinif INTEGER,
                        sube TEXT,
                        adres TEXT,
                        telefon TEXT,
                        kart_durumu TEXT,
                        bilgi TEXT,
                        eklenme_tarihi TEXT NOT NULL """
def uye():
    karekod = 100008

    with open('/home/kemal/YazılımGelistirme/Python/küyösisQt6/test/personel.csv', 'r') as file:
        reader = csv.reader(file)
        for r in reader:
            print(r)
            tc = r[0]
            ad = r[1]
            soyad = r[2]
            dogum = r[3]
            cinsiyet = None
            ogrenci_no = random.randint(100, 854789654)
            sinif = random.randint(1, 12)
            sube = random.choice(["A", "B", "C", "Ç","D","E","F"])
            adres = "KARS"
            tel = "5422967429"
            kart = False
            bilgi = ""

            print(sinif, sube, ogrenci_no)

            try:           
                cur.execute("""INSERT INTO Uyeler
                            (id, uye_tc, uye_adi, uye_soyadi, d_tarihi, cinsiyet, ogrenci_no, sinif, sube, adres, telefon, kart_durumu, bilgi, eklenme_tarihi) 
                             VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?, datetime('now', 'localtime'))""", 
                            (None, tc, ad, soyad, dogum, cinsiyet, ogrenci_no, sinif, sube, adres, tel, kart, bilgi)
                ) 

                db.commit()
            except:
                print("hata")

        db.close()



