import sys

from PyQt6 import QtCore
from PyQt6.QtWidgets import QApplication, QVBoxLayout, QHBoxLayout, QPushButton, QStackedWidget, \
    QFrame, QWidget


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        # self.userInfo = user

         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint)
        # seffaf pencere

        self.setContentsMargins(0,0,0,0)
        self.setFixedSize(1000, 700)
        self.show()

        #ana layout bu layouta sol tarafında bir tane menu farme ve sağ tarında vertical layout yer alacak
        self.mainLayout = QHBoxLayout(self)
        self.mainLayout.setContentsMargins(0,0,0,0)


        self.menuFrame = QFrame() 
        self.windowFrame = QFrame()     # sol menuler
        

        self.windowFrame.setFixedSize(900, 860)
        self.windowFrame.setObjectName("windowFrame")
        self.menuFrame.setContentsMargins(0,0,0,0)
        self.menuFrame.setObjectName("menuFrame")
        

        self.mainLayout.addWidget(self.menuFrame)
        self.mainLayout.addWidget(self.windowFrame)

        self.setLayout(self.mainLayout)

## MenuFrame ve ögeleri ve bu ögeye bağlı vertical layout yer alacak
        self.menuFrameLayout = QVBoxLayout(self.menuFrame)

        self.menuBut1 = QPushButton("1")
        self.menuBut2 = QPushButton("2")
        self.menuBut3 = QPushButton("3")
        self.menuBut4 = QPushButton("4")
        self.menuBut5 = QPushButton("5")
        self.menuBut6 = QPushButton("5")
       
        self.menuBut1.setObjectName("menuBut1")
        self.menuBut2.setObjectName("menuBut2")
        self.menuBut3.setObjectName("menuBut3")
        self.menuBut4.setObjectName("menuBut4")
        self.menuBut5.setObjectName("menuBut5")
        self.menuBut6.setObjectName("menuBut6")
        
        self.menuBut1.setFixedSize(100, 35)
        self.menuBut2.setFixedSize(100, 35)
        self.menuBut3.setFixedSize(100, 35)
        self.menuBut4.setFixedSize(100, 35)
        self.menuBut5.setFixedSize(100, 35)
        self.menuBut6.setFixedSize(100, 35)


        self.menuFrameLayout.addWidget(self.menuBut1,     alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.menuFrameLayout.addWidget(self.menuBut2,     alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.menuFrameLayout.addWidget(self.menuBut3,     alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.menuFrameLayout.addWidget(self.menuBut4,     alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.menuFrameLayout.addWidget(self.menuBut5,     alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.menuFrameLayout.addWidget(self.menuBut6,     alignment=QtCore.Qt.AlignmentFlag.AlignLeft)



        # window layouta bağlı bir tane üste bar görevi görecek frame ve altta ise stack widgetler yer alacak
        self.windowLayout = QVBoxLayout(self.windowFrame) # stack widget burada yer alacak
        self.windowLayout.setContentsMargins(0,0,0,0)

        self.tabBarFrame = QFrame()
        self.stackedWidget = QStackedWidget()

        self.stackedWidget.setObjectName("stacked")

        self.tabBarFrame.setObjectName("tabBarFrame")
        self.tabBarFrame.setFixedHeight(45)
        self.stackedWidget.setContentsMargins(0,0,0,0)
        self.stackedWidget.setFixedSize(900, 800)
    #    self.tabBarFrame.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Maximum)

        self.windowLayout.addWidget(self.tabBarFrame)
        self.windowLayout.addWidget(self.stackedWidget)






## windows layout ögeleri burada yer alacak




with open("./assets/stil/stil_mainWindow.qss", "r") as file:
    stil = file.read()
    uyg = QApplication(sys.argv)
    pen = MainWindow()
    pen.setStyleSheet(stil)
    uyg.exec()