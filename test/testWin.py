import os
import sys

from PyQt6.QtCharts import QChart, QChartView, QLineSeries, QBarSeries, QBarSet, QPieSeries, QScatterSeries, \
    QSplineSeries, QPercentBarSeries, QBarCategoryAxis
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QColor
from PyQt6.QtWidgets import QApplication, QMainWindow, QTabWidget
import math


class ChartWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        basedir = os.path.dirname(__file__)
        print(basedir)

        self.setWindowTitle("PyQt6 Charts Examples")
        self.setGeometry(100, 100, 800, 600)

        self.tabs = QTabWidget()
        self.setCentralWidget(self.tabs)

        self.line_chart = self.create_line_chart()
        self.bar_chart = self.create_bar_chart()
        self.pie_chart = self.create_pie_chart()
        self.scatter_chart = self.create_scatter_chart()
        self.spline_chart = self.create_spline_chart()
        self.percent_bar_chart = self.create_percent_bar_chart()

        self.tabs.addTab(self.line_chart, "Line Chart")
        self.tabs.addTab(self.bar_chart, "Bar Chart")
        self.tabs.addTab(self.pie_chart, "Pie Chart")
        self.tabs.addTab(self.scatter_chart, "Scatter Chart")
        self.tabs.addTab(self.spline_chart, "Spline Chart")
        self.tabs.addTab(self.percent_bar_chart, "Percent Bar Chart")

    def yuzdeKaci(self, bir_sayi, toplam_sayi):
        if bir_sayi >0 and toplam_sayi >0:
              a = (bir_sayi / toplam_sayi) * 100
              print("yuzde", a)
              return  a
        else:
            return 0
    

    def create_line_chart(self):
        series = QLineSeries()
        series.append(0, 6)
        series.append(2, 4)
        series.append(3, 8)
        series.append(7, 4)

        chart = QChart()
        chart.addSeries(series)
        chart.createDefaultAxes()
        chart.setTitle("Line Chart Example")

        view = QChartView(chart)


        return view

    def create_bar_chart(self):
        set1 = QBarSet("Data Set 1 test")
        set1.append(90)
       

        set2 = QBarSet("Data Set 2")
        set2.append(2)
       

        series = QBarSeries()
        series.append(set1)
        series.append(set2)
        # series.append(set2)

        chart = QChart()
        chart.addSeries(series)
        chart.createDefaultAxes()
        chart.setTitle("Bar Chart Example")

        view = QChartView(chart)


        return view

    def create_pie_chart(self):
            series = QPieSeries()

            series.append("1.S", 1)
            series.append("2.S", 2)
            series.append("3.S", 3)
            series.append("4.S", 3)
            series.append("5.S", 3)



            chart = QChart()
            chart.addSeries(series)
            chart.setTitle("Pie Chart Example")

            view = QChartView(chart)


            return view

    def create_scatter_chart(self):
            series = QScatterSeries()
            series.append(0, 0)
            series.append(2, 2)
            series.append(4, 4)

            chart = QChart()
            chart.addSeries(series)
            chart.createDefaultAxes()
            chart.setTitle("Scatter Chart Example")

            view = QChartView(chart)

            return view

    def create_spline_chart(self):
            series = QSplineSeries()
            series.append(0, 6)
            series.append(2, 4)
            series.append(3, 8)
            series.append(7, 4)

            chart = QChart()
            chart.addSeries(series)
            chart.createDefaultAxes()
            chart.setTitle("Spline Chart Example")

            view = QChartView(chart)

            return view

    def create_percent_bar_chart(self):
            

            set0 = QBarSet("Jane")
            set1 = QBarSet("John")

            set1.setColor(QColor(250,250,250, 0))
          
            sayilar = [50, 40, 30, 60, 90]
            toplam = sum(sayilar)
            oranlar = [self.yuzdeKaci(i, toplam) for i in sayilar]

            print(toplam)
            print(oranlar, math.fsum(oranlar))

            set0.append(oranlar)
            set1.append([100-i for i in oranlar])
            

            series = QPercentBarSeries()
            series.append(set0)
            series.append(set1)
           

            chart = QChart()
            chart.addSeries(series)
            chart.setTitle("Simple percentbarchart example")
            chart.setTheme(QChart.ChartTheme.ChartThemeLight)
           

            categories = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
            axis = QBarCategoryAxis()
            axis.append(categories)
            chart.createDefaultAxes()
            chart.addAxis(axis, Qt.AlignmentFlag.AlignBottom)
            series.attachAxis(axis)

            chart.legend().setVisible(True)
            chart.legend().setAlignment(Qt.AlignmentFlag.AlignBottom)

            chart_view = QChartView(chart)
            # chart_view.setRenderHint(QPainter.Antialiasing)

            return chart_view
    
    


app = QApplication(sys.argv)
window = ChartWindow()
window.show()
sys.exit(app.exec())