import qrcode
from PIL import Image, ImageDraw, ImageFont

def qrCode(ad: str, no: str) -> Image:

    FONT = "/home/kemal/YazılımGeliştirme/Python/kütosiQt6/font/Arial.ttf"

    textAd = ad[0:50]
    textNo = no

    # Qr Code 
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=5
    )
    qr.add_data(no)
    qr.make(fit=True)

    # return image
    im = qr.make_image(fill_color="black", back_color="white")

    print("Boyutlandırma öncesi: ", im.size)

    img = im.resize((145,145))

    print("qrCode boyut", img.size)

    qrWidth,qrHeight = img.size
    # draw 
    dr = ImageDraw.Draw(img) # arguman olarak oluşturulan qrcode i alır

    # font
    ftAd = ImageFont.truetype(FONT, 9)
    ftNo = ImageFont.truetype(FONT, 20)

    # Yazıyı ölç 310 pixel olan qrcodeden küçük ise doğrudab barkode üzerine yaz 
    x, y, textWidth, textHeight = dr.textbbox((0, 0), textAd, font=ftAd)
    x, y, textNoWidth, textNoHeight = dr.textbbox((0, 0), textNo, font=ftNo)

    print("yazı boyutu ", x, y, textWidth, textHeight)
    # kenar boşlukları göz önünde bulundurulursa 290 pixel olarak hesaplama yap
    if textWidth <= 140:
        # ad write
        dr.text(((qrWidth - textWidth)/2, 5), text=textAd, fill ="black", font = ftAd, spacing = 0, align ="center")
        # no write
        dr.text(((qrWidth - textNoWidth)/2, 123), text=textNo, fill ="black", font = ftNo, spacing = 2, align ="center")

        ## biten işlemi ımage olarak döndür
        return img

    else:
        ## Sığmayan karakterleri göstermek için yazıya ... 3 nokta ekle
       
        dilimle = textAd.split()
        print(dilimle, len(textAd))
        ## içeriğin kaç parça olduğunu ölç
        icerikBirim = len(dilimle)
        bolum = int(round(len(dilimle) / 2, 0))
        print(icerikBirim, bolum)

        print(dilimle[0: bolum])
        print(dilimle[bolum:])

        row1 = ' '.join(map(str, dilimle[0: bolum]))
        row2 = ' '.join(map(str, dilimle[bolum:]))

        x, y, textWidth1, textHeight1 = dr.textbbox((0, 0), row1, font=ftAd)
        x, y, textWidth2, textHeight2 = dr.textbbox((0, 0), row2, font=ftAd)

        # ad write
        dr.text(((qrWidth - textWidth1)/2, 1), text=row1, fill ="black", font = ftAd, spacing = 0, align ="center")
        dr.text(((qrWidth - textWidth2)/2, 10), text=row2, fill ="black", font = ftAd, spacing = 0, align ="center")

        # no write
        dr.text(((qrWidth - textNoWidth)/2, 123), text=textNo, fill ="black", font = ftNo, spacing = 2, align ="center")

        return img




  
qr = qrCode("İSTİKLAL MARŞI'NIN KABULÜ VE M. AKİF ERSOY'U ANMA ETKİNLİĞİ", "100015")
qr.save("aaa1.png")
print(qr)


