# import library
import time

start = time.time()

liste = []
for i in range(1, 2000000):
    liste.append(i)

stop = time.time()
print(stop - start)


start1 = time.time()

liste = [i for i in range(1, 2000000)]

stop1 = time.time()
print(stop1 - start1)

liste1 = [i for i, ii in [(0,1), (2,3), (3,4), (5,6)]]