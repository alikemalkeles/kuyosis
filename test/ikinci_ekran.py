import sys

from PyQt6 import QtCore
from PyQt6.QtCore import QTimer
from PyQt6.QtGui import QColor
from PyQt6.QtWidgets import QApplication, QMainWindow, QPushButton, QWidget
from PyQt6.QtWidgets import QGraphicsDropShadowEffect


class App(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        

        self.buton = QPushButton("İşlem ekrani")
        self.buton.clicked.connect(self.surecEkrani)

        self.setCentralWidget(self.buton)

        self.setMinimumSize(500, 500)
        self.show()

    def surecEkrani(self):
        self.surec = QWidget()
        self.surec.showFullScreen()
        ## pencerey12i baslıksız yapma
        self.surec.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        efekt = QGraphicsDropShadowEffect()
        efekt.setColor(QColor(255, 255, 255, 255))
        efekt.setBlurRadius(5.0)

        self.surec.setGraphicsEffect(efekt)

        self.surec.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)

        
        QTimer.singleShot(3000, self.surec.close)
        




app = QApplication(sys.argv)
ornek = App()
app.exec()