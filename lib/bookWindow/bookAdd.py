from PyQt6 import QtCore
from PyQt6.QtCore import Qt, pyqtSlot
from PyQt6.QtGui import QRegularExpressionValidator
from PyQt6.QtWidgets import QHBoxLayout, QPushButton, QFrame, QGridLayout, QLabel, QLineEdit, \
    QDialog

from customWidget.customWidgets import CustomButton
from query.dbQuery import DbQuery
from customWidget.messageBox import MessageBOX
from models.model import BookModel
from customWidget.shadowEffect import BoxShadowEffect

# self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
with open('assets/stil/stil_books.qss') as file:
    stil = file.read()


class NewBook(QDialog):
    def __init__(self):
        super().__init__()

        # self.setWindowOpacity(0.1)

        # self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint)
        
        self.setStyleSheet(stil)


        WINDOWS_WIDTH = 880
        WINDOWS_HEIGHT = 530

        ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

      #  self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)

        self.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)

        self.mainFrameBookAdd = QFrame(self)
        self.mainFrameBookAdd.setObjectName("mainFrameBookAdd")
        self.mainFrameBookAdd.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)
        self.mainFrameBookAdd.setContentsMargins(0, 0, 0, 0)

        ## Book FrameAnaLayout
        self.mainBookLayout = QHBoxLayout()
        self.mainBookLayout.setContentsMargins(0, 0, 0, 0)

        ## kayıt ogeleri lineedit
        self.bookLayoutAdd = QGridLayout()
        self.bookLayoutAdd.setContentsMargins(25, 8, 12, 8)
        self.bookLayoutAdd.setSpacing(0)

        self.gorsel = QLabel()
        self.gorsel.setObjectName("gorselBookAdd")
        self.gorsel.setFixedSize(300, 530)

        self.mainBookLayout.addWidget(self.gorsel)
        self.mainBookLayout.addLayout(self.bookLayoutAdd)

        self.mainFrameBookAdd.setLayout(self.mainBookLayout)

        

        ## Kayıt sayfası

        # self.bookLayoutAdd = QGridLayout() eklenecek

        self.closeBut = QPushButton()
        self.closeBut.setFixedSize(24, 24)
        self.closeBut.setObjectName("closeBut")

        self.lineEditKarekodNo = QLineEdit()
        self.lineEditBookSyf = QLineEdit()
        self.lineEditBookAd = QLineEdit()
        self.lineEditBookYazar = QLineEdit()
        self.lineEditBookYayinevi = QLineEdit()
        self.lineEditBookTur = QLineEdit()
        self.lineEditBookRaf = QLineEdit()
        self.lineEditBookAciklama = QLineEdit()

        self.lineEditKarekodNo.setPlaceholderText("")
        self.lineEditBookSyf.setPlaceholderText("")
        self.lineEditBookAd.setPlaceholderText("")
        self.lineEditBookYazar.setPlaceholderText("")
        self.lineEditBookYayinevi.setPlaceholderText("")
        self.lineEditBookTur.setPlaceholderText("")
        self.lineEditBookRaf.setPlaceholderText("")
        self.lineEditBookAciklama.setPlaceholderText("")

        rx1 = QtCore.QRegularExpression("[0-9]{8}")
        v1 = QRegularExpressionValidator(rx1, self.lineEditKarekodNo)
        self.lineEditKarekodNo.setValidator(v1)

        rx2 = QtCore.QRegularExpression("[0-9]{5}")
        v2 = QRegularExpressionValidator(rx2, self.lineEditBookSyf)
        self.lineEditBookSyf.setValidator(v2)

        self.lineEditBookAd.setMaxLength(140)
        self.lineEditBookYazar.setMaxLength(70)
        self.lineEditBookYayinevi.setMaxLength(40)
        self.lineEditBookTur.setMaxLength(40)
        self.lineEditBookRaf.setMaxLength(40)
        self.lineEditBookAciklama.setMaxLength(150)

        self.label0 = QLabel("YENİ KİTAP EKLE")
        self.kaydetBut = QPushButton("Kaydet")

        self.label1 = QLabel("Kitap Adı *")

        self.label2 = QLabel("Kitap Karekod No ")
        self.label3 = QLabel("Yazar *")

        self.label4 = QLabel("Sayfa Sayısı * ")
        self.label5 = QLabel("Yayınevi * ")

        self.label6 = QLabel("Tür * ")
        self.label7 = QLabel("Raf Konumu")

        self.label8 = QLabel("Açıklama")

        checkButLayout = QHBoxLayout()

        self.checkBut = CustomButton(width_=45)  # = QCheckBox("Kitap karekod numarasını otomatik ver.\n(Otomatik karekod no tercih edilir.)")
        title = QLabel("   Kitap karekod numarasını otomatik ver.\n   (Otomatik karekod no tercih edilir.)")
        title.setStyleSheet("font-weight: 300; font-size: 13px;")

        checkButLayout.addWidget(self.checkBut)
        checkButLayout.addWidget(title)

        self.label0.setObjectName("newBook")
        self.kaydetBut.setObjectName("genel")

        self.label1.setObjectName("newBookEtiket")
        self.label2.setObjectName("newBookEtiket")
        self.label3.setObjectName("newBookEtiket")
        self.label4.setObjectName("newBookEtiket")
        self.label5.setObjectName("newBookEtiket")
        self.label6.setObjectName("newBookEtiket")
        self.label7.setObjectName("newBookEtiket")
        self.label8.setObjectName("newBookEtiket")

        self.checkBut.setObjectName("checkBut")
        self.checkBut.setChecked(False)
        self.checkBut.animateClick()
        self.checkBut.colorCount()
        self.kaydetBut.setGraphicsEffect(BoxShadowEffect.colorBlue())

        self.label1.setFixedHeight(14)
        self.label2.setFixedHeight(14)
        self.label3.setFixedHeight(14)
        self.label4.setFixedHeight(14)
        self.label5.setFixedHeight(14)
        self.label6.setFixedHeight(14)
        self.label7.setFixedHeight(14)
        self.label8.setFixedHeight(14)

        self.label0.setFixedSize(WINDOWS_WIDTH - 250, 35)

        self.lineEditKarekodNo.setFixedHeight(40)
        self.lineEditBookSyf.setFixedHeight(40)

        self.lineEditBookAd.setFixedHeight(40)
        self.lineEditBookYazar.setFixedHeight(40)
        self.lineEditBookYayinevi.setFixedHeight(40)
        self.lineEditBookTur.setFixedHeight(40)
        self.lineEditBookRaf.setFixedHeight(40)
        self.lineEditBookAciklama.setFixedHeight(40)

        self.lineEditKarekodNo.setObjectName("newBookLine")
        self.lineEditBookSyf.setObjectName("newBookLine")
        self.lineEditBookAd.setObjectName("newBookLine")
        self.lineEditBookYazar.setObjectName("newBookLine")
        self.lineEditBookYayinevi.setObjectName("newBookLine")
        self.lineEditBookTur.setObjectName("newBookLine")
        self.lineEditBookRaf.setObjectName("newBookLine")
        self.lineEditBookAciklama.setObjectName("newBookLine")

        self.bookLayoutAdd.addWidget(self.closeBut, 0, 5, 1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignRight)

        # Yeni kitap Ekle etiketi
        self.bookLayoutAdd.addWidget(self.label0, 1, 0, 1, 6)

        self.bookLayoutAdd.setVerticalSpacing(14)

        self.bookLayoutAdd.addWidget(self.label1, 3, 0, 1, 6)
        self.bookLayoutAdd.addWidget(self.lineEditBookAd, 4, 0, 1, 6)

        self.bookLayoutAdd.addWidget(self.label2, 5, 0, 1, 2)
        self.bookLayoutAdd.addWidget(self.lineEditKarekodNo, 6, 0, 1, 2)

        self.bookLayoutAdd.addWidget(self.label3, 5, 2, 1, 4)
        self.bookLayoutAdd.addWidget(self.lineEditBookYazar, 6, 2, 1, 4)  #

        self.bookLayoutAdd.addWidget(self.label4, 7, 0, 1, 2)
        self.bookLayoutAdd.addWidget(self.lineEditBookSyf, 8, 0, 1, 2)

        self.bookLayoutAdd.addWidget(self.label5, 7, 2, 1, 4)
        self.bookLayoutAdd.addWidget(self.lineEditBookYayinevi, 8, 2, 1, 4)

        self.bookLayoutAdd.addWidget(self.label6, 9, 0, 1, 3)
        self.bookLayoutAdd.addWidget(self.lineEditBookTur, 10, 0, 1, 3)

        self.bookLayoutAdd.addWidget(self.label7, 9, 3, 1, 3)
        self.bookLayoutAdd.addWidget(self.lineEditBookRaf, 10, 3, 1, 3)

        self.bookLayoutAdd.addWidget(self.label8, 11, 0, 1, 6)
        self.bookLayoutAdd.addWidget(self.lineEditBookAciklama, 12, 0, 1, 6)

        self.bookLayoutAdd.addLayout(checkButLayout, 13, 0, 1, 4)
        self.bookLayoutAdd.addWidget(self.kaydetBut, 13, 4, 1, 2)

        ## Sinyal slot
        self.closeBut.clicked.connect(self.closeFonk)
        self.lineEditBookAd.textChanged.connect(
            lambda: self.lineEditBookAd.setText(self.karakterBuyut(self.lineEditBookAd.text())))
        self.lineEditBookTur.textChanged.connect(
            lambda: self.lineEditBookTur.setText(self.karakterBuyut(self.lineEditBookTur.text())))
        self.lineEditBookYayinevi.textChanged.connect(
            lambda: self.lineEditBookYayinevi.setText(self.karakterBuyut(self.lineEditBookYayinevi.text())))
        self.lineEditBookYazar.textChanged.connect(
            lambda: self.lineEditBookYazar.setText(self.karakterBuyut(self.lineEditBookYazar.text())))
        self.lineEditBookRaf.textChanged.connect(
            lambda: self.lineEditBookRaf.setText(self.karakterBuyut(self.lineEditBookRaf.text())))

        self.checkBut.stateChanged.connect(self.checkBoxKontrolFonk)

        self.kaydetBut.clicked.connect(self.yeniKitapEkleFonk)

        ## Başlangıç Fonksiyonları ve Orneklemeler
        self.database = DbQuery()

    ## CheckBox seçim durumuna göre değişen kitap no düzenleme özelliği
    @pyqtSlot()
    def checkBoxKontrolFonk(self):

        if self.checkBut.isChecked() == True:
            # Check seçili ise Kitap no düzenleme alanını devre dışı bırak
            self.lineEditKarekodNo.setEnabled(False)

            self.kitapNoFonk()

        else:
            # seçili değil ise kitap no alanını düzenlemeyi aktif et
            self.lineEditKarekodNo.setEnabled(True)
            self.lineEditKarekodNo.setFocus()

    def kitapNoFonk(self):
        result = DbQuery.otoKitapNo()
        print("result: ", result)
        ## sonucu kitap no alanına ekle
        self.lineEditKarekodNo.setText(str(result))

    def closeFonk(self):
        print("cık")
        self.close()

    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i", "İ").upper()

    @pyqtSlot()
    def yeniKitapEkleFonk(self):
        ad = self.lineEditBookAd.text()
        karekod_no = self.lineEditKarekodNo.text()
        yazar = None if len(self.lineEditBookYazar.text()) == 0 else self.lineEditBookYazar.text()
        yayinevi = None if len(self.lineEditBookYayinevi.text()) == 0 else self.lineEditBookYayinevi.text()
        tur = None if len(self.lineEditBookTur.text()) == 0 else self.lineEditBookTur.text()
        syf = None if len(self.lineEditBookSyf.text()) == 0 else self.lineEditBookSyf.text()
        bilgi = self.lineEditBookAciklama.text()
        raf = "GENEL" if self.lineEditBookRaf.text() == "" else self.lineEditBookRaf.text()

        print("bilgi alanı türü: ", type(bilgi))

        kitap = BookModel(None, karekod_no, ad, yazar, yayinevi, tur, syf, bilgi, raf, "tarih")

        if len(kitap.karekod_no) >= 1:
            print("")

            if int(karekod_no) > 0:

                if len(kitap.ad) >= 2:
                    print("isim yeterli")

                    # sayfa 0 kontrölü yap
                    if syf is not None:

                        if int(kitap.sayfa) > 0:
                            print("kayıt yapılabilir")
                            result = DbQuery.yeniKitapEkle(kitap=kitap)
                            print("ekleme sonucu: ", result)

                            if result == 1:
                                self.ornek = MessageBOX("KAYIT BAŞARILI",
                                                        f" {kitap.ad} adlı kitap başarılı bir şeklilde eklendi.", 1)

                            elif result == 2:
                                self.ornek = MessageBOX("EŞ KAYIT HATASI",
                                                        f"""<font color = "red" weight = "bold"> {kitap.karekod_no} </font>  Bu karekod numarası başka bir kitaba kayıtlı""",
                                                        0)

                            elif result == 3:
                                self.ornek = MessageBOX("KAYIT HATASI",
                                                        f" Bilinmeyen bir hata oluştu. Tekrar deneyiniz ya da geliştirici ile iletişim kurunuz",
                                                        0)

                        else:
                            print("Sayfa sayısı sıfır olamaz")
                            self.ornek = MessageBOX("HATALI GİRİŞ", " Sayfa sayısı 0 olamaz", 0)

                    #  Sayfa None ise sayfa sayısı 0 kontrölü yapmadan devam et
                    else:

                        print("kayıt yapılabilir")
                        result = DbQuery.yeniKitapEkle(kitap=kitap)
                        print("ekleme sonucu: ", result)

                        if result == 1:
                            self.ornek = MessageBOX("KAYIT BAŞARILI",
                                                    f" {kitap.ad} adlı kitap başarılı bir şeklilde eklendi.", 1)
                        elif result == 2:
                            self.ornek = MessageBOX("EŞ KAYIT HATASI",
                                                    f"""<font color = "red" weight = "bold"> {kitap.karekod_no} </font>  Bu karekod numarası başka bir kitaba kayıtlı""",
                                                    0)
                        elif result == 3:
                            self.ornek = MessageBOX("KAYIT HATASI",
                                                    f" Bilinmeyen bir hata oluştu. Tekrar deneyiniz ya da geliştirici ile iletişim kurunuz",
                                                    0)


                else:
                    print("ad, yazar, yayınevi, tur boş geçilemez")
                    self.ornek = MessageBOX("EKSİK ALANLAR VAR!", " Kitap adı boş bırakılamaz", 0)

            else:
                self.ornek = MessageBOX("EKSİK ALANLAR VAR!", " Karekod numarası 0 olamaz", 0)

        else:
            print("No uygun değil")
            self.ornek = MessageBOX("EKSİK ALANLAR VAR", " Kitap karekod no en az 1 karakter olmalı", 0)

    def mousePressEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self._move()
            return super().mousePressEvent(event)

    def _move(self):
        window = self.window().windowHandle()
        window.startSystemMove()
# uyg = QApplication(sys.argv)
#     # QApplication.setStyle(QStyleFactory.create('Fusion'))
# QApplication.setHighDpiScaleFactorRoundingPolicy(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
# pen = NewBook()
# uyg.exec()
