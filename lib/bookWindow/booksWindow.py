# -*- coding: utf-8 -*-

from pathlib import Path

from PyQt6 import QtCore, QtWidgets, QtGui
from PyQt6.QtCore import pyqtSignal, QDate, Qt, pyqtSlot
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, \
    QLabel, QFrame, QLineEdit, QComboBox, QGroupBox, QGridLayout, QTableWidget, QFileDialog, QAbstractItemView, QMenu
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font
from openpyxl.styles.borders import Border, Side, BORDER_THIN

from bookWindow.bookAdd import NewBook
from bookWindow.bookQrCode import BookQrCode
from bookWindow.bookUpdate import UpdateBook
from query.dbQuery import DbQuery
from customWidget.messageBox import MessageBOX, MessageBOXQuestion
from models.model import *
from settingWindows.settings import SettingsConf
from customWidget.shadowEffect import BoxShadowEffect

with open("assets/stil/stil_books.qss") as file:
    stil = file.read()


class BooksWindow(QFrame):
    sinyal = pyqtSignal(int)

    def __init__(self, users: Users):
        super().__init__()

        ## sistemde aktif olan kullanıcı
        self.user = users

        # print("..:", self.user.id, self.user.isim,self.user.statu,self.user.parola,self.user.eklenme_tarihi,self.user.ekleme_yetkisi,self.user.guncelleme_yetkisi,self.user.silme_yetkisi,self.user.ayar_yetkisi)

        ## veritabanındaki sorguların sonucu buraya döner buradan tabloya yazılır
        self.bookList: BookModel = []

        # Ekran çözünürlüğü
        screen = self.screen()
        #  print(screen.size().width(), screen.size().height())
        self.w, self.h = (screen.size().width(), screen.size().height())

        # ComboBox ve lineedit widget yükseklikleri
        self.h1 = 40 if self.h > 900 else 32  # combobox ve lineedit
        self.h2 = 170 if self.h > 900 else 150  # GroupBox yüksekliği
        self.h3 = 60 if self.h > 900 else 50  # groupBox butonları

        self.setObjectName("frameBooks")

        self.setStyleSheet(stil)

        self.mainLayoutBooks = QVBoxLayout()
        self.mainLayoutBooks.setContentsMargins(15, 0, 15, 0)
        self.setLayout(self.mainLayoutBooks)

        self.topBarLayout = QHBoxLayout()
        self.tableWidget = QTableWidget()
        self.bottomLayout = QHBoxLayout()

        self.mainLayoutBooks.addLayout(self.topBarLayout)
        self.mainLayoutBooks.addSpacing(18)
        self.mainLayoutBooks.addWidget(self.tableWidget)
        self.mainLayoutBooks.addLayout(self.bottomLayout)

        ## TopBarLayout ögeleri (3 tane groupBox bağlanacak)
        self.groupBox1 = QGroupBox()
        self.groupBox2 = QGroupBox()
        self.groupBox3 = QGroupBox()

        self.groupBox1.setFixedHeight(self.h2)
        self.groupBox2.setFixedHeight(self.h2)
        self.groupBox3.setFixedHeight(self.h2)

        #  self.groupBox1.setMaximumWidth(500 if self.w >1400 else 350)

        self.groupBox1.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.groupBox2.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.groupBox3.setGraphicsEffect(BoxShadowEffect.colorBlue())

        # self.groupBox1.setTitle("İşlemler")
        self.groupBox2.setTitle("Yayınevi, yazar, tür vb. listele")
        self.groupBox3.setTitle("   Kitap adı veya kitap no ara   ")

        self.groupBox1.setObjectName("groupBox1")
        self.groupBox2.setObjectName("groupBox2")
        self.groupBox3.setObjectName("groupBox3")

        self.topBarLayout.addWidget(self.groupBox2, stretch=1)
        self.topBarLayout.addWidget(self.groupBox3, stretch=1)
        self.topBarLayout.addWidget(self.groupBox1, stretch=1)

        ## GroupBox1 ögeleri
        self.groupBox1GridLayout = QGridLayout()
        self.groupBox1GridLayout.setContentsMargins(5, 0, 5, 0)
        self.groupBox1.setLayout(self.groupBox1GridLayout)

        self.yeniKitapEkleBut = QPushButton("Kitap Ekle")
        self.kitapSilBut = QPushButton("Kitap Sil")
        self.secileniGuncelleBut = QPushButton("Güncelle")
        self.karekodYazdir = QPushButton("Karekod Yazdır")
        self.cokluKitapEklemeBut = QPushButton("Çoklu Kitap Ekle")

        self.yeniKitapEkleBut.setFixedHeight(self.h3)
        self.kitapSilBut.setFixedHeight(self.h3)
        self.secileniGuncelleBut.setFixedHeight(self.h3)
        self.karekodYazdir.setFixedHeight(self.h3)
        self.cokluKitapEklemeBut.setFixedHeight(self.h3)

        self.yeniKitapEkleBut.setObjectName("yeniKitapEkle")
        self.kitapSilBut.setObjectName("kitapSil")
        self.secileniGuncelleBut.setObjectName("secileniGuncelle")
        self.karekodYazdir.setObjectName("karekodYazdir")
        self.cokluKitapEklemeBut.setObjectName("cokluKitapEkleme")

        # self.groupBox1GridLayout.setRowStretch(0, 55)
        self.groupBox1GridLayout.addWidget(self.yeniKitapEkleBut, 0, 0, 1, 1)
        self.groupBox1GridLayout.addWidget(self.kitapSilBut, 0, 1, 1, 1)
        self.groupBox1GridLayout.addWidget(self.secileniGuncelleBut, 1, 0, 1, 1)
        self.groupBox1GridLayout.addWidget(self.karekodYazdir, 1, 1, 1, 1)
        self.groupBox1GridLayout.addWidget(self.cokluKitapEklemeBut, 0, 2, 1, 1)

        ## GroupBox2 ögeleri
        self.groupBox2GridLayout = QGridLayout()
        self.groupBox2GridLayout.setContentsMargins(5, 20, 5, 0)
        self.groupBox2.setLayout(self.groupBox2GridLayout)

        self.comboBox1 = QComboBox()
        self.comboBox2 = QComboBox()
        self.secileniListele = QPushButton("Seçimi Listele")

        self.comboBox1.setFixedHeight(self.h1)
        self.comboBox2.setFixedHeight(self.h1)
        self.secileniListele.setFixedHeight(self.h3)

        self.comboBox1.setObjectName("comboBox")
        self.comboBox2.setObjectName("comboBox")
        self.secileniListele.setObjectName("secileniListele")

        self.comboBox2.setInsertPolicy(QComboBox.InsertPolicy.InsertAlphabetically)

        self.comboBox1.addItem("Tür, yazar, yayınevi, konum vb.")
        self.comboBox2.addItem("Öncelikle tür, yazar vb. seçiniz")

        self.comboBox1.addItems(["Yazar", "Yayınevi", "Tür", "Raf Konumu"])
        self.comboBox2.setEnabled(False)
        self.secileniListele.setEnabled(False)

        # self.groupBox1GridLayout.setRowStretch(0, 55)
        self.groupBox2GridLayout.addWidget(self.comboBox1, 0, 0, 1, 3)
        self.groupBox2GridLayout.addWidget(self.comboBox2, 0, 3, 1, 3)
        self.groupBox2GridLayout.addWidget(self.secileniListele, 1, 0, 1, 6)

        # GRoupBox3 Ögeleri (arama yapma)
        self.groupBox3GridLayout = QGridLayout()
        self.groupBox3GridLayout.setContentsMargins(0, 20, 0, 0)
        self.groupBox3GridLayout.setSpacing(0)
        self.groupBox3.setLayout(self.groupBox3GridLayout)

        self.lineEditBook = QLineEdit()
        self.booksSearchBut = QPushButton()
        self.searchFilterComboBox = QComboBox()
        self.tumunuListele = QPushButton("Tüm Kitaplar")
        self.emanettekilerBut = QPushButton("Emanetteki Kitaplar")

        self.lineEditBook.setFixedHeight(self.h1)
        self.searchFilterComboBox.setFixedHeight(self.h1)
        self.booksSearchBut.setFixedSize(55, self.h1)
        self.emanettekilerBut.setFixedHeight(self.h3)
        self.tumunuListele.setFixedHeight(self.h3)

        self.lineEditBook.setMaxLength(40)
        self.lineEditBook.setClearButtonEnabled(True)
        self.lineEditBook.setTabletTracking(True)
        self.lineEditBook.setPlaceholderText("ID, karekod, kitap adı ile arama yapabilirsiniz.")
        self.searchFilterComboBox.addItems(["ID", "Karekod No", "Kitap Adı", "Tüm Alanlar"])
        self.searchFilterComboBox.setEditable(True)
        self.searchFilterComboBox.lineEdit().setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.searchFilterComboBox.lineEdit().setReadOnly(True)
        self.searchFilterComboBox.setCurrentIndex(3)

        self.lineEditBook.setObjectName("lineEditBook")
        self.searchFilterComboBox.setObjectName("searchFilter")
        self.booksSearchBut.setObjectName("booksSearch")
        self.emanettekilerBut.setObjectName("emanettekilerBut")
        self.tumunuListele.setObjectName("tumunuListele")

        # self.groupBox1GridLayout.setRowStretch(0, 55)
        self.groupBox3GridLayout.addWidget(self.lineEditBook, 0, 0, 1, 7)
        self.groupBox3GridLayout.addWidget(self.searchFilterComboBox, 0, 7, 1, 2)
        self.groupBox3GridLayout.addWidget(self.booksSearchBut, 0, 9, 1, 1)
        self.groupBox3GridLayout.addWidget(self.emanettekilerBut, 1, 2, 1, 2)
        self.groupBox3GridLayout.addWidget(self.tumunuListele, 1, 5, 1, 2)

        ## QtableWidget işlemleri
        self.tableWidget.setColumnCount(10)
        self.tableWidget.setRowCount(50)
        self.tabloTitle = ["ID", "Karekod N.", "Kitap Adı", "Yazar", "Yayınevi", "Tür", "Syf. Sayısı", "Eklenme Tarihi",
                           "Raf Konumu", "Açıklama"]
        self.tableWidget.setHorizontalHeaderLabels(self.tabloTitle)
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch) ## tüm tabloya genişleme
        # self.tableWidget.horizontalHeader().setFixedHeight(32)
        self.tableWidget.horizontalScrollBar().setStyleSheet(stil)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setSelectionBehavior(
            QtWidgets.QTableView.SelectionBehavior.SelectRows)  # satırın tamamını seçme
        # self.tableWidget.setSortingEnabled(True) # sıralam özelliğni devre dışı birak

        self.tableWidget.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.tableWidget.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)  # satırı düzenlenemez yapma

        # Tekli satır seçme
        # self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.SingleSelection)

        ## bottomLayout Ögeleri
        self.bottomLayout.setContentsMargins(5, 2, 10, 2)
        self.csvToBut = QPushButton()
        self.listeyiTemizle = QPushButton("")
        self.bottomEtiket = QLabel("Kırmızı satırlar emanetteki kitapları gösterir")

        self.csvToBut.setFixedHeight(32)
        self.csvToBut.setObjectName("csvToBut")
        self.listeyiTemizle.setObjectName("listeyiTemizle")
        self.bottomEtiket.setObjectName("bottomEtiket")
        self.listeyiTemizle.setFixedSize(40, 32)
        self.bottomEtiket.setFixedHeight(25)
        self.csvToBut.setToolTip("Tabloyu xlsx dosyasına aktar")
        self.listeyiTemizle.setToolTip("Tabloyu temizle")

        self.bottomLayout.addWidget(self.bottomEtiket)
        self.bottomLayout.addStretch()
        self.bottomLayout.addWidget(self.listeyiTemizle)
        self.bottomLayout.addSpacing(60)
        self.bottomLayout.addWidget(self.csvToBut)

        ## sinyal slots
        self.lineEditBook.textChanged.connect(
            lambda: self.lineEditBook.setText(self.karakterBuyut(self.lineEditBook.text())))

        self.yeniKitapEkleBut.clicked.connect(self.newBookAddFonk)
        self.karekodYazdir.clicked.connect(self.qrCodePrintFonk)
        self.comboBox1.currentTextChanged.connect(self.comboBox1SecimFonk)
        self.secileniListele.clicked.connect(self.comboBox2SecimFonk)
        self.tumunuListele.clicked.connect(self.tumKitaplarFonk)
        self.listeyiTemizle.clicked.connect(self.tabloVeriSilFonk)
        self.booksSearchBut.clicked.connect(self.kitapNoAdAraFonk)
        self.searchFilterComboBox.currentTextChanged.connect(self.seacrhFilterFonk)
        self.lineEditBook.returnPressed.connect(self.kitapNoAdAraFonk)
        self.emanettekilerBut.clicked.connect(self.emanettekiTumKitaplarFonk)
        self.kitapSilBut.clicked.connect(self.kitapSilmeFonk)
        self.secileniGuncelleBut.clicked.connect(self.bookUpdateFonk)
        self.csvToBut.clicked.connect(self.tabloXlsxFonk)

        # coklu kitap ekleme penceresi sinyal ile mainwindow (ana pencere) sinyal gönderme
        self.cokluKitapEklemeBut.clicked.connect(lambda: self.sinyal.emit(0))

        #  self.initialFonk()

        self.tableWidgetWidth()

        # self.tableWidget.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.CustomContextMenu)

    #   self.tableWidget.customContextMenuRequested.connect(self.generateMenu)  # +++

    #   self.tableWidget.viewport().installEventFilter(self)

    def eventFilter(self, source, event):
        if (
                event.type() == QtCore.QEvent.Type.MouseButtonPress and event.buttons() == Qt.MouseButton.RightButton and source is self.tableWidget.viewport()):
            item = self.tableWidget.itemAt(event.pos())

            if item is not None:
                print('Table Item:', item.row(), item.column())

                self.menu = QMenu(self)
                self.menu.addAction("Düzenle")
                self.menu.addAction("Sil")
                self.menu.setStyleSheet("""
        QMenu {background-color: #ffffff; color: black; padding: 0;}
        QMenu:selected {background-color: blue; color: #000;}
    """)

        return super(BooksWindow, self).eventFilter(source, event)

    ### +++    
    def generateMenu(self, pos):
        print("Position: ", pos)
        row = self.tableWidget.currentRow()

        print("satır bilgisi:", row)

        data = self.tableWidget.item(row, 0)

        if data != None:

            print("data:", data.text(), data)
            print("saatırda bilgi var")
            self.menu.exec(self.tableWidget.mapToGlobal(pos))  # +++



        else:
            print("satırda bilgi yok")
            # self.menu.exec(self.tableWidget.mapToGlobal(pos))   # +++

            # self.menu.setStyleSheet("QMenu {color: black; border-radius: 8px;}")

    def resizeEvent(self, event):
        super().resizeEvent(event)

        self.tableWidgetWidth()

    # Kitap sayfası table widget sutun genişliği
    def tableWidgetWidth(self):
        # tabe widget sutun genişliklerini hesapla 
        table = self.tableWidget.width()
        toplam = 0
        for w in [1, 2, 7, 4, 4, 3, 2, 3, 3, 3]:
            toplam = toplam + int((table / 32) * w)

        self.tableWidget.setColumnWidth(0, int(((table) / 32) * 1))
        self.tableWidget.setColumnWidth(1, int(((table) / 32) * 2))
        self.tableWidget.setColumnWidth(2, int(((table) / 32) * 7))
        self.tableWidget.setColumnWidth(3, int(((table) / 32) * 4))
        self.tableWidget.setColumnWidth(4, int(((table) / 32) * 4))
        self.tableWidget.setColumnWidth(5, int(((table) / 32) * 3))
        self.tableWidget.setColumnWidth(6, int(((table) / 32) * 2))
        self.tableWidget.setColumnWidth(7, int(((table) / 32) * 3))
        self.tableWidget.setColumnWidth(8, int(((table) / 32) * 3))
        self.tableWidget.setColumnWidth(9, int(((table) / 32) * 3) + (
                self.tableWidget.width() - toplam) - self.tableWidget.verticalHeader().width() - 22)

    def initialFonkBooksWindow(self):

        self.yeniKitapEkleBut.setEnabled(self.user.ekleme_yetkisi)
        self.kitapSilBut.setEnabled(self.user.silme_yetkisi)
        self.secileniGuncelleBut.setEnabled(self.user.guncelleme_yetkisi)
        self.karekodYazdir.setEnabled(self.user.karekod_kimlik_yetkisi)
        self.cokluKitapEklemeBut.setEnabled(self.user.coklu_aktarim_yetkisi)
        self.csvToBut.setEnabled(self.user.raporlama_yetkisi)

        group_name = "comboBoxIndex"
        setting = SettingsConf.getValueConf(group_name=group_name, key="comboBoxBook")

        if setting is None:
            self.searchFilterComboBox.setCurrentIndex(3)

        else:
            self.searchFilterComboBox.setCurrentIndex(int(setting))

        # print("settings book: ", setting)

    # Kİtap arama filtresinden seçim yapılınca

    ## Yeni kitap kaydet
    def newBookAddFonk(self):
        self.newBook_ = NewBook()
        self.newBook_.show()
        self.newBook_.exec()

    # Arama filtresi seçince
    @pyqtSlot()
    def seacrhFilterFonk(self):
        #  print(self.searchFilterComboBox.currentIndex())
        """["ID No","Karekod No", "Kitap Adı", "Tüm Alanlar"]"""

        # secimi ayarlara kaydetme
        group_name = "comboBoxIndex"
        settings = SettingsConf.setValueConf(group_name=group_name, key="comboBoxBook",
                                             value=self.searchFilterComboBox.currentIndex())
        #   print("ayar kaydedildi: ", self.searchFilterComboBox.currentIndex() )

        if self.searchFilterComboBox.currentIndex() == 0:
            #  print("id")
            self.lineEditBook.setPlaceholderText("Kitap ID ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 1:
            #  print("karekod_")
            self.lineEditBook.setPlaceholderText("Karekod no ile arama yapabilirsiniz")
        elif self.searchFilterComboBox.currentIndex() == 2:
            #  print("kitap adı")
            self.lineEditBook.setPlaceholderText("Kitap adı ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 3:
            #  print("tüm alanlar")
            self.lineEditBook.setPlaceholderText("Kitap ID, karekod no, kitap adı adı ile arama yapabilirsiniz")

    ## Kitap Güncelle
    @pyqtSlot()
    def bookUpdateFonk(self):

        secilenler = []
        kitapIndex = 0
        indexes = self.tableWidget.selectionModel().selectedRows(column=0)  # seçilen satırları döndür
        for index in indexes:
            if index.data() != None:  # eğer boş satır seçilirse 
                kitapIndex = index.row()
                # print(self.tableWidget.item(index.row(),  0).text(),     self.tableWidget.item(index.row(),  1).text())
                secilenler.append(index)
        print(secilenler)

        ## Ayrıcalık gerektirmez
        if self.user:

            if len(secilenler) == 0:
                MessageBOX("UYARI", " Öncelikle güncellemek istediğiniz kitabı listeden seçiniz", 0)

            elif len(secilenler) > 1:
                MessageBOX("UYARI", " Yalnızca bir kitap seçiniz.", 0)

            elif len(secilenler) == 1:
                ## kitap güncelleme ekranı çağrılabilir
                self.updateBook_ = UpdateBook(book=self.bookList[kitapIndex])
                self.updateBook_.show()
                self.updateBook_.exec()

            ## QrCode yazdırma

    @pyqtSlot()
    def qrCodePrintFonk(self):

        # secilen ögeler
        secilenler = []
        kitapIndex = 0
        indexes = self.tableWidget.selectionModel().selectedRows(column=0)  # seçilen satırları döndür
        for index in indexes:
            if index.data() != None:  # eğer boş satır seçilirse 
                secilenler.append(self.bookList[index.row()])
        #     print(self.bookList[index.row()])
        if secilenler:

            self.orn = BookQrCode(secilenler)
        else:
            MessageBOX("UYARI",
                       """ Öncelikle kitap seçmeniz gerekiyor.\nNot: 'CTR' tuşuna basılı tutarak çoklu seçim yapabilirsiniz.""",
                       0)
            print("Herhangi bir kitap seçilmedi")

    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i", "İ").upper()

    ## ComboBox1 katogori seçimi ve seçilen katogorideki ögeler ikinci comBOBoxa yazdırma
    @pyqtSlot()
    def comboBox1SecimFonk(self):

        secim = self.comboBox1.currentText()

        if secim == "Tür, yazar, yayınevi, konum vb.":
            self.comboBox2.clear()
            self.comboBox2.addItem("Öncelikle tür, yazar vb. seçiniz")
            self.comboBox2.setEnabled(False)

            self.secileniListele.setEnabled(False)

        elif secim == "Yazar":
            result = DbQuery.comboBox1Secim("yazar")
            #     print(result)
            liste = []
            for i in result:
                liste.append(i[0])

            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        elif secim == "Yayınevi":
            result = DbQuery.comboBox1Secim("yayinevi")
            #     print(result)
            liste = []
            for i in result:
                liste.append(i[0])

            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        elif secim == "Tür":
            result = DbQuery.comboBox1Secim("tur")
            #     print(result)
            liste = []
            for i in result:
                liste.append(i[0])

            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        elif secim == "Raf Konumu":
            result = DbQuery.comboBox1Secim("raf")
            #     print(result)
            liste = []
            for i in result:
                liste.append(i[0])

            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        self.comboBox2.model().sort(0)

    ## seçimi filtrele
    def comboBox2SecimFonk(self):

        rowColor = QtGui.QColor(133, 174, 255)

        cvr = {"Yazar": "yazar", "Yayınevi": "yayinevi", "Tür": "tur", "Raf Konumu": "raf"}

        secim1 = self.comboBox1.currentText()
        secim2 = self.comboBox2.currentText()
        # print(secim2)

        print("secim2 __: ", secim2, len(secim2), type(secim2))

        if secim2 == "Öncelikle tür, yazar vb. seçiniz":
            print("Önce tür seçiniz")

        else:
            #    veritabanı sorgusu sonucunda dönen değerleri değişkene atama
            self.bookList = DbQuery.comboBox2Secim(cvr[secim1], secim2)

            ## sonuca göre tablodaki satır sayısını belirle
            if len(self.bookList) >= 50:
                self.tableWidget.setRowCount(len(self.bookList))
            else:
                self.tableWidget.setRowCount(50)

            self.kitapListesiTabloyaYaz(self.bookList)

    def tumKitaplarFonk(self):

        rowColor = QtGui.QColor(133, 174, 255)

        self.bookList = DbQuery.tumKitaplar()

        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.bookList) >= 50:
            self.tableWidget.setRowCount(len(self.bookList))
        else:
            self.tableWidget.setRowCount(50)

        self.kitapListesiTabloyaYaz(self.bookList)

    ## tablodaki verileri sil
    def tabloVeriSilFonk(self):
        self.tableWidget.clear()
        self.tableWidget.setHorizontalHeaderLabels(self.tabloTitle)
        self.tableWidget.setRowCount(50)

        self.bookList.clear()  # kitap listesini de temizle

        self.tableWidgetWidth()

    ## Emanetteki tüm kitaplar
    def emanettekiTumKitaplarFonk(self):
        rowColor = QtGui.QColor(133, 174, 255)

        self.bookList = DbQuery.emanettekiKitaplar()

        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.bookList) >= 50:
            self.tableWidget.setRowCount(len(self.bookList))
        else:
            self.tableWidget.setRowCount(50)

        self.kitapListesiTabloyaYaz(self.bookList)

    ## Kitap no ve kitap adı ara
    def kitapNoAdAraFonk(self):

        rowColor = QtGui.QColor(133, 174, 255)

        ## eğer arama yeri boş gelirse 
        ara = "####" if self.lineEditBook.text() == "" else self.lineEditBook.text()

        self.bookList = DbQuery.kitapNoAdAra(ara, self.searchFilterComboBox.currentIndex())

        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.bookList) >= 50:
            self.tableWidget.setRowCount(len(self.bookList))
        else:
            self.tableWidget.setRowCount(50)

        self.kitapListesiTabloyaYaz(self.bookList)

    ## Kitap silme 
    def kitapSilmeFonk(self):

        secilenler = []
        kitapIndex = 0
        indexes = self.tableWidget.selectionModel().selectedRows(column=0)  # seçilen satırları döndür
        for index in indexes:
            if index.data() != None:  # eğer boş satır seçilirse 
                kitapIndex = index.row()
                # print(self.tableWidget.item(index.row(),  0).text(),     self.tableWidget.item(index.row(),  1).text())
                secilenler.append(index)
        print(secilenler)

        ## Yönetici hesabı gerektirir.
        if self.user:

            if len(secilenler) == 0:
                MessageBOX("UYARI", " Öncelikle silmek istediğiniz kitabı listeden seçiniz", 0)

            elif len(secilenler) > 1:
                MessageBOX("UYARI", " Tek seferde en fazla 1 kitap silebilirsiniz. Yalnızca bir kitap seçiniz.", 0)

            elif len(secilenler) == 1:

                kitapDurum = DbQuery.emanettekiKitapUyeID()

                if kitapDurum is not None:

                    if self.bookList[kitapIndex].id in kitapDurum[0]:
                        MessageBOX("UYARI",
                                   f" {self.bookList[kitapIndex].ad} adlı kitap emanette olduğu için silemezsiniz. ", 0)

                    else:
                        # print("kitapListsi", type(self.bookList), index)
                        # print("secim : ", self.bookList[kitapIndex].ad)

                        msg = MessageBOXQuestion("BİLGİ",
                                                 f" {self.bookList[kitapIndex].ad} adlı kitabı silmek istediğinize emin misiniz?",
                                                 1)
                        msg.okBut.clicked.connect(lambda: self.kitapSilFonk(self.bookList[kitapIndex].id, kitapIndex))

                else:
                    MessageBOX("HATA",
                               f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için silme işlemi gerçekleştirilemiyor. Programı kapatıp tekrar deneyiniz.",
                               0)

        else:  ## Yönetici hesabı değilse
            MessageBOX("UYARI",
                       " Yalnızca yönetici hesabına sahip olanlar kitap silebilir. Lütfen, yönetici hesabıyla giriş yapınız.",
                       0)

    ## kitapSil
    def kitapSilFonk(self, no, index):
        result = DbQuery.kitapSil(no)
        if result == 1:

            # MessageBOXOk(1400)
            self.bookList.pop(index)

            self.kitapListesiTabloyaYaz(self.bookList)

        else:
            MessageBOX("UYARI", " Bilinmeyen bir hata oluştu. Tekrar deneyiniz", 0)

    ## KitapListesi değişkeni üzerinde yer alan ögeleri tabloya yazma
    def kitapListesiTabloyaYaz(self, kitapListesi: BookModel):

        # Emanetteki kitaplar listesi numarası
        emanettekiKitaplar = DbQuery.emanettekiKitapUyeID()
        # print("emanetteki kitaplar listesi no: ", emanettekiKitaplar)

        rowColor = QtGui.QColor(255, 50, 0)
        self.tableWidget.clear()
        row = 0

        if emanettekiKitaplar is not None:

            for i in kitapListesi:

                if i.id in emanettekiKitaplar[0]:
                    item0 = QtWidgets.QTableWidgetItem(str(i.id))
                    item0.setForeground(rowColor)
                    item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 0, item0)

                    item1 = QtWidgets.QTableWidgetItem(str(i.karekod_no))
                    item1.setForeground(rowColor)
                    item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 1, item1)

                    item2 = QtWidgets.QTableWidgetItem("" if i.ad == None else str(i.ad))
                    item2.setForeground(rowColor)
                    self.tableWidget.setItem(row, 2, item2)

                    item3 = QtWidgets.QTableWidgetItem("" if i.yazar == None else str(i.yazar))
                    item3.setForeground(rowColor)
                    self.tableWidget.setItem(row, 3, item3)

                    item4 = QtWidgets.QTableWidgetItem("" if i.yayınevi == None else str(i.yayınevi))
                    item4.setForeground(rowColor)
                    self.tableWidget.setItem(row, 4, item4)

                    item5 = QtWidgets.QTableWidgetItem("" if i.tür == None else str(i.tür))
                    item5.setForeground(rowColor)
                    self.tableWidget.setItem(row, 5, item5)

                    item6 = QtWidgets.QTableWidgetItem("" if i.sayfa == None else str(i.sayfa))
                    item6.setForeground(rowColor)
                    item6.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 6, item6)

                    item7 = QtWidgets.QTableWidgetItem("" if i.tarih == None else str(i.tarih))
                    item7.setForeground(rowColor)
                    item7.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 7, item7)

                    item8 = QtWidgets.QTableWidgetItem("" if i.raf == None else str(i.raf))
                    item8.setForeground(rowColor)
                    self.tableWidget.setItem(row, 8, item8)

                    item9 = QtWidgets.QTableWidgetItem("" if i.bilgi == None else str(i.bilgi))
                    item9.setForeground(rowColor)
                    self.tableWidget.setItem(row, 9, item9)
                    row += 1
                else:
                    item0 = QtWidgets.QTableWidgetItem(str(i.id))
                    # item0.setForeground(rowColor)
                    item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 0, item0)

                    item1 = QtWidgets.QTableWidgetItem(str(i.karekod_no))
                    # item1.setForeground(rowColor)
                    item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 1, item1)

                    item2 = QtWidgets.QTableWidgetItem("" if i.ad == None else str(i.ad))
                    #  item2.setForeground(rowColor)
                    self.tableWidget.setItem(row, 2, item2)

                    item3 = QtWidgets.QTableWidgetItem("" if i.yazar == None else str(i.yazar))
                    #  item3.setForeground(rowColor)
                    self.tableWidget.setItem(row, 3, item3)

                    item4 = QtWidgets.QTableWidgetItem("" if i.yayınevi == None else str(i.yayınevi))
                    #   item4.setForeground(rowColor)
                    self.tableWidget.setItem(row, 4, item4)

                    item5 = QtWidgets.QTableWidgetItem("" if i.tür == None else str(i.tür))
                    #  item5.setForeground(rowColor)
                    self.tableWidget.setItem(row, 5, item5)

                    item6 = QtWidgets.QTableWidgetItem("" if i.sayfa == None else str(i.sayfa))
                    # item6.setForeground(rowColor)
                    item6.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 6, item6)

                    item7 = QtWidgets.QTableWidgetItem("" if i.tarih == None else str(i.tarih))
                    # item7.setForeground(rowColor)
                    item7.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 7, item7)

                    item8 = QtWidgets.QTableWidgetItem("" if i.raf == None else str(i.raf))
                    #  item8.setForeground(rowColor)
                    self.tableWidget.setItem(row, 8, item8)

                    item9 = QtWidgets.QTableWidgetItem("" if i.bilgi == None else str(i.bilgi))
                    # item9.setForeground(rowColor)
                    self.tableWidget.setItem(row, 9, item9)
                    row += 1

            self.tableWidget.setHorizontalHeaderLabels(self.tabloTitle)

        else:
            MessageBOX("HATA",
                       f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için kitaplar listelenemiyor. Programı kapatıp tekrar deneyiniz.",
                       0)

    # Tablodaki içerikleri xlsx dosyasına aktar
    def tabloXlsxFonk(self):

        kurumBilgileri = DbQuery.ayarlarVeYapilandirmalar()
        date = QDate.currentDate()
        sorgu_tarihi = date.toString('dd.MM.yyyy')

        ## Yönetici hesabı gerektirir.
        if self.user:

            if len(self.bookList) > 0:
                ## modeli listeye aktar
                home_dir = str(Path.home())

                # tablo başlık yazısı
                title = "KİTAP LİSTESİ"

                # 10 SÜTUN
                tablo = ["SIRA", "KAREKOD", "KİTAP ADI", "YAZAR", "YAYINEVİ", "TÜR", "SAYFA SAYISI", "EKLENME TARİHİ",
                         "RAF KONUMU", "AÇIKLAMA"]

                fnamePath = QFileDialog.getSaveFileName(self, home_dir, "Kitap Listesi", ".xlsx")
                print(fnamePath, home_dir, bool(fnamePath))

                if bool(fnamePath[0]) == True:

                    thin_border = Border(
                        left=Side(border_style=BORDER_THIN, color='00000000'),
                        right=Side(border_style=BORDER_THIN, color='00000000'),
                        top=Side(border_style=BORDER_THIN, color='00000000'),
                        bottom=Side(border_style=BORDER_THIN, color='00000000')
                    )

                    work_book = Workbook()
                    work_sheet = work_book.active

                    work_sheet.page_setup.orientation = work_sheet.ORIENTATION_LANDSCAPE
                    work_sheet.page_setup.paperSize = work_sheet.PAPERSIZE_A4

                    work_sheet.sheet_properties.pageSetUpPr.fitToPage = True  # sayfayı genişliğe sığdır
                    work_sheet.page_setup.fitToHeight = False
                    work_sheet.page_setup.fitToWidth = True

                    work_sheet.page_margins.left = 0.2
                    work_sheet.page_margins.right = 0.1
                    work_sheet.page_margins.top = 0.1
                    work_sheet.page_margins.bottom = 0.1

                    work_sheet.column_dimensions['A'].width = 7  # sütun genişlikleri
                    work_sheet.column_dimensions['B'].width = 11  # sütun genişlikleri
                    work_sheet.column_dimensions['C'].width = 46
                    work_sheet.column_dimensions['D'].width = 23
                    work_sheet.column_dimensions['E'].width = 20
                    work_sheet.column_dimensions['F'].width = 12
                    work_sheet.column_dimensions['G'].width = 8
                    work_sheet.column_dimensions['H'].width = 15
                    work_sheet.column_dimensions['I'].width = 10
                    work_sheet.column_dimensions['J'].width = 10

                    work_sheet.merge_cells('A1:J2')  # hücre birleştirme

                    # kurum bilgileri ve kurum adı
                    work_sheet.row_dimensions[1].height = 20
                    work_sheet.row_dimensions[2].height = 20
                    cell = work_sheet.cell(row=1, column=1)
                    cell.value = f"{kurumBilgileri.kurum_adi}\n {title}"
                    cell.font = Font(bold=True, size=15)
                    cell.alignment = Alignment(horizontal='center', vertical='center', wrapText=True)

                    ## tablonun başlıkları
                    titleColumn = 1
                    work_sheet.row_dimensions[3].height = 30
                    for title_ in tablo:  # tablo başlığını yaz
                        baslik = work_sheet.cell(row=3, column=titleColumn)
                        baslik.value = f"{title_}"
                        baslik.font = Font(bold=True, size=10)
                        baslik.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True,
                                                     wrapText=True)
                        titleColumn += 1

                    titleRow = 4

                    siraNumarasi = 1
                    for kitaplar in self.bookList:
                        # print("bookListTaken", kitaplar)
                        work_sheet.row_dimensions[titleRow].height = 25

                        siraNo = work_sheet.cell(row=titleRow, column=1)
                        siraNo.font = Font(bold=True, size=10)
                        siraNo.value = f"{siraNumarasi}"
                        siraNo.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)
                        siraNo.border = thin_border

                        karekodNo = work_sheet.cell(row=titleRow, column=2)
                        karekodNo.font = Font(bold=False, size=10)
                        karekodNo.value = f"{kitaplar.karekod_no}"
                        karekodNo.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)
                        karekodNo.border = thin_border

                        kitapAdi = work_sheet.cell(row=titleRow, column=3)
                        kitapAdi.font = Font(bold=False, size=10)
                        kitapAdi.value = f"{kitaplar.ad}"
                        kitapAdi.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True,
                                                       wrapText=True)
                        kitapAdi.border = thin_border

                        yazar = work_sheet.cell(row=titleRow, column=4)
                        yazar.font = Font(bold=False, size=10)
                        yazar.value = f"{kitaplar.yazar}"
                        yazar.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True,
                                                    wrapText=True)
                        yazar.border = thin_border

                        yayinevi = work_sheet.cell(row=titleRow, column=5)
                        yayinevi.font = Font(bold=False, size=10)
                        yayinevi.value = f"{kitaplar.yayınevi}"
                        yayinevi.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True,
                                                       wrapText=True)
                        yayinevi.border = thin_border

                        tur = work_sheet.cell(row=titleRow, column=6)
                        tur.font = Font(bold=False, size=10)
                        tur.value = f"""{"" if kitaplar.tür == None else f"{kitaplar.tür}"}"""
                        tur.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True)
                        tur.border = thin_border

                        sayfaSayisi = work_sheet.cell(row=titleRow, column=7)
                        sayfaSayisi.font = Font(bold=False, size=10)
                        sayfaSayisi.value = f"{kitaplar.sayfa}"
                        sayfaSayisi.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)
                        sayfaSayisi.border = thin_border

                        eklenmeTarihi = work_sheet.cell(row=titleRow, column=8)
                        eklenmeTarihi.font = Font(bold=False, size=10)
                        eklenmeTarihi.value = f"{kitaplar.tarih[0:-3]}"
                        eklenmeTarihi.alignment = Alignment(horizontal='center', vertical='center',
                                                            justifyLastLine=True, wrapText=True)
                        eklenmeTarihi.border = thin_border

                        rafKonumu = work_sheet.cell(row=titleRow, column=9)
                        rafKonumu.font = Font(bold=False, size=10)
                        rafKonumu.value = f"{kitaplar.raf}"
                        rafKonumu.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)
                        rafKonumu.border = thin_border

                        aciklama = work_sheet.cell(row=titleRow, column=10)
                        aciklama.font = Font(bold=False, size=10)
                        aciklama.value = f"{kitaplar.bilgi}"
                        aciklama.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True,
                                                       wrapText=True)
                        aciklama.border = thin_border

                        titleRow += 1

                        siraNumarasi += 1

                    work_sheet.merge_cells(
                        f"H{titleRow + 1}:J{titleRow + 1}")  # hücre birleştirme ( kitap listesini yazdıktan sonra 1 satır boşluk bırakıp kurum yöneticini yaz)
                    work_sheet.row_dimensions[titleRow].height = 25

                    work_sheet.row_dimensions[titleRow + 1].height = 50
                    cell2 = work_sheet.cell(row=titleRow + 1, column=8)
                    cell2.value = f"{sorgu_tarihi}\n{kurumBilgileri.kurum_yoneticisi}\nKurum Yöneticisi"
                    cell2.font = Font(bold=True, size=10)
                    cell2.alignment = Alignment(horizontal='center', vertical='center', wrapText=True)

                    work_book.save(f"{fnamePath[0]}.xlsx")
                    print(("tabloya yazdık"))
                    MessageBOX("BİLGİLENDİRME", " Tablodaki içerikler .xlsx dosyasına başarılı bir şekilde aktarıldı.",
                               1)

                else:
                    print("dosya yolu alınamadı")
                    pass

            else:
                MessageBOX("UYARI", " Aktarılacak veri bulunmadı", 0)
                print("öncelikle kitap listele")

        else:
            MessageBOX("UYARI",
                       " Yalnızca yöneticiler kitap listesini aktarabilir. Lütfen, yönetici hesabıyla giriş yapınız.",
                       0)
