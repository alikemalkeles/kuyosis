from pathlib import Path

import qrcode
from PIL import Image, ImageDraw, ImageFont
from PyQt6 import QtCore
from PyQt6.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt6.QtCore import QTimer, QObject
from PyQt6.QtWidgets import QPushButton, QFrame, QGridLayout, QLabel, QDialog, QListWidget, QProgressBar, QFileDialog

from models.model import BookModel
from customWidget.shadowEffect import BoxShadowEffect


class Worker(QObject):

    proces = pyqtSignal(int)
    mesaj = pyqtSignal(str)

    def __init__(self, veri: BookModel):
        super().__init__()
        self.kitapListesi = veri
        self.dosyaYolu = f"{str(Path.home())}/qrcode.pdf"

    def setFilePath(self, path):
        self.dosyaYolu = path
        

    def qrCode(self, ad: str, no: str) -> Image:

        FONT = "assets/font/Arial.ttf"

        textAd = str(ad)[0:35]
        textNo = str(no)

        # Qr Code 
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=5,
            border=6
        )
        qr.add_data(no)
        qr.make(fit=True)

        # return image
        im = qr.make_image(fill_color="black", back_color="white")

       # print("Boyutlandırma öncesi: ", im.size)

        im_  = im.resize((145,145))

        img = im_.convert("RGB")

     #   print("qrCode boyut", img.size)

        qrWidth,qrHeight = img.size
        # draw 
        dr = ImageDraw.Draw(img) # arguman olarak oluşturulan qrcode i alır

        # font
        ftAd = ImageFont.truetype(FONT, 10)
        ftNo = ImageFont.truetype(FONT, 20)

        # Yazıyı ölç 310 pixel olan qrcodeden küçük ise doğrudab barkode üzerine yaz 
        x, y, textWidth, textHeight = dr.textbbox((0, 0), textAd, font=ftAd)
        x, y, textNoWidth, textNoHeight = dr.textbbox((0, 0), textNo, font=ftNo)

        # print("yazı boyutu ", x, y, textWidth, textHeight)

        # kenar boşlukları göz önünde bulundurulursa 290 pixel olarak hesaplama yap
        if textWidth <= 130:
            # ad write
            dr.text(((qrWidth - textWidth)/2, 7), text=textAd, fill=(0), font = ftAd, spacing = 0, align ="center")
            # no write
            dr.text(((qrWidth - textNoWidth)/2, 121), text=textNo, fill=(0), font = ftNo, spacing = 2, align ="center")


            ## biten işlemi ımage olarak döndür
            return img

        else:

            dilimle = textAd.split()
            print(dilimle, len(textAd))
            ## içeriğin kaç parça olduğunu ölç
            icerikBirim = len(dilimle)
            bolum = int(round(len(dilimle) / 2, 0))
            # print("dilimleme ", icerikBirim, bolum)


            self.row1 = ' '.join(map(str, dilimle[0: icerikBirim - bolum]))
            self.row2 = ' '.join(map(str, dilimle[icerikBirim - bolum:]))

            x, y, self.textWidth1, textHeight1 = dr.textbbox((0, 0), self.row1, font=ftAd)
            x, y, self.textWidth2, textHeight2 = dr.textbbox((0, 0), self.row2, font=ftAd)

            # print("yazı boyutu ", x, y, textWidth, textHeight)

            ## dilimlenip ölçülen parçalar eğer genişliklleri 135 pixelden fazla ise birim sayıları 3 ve daha az yap
            if self.textWidth1 <= 120 and self.textWidth2 <= 120:
                # print("bölümler 135 ten küçük")

                # ad write
                dr.text(((qrWidth - self.textWidth1)/2, 1), text=self.row1, fill=(0), font = ftAd, spacing = 0, align ="center")
                dr.text(((qrWidth - self.textWidth2)/2, 12), text=self.row2, fill=(0), font = ftAd, spacing = 0, align ="center")

                # no write
                dr.text(((qrWidth - textNoWidth)/2, 121), text=textNo, fill ="black", font = ftNo, spacing = 2, align ="center")

                return img
            
            else:
                # print("bölümler 135 ten büyük")
                self.row1 = ' '.join(map(str, dilimle[0: icerikBirim - bolum]))
                self.row2 = ' '.join(map(str, dilimle[icerikBirim - bolum:]))

                x, y, self.textWidth1, textHeight1 = dr.textbbox((0, 0), self.row1, font=ftAd)
                x, y, self.textWidth2, textHeight2 = dr.textbbox((0, 0), self.row2, font=ftAd)

                # ad write
                dr.text(((qrWidth - self.textWidth1)/2, 1), text=self.row1, fill=(0), font = ftAd, spacing = 0, align ="center")
                dr.text(((qrWidth - self.textWidth2)/2, 12), text=self.row2, fill=(0), font = ftAd, spacing = 0, align ="center")

                # no write
                dr.text(((qrWidth - textNoWidth)/2, 121), text=textNo, fill ="black", font = ftNo, spacing = 2, align ="center")

                return img



    
    
    def run(self):
        print("Baskı başladı")

        # Basılacak toplam qrcode sayısı
        self.listeBoyutu = len(self.kitapListesi) + 2

        islem = 0
        
        self.proces.emit(0)
        self.mesaj.emit("Qrkodlar oluşturuluyor...")

        QThread().sleep(1) 

        # arguman kontrolu
        if bool(self.kitapListesi) == True:

            SAYFAARKAPLAN = (252,252,252)
            SAYFASIZE = (1240, 1754)

            # tolam işlem adedini tutan barkod sayacı
            islemSayisi = 0
            # islem raporlarını tutacak olan değişken
            rapor =[]
            # Yeni boş resim sayfası. karekodlaar bu boş sayfaya yerleştirilecek sırasıyla. 88 karekoda ulaşınca yeni bir sayfa oluşturulucak.
            yeni = Image.new("RGB", SAYFASIZE, SAYFAARKAPLAN).convert("RGB")
            # Başlangıc sutun degeri her karekod eklenince sutun değeri artacak
            sutun = 20
            # Başlangıc satir değeri . Her karekod eklenince ( 8 karekodda bir) bir karekod boyu değeri artar
            satir = 45
            # Bu sayac sayfa sayısını kontrol edecek. Sayac 88 olunca yeni bira sayfa oluşturacak ve değeri tekrar sıfırlanacak ve satırları kontrol    edecek     kontrol değişkeninin içindeki bir değerlerle eşitlenirse satır atlatılacak
            sayac = 0   
            # kontrol değişkeni satırdaki karekod sayısı satır atlamayı kontrol eder.   
            kontrol = [8,16,24,32,40,48,56,64,72,80,88] 
            # yeniAd değişkenindeki sayfa numarasını kontrol eder sayfa1, sayfa2 ...    
            sayfa = 1   
            # karekodların yapıştırıldığı png sayfası.  başlangıc değeri sayfa1.png. her bir sayfa artılınca yeniAd değeri sayfa2.png ... şeklinde  
          ###  yeniAd = f"{os.getcwd()}/barkode/{sayfa}sayfa.png"
            # Değişkene verilen değerleri dondüren değişken (fonksiyon argumanı)
            
            # olusan sayfaların yolları
            karekodluSayfalar = []

            

            for kitap in self.kitapListesi: 
                # kitap adı veya karekod numarası boş gelirse pas geç
                # liste içindeki no ve isim karakter dizisini dilimleme 
                if len(kitap.ad) != 0 and len(str(kitap.karekod_no)) !=0:

                    img_ = self.qrCode(kitap.ad, str(kitap.karekod_no))

                    yeni.paste(img_, (sutun, satir))    
                    # yapısan karekoddan sonra sutun sayısını arttır.   
                    sutun+=150  
                    # sayfayı ve satırları kontrol eden sayac değişkenini arttır. ( 88 olunca  sayfa değişecek, kontroldeğişkeni    içindeki değerle  eşleşi   rse    satır atlayack)
                    sayac+=1    
                    # sayac değişkeni kontrol değişkeni içinde varsa satır atla 
                    if sayac in kontrol:    
                        # satırı 150 pixel arttir   
                        satir+=150  
                        # sutunu tekrar başa al. varsayılan değerine (varsayılan 20)    
                        sutun-=1200 
                        # işelm sayısı sayacını artır
                        islemSayisi+=1
                        # durumu labela yaz
               #           Eğer sayac 88 olmuşsa bir sayfada yer kalmadığı ve yeni bir sayfaya gecilmesi gerektiğini ifade eder.  
                    if sayac ==88:  
                        # sayac 88 olunca sayfada yer kalmamıstır. ve var olan sayfa üzerine eklenen karekodlarıylakaydedilir. 

                        # sayfa yolunu yol değişkeninde tut
                        karekodluSayfalar.append(yeni)
                        # Sayfa dolduktan sonra yeni bir sayfaya gecirilir. ve satır sutun sayac değişkeni varsayılandeğerlere  döndürülür. 
                        sutun = 20  
                        satir = 45  
                        sayac = 0   
                        # sayfa adındaki sayfa1 sayfa2 .. gibi sayfa ismindeki sayfa numarasını kontrol eder.   
                        sayfa+=1
                        # Bir sayfa dolunca kaydedilir. ( yol arguman olarak alınır ve istenilen sayfaya yazdırılır.)
                      ###  yeniAd = f"{os.getcwd()}/barkode/{sayfa}sayfa.png"
                        # bir sayfanın işlemi tamamlandıktan sonra yeni adlı (boş png sayfası) bir sayfa daha oluşturulur. Veyeni   değişkenine atanır
                        yeni = Image.new("RGB", SAYFASIZE, SAYFAARKAPLAN).convert("RGB")

                        # oluşturulan karekod sayfalarını yollarını listede tut. daha sonra lisredeki yoolrdan yola cıkarakpdf ye   döniştür
                
                islem+=1
                
                yuzdelik = (islem * 100) / self.listeBoyutu
                # print("öge sayısı", self.listeBoyutu, islem, f"yuzdelik {int(yuzdelik)}/100")
                
                self.proces.emit(int(yuzdelik))

            

            islem+=1
            yuzdelik = (islem * 100) / self.listeBoyutu
            print("öge sayısı", self.listeBoyutu, islem, f"yuzdelik {int(yuzdelik)}/100")

            print("Pdf oluşturulmaya başlandı")   
            self.mesaj.emit("Oluşturulan qrkodlar pdf sayfasına ekleniyor.")

            QThread().sleep(2)  

        # Eğer basılacak karekod miktarı 88 değilse( bir sayfayı dolduramayack kadar az ise) var olan acık sayfa üzerine yazılır.
           # print("yeni ad ", yeniAd)
          #  yeni.save(yeniAd)
            karekodluSayfalar.append(yeni) #karekod dosyasının yolunu tutan değişken
             # işelm sayısı sayacını artır
            islemSayisi+=1
            # durumu labela yaz

            print("dosya yolu 3", self.dosyaYolu)

            karekodluSayfalar[0].save(self.dosyaYolu, "PDF",  quality=100,  save_all=True, append_images=karekodluSayfalar[1:])

            islem+=1
            yuzdelik = (islem * 100) / self.listeBoyutu
            print("öge sayısı", self.listeBoyutu, islem, f"yuzdelik {int(yuzdelik)}/100")
            print("işlem bitti")

            self.proces.emit(int(yuzdelik))
            self.mesaj.emit("İşlem başarılı bir şekilde tamamlandı.")

            QThread.currentThread().quit()

        else:
            print("Arguman hatası arguman list  ve demet tipinde olmalı")

            QThread.currentThread().quit()



with open("./assets/stil/stil_books.qss") as file:
    stil = file.read()

class BookQrCode(QDialog):
    def __init__(self, kitapListesi: BookModel):
        super(). __init__()
        
        self.kitapListesi = kitapListesi

        # İçerik kaydetme konumu
        # self.dosyaYolu = "qrcode.pdf"

         # self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        
        self.setStyleSheet(stil)

        WINDOWS_WIDTH = 880
        WINDOWS_HEIGHT = 530

         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)

        self.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)

        self.mainFrameBookQrCode = QFrame(self)
        self.mainFrameBookQrCode.setObjectName("mainFrameBookQrCode")
        self.mainFrameBookQrCode.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)
        self.mainFrameBookQrCode.setContentsMargins(0,0,0,0)


##  FrameAnaLayout
        self.mainBookLayout = QGridLayout()
        self.mainBookLayout.setContentsMargins(8, 8, 8, 10)

        self.mainFrameBookQrCode.setLayout(self.mainBookLayout)

        self.show()


        self.title = QLabel("QRCODE YAZDIRMA LİSTESİ")
        self.closeBut = QPushButton()
        self.listWidget = QListWidget()
        self.yazdirBut = QPushButton("QRCODE YAZDIR")
        self.label1 = QLabel()
        self.label2 = QLabel("etiket bilgi")
        self.progres = QProgressBar()
        
       # self.label1.setFixedSize(250,250)
        self.closeBut.setFixedSize(24, 24)
        self.label1.setFixedSize(200, 175)
        self.label2.setFixedSize(250, 65)
        self.listWidget.setFixedWidth(600)
        self.progres.setFixedHeight(20)


        self.closeBut.setObjectName("closeBut")
        self.yazdirBut.setObjectName("genel")
        self.listWidget.setObjectName("qr")
        self.title.setObjectName("titleQr")
        self.label1.setObjectName("labelQr1")
        self.label2.setObjectName("labelQr2")
       

        self.progres.setValue(0)
        self.progres.setMaximum(100)

        self.yazdirBut.setGraphicsEffect(BoxShadowEffect.colorTeal())

        
        self.label1.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.label2.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.label2.setWordWrap(True)
        self.label2.setText("")
        

        self.mainBookLayout.addWidget(self.title,       0, 2, 1, 2)
        self.mainBookLayout.addWidget(self.closeBut,    0, 6, 1, 1,  alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.mainBookLayout.addWidget(self.listWidget,  1, 0, 7, 5)
        self.mainBookLayout.addWidget(self.label1,      1, 6, 2, 1, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        self.mainBookLayout.addWidget(self.label2,      3, 6, 2, 1)
        self.mainBookLayout.addWidget(self.progres,     5, 6, 1, 1)
        self.mainBookLayout.addWidget(self.yazdirBut,   7, 6, 1, 1)

     


        self._thread = QThread(self)
        self._worker = Worker(self.kitapListesi)
        self._worker.moveToThread(self._thread)   
           
        self._thread.finished.connect(self.threadFinished)
        self._worker.proces.connect(self.progresBarSetValue)
        self._worker.mesaj.connect(self.mesajSetValue)


        self.closeBut.clicked.connect(self.windowClosed)
        self.yazdirBut.clicked.connect(self.onStart)
    

        self.initialFonk()


    @pyqtSlot()
    def windowClosed(self):
        self._thread.deleteLater()
        self.close()
        print(self._thread.isRunning())

    @pyqtSlot()
    def onStart(self):
        home_dir = str(Path.home())

        fnamePath = QFileDialog.getSaveFileName(self, home_dir, "Qrkod listesi", ".pdf")
       # print(fnamePath, home_dir, bool(fnamePath[0]))
        
        self._worker.setFilePath(f"{fnamePath[0]}.pdf") # dosya yolunu işçiye gönderme
        
        if bool(fnamePath[0]) == True:

            self._thread.start()    
                                    
            QTimer.singleShot(2, self._worker.run)
            self.yazdirBut.setEnabled(False)
            self.closeBut.setEnabled(False)

            print(self._thread.isRunning())

    
        else:
            print( "Yol alınamdı")



    @pyqtSlot()
    def threadFinished(self):
        print("kapnma sinyali alındı")
        self.yazdirBut.setEnabled(True)
        self.closeBut.setEnabled(True)

    @pyqtSlot(int)
    def progresBarSetValue(self, value):
        self.progres.setValue(value)

    @pyqtSlot(str)
    def mesajSetValue(self, value):
        self.label2.setText(value)


# baslangıc fonksiyonu kitap listesini list widgete yaz
    def initialFonk(self):
        syc = 1
      #  print(len(str(len(self.kitapListesi))))
        for book in self.kitapListesi:
            self.listWidget.addItem(f"""{str(syc).rjust(len(str(len(self.kitapListesi))), "0")}      {book.karekod_no}   {book.ad}""")
            syc+=1



    def mousePressEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self._move()
            return super().mousePressEvent(event)

    def _move(self):
        window = self.window().windowHandle()
        window.startSystemMove()



""" uyg = QApplication(sys.argv)
#     # QApplication.setStyle(QStyleFactory.create('Fusion'))
# QApplication.setHighDpiScaleFactorRoundingPolicy(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
pen = BookQrCode()
uyg.exec() """