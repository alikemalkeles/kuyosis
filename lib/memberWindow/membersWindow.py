# -*- coding: utf-8 -*-

from pathlib import Path

from PyQt6 import QtCore, QtWidgets, QtGui
from PyQt6.QtCore import pyqtSignal, QDate
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, \
    QLabel, QFrame, QLineEdit, QComboBox, QGroupBox, QGridLayout, QTableWidget, QFileDialog, QAbstractItemView
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font
from openpyxl.styles.borders import Border, Side, BORDER_THIN

from query.dbQuery import DbQuery
from memberWindow.memberAdd import NewMember
from memberWindow.memberReadingHistory import MemberReadingHistory
from memberWindow.memberUpdate import UpdateMember
from customWidget.messageBox import MessageBOX, MessageBOXQuestion
from models.model import *
from settingWindows.settings import SettingsConf
from customWidget.shadowEffect import BoxShadowEffect

with open("assets/stil/stil_members.qss") as file:
    stil = file.read()

class MembersWindow(QFrame):

    sinyal = pyqtSignal(int) # çoklu kitap ve coklu üye penceresine doğrudan gitmek için kullanılan sinyal

    def __init__(self, user: Users) :
        super().__init__()

        ## sistemde aktif olan kullanıcı
        self.user = user

        ## veritabanındaki sorguların sonucu buraya döner buradan tabloya yazılır
        self.memberList: MemberModel = []
        

        #Ekran çözünürlüğü
        screen = self.screen()
      #  print(screen.size().width(), screen.size().height())
        self.w, self.h = (screen.size().width(), screen.size().height())

    # ComboBox ve lineedit widget yükseklikleri
        self.h1 = 40 if self.h > 900 else 32 # combobox ve lineedit
        self.h2 = 170 if self.h > 900 else 150 # GroupBox yüksekliği
        self.h3 = 60 if self.h > 900 else 50 # groupBox butonları




        self.database = DbQuery()
        
        self.setObjectName("frameMember")
        
        
        self.setStyleSheet(stil)

        self.mainLayoutMembers = QVBoxLayout()
        self.mainLayoutMembers.setContentsMargins(15, 0, 15, 0)
        self.setLayout(self.mainLayoutMembers)

        self.topBarLayout = QHBoxLayout()
        self.tableWidget = QTableWidget()
        self.bottomLayout = QHBoxLayout()

    

        self.mainLayoutMembers.addLayout(self.topBarLayout)
        self.mainLayoutMembers.addSpacing(18)
        self.mainLayoutMembers.addWidget(self.tableWidget)
        self.mainLayoutMembers.addLayout(self.bottomLayout)


## TopBarLayout ögeleri (3 tane groupBox bağlanacak)
        self.groupBox1 = QGroupBox()
        self.groupBox2 = QGroupBox()
        self.groupBox3 = QGroupBox()

        self.groupBox1.setFixedHeight(self.h2)
        self.groupBox2.setFixedHeight(self.h2)
        self.groupBox3.setFixedHeight(self.h2)

      #  self.groupBox1.setMaximumWidth(500 if self.w >1400 else 350)
       # self.groupBox3.setMinimumWidth(400 if self.w >1400 else 300)


        self.groupBox1.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.groupBox2.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.groupBox3.setGraphicsEffect(BoxShadowEffect.colorBlue())


       # self.groupBox1.setTitle("İşlemler")
        self.groupBox2.setTitle("Sınıf, şube, sınıf/şube, kimlik")
        self.groupBox3.setTitle("    Ad, soyad, kimlik ve tel no    ")

        self.groupBox1.setObjectName("groupBox1")
        self.groupBox2.setObjectName("groupBox2")
        self.groupBox3.setObjectName("groupBox3")

        
        self.topBarLayout.addWidget(self.groupBox2, stretch=1)
        self.topBarLayout.addWidget(self.groupBox3, stretch=1)
        self.topBarLayout.addWidget(self.groupBox1, stretch=1)

## GroupBox1 ögeleri
        self.groupBox1GridLayout = QGridLayout()
        self.groupBox1GridLayout.setContentsMargins(5, 0, 5, 0)
        self.groupBox1.setLayout(self.groupBox1GridLayout)

        self.yeniUyeEkleBut = QPushButton("Üye Ekle")
        self.uyeSilBut = QPushButton("Üye Sil")
        self.secileniGuncelleBut = QPushButton("Güncelle")
        self.karekodYazdir = QPushButton("Kimlik Yazdır")
        self.uyeninOkuduklariListeleBut = QPushButton("Üyenin Okudukları")
        self.cokluUyeEklemeBut = QPushButton("Çoklu Üye Ekle")

        self.yeniUyeEkleBut.setFixedHeight(self.h3)
        self.uyeSilBut.setFixedHeight(self.h3)
        self.secileniGuncelleBut.setFixedHeight(self.h3)
        self.karekodYazdir.setFixedHeight(self.h3)
        self.uyeninOkuduklariListeleBut.setFixedHeight(self.h3)
        self.cokluUyeEklemeBut.setFixedHeight(self.h3)

        self.yeniUyeEkleBut.setObjectName("yeniUyeEkle")
        self.uyeSilBut.setObjectName("uyeSil")
        self.secileniGuncelleBut.setObjectName("secileniGuncelle")
        self.karekodYazdir.setObjectName("karekodYazdir")
        self.uyeninOkuduklariListeleBut.setObjectName("uyeninOkuduklari")
        self.cokluUyeEklemeBut.setObjectName("cokluUyeEkleme")
        

        

        # self.groupBox1GridLayout.setRowStretch(0, 55)
        self.groupBox1GridLayout.addWidget(self.yeniUyeEkleBut,             0, 0, 1, 1)
        self.groupBox1GridLayout.addWidget(self.uyeSilBut,                  0, 1, 1, 1)
        self.groupBox1GridLayout.addWidget(self.uyeninOkuduklariListeleBut, 0, 2, 1, 1,           alignment=QtCore.Qt.AlignmentFlag.AlignTop)
        self.groupBox1GridLayout.addWidget(self.secileniGuncelleBut,        1, 0, 1, 1)
        # self.groupBox1GridLayout.addWidget(self.karekodYazdir,              1, 1, 1, 1)
        self.groupBox1GridLayout.addWidget(self.cokluUyeEklemeBut,          1, 1, 1, 1)
        
        


## GroupBox2 ögeleri
        self.groupBox2GridLayout = QGridLayout()
        self.groupBox2GridLayout.setContentsMargins(5, 20, 5, 0)
        self.groupBox2.setLayout(self.groupBox2GridLayout)

        self.comboBox1 = QComboBox()
        self.comboBox2 = QComboBox()

    
        self.secileniListele  = QPushButton("Seçimi Listele")
        
        self.comboBox1.setFixedHeight(self.h1)
        self.comboBox2.setFixedHeight(self.h1)
        self.secileniListele.setFixedHeight(self.h3)
        
    
        self.comboBox1.setObjectName("comboBox")
        self.comboBox2.setObjectName("comboBox")
        self.secileniListele.setObjectName("secileniListele")
        
        

        self.comboBox1.addItem("Sınıf, Şube, Sınıf/şube, Kimlik vb.")
        self.comboBox2.addItem("Öncelikle sınıf, şube vb. seçiniz")


        self.comboBox1.addItems(["Sınıf", "Şube", "Sınıf/Şube", "Kimlik durumu", ])
        self.comboBox2.setEnabled(False)
        self.secileniListele.setEnabled(False)

        # self.groupBox1GridLayout.setRowStretch(0, 55)
        self.groupBox2GridLayout.addWidget(self.comboBox1,          0, 0, 1, 3)
        self.groupBox2GridLayout.addWidget(self.comboBox2,          0, 3, 1, 3)

        self.groupBox2GridLayout.addWidget(self.secileniListele,    1, 2, 1, 2)


#GRoupBox3 Ögeleri (arama yapma)
        self.groupBox3GridLayout = QGridLayout()
        self.groupBox3GridLayout.setContentsMargins(0, 20, 0, 0)
        self.groupBox3GridLayout.setSpacing(0)
        self.groupBox3.setLayout(self.groupBox3GridLayout)

        self.lineEditMember = QLineEdit()
        self.membersSearchBut = QPushButton()
        self.searchFilterComboBox = QComboBox()
        self.tumunuListele  = QPushButton("Tüm Üyeler")
        self.tumOduncAlanlarBut =QPushButton("Okuyan Üyeler")
        
       # self.etiketSearchMembers.setText("  T.C. kimlik numarası, ad, soyad öğrenci no ve telefon numarası arayabilirsiniz. (Birkaç karakter yazmanız yeterli.)")
     #   self.etiketSearchMembers.setWordWrap(True)

        self.lineEditMember.setFixedHeight(self.h1)
        self.membersSearchBut.setFixedSize(55 , self.h1)
        self.searchFilterComboBox.setFixedHeight(self.h1)    
        self.tumOduncAlanlarBut.setFixedHeight(self.h3)
        self.tumunuListele.setFixedHeight(self.h3)
        

        
        self.lineEditMember.setMaxLength(40)
        self.lineEditMember.setClearButtonEnabled(True)
        self.lineEditMember.setPlaceholderText("Kimlik no, ad soyad, adres, d.tarihi, tel...")
        self.searchFilterComboBox.addItems(["ID","Ad", "Soyad", "Öğrenci No", "T.C. No", "Tel No", "Tüm Alanlar"])
        self.searchFilterComboBox.setEditable(True)
        self.searchFilterComboBox.lineEdit().setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.searchFilterComboBox.lineEdit().setReadOnly(True)
        self.searchFilterComboBox.setCurrentIndex(6)

        self.lineEditMember.setObjectName("lineEditMember")
        self.membersSearchBut.setObjectName("membersSearch")
        self.searchFilterComboBox.setObjectName("searchFilter")
        self.tumOduncAlanlarBut.setObjectName("tumOduncAlanlarBut")
        self.tumunuListele.setObjectName("tumunuListele")
        

        # self.groupBox1GridLayout.setRowStretch(0, 55)
        self.groupBox3GridLayout.addWidget(self.lineEditMember,            0, 0, 1, 7)
        self.groupBox3GridLayout.addWidget(self.searchFilterComboBox,      0, 7, 1, 2)
        self.groupBox3GridLayout.addWidget(self.membersSearchBut,          0, 9, 1, 1)
        self.groupBox3GridLayout.addWidget(self.tumunuListele,             1, 2, 1, 2)
        self.groupBox3GridLayout.addWidget(self.tumOduncAlanlarBut,        1, 5, 1, 2)
       

        
## QtableWidget işlemleri

        self.tableWidget.setColumnCount(14)
        self.tableWidget.setRowCount(50)
        self.tablo = ["ID", "Kimlik No", "Ad", "Soyad", "D. Tarihi", "Cinsiyet", "Öğr. No", "Sınıf", "Şubes", "Adres", "Tel. No", "Kart Durumu", "Eklenme Tarihi", "Açıklama"]
        self.tableWidget.setHorizontalHeaderLabels(self.tablo)
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch) ## tüm tabloya genişleme
       # self.tableWidget.horizontalHeader().setFixedHeight(32)
        self.tableWidget.horizontalScrollBar().setStyleSheet(stil)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        # self.tableWidget.setSortingEnabled(True) # sıralam özelliğni devre dışı birak
        self.tableWidget.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma

        self.tableWidget.setGraphicsEffect(BoxShadowEffect.colorBlue())
        
        

## bottomLayout Ögeleri
        self.bottomLayout.setContentsMargins(5, 2, 10, 2)
        self.csvToBut = QPushButton()
        self.listeyiTemizle = QPushButton()
        self.bottomEtiket = QLabel("Kırmızı satırlar kitap alan üyeleri gösterir.")

        self.csvToBut.setFixedHeight(32)
        self.csvToBut.setObjectName("csvToBut")
        self.listeyiTemizle.setObjectName("listeyiTemizle")
        self.bottomEtiket.setObjectName("bottomEtiket")
        self.listeyiTemizle.setFixedSize(40, 32)
        self.bottomEtiket.setFixedHeight(25)

        self.csvToBut.setToolTip("Tabloyu xlsx dosyasına aktar")
        self.listeyiTemizle.setToolTip("Tabloyu temizle")

        self.bottomLayout.addWidget(self.bottomEtiket)
        self.bottomLayout.addStretch()
        self.bottomLayout.addWidget(self.listeyiTemizle)
        self.bottomLayout.addSpacing(60)
        self.bottomLayout.addWidget(self.csvToBut)




## sinyal slots
        self.lineEditMember.textChanged.connect(lambda: self.lineEditMember.setText(self.karakterBuyut(self.lineEditMember.text())))

        self.yeniUyeEkleBut.clicked.connect(self.yeniUyeEkleFonk)
        self.tumunuListele.clicked.connect(self.tumUyelerFonk)
        self.secileniGuncelleBut.clicked.connect(self.memberUpdateFonk)
        self.uyeSilBut.clicked.connect(self.uyeSilmeFonk)
        self.uyeninOkuduklariListeleBut.clicked.connect(self.uyeninOkumaGecmisiFonk)
        self.csvToBut.clicked.connect(self.tabloXlsxFonk)
        self.listeyiTemizle.clicked.connect(self.tabloVeriSilFonk)
        self.comboBox1.currentTextChanged.connect(self.comboBox1SecimFonk)
        self.secileniListele.clicked.connect(self.comboBox2SecimFonk)
        self.tumOduncAlanlarBut.clicked.connect(self.okuyanUyelerFonk)
        self.membersSearchBut.clicked.connect(self.uyeIdAdSoyadTcNoAraFonk)
        self.searchFilterComboBox.currentTextChanged.connect(self.seacrhFilterFonk)
        self.lineEditMember.returnPressed.connect(self.uyeIdAdSoyadTcNoAraFonk)

        # coklu üye ekleme penceresi sinyal ile mainwindow (ana pencere) sinyal gönderme
        self.cokluUyeEklemeBut.clicked.connect(lambda: self.sinyal.emit(1))


   #     self.initialFonk()


    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.tableWidgetWidth()

    
    def tableWidgetWidth(self):
        table = self.tableWidget.width()
        toplam = 0
        for w in [1,2,3,3,1.5,1.2,2,1,1,2,2,2,2.5,1.8]:
            toplam = toplam + int((table / 26) * w)

        self.tableWidget.setColumnWidth(0, int(((table) / 26) * 1)) 
        self.tableWidget.setColumnWidth(1, int(((table) / 26) * 2))
        self.tableWidget.setColumnWidth(2, int(((table) / 26) * 3)) # ad
        self.tableWidget.setColumnWidth(3, int(((table) / 26) * 3))
        self.tableWidget.setColumnWidth(4, int(((table) / 26) * 1.5)) # d_tarihi
        self.tableWidget.setColumnWidth(5, int(((table) / 26) * 1.2))
        self.tableWidget.setColumnWidth(6, int(((table) / 26) * 2)) # öğrenci no
        self.tableWidget.setColumnWidth(7, int(((table) / 26) * 1))
        self.tableWidget.setColumnWidth(8, int(((table) / 26) * 1))
        self.tableWidget.setColumnWidth(9, int(((table) / 26) * 2)) # adres
        self.tableWidget.setColumnWidth(10, int(((table) / 26) * 2))
        self.tableWidget.setColumnWidth(11, int(((table) / 26) * 2)) # kart 
        self.tableWidget.setColumnWidth(12, int(((table) / 26) * 2.5))
        self.tableWidget.setColumnWidth(13, int(((table) / 26) * 1.8) + (self.tableWidget.width() - toplam) - self.tableWidget.verticalHeader().width() - 22) # acıklama


    def initialFonkMemberWindow(self):
        self.yeniUyeEkleBut.setEnabled(self.user.ekleme_yetkisi)
        self.uyeSilBut.setEnabled(self.user.silme_yetkisi)
        self.secileniGuncelleBut.setEnabled(self.user.guncelleme_yetkisi)
        self.karekodYazdir.setEnabled(self.user.karekod_kimlik_yetkisi)
        self.uyeninOkuduklariListeleBut.setEnabled(self.user.raporlama_yetkisi)
        self.cokluUyeEklemeBut.setEnabled(self.user.coklu_aktarim_yetkisi)
        self.csvToBut.setEnabled(self.user.raporlama_yetkisi)


        group_name = "comboBoxIndex"
        setting = SettingsConf.getValueConf(group_name=group_name, key="comboBoxMember")
        if setting is None:
            self.searchFilterComboBox.setCurrentIndex(6)
        else:
            self.searchFilterComboBox.setCurrentIndex(int(setting))
        # print("settings member: ", setting)

# Arama filtresi seçince
    def seacrhFilterFonk(self):
        #print(self.searchFilterComboBox.currentIndex())
        """["ID","Ad", "Soyad", "Öğrenci No", "T.C. No", "Tel No", "Tüm Alanlar"]"""

        #secimi ayarlara kaydetme
        group_name = "comboBoxIndex"
        settings = SettingsConf.setValueConf(group_name=group_name, key="comboBoxMember", value=self.searchFilterComboBox.currentIndex())
       # print("ayar kaydedildi: ", self.searchFilterComboBox.currentIndex())

        if self.searchFilterComboBox.currentIndex() == 0:
            self.lineEditMember.setPlaceholderText("Üye ID ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 1:
            self.lineEditMember.setPlaceholderText("Üye adı ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 2:
            self.lineEditMember.setPlaceholderText("Üye soyadı ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 3:
            self.lineEditMember.setPlaceholderText("Öğrenci no ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 4:
            self.lineEditMember.setPlaceholderText("T.C. Kimlik no ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 5:
            self.lineEditMember.setPlaceholderText("Telefon no ile arama yapabilirsiniz")

        elif self.searchFilterComboBox.currentIndex() == 6:
            self.lineEditMember.setPlaceholderText("ID, ad, soyad, öğrenci no, kimlik no, tel no ile arama yapabilirsiniz")


## Yeni Üye ekle
    def yeniUyeEkleFonk(self):
        self.newMembers_ = NewMember()
        self.newMembers_.show()
        self.newMembers_.exec()


# üyenin okudukları penceresi
    def uyeninOkumaGecmisiFonk(self):
        secilenler = []
        uyeIndex = 0
        indexes = self.tableWidget.selectionModel().selectedRows(column=0) # seçilen satırları döndür
        for index in indexes:
            if index.data() != None: # eğer boş satır seçilirse 
                uyeIndex = index.row()
                # print(self.tableWidget.item(index.row(),  0).text(),     self.tableWidget.item(index.row(),  1).text())
                secilenler.append(index)
        # print(secilenler)

      ## Yönetici hesabı gerektirmez
        if self.user:
            
            if len(secilenler) == 0:
                    MessageBOX("UYARI", " Üyenin okuduğu kitapları görüntüleyebilmek için öncelikle bir üye seçiniz.", 0) 

            elif len(secilenler)  > 1: 
                MessageBOX("UYARI", " Yalnızca bir üye seçiniz.", 0)

            elif len(secilenler)  == 1:
                ## Üye güncelleme ekranı çağrılabilir
                self.ornek = MemberReadingHistory(member=self.memberList[uyeIndex], user=self.user)  
                self.ornek.exec()
                




## Üye güncelle
    def memberUpdateFonk(self):

        secilenler = []
        uyeIndex = 0
        indexes = self.tableWidget.selectionModel().selectedRows(column=0) # seçilen satırları döndür
        for index in indexes:
            if index.data() != None: # eğer boş satır seçilirse 
                uyeIndex = index.row()
                # print(self.tableWidget.item(index.row(),  0).text(),     self.tableWidget.item(index.row(),  1).text())
                secilenler.append(index)
        print(secilenler)


      ## Yönetici hesabı gerektirmez
        if self.user:
            
            if len(secilenler) == 0:
                    MessageBOX("UYARI", " Öncelikle güncellemek istediğiniz üyeyi listeden seçiniz", 0) 

            elif len(secilenler)  > 1: 
                MessageBOX("UYARI", " Yalnızca bir üye seçiniz.", 0)

            elif len(secilenler)  == 1:
                ## Üye güncelleme ekranı çağrılabilir
                self.membersUpdate_ = UpdateMember(member=self.memberList[uyeIndex])
                self.membersUpdate_.show()
                self.membersUpdate_.exec()


## Üye silme 
    def uyeSilmeFonk(self):

        secilenler = []
        uyeIndex = 0
        indexes = self.tableWidget.selectionModel().selectedRows(column=0) # seçilen satırları döndür
        for index in indexes:
            if index.data() != None: # eğer boş satır seçilirse 
                uyeIndex = index.row()
                # print(self.tableWidget.item(index.row(),  0).text(),     self.tableWidget.item(index.row(),  1).text())
                secilenler.append(index)
        print(secilenler)


      ## Yönetici hesabı gerektirir.
        if self.user.silme_yetkisi:
            
            if len(secilenler) == 0:
                    MessageBOX("UYARI", " Öncelikle silmek istediğiniz üyeyi listeden seçiniz", 0) 

            elif len(secilenler)  > 1: 
                MessageBOX("UYARI", " Tek seferde en fazla 1 üye silebilirsiniz. Yalnızca bir üye seçiniz.", 0)

            elif len(secilenler)  == 1:

                result = DbQuery.emanettekiKitapUyeID()

                if result is not None:

                    print("üye silme üye okuyor mu  ", result, self.memberList[uyeIndex].id)

                    if self.memberList[uyeIndex].id in result[1]:
                        MessageBOX("UYARI", f" {self.memberList[uyeIndex].ad} {self.memberList[uyeIndex].soyad} adlı üye emanet aldığı kitabı teslim etmediği için silemezsiniz. Öncelikle kitabı teslim alınız", 0)
                    else:
                        print("üye listesi", type(self.memberList), index)
                        print("secim : ", self.memberList[uyeIndex].ad)

                        msg = MessageBOXQuestion("BİLGİ", f" {self.memberList[uyeIndex].ad} {self.memberList[uyeIndex].soyad} adlı üyeyi silmek istediğinize emin misiniz?", 1)
                        msg.okBut.clicked.connect(lambda: self.uyeSilFonk(self.memberList[uyeIndex].id, uyeIndex))
                else:
                    MessageBOX("HATA", f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için üye silinemiyor. Programı kapatıp tekrar deneyiniz.", 0)



    ## üye silme
    def uyeSilFonk(self, no, index):
        result = DbQuery.uyeSil(no)
        if result == 1:
            self.memberList.pop(index)

            self.uyeListesiniTabloyaYaz(self.memberList)

        else:
            MessageBOX("UYARI", " Bilinmeyen bir hata oluştu. Tekrar deneyiniz", 0)
    

## Tüm üyeler
    def tumUyelerFonk(self):

        self.memberList = DbQuery.tumUyeler()

        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.memberList) >= 50:
            self.tableWidget.setRowCount(len(self.memberList))
        else:
            self.tableWidget.setRowCount(50)

        self.uyeListesiniTabloyaYaz(self.memberList)
       



## Üye tc ad soyad arama yap
    def uyeIdAdSoyadTcNoAraFonk(self):
        rowColor = QtGui.QColor(133, 174, 255)
        ## eğer arama yeri boş gelirse 
        ara =  "####" if self.lineEditMember.text() == "" else self.lineEditMember.text()

        self.memberList = DbQuery.uyeIdAdSoyadTcNoAra(ara, 50000, self.searchFilterComboBox.currentIndex()) # arana limiti 50 bin en fazla 50 bin üye

        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.memberList) >= 50:
            self.tableWidget.setRowCount(len(self.memberList))
        else:
            self.tableWidget.setRowCount(50)

        self.uyeListesiniTabloyaYaz(self.memberList)

## okuyan üyeler
    def okuyanUyelerFonk(self):
        self.memberList = DbQuery.okuyanUyeler()

        if len(self.memberList) >=50 :
            self.tableWidget.setRowCount(len(self.memberList))
        else:
            self.tableWidget.setRowCount(50)

        self.uyeListesiniTabloyaYaz(self.memberList)
        

## ComboBox1 katogori seçimi ve seçilen katogorideki ögeler ikinci comBOBoxa yazdırma
    def comboBox1SecimFonk(self):

        secim = self.comboBox1.currentText()

        if secim == "Sınıf, Şube, Sınıf/şube, Kimlik vb.":
            self.comboBox2.clear()
            self.comboBox2.addItem("Öncelikle sınıf, şube vb. seçiniz")
            self.comboBox2.setEnabled(False)

            self.secileniListele.setEnabled(False)
    
        elif secim == "Sınıf":
            liste = []
            result = DbQuery.comboBox1SecimUye("sinif")
        #     print(result)
            liste = []
            for i in result:
                print("sınıf NOne", i, type(i))
                # dönen sonuc NUll ise boş string olarak yansıt listeye
                if i[0] == None:
                    liste.append("")
                else:
                    liste.append(str(i[0]))

            liste.sort()

            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        elif secim == "Şube":
            liste = []
            result = DbQuery.comboBox1SecimUye("sube")
        #     print(result)
            liste = []
            for i in result:
                print("sınıf NOne", i, type(i))
                # dönen sonuc NUll ise boş string olarak yansıt listeye
                if i[0] == None:
                    liste.append("")
                else:
                    liste.append(str(i[0]))
        
            
            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        elif secim == "Sınıf/Şube":
            liste = []
            result = DbQuery.comboBox1SecimUye("Sınıf/Şube")
        #     print(result)
            liste = []
            for i in result:
                print("sınıf sube comboBox1", i)
                # Gelen deger None ise Boş string olarak döndür
                liste.append(f"""{"" if i[0] == None else i[0]}/{"" if i[1] == None else i[1]}""")
        
            liste.sort()
            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)

        elif secim == "Kimlik durumu":
            liste = ["ÜYE KARTI YOK", "ÜYE KARTI VAR"]
            liste.sort()
            self.comboBox2.clear()
            self.comboBox2.addItems(liste)
            self.comboBox2.setEnabled(True)

            self.secileniListele.setEnabled(True)


## seçimi filtrele
    def comboBox2SecimFonk(self):

        rowColor = QtGui.QColor(133, 174, 255)

        cvr = {"Sınıf": "sinif", "Şube": "sube", "Sınıf/Şube": "Sınıf/Şube", "Kimlik durumu": "Kimlik durumu"}

        secim1 = self.comboBox1.currentText()
        secim2 = self.comboBox2.currentText()
        # print(secim2)

        if secim2 == "Öncelikle tür, yazar vb. seçiniz":
            print("Önce tür seçiniz")

        else:
        #    veritabanı sorgusu sonucunda dönen değerleri değişkene atama
            self.memberList = DbQuery.comboBox2SecimUye(cvr[secim1], secim2)
            

            ## sonuca göre tablodaki satır sayısını belirle
            if len(self.memberList) >= 50:
                self.tableWidget.setRowCount(len(self.memberList))
            else:
                self.tableWidget.setRowCount(50)


            self.uyeListesiniTabloyaYaz(self.memberList)


## uyeListesi değişkeni üzerinde yer alan ögeleri tabloya yazma
    def uyeListesiniTabloyaYaz(self, uyeListesi: MemberModel):
        
        # Okuyan üyelerin listesi
        okuyanUyeler = DbQuery.emanettekiKitapUyeID()
    
        rowColor = QtGui.QColor(255, 50, 0)
        self.tableWidget.clear()
        row = 0

        if okuyanUyeler is not None:
            for i in uyeListesi:

                # print(type(i.kart_durumu), i.kart_durumu)
                # kitap emanet listesi içind evar ise renkli göster
                if i.id in okuyanUyeler[1]:
                    # print("no",i.no)
                    item0 = QtWidgets.QTableWidgetItem(str(i.id))
                    item0.setForeground(rowColor)
                    item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 0, item0)

                    item1 = QtWidgets.QTableWidgetItem("" if i.tc == None else f"{str(i.tc)}")
                    item1.setForeground(rowColor)
                    item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 1, item1)

                    item2 = QtWidgets.QTableWidgetItem(i.ad)
                    item2.setForeground(rowColor)
                    self.tableWidget.setItem(row, 2, item2)

                    item3 = QtWidgets.QTableWidgetItem(i.soyad)
                    item3.setForeground(rowColor)
                    self.tableWidget.setItem(row, 3, item3)

                    item4 = QtWidgets.QTableWidgetItem("" if i.d_tarihi == None else str(i.d_tarihi))
                    item4.setForeground(rowColor)
                    item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 4, item4)

                    item5 = QtWidgets.QTableWidgetItem("" if i.cinsiyet == None else i.cinsiyet)
                    item5.setForeground(rowColor)
                    item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 5, item5)

                    item6 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
                    item6.setForeground(rowColor)
                    item6.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 6, item6)

                    item7 = QtWidgets.QTableWidgetItem("" if i.sinif == None else str(i.sinif))
                    item7.setForeground(rowColor)
                    item7.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 7, item7)

                    item8 = QtWidgets.QTableWidgetItem("" if i.sube == None else str(i.sube))
                    item8.setForeground(rowColor)
                    item8.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 8, item8)

                    item9 = QtWidgets.QTableWidgetItem(i.adres)
                    item9.setForeground(rowColor)
                    self.tableWidget.setItem(row, 9, item9)

                    item10 = QtWidgets.QTableWidgetItem(str(i.tel))
                    item10.setForeground(rowColor)
                    item10.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 10, item10)

                    item11 = QtWidgets.QTableWidgetItem("ÜYE KARTI YOK" if i.kart_durumu == "0" else "ÜYE KARTI VAR")
                    item11.setForeground(rowColor)
                    item11.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 11, item11)

                    item12 = QtWidgets.QTableWidgetItem(i.eklenme_tarihi)
                    item12.setForeground(rowColor)
                    item12.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 12, item12)

                    item13 = QtWidgets.QTableWidgetItem(i.bilgi)
                    item13.setForeground(rowColor)
                    self.tableWidget.setItem(row, 13, item13)

                    row+=1
                else:
                    item0 = QtWidgets.QTableWidgetItem(str(i.id))
                    # item0.setForeground(rowColor)
                    item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 0, item0)

                    item1 = QtWidgets.QTableWidgetItem("" if i.tc == None else f"{str(i.tc)}")
                    # item1.setForeground(rowColor)
                    item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 1, item1)

                    item2 = QtWidgets.QTableWidgetItem(i.ad)
                    # item2.setForeground(rowColor)
                    self.tableWidget.setItem(row, 2, item2)

                    item3 = QtWidgets.QTableWidgetItem(i.soyad)
                    # item3.setForeground(rowColor)
                    self.tableWidget.setItem(row, 3, item3)

                    item4 = QtWidgets.QTableWidgetItem("" if i.d_tarihi == None else str(i.d_tarihi))
                    # item4.setForeground(rowColor)
                    item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 4, item4)

                    item5 = QtWidgets.QTableWidgetItem("" if i.cinsiyet == None else i.cinsiyet)
                    # item5.setForeground(rowColor)
                    item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 5, item5)

                    item6 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
                    # item6.setForeground(rowColor)
                    item6.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 6, item6)

                    item7 = QtWidgets.QTableWidgetItem("" if i.sinif == None else str(i.sinif))
                    # item7.setForeground(rowColor)
                    item7.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 7, item7)

                    item8 = QtWidgets.QTableWidgetItem("" if i.sube == None else str(i.sube))
                    # item8.setForeground(rowColor)
                    item8.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 8, item8)

                    item9 = QtWidgets.QTableWidgetItem(i.adres)
                    # item9.setForeground(rowColor)
                    self.tableWidget.setItem(row, 9, item9)

                    item10 = QtWidgets.QTableWidgetItem(str(i.tel))
                    # item10.setForeground(rowColor)
                    item10.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 10, item10)

                    item11 = QtWidgets.QTableWidgetItem("ÜYE KARTI YOK" if i.kart_durumu == "0" else "ÜYE KARTI VAR")
                    # item11.setForeground(rowColor)
                    item11.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 11, item11)

                    item12 = QtWidgets.QTableWidgetItem(i.eklenme_tarihi)
                    # item12.setForeground(rowColor)
                    item12.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableWidget.setItem(row, 12, item12)

                    item13 = QtWidgets.QTableWidgetItem(i.bilgi)
                    # item13.setForeground(rowColor)
                    self.tableWidget.setItem(row, 13, item13)

                    row+=1

            self.tableWidget.setHorizontalHeaderLabels(self.tablo)

        else:
            MessageBOX("HATA", f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için üyeler listelenemiyor. Programı kapatıp tekrar deneyiniz.", 0)



    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()


## tablodaki verileri sil
    def tabloVeriSilFonk(self):
        self.tableWidget.clear()
        self.tableWidget.setHorizontalHeaderLabels(self.tablo)
        self.tableWidget.setRowCount(50)

        self.memberList.clear() # kitap listesini de temizle

        self.tableWidgetWidth()


# Tablodaki içerikleri xlsx dosyasına aktar
    def tabloXlsxFonk(self):

        kurumBilgileri = DbQuery.ayarlarVeYapilandirmalar()
        date = QDate.currentDate()
        sorgu_tarihi = date.toString('dd.MM.yyyy')
        
        if self.user.raporlama_yetkisi:

            if len(self.memberList) > 0:
                ## modeli listeye aktar
                home_dir = str(Path.home())

                # tablo başlık yazısı
                title = "ÜYE LİSTESİ"

                # 14 SÜTUN


                fnamePath = QFileDialog.getSaveFileName(self, home_dir, "Üye Listesi", ".xlsx")
                print(fnamePath, home_dir, bool(fnamePath))

                if bool(fnamePath[0]) == True:

                    thin_border = Border(
                            left=Side(border_style=BORDER_THIN, color='00000000'),
                            right=Side(border_style=BORDER_THIN, color='00000000'),
                            top=Side(border_style=BORDER_THIN, color='00000000'),
                            bottom=Side(border_style=BORDER_THIN, color='00000000')
                            )

                    work_book = Workbook()  
                    work_sheet = work_book.active  

                    work_sheet.page_setup.orientation = work_sheet.ORIENTATION_LANDSCAPE
                    work_sheet.page_setup.paperSize = work_sheet.PAPERSIZE_A4

                    work_sheet.sheet_properties.pageSetUpPr.fitToPage = True # sayfayı genişliğe sığdır
                    work_sheet.page_setup.fitToHeight = False
                    work_sheet.page_setup.fitToWidth = True

                    work_sheet.page_margins.left = 0.2
                    work_sheet.page_margins.right = 0.1
                    work_sheet.page_margins.top = 0.1
                    work_sheet.page_margins.bottom = 0.1

                    work_sheet.column_dimensions['A'].width = 7   # sütun genişlikleri
                    work_sheet.column_dimensions['B'].width = 13  # sütun genişlikleri
                    work_sheet.column_dimensions['C'].width = 16
                    work_sheet.column_dimensions['D'].width = 12
                    work_sheet.column_dimensions['E'].width = 11   # dogum tarihi
                    work_sheet.column_dimensions['F'].width = 9
                    work_sheet.column_dimensions['G'].width = 10
                    work_sheet.column_dimensions['H'].width = 7
                    work_sheet.column_dimensions['I'].width = 7   # şube
                    work_sheet.column_dimensions['J'].width = 12  # adres
                    work_sheet.column_dimensions['K'].width = 12  # telefon
                    work_sheet.column_dimensions['L'].width = 7
                    work_sheet.column_dimensions['M'].width = 15
                    work_sheet.column_dimensions['N'].width = 10

                    work_sheet.merge_cells('A1:N2')  # hücre birleştirme
    
                # kurum bilgileri ve kurum adı
                    work_sheet.row_dimensions[1].height = 20
                    work_sheet.row_dimensions[2].height = 22
                    cell = work_sheet.cell(row=1, column=1)  
                    cell.value = f"{kurumBilgileri.kurum_adi}\n {title}"
                    cell.font = Font(bold=True, size=15) 
                    cell.alignment = Alignment(horizontal='center', vertical='center', wrapText=True) 

                ## tablonun başlıkları
                    titleColumn = 1
                    work_sheet.row_dimensions[3].height = 30
                    for title_ in self.tablo: # tablo başlığını yaz
                        baslik = work_sheet.cell(row=3, column=titleColumn)  
                        baslik.value = f"{title_}"  
                        baslik.font = Font(bold=True, size=10)
                        baslik.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                        titleColumn+=1

                    titleRow = 4

                    siraNumarasi = 1
                    for uyeler in self.memberList:
                       # print("bookListTaken", kitaplar)
                        work_sheet.row_dimensions[titleRow].height = 25

                        siraNo = work_sheet.cell(row=titleRow, column=1)  
                        siraNo.font = Font(bold=True, size=10)
                        siraNo.value = f"{siraNumarasi}"  
                        siraNo.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        siraNo.border = thin_border

                        tc = work_sheet.cell(row=titleRow, column=2)  
                        tc.font = Font(bold=False, size=10)
                        tc.value = f"{'' if uyeler.tc == None else uyeler.tc}"  
                        tc.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        tc.border = thin_border

                        ad = work_sheet.cell(row=titleRow, column=3) 
                        ad.font = Font(bold=False, size=10) 
                        ad.value = f"{uyeler.ad}"  
                        ad.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True) 
                        ad.border = thin_border

                        soyad = work_sheet.cell(row=titleRow, column=4)  
                        soyad.font = Font(bold=False, size=10)
                        soyad.value = f"{uyeler.soyad}"  
                        soyad.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True)  
                        soyad.border = thin_border

                        d_tarihi = work_sheet.cell(row=titleRow, column=5)  
                        d_tarihi.font = Font(bold=False, size=10)
                        d_tarihi.value = f"{'' if uyeler.d_tarihi == None else uyeler.d_tarihi}"  
                        d_tarihi.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                        d_tarihi.border = thin_border

                        cinsiyet = work_sheet.cell(row=titleRow, column=6) 
                        cinsiyet.font = Font(bold=False, size=10) 
                        cinsiyet.value = f"""{'' if uyeler.cinsiyet == None else uyeler.cinsiyet}"""  
                        cinsiyet.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        cinsiyet.border = thin_border 

                        ogrenci_no = work_sheet.cell(row=titleRow, column=7)  
                        ogrenci_no.font = Font(bold=False, size=10)
                        ogrenci_no.value = f"{'' if uyeler.ogrenci_no == None else uyeler.ogrenci_no}"  
                        ogrenci_no.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                        ogrenci_no.border = thin_border

                        sinif = work_sheet.cell(row=titleRow, column=8)  
                        sinif.font = Font(bold=False, size=10)
                        sinif.value = f"{'' if uyeler.sinif == None else uyeler.sinif}"  
                        sinif.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)  
                        sinif.border = thin_border

                        sube = work_sheet.cell(row=titleRow, column=9)  
                        sube.font = Font(bold=False, size=10)
                        sube.value = f"{'' if uyeler.sube == None else uyeler.sube}"  
                        sube.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        sube.border = thin_border

                        adres = work_sheet.cell(row=titleRow, column=10)  
                        adres.font = Font(bold=False, size=10)
                        adres.value = f"{uyeler.adres}"  
                        adres.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True)   
                        adres.border = thin_border

                        telefon = work_sheet.cell(row=titleRow, column=11)  
                        telefon.font = Font(bold=False, size=10)
                        telefon.value = f"{uyeler.tel}"  
                        telefon.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)   
                        telefon.border = thin_border

                        kart = work_sheet.cell(row=titleRow, column=12)  
                        kart.font = Font(bold=False, size=10)
                        kart.value = f"{'YOK' if uyeler.kart_durumu == '0' else 'VAR'}"  
                        kart.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)
                        kart.border = thin_border   

                        eklenme_tarihi = work_sheet.cell(row=titleRow, column=13)  
                        eklenme_tarihi.font = Font(bold=False, size=10)
                        eklenme_tarihi.value = f"{uyeler.eklenme_tarihi[0:-3]}"  
                        eklenme_tarihi.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)   
                        eklenme_tarihi.border = thin_border

                        aciklama = work_sheet.cell(row=titleRow, column=14)  
                        aciklama.font = Font(bold=False, size=10)
                        aciklama.value = f"{uyeler.bilgi}"  
                        aciklama.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True)   
                        aciklama.border = thin_border

                        titleRow+=1

                        siraNumarasi+=1


                    work_sheet.merge_cells(f"M{titleRow + 1}:N{titleRow + 1}")  # hücre birleştirme ( kitap listesini yazdıktan sonra 1 satır boşluk bırakıp kurum yöneticini yaz)
                    work_sheet.row_dimensions[titleRow].height = 25

                    work_sheet.row_dimensions[titleRow + 1].height = 50
                    cell2 = work_sheet.cell(row=titleRow + 1, column=13)  
                    cell2.value = f"{sorgu_tarihi}\n{kurumBilgileri.kurum_yoneticisi}\nKurum Yöneticisi"  
                    cell2.font = Font(bold=True, size=10)
                    cell2.alignment = Alignment(horizontal='center', vertical='center', wrapText=True)


                    work_book.save(f"{fnamePath[0]}.xlsx")
                    print(("tabloya yazdık"))
                    MessageBOX("BİLGİLENDİRME", " Tablodaki içerikler .xlsx dosyasına başarılı bir şekilde aktarıldı.", 1)

                else:
                    print("dosya yolu alınamadı")
                    pass

            else:
                MessageBOX("UYARI", " Aktarılacak veri bulunmadı", 0)
                print("öncelikle üye listele")

       
   
