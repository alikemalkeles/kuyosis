from pathlib import Path

from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import Qt, QDate
from PyQt6.QtWidgets import QPushButton, QFrame, QGridLayout, QLabel, QDialog, QTableWidget, QAbstractItemView, \
    QFileDialog
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font
from openpyxl.styles.borders import Border, Side, BORDER_THIN

from query.dbQuery import DbQuery
from customWidget.messageBox import MessageBOX
from models.model import *
from customWidget.shadowEffect import BoxShadowEffect

# self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
with open("assets/stil/stil_members.qss") as file:
    stil = file.read()


class MemberReadingHistory(QDialog):
    def __init__(self, member: MemberModel, user: Users):
        super().__init__(parent=None)

        self.user = user

        self.member = member

        self.uyeninOkuduklari = []

        self.toplamSayfa = 0

        self.setStyleSheet(stil)

        self.database = DbQuery()

        #Ekran çözünürlüğü
        screen = self.screen()
       # print(screen.size().width(), screen.size().height())
        self.w, self.h = (screen.size().width(), screen.size().height())
        ## Ekranın en fazla 30% oranında küçülmesini sağla
        self.w_percent, self.h_percent = (int(self.w / 100) * 80, int(self.h / 100) * 90)
        
    
         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)
        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)
        self.setFixedSize(self.w_percent, self.h_percent)

        self.mainFrame = QFrame(self)
        self.mainFrame.setObjectName("FrameMemberReadingHistory")
        self.mainFrame.setFixedSize(self.w_percent, self.h_percent)
        self.mainFrame.setContentsMargins(0,0,0,0)
        

    # ana layout
        self.mainLayout = QGridLayout(self.mainFrame)
        self.mainLayout.setContentsMargins(10, 8, 10, 12)
        self.mainLayout.setSpacing(0)
        

        self.closeBut = QPushButton()
        self.title = QLabel("    ÜYENİN OKUMA GEÇMİŞİ")
        self.csvToBut = QPushButton("  Listeyi xlsx olarak aktar")
        self.toplamOkunanSayfa = QLabel(".")
        

        self.memberName = QLabel(f""" İsim: <font color = "#339af0" weight = "bold">{self.member.ad} {self.member.soyad} </font> """)
        self.memberSurname = QLabel(self.member.soyad)
        self.memberTc = QLabel(f"""Kimlik No: <font color = "#339af0" weight = "bold">{str(self.member.tc)}</font>""")
        self.memberStudentNo = QLabel(f"""Öğrenci No: <font color = "#339af0" weight = "bold"> {str(self.member.ogrenci_no)} </font>""")
        self.memberGender = QLabel("" if self.member.cinsiyet == None else self.member.cinsiyet)
        self.memberBirth = QLabel(self.member.d_tarihi)
        self.memberClass = QLabel(f"""Sınıf/Şube: <font color = "#339af0" weight = "bold"> {str(self.member.sinif)}-{self.member.sube} </font> """)
        self.memberBranch = QLabel(f"{self.member.sube}")
        self.memberAddress = QLabel(self.member.adres)
        self.memberPhone = QLabel(str(self.member.tel))
        self.memberCard = QLabel(self.member.kart_durumu)
        self.memberInfo = QLabel(self.member.bilgi)


        self.table = QTableWidget()
        self.table.setColumnCount(6)
        
        self.tablo = ["Kitap ID", "Kitap Adı", "Yazarı", "Verilme Tarihi", "Teslim Tarihi", "S. Sayısı"]
        self.table.setHorizontalHeaderLabels(self.tablo)
        
        # self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch) ## tüm tabloya genişleme
        self.table.horizontalHeader().setFixedHeight(32)
        self.table.horizontalHeader().setDefaultAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.table.horizontalScrollBar().setStyleSheet(stil)
        self.table.setAlternatingRowColors(True)
        self.table.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        # self.tableWidget.setSortingEnabled(True) # sıralam özelliğni devre dışı birak
        self.table.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma

        self.table.setGraphicsEffect(BoxShadowEffect.colorBlue())

        #window width 1000px


        self.title.setObjectName("titleHistory")
        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.csvToBut.setFixedSize(220, 40)
        self.csvToBut.setObjectName("csvToBut")
        self.toplamOkunanSayfa.setObjectName("toplamOkunanSayfa")

        self.memberName.setObjectName("memberReadingHistory")
        self.memberSurname.setObjectName("memberReadingHistory")
        self.memberTc.setObjectName("memberReadingHistory")
        self.memberStudentNo.setObjectName("memberReadingHistory")
        self.memberGender.setObjectName("memberReadingHistory")
        self.memberBirth.setObjectName("memberReadingHistory")
        self.memberClass.setObjectName("memberReadingHistory")
        self.memberBranch.setObjectName("memberReadingHistory")
        self.memberAddress.setObjectName("memberReadingHistory")
        self.memberPhone.setObjectName("memberReadingHistory")
        self.memberCard.setObjectName("memberReadingHistory")
        self.memberInfo.setObjectName("memberReadingHistory")



        self.closeBut.setFixedSize(24, 24)



        self.closeBut.setObjectName("closeBut")


        self.mainLayout.addWidget(self.closeBut,           0,  5, 1, 1,       alignment=QtCore.Qt.AlignmentFlag.AlignTop | QtCore.Qt.AlignmentFlag.AlignRight)
        self.mainLayout.addWidget(self.title,              0,  1, 1, 4,       alignment=QtCore.Qt.AlignmentFlag.AlignCenter)

        self.mainLayout.addWidget(self.memberName,         1,  0, 1,  3)
       
        self.mainLayout.addWidget(self.memberTc,           1,  3,  1,  1)
        self.mainLayout.addWidget(self.memberClass,        1,  4,  1,  1,    alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.mainLayout.addWidget(self.memberStudentNo,    1,  5,  1,  1,    alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

        self.mainLayout.addWidget(self.table,              2,  0,  12,  6)

        self.mainLayout.addWidget(self.toplamOkunanSayfa,  14, 4,  1,  2,   alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.mainLayout.addWidget(self.csvToBut,           14, 0,  1,  2,   alignment=QtCore.Qt.AlignmentFlag.AlignBottom)

       
      
        self.closeBut.clicked.connect(lambda: self.close())
        self.csvToBut.clicked.connect(self.tabloXlsxFonk)


        self.initialFonk()

        
    def tableWidgetWidth(self):

        table_width = self.w_percent - self.table.verticalHeader().width() - 16 # pencere - tablo satır numrası genişliği - padding
        x = int(table_width / 26)

        self.table.setColumnWidth(0,  int(x * 2))
        self.table.setColumnWidth(1,  int(x * 8))
        self.table.setColumnWidth(2,  int(x * 5.5))
        self.table.setColumnWidth(3,  int(x * 4))
        self.table.setColumnWidth(4,  int(x * 4))
        self.table.setColumnWidth(5,  int(x * 2))

    def initialFonk(self):
        result = DbQuery.uyeninOkumaGecmisi(self.member.id)
        row = 0

        self.uyeninOkuduklari = result

        for i in result:
            if i[3] is not None:
                self.toplamSayfa+=i[3]

        self.toplamOkunanSayfa.setText(f""" Okunan sayfa sayısı: <font color = "red" weight = "bold">{self.toplamSayfa} </font> """)
        
        if len(result) >= 40:
            self.table.setRowCount(len(result))

        else:
            self.table.setRowCount(40)

        for kitap_id, kitap_adi, yazar, sayfa_sayisi, v_tarihi, t_tarihi, v_saati, t_saati in result:
            print(kitap_id, kitap_adi, yazar, sayfa_sayisi, v_tarihi, t_tarihi, v_saati, t_saati)
            # print(type(t_tarihi))

            item0 = QtWidgets.QTableWidgetItem(str(kitap_id))
            item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.table.setItem(row, 0, item0)

            item1 = QtWidgets.QTableWidgetItem(kitap_adi)
            self.table.setItem(row, 1, item1)

            item2 = QtWidgets.QTableWidgetItem(yazar)
            self.table.setItem(row, 2, item2)

            item3 = QtWidgets.QTableWidgetItem(f"{v_tarihi}  {v_saati}")
            item3.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.table.setItem(row, 3, item3)

            item4 = QtWidgets.QTableWidgetItem("" if t_tarihi == None else f"{t_tarihi}  {t_saati}")
            item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.table.setItem(row, 4, item4)

            item5 = QtWidgets.QTableWidgetItem(str(sayfa_sayisi))
            item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.table.setItem(row, 5, item5)

            row+=1

        self.tableWidgetWidth()


#xlsx olarak dışarı aktar
    def tabloXlsxFonk(self):
        kurumBilgileri = DbQuery.ayarlarVeYapilandirmalar()
        date = QDate.currentDate()
        sorgu_tarihi = date.toString('dd.MM.yyyy')

         ## Yönetici hesabı gerektirir.
        if self.user.raporlama_yetkisi:

            if len(self.uyeninOkuduklari) > 0:
                ## modeli listeye aktar
                home_dir = str(Path.home())

                tablo = ["SIRA", "KİTAP ID", "KİTAP ADI", "YAZARI", "ALINMA TARİHİ", "TESLİM TARİHİ", "SAYFA SAYISI"]

                fnamePath = QFileDialog.getSaveFileName(self, home_dir, f"{self.member.ad} {self.member.soyad} - Okuma Raporu", ".xlsx")
                print(fnamePath, home_dir, bool(fnamePath))


                if bool(fnamePath[0]) == True:

                    thin_border = Border(
                            left=Side(border_style=BORDER_THIN, color='00000000'),
                            right=Side(border_style=BORDER_THIN, color='00000000'),
                            top=Side(border_style=BORDER_THIN, color='00000000'),
                            bottom=Side(border_style=BORDER_THIN, color='00000000')
                            )

                    work_book = Workbook()  
                    work_sheet = work_book.active  

                    work_sheet.page_setup.orientation = work_sheet.ORIENTATION_PORTRAIT
                    work_sheet.page_setup.paperSize = work_sheet.PAPERSIZE_A4

                    work_sheet.sheet_properties.pageSetUpPr.fitToPage = True # sayfayı genişliğe sığdır
                    work_sheet.page_setup.fitToHeight = False
                    work_sheet.page_setup.fitToWidth = True

                    work_sheet.page_margins.left = 0.2
                    work_sheet.page_margins.right = 0.1
                    work_sheet.page_margins.top = 0.1
                    work_sheet.page_margins.bottom = 0.1

                    work_sheet.column_dimensions['A'].width = 7
                    work_sheet.column_dimensions['B'].width = 10 # sütun genişlikleri
                    work_sheet.column_dimensions['C'].width = 56
                    work_sheet.column_dimensions['D'].width = 30
                    work_sheet.column_dimensions['E'].width = 17
                    work_sheet.column_dimensions['F'].width = 20
                    work_sheet.column_dimensions['G'].width = 13


                    work_sheet.merge_cells('A1:G2')  # hücre birleştirme
                    work_sheet.row_dimensions[1].height = 25
                    work_sheet.row_dimensions[2].height = 25
                    cell = work_sheet.cell(row=1, column=1)  
                    cell.value = "ÜYENİN OKUDUĞU KİTAPLAR" 
                    cell.font = Font(bold=True, size=15) 
                    cell.alignment = Alignment(horizontal='center', vertical='center') 


                # Üye bilgileri
                    work_sheet.merge_cells('A3:B3')  # hücre birleştirme
                    work_sheet.row_dimensions[3].height = 30
                    cell = work_sheet.cell(row=3, column=1)  
                    cell.value = "AD SOYAD" 
                    cell.font = Font(bold=True, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[3].height = 30
                    cell = work_sheet.cell(row=3, column=3)  
                    cell.value = f"{self.member.ad} {self.member.soyad}" 
                    cell.font = Font(bold=False, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[3].height = 30
                    cell = work_sheet.cell(row=3, column=4)  
                    cell.value = "SINIF" 
                    cell.font = Font(bold=True, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[3].height = 30
                    cell = work_sheet.cell(row=3, column=5)  
                    cell.value = f"{self.member.sinif} {self.member.sube}" 
                    cell.font = Font(bold=False, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[3].height = 30
                    cell = work_sheet.cell(row=3, column=6)  
                    cell.value = "ÖĞRENCİ NO" 
                    cell.font = Font(bold=True, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[3].height = 30
                    cell = work_sheet.cell(row=3, column=7)  
                    cell.value = f"{self.member.ogrenci_no}" 
                    cell.font = Font(bold=False, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.merge_cells('A4:B4')  # hücre birleştirme
                    work_sheet.row_dimensions[4].height = 30
                    cell = work_sheet.cell(row=4, column=1)  
                    cell.value = "KİMLİK NO" 
                    cell.font = Font(bold=True, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[4].height = 30
                    cell = work_sheet.cell(row=4, column=3)  
                    cell.value = f"{self.member.tc}" 
                    cell.font = Font(bold=False, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[4].height = 30
                    cell = work_sheet.cell(row=4, column=4)  
                    cell.value = "EKLENME TARİHİ" 
                    cell.font = Font(bold=True, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[4].height = 30
                    cell = work_sheet.cell(row=4, column=5)  
                    cell.value = f"{self.member.eklenme_tarihi[0:10]}" 
                    cell.font = Font(bold=False, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[4].height = 30
                    cell = work_sheet.cell(row=4, column=6)  
                    cell.value = "DOĞUM TARİHİ" 
                    cell.font = Font(bold=True, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center') 

                    work_sheet.row_dimensions[4].height = 30
                    cell = work_sheet.cell(row=4, column=7)  
                    cell.value = f"{self.member.d_tarihi}" 
                    cell.font = Font(bold=False, size=13) 
                    cell.alignment = Alignment(horizontal='left', vertical='center')

                    
                #Başlık
                    titleColumn = 1
                    work_sheet.row_dimensions[5].height = 30
                    for title_ in tablo: # tablo başlığını yaz
                        baslik = work_sheet.cell(row=5, column=titleColumn)  
                        baslik.value = f"{title_}"  
                        baslik.font = Font(bold=True)
                        baslik.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                        baslik.border = thin_border
                        titleColumn+=1

                    
                    siraNo = 1
                    titleRow = 6 # satırlar

                    for kitaplar in self.uyeninOkuduklari:
                        
                        work_sheet.row_dimensions[titleRow].height = 25

                        sira = work_sheet.cell(row=titleRow, column=1)  
                        sira.value = f"{str(siraNo)}"  
                        sira.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        sira.border = thin_border

                        id_ = work_sheet.cell(row=titleRow, column=2)  
                        id_.value = f"{kitaplar[0]}"  
                        id_.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        id_.border = thin_border

                        ad = work_sheet.cell(row=titleRow, column=3)  
                        ad.value = f"{kitaplar[1]}"  
                        ad.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True) 
                        ad.border = thin_border

                        yazar = work_sheet.cell(row=titleRow, column=4)  
                        yazar.value = f"{kitaplar[2]}"  
                        yazar.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True)  
                        yazar.border = thin_border

                        verilme_t = work_sheet.cell(row=titleRow, column=5)  
                        verilme_t.value = f"{kitaplar[4]} {kitaplar[6][0:-3]}"  
                        verilme_t.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)  
                        verilme_t.border = thin_border

                        teslim_t = work_sheet.cell(row=titleRow, column=6)  
                        teslim_t.value = f"""{"" if kitaplar[5] == None else f"{kitaplar[5]}  {kitaplar[7][0:-3]}"}"""  
                        teslim_t.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)  
                        teslim_t.border = thin_border

                        sayfa = work_sheet.cell(row=titleRow, column=7)  
                        sayfa.value = f"{kitaplar[3]}"  
                        sayfa.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True) 
                        sayfa.border = thin_border 

                        titleRow+=1
                        siraNo+=1


                    work_sheet.merge_cells(f"A{titleRow}:F{titleRow}")  # hücre birleştirme

                    work_sheet.row_dimensions[titleRow].height = 25
                    cell2 = work_sheet.cell(row=titleRow, column=1)  
                    cell2.value = "Okunan Sayfa Sayısı  "  
                    cell2.font = Font(bold=True)
                    cell2.alignment = Alignment(horizontal='right', vertical='center')

                    cell3 = work_sheet.cell(row=titleRow, column=7)  
                    cell3.value = f"{self.toplamSayfa}"  
                    cell3.font = Font(bold=True, size=15)
                    cell3.alignment = Alignment(horizontal='center', vertical='center')


                    work_sheet.merge_cells(f"F{titleRow + 2}:G{titleRow + 2}")  # hücre birleştirme
                    work_sheet.row_dimensions[titleRow].height = 25


                    work_sheet.row_dimensions[titleRow + 2].height = 50
                    cell2 = work_sheet.cell(row=titleRow + 2, column=6)  
                    cell2.value = f"{sorgu_tarihi}\n{kurumBilgileri.kurum_yoneticisi}\nKurum Yöneticisi"  
                    cell2.font = Font(bold=True, size=10)
                    cell2.alignment = Alignment(horizontal='center', vertical='center', wrapText=True)



                    work_book.save(f"{fnamePath[0]}.xlsx")
                    print(("tabloya yazdık"))
                    MessageBOX("BİLGİLENDİRME", " Tablodaki içerikler .xlsx dosyasına başarılı bir şekilde aktarıldı.", 1)

                else:
                    print("dosya yolu alınamadı")
                    pass

            else:
                MessageBOX("UYARI", " Aktarılacak veri bulunmadı", 0)
                print("öncelikle üye listele")
        else:
            MessageBOX("UYARI", " Bunun için yetkiniz yoktur",  0)





    def mousePressEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self._move()
            return super().mousePressEvent(event)

    def _move(self):
        window = self.window().windowHandle()
        window.startSystemMove()