from PyQt6 import QtCore
from PyQt6.QtCore import Qt, QDate
from PyQt6.QtGui import QRegularExpressionValidator
from PyQt6.QtWidgets import QHBoxLayout, QPushButton, QFrame, QGridLayout, QLabel, QLineEdit, QComboBox, QDialog, \
    QDateEdit

from query.dbQuery import DbQuery
from customWidget.messageBox import MessageBOX
from models.model import MemberModel
from customWidget.shadowEffect import BoxShadowEffect

# self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
with open("./assets/stil/stil_members.qss") as file:
    stil = file.read()


class UpdateMember(QDialog):
    def __init__(self, member: MemberModel):
        super(). __init__()

        self.member = member

    

        WINDOWS_WIDTH = 880
        WINDOWS_HEIGHT = 540

         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

      #  self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)

        self.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)

        self.mainFrameMemberAdd = QFrame(self)
        self.mainFrameMemberAdd.setObjectName("mainFrameMemberAdd")
        self.mainFrameMemberAdd.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)
        self.mainFrameMemberAdd.setContentsMargins(0,0,0,0)



## Book FrameAnaLayout
        self.mainMemberLayout = QHBoxLayout()
        self.mainMemberLayout.setContentsMargins(0,0,0,0)

        ## kayıt ogeleri lineedit
        self.memberLayoutAdd = QGridLayout()
        self.memberLayoutAdd.setContentsMargins(25, 8, 12, 8)
        self.memberLayoutAdd.setSpacing(0)
        

        self.gorsel = QLabel()
        self.gorsel.setObjectName("gorselMemberAdd")
        self.gorsel.setFixedSize(300, 540)


        self.mainMemberLayout.addWidget(self.gorsel)
        self.mainMemberLayout.addLayout(self.memberLayoutAdd)


        self.mainFrameMemberAdd.setLayout(self.mainMemberLayout)


        
## Kayıt sayfası

        # self.memberLayoutAdd = QGridLayout() eklenecek


        self.closeBut = QPushButton()
        self.closeBut.setFixedSize(24, 24)
        self.closeBut.setObjectName("closeBut")
       

        self.memberName = QLineEdit()
        self.memberSurname = QLineEdit()
        self.memberTc = QLineEdit()
        self.memberStudentNo = QLineEdit()
        self.memberGender = QComboBox() # cinsiyet
        self.memberBirth = QDateEdit(calendarPopup=True) # d tarihi
        self.memberClass = QComboBox()
        self.memberBranch = QComboBox() # şube
        self.memberAddress = QLineEdit()
        self.memberPhone = QLineEdit()
        self.memberCard = QComboBox()
        self.memberInfo = QLineEdit()

        


        rx1 = QtCore.QRegularExpression("[0-9]{11}")
        v1 = QRegularExpressionValidator(rx1, self.memberTc)
        self.memberTc.setValidator(v1)

        rx2 = QtCore.QRegularExpression("[0-9]{11}")
        v2 = QRegularExpressionValidator(rx2, self.memberPhone)
        self.memberPhone.setValidator(v2)

        rx3 = QtCore.QRegularExpression("[0-9]{10}")
        v3 = QRegularExpressionValidator(rx3, self.memberStudentNo)
        self.memberPhone.setValidator(v3)

        self.memberName.setMaxLength(25)
        self.memberSurname.setMaxLength(20)
        self.memberAddress.setMaxLength(75)
        self.memberInfo.setMaxLength(75)

        d1 = QDate(1910, 1, 1)
        d2 = QDate.currentDate()
        self.memberBirth.setDateRange(d1, d2) # pencere açılışında varsayılan tarihi olarak bugünün tarihini göster

        self.memberBirth.setDate(d2)

        self.memberClass.addItem("SEÇİNİZ")
        self.memberClass.addItems([str(i) for i in range(1, 13)])
        self.memberClass.addItems(["ANA SINIFI", "OKUL ÖNCESİ", "ETÜT", "HAZIRLIK", "SÖZEL", "SAYISAL", "EŞİT AĞIRLIK"])
         
        self.memberBranch.addItem("SEÇİNİZ")
        self.memberBranch.addItems([i for i in ["A","B","C","Ç","D","E","F","G","Ğ","H","I","İ","J","K","L","M","N","O","Ö","P","R","S","Ş","T","U","Ü","V","Y","Z"]])
        self.memberBranch.addItems(["GECE", "GÜNDÜZ", "İKİNCİ ÖĞRETİM","GENEL", "TEMEL", "1", "2", "3", "4", "5"])
     

        self.memberGender.addItems(["CİNSİYET", "ERKEK", "KIZ"])
        self.memberCard.addItems(["KARTI YOK", "KARTI VAR"])


        self.memberName.setText(self.member.ad)
        self.memberSurname.setText(self.member.soyad)
        self.memberTc.setText("" if self.member.tc == None else str(self.member.tc))
        self.memberStudentNo.setText("" if self.member.ogrenci_no == None else str(self.member.ogrenci_no))
        self.memberGender.setCurrentText("CİNSİYET" if self.member.cinsiyet == None else self.member.cinsiyet)
        self.memberBirth.setDate(d2 if self.member.d_tarihi == None else QDate(int(self.member.d_tarihi.split("-")[0]), int(self.member.d_tarihi.split("-")[1]), int(self.member.d_tarihi.split("-")[2]))) # d2 bu günün tarihi
        self.memberStudentNo.setText("" if self.member.ogrenci_no == None else str(self.member.ogrenci_no))
        self.memberClass.setCurrentText("SEÇİNİZ" if self.member.sinif == None else str(self.member.sinif))
        self.memberBranch.setCurrentText("SEÇİNİZ" if self.member.sube == None else str(self.member.sube))
        self.memberPhone.setText(str(self.member.tel))
        self.memberAddress.setText(str(self.member.adres))
        self.memberCard.setCurrentIndex(int(self.member.kart_durumu))
        
        


        self.memberBranch.setEnabled(False) #acılısta sube secimi devre dışı . sınıf seçtikten sonra aktif olacak

        self.label0 = QLabel("ÜYE GÜNCELLEME")
        self.kaydetBut = QPushButton("      Güncelle      ")

        self.label1 = QLabel("Üye Adı *")
        self.label2 = QLabel("Üye Soyadı* ")

        self.label3 = QLabel("Kimlik Numarası *")
        self.label3s = QLabel("Açıklama")
        self.label4 = QLabel("Cinsiyeti")
        self.label5 = QLabel("Doğum Tarihi")

        self.label6 = QLabel("Telefon Numarası ")
        self.label7 = QLabel("Adresi **")

        self.label8 = QLabel("Öğrenci Numarası")
        self.label9 = QLabel("Sınıfı **")
        self.label10 = QLabel("Şubesi **")

        self.label11 = QLabel("Kart Durumu")
      
        self.label0.setObjectName("newMember")
        self.kaydetBut.setObjectName("kaydetBut")

        self.label1.setObjectName("newMemberEtiket")
        self.label2.setObjectName("newMemberEtiket")
        self.label3.setObjectName("newMemberEtiket")
        self.label3s.setObjectName("newMemberEtiket")
        self.label4.setObjectName("newMemberEtiket")
        self.label5.setObjectName("newMemberEtiket")
        self.label6.setObjectName("newMemberEtiket")
        self.label7.setObjectName("newMemberEtiket")
        self.label8.setObjectName("newMemberEtiket")
        self.label9.setObjectName("newMemberEtiket")
        self.label10.setObjectName("newMemberEtiket")
        self.label11.setObjectName("newMemberEtiket")


        

        self.label1.setFixedHeight(15)
        self.label2.setFixedHeight(15)
        self.label3.setFixedHeight(15)
        self.label3s.setFixedHeight(15)
        self.label4.setFixedHeight(15)
        self.label5.setFixedHeight(15)
        self.label6.setFixedHeight(15)
        self.label7.setFixedHeight(15)
        self.label8.setFixedHeight(15)
        self.label9.setFixedHeight(15)
        self.label10.setFixedHeight(15)
        self.label11.setFixedHeight(15)

        
        self.label0.setFixedSize(WINDOWS_WIDTH - 250, 35)

        
        self.memberName.setFixedHeight(40)
        self.memberSurname.setFixedHeight(40)
        self.memberTc.setFixedHeight(40)
        self.memberStudentNo.setFixedHeight(40)
        self.memberGender.setFixedHeight(40)
        self.memberBirth.setFixedHeight(40)
        self.memberClass.setFixedHeight(40)
        self.memberBranch.setFixedHeight(40)
        self.memberAddress.setFixedHeight(40)
        self.memberPhone.setFixedHeight(40)
        self.memberCard.setFixedHeight(40)
        self.memberInfo.setFixedHeight(40)




        self.memberName.setObjectName("memberData")
        self.memberSurname.setObjectName("memberData")
        self.memberTc.setObjectName("memberData")
        self.memberStudentNo.setObjectName("memberData")
        self.memberGender.setObjectName("comboBoxMember")
        # self.memberBirth.setObjectName("memberData")
        self.memberClass.setObjectName("comboBoxMember")
        self.memberBranch.setObjectName("comboBoxMember")
        self.memberAddress.setObjectName("memberData")
        self.memberPhone.setObjectName("memberData")
        self.memberCard.setObjectName("comboBoxMember")
        self.memberInfo.setObjectName("memberData")


        self.memberLayoutAdd.addWidget(self.closeBut,                 0, 9, 1, 1,    alignment=QtCore.Qt.AlignmentFlag.AlignRight)

        # Yeni kitap Ekle etiketi
        self.memberLayoutAdd.addWidget(self.label0,                   1, 0,  1, 10,   alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

        self.memberLayoutAdd.setVerticalSpacing(14)
        
        self.memberLayoutAdd.addWidget(self.label1,                   3, 0,  1, 5)
        self.memberLayoutAdd.addWidget(self.label2,                   3, 5,  1, 5)
        self.memberLayoutAdd.addWidget(self.memberName,               4, 0,  1, 5)
        self.memberLayoutAdd.addWidget(self.memberSurname,            4, 5,  1, 5)

        self.memberLayoutAdd.addWidget(self.label3,                   5, 0,  1, 4)
        self.memberLayoutAdd.addWidget(self.label4,                   5, 4,  1, 3)
        self.memberLayoutAdd.addWidget(self.label5,                   5, 7,  1, 3)
        self.memberLayoutAdd.addWidget(self.memberTc,                 6, 0,  1, 4)
        self.memberLayoutAdd.addWidget(self.memberGender,             6, 4,  1, 3)
        self.memberLayoutAdd.addWidget(self.memberBirth,              6, 7,  1, 3)


        self.memberLayoutAdd.addWidget(self.label8,                   7, 0,  1, 4)
        self.memberLayoutAdd.addWidget(self.label9,                   7, 4,  1, 3)
        self.memberLayoutAdd.addWidget(self.label10,                  7, 7,  1, 3)
        self.memberLayoutAdd.addWidget(self.memberStudentNo,          8, 0,  1, 4)
        self.memberLayoutAdd.addWidget(self.memberClass,              8, 4,  1, 3)
        self.memberLayoutAdd.addWidget(self.memberBranch,             8, 7,  1, 3)


        self.memberLayoutAdd.addWidget(self.label6,                   9, 0,  1, 4)
        self.memberLayoutAdd.addWidget(self.label7,                   9, 4,  1, 6)
        self.memberLayoutAdd.addWidget(self.memberPhone,             10, 0,  1, 4)
        self.memberLayoutAdd.addWidget(self.memberAddress,           10, 4,  1, 6)

        
        self.memberLayoutAdd.addWidget(self.label11,                 11, 0,  1, 3)
        self.memberLayoutAdd.addWidget(self.label3s,                 11, 3,  1, 7)
        self.memberLayoutAdd.addWidget(self.memberCard,              12, 0,  1, 3)
        self.memberLayoutAdd.addWidget(self.memberInfo,              12, 3,  1, 7)



        self.memberLayoutAdd.addWidget(self.kaydetBut,               14, 7,  1, 3)


        self.setStyleSheet(stil)


        ## butonlara efekt uygulama
        self.kaydetBut.setGraphicsEffect(BoxShadowEffect.colorTeal())
        self.closeBut.setGraphicsEffect(BoxShadowEffect.colorBlue())

        
        

## Sinyal slot
        self.closeBut.clicked.connect(self.closeFonk)
        self.memberName.textChanged.connect(lambda: self.memberName.setText(self.karakterBuyut(self.memberName.text())))
        self.memberSurname.textChanged.connect(lambda: self.memberSurname.setText(self.karakterBuyut(self.memberSurname.text())))
        self.memberAddress.textChanged.connect(lambda: self.memberAddress.setText(self.karakterBuyut(self.memberAddress.text())))
        self.kaydetBut.clicked.connect(self.uyeGuncelleFonk)
        self.memberClass.currentTextChanged.connect(self.subeSecimComboAktivasyonu)


## Başlangıç Fonksiyonları ve Orneklemeler
        self.database = DbQuery()
        

  

    def closeFonk(self):
        print("cık")
        self.close()

        
    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()
    

## sınıf seciçimi yapılırsa şube seçimi aktif olsun
    def subeSecimComboAktivasyonu(self):
        if self.memberClass.currentText() == "SEÇİNİZ":
            self.memberBranch.setCurrentText("SEÇİNİZ")
            self.memberBranch.setEnabled(False)
        else:
            self.memberBranch.setEnabled(True)
    


    def uyeGuncelleFonk(self):
        # doğum tarihi varsayılan olaak bugünün tarihi girilecek eğer değişiklik yapılmazsa boş geçilecek
        id_ = self.member.id # sistem tarafından otomatik verilir
        tc = None if self.memberTc.text().strip() == "" else self.memberTc.text().strip() # üye no boş ise NOne döndür
        ad = self.memberName.text().strip()
        soyad = self.memberSurname.text().strip()
        dTarihi =  None if self.memberBirth.date() == QDate.currentDate() else self.memberBirth.date().toPyDate() 
        cinsiyet = None if self.memberGender.currentText() == "CİNSİYET" else self.memberGender.currentText()
        ogrenci_no = None if self.memberStudentNo.text().strip() == "" else self.memberStudentNo.text().strip() # üye no boş ise NOne döndür
        sinif = None if self.memberClass.currentText() == "SEÇİNİZ" else self.memberClass.currentText()
        sube = None if self.memberBranch.currentText() == "SEÇİNİZ" else self.memberBranch.currentText()
        adres = self.memberAddress.text().strip()
        tel = self.memberPhone.text().strip()
        kart = False if self.memberCard.currentText() == "KARTI YOK" else True
        bilgi = self.memberInfo.text().strip()


        uye = MemberModel(id_, tc, ad, soyad, dTarihi, cinsiyet, ogrenci_no, sinif, sube, adres, tel,  kart,  bilgi, "eklenme tarihi otomatik verilir")
        self.sinif = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "ANA SINIFI", "OKUL ÖNCESİ", "ETÜT", "HAZIRLIK", "SÖZEL", "SAYISAL", "EŞİT AĞIRLIK"]
        self.sube = ["A","B","C","Ç","D","E","F","G","Ğ","H","I","İ","J","K","L","M","N","O","Ö","P","R","S","Ş","T","U","Ü","V","Y","Z", "GECE", "GÜNDÜZ", "İKİNCİ ÖĞRETİM","GENEL", "TEMEL", "SAYISAL", "SÖZEL", "EŞİT AĞIRLIK", "1", "2", "3", "4", "5", "6"]

        if len(ad) > 2 and len(soyad) > 1: # ad ve soyad boş bırakılamaz
            # print("adım 2", kimlik_no, len(kimlik_no), kimlik_no.isnumeric(), type(kimlik_no))
            if tc == None or (len(tc) == 11 and tc.isnumeric() == True):
                # print("doğum tarihi", d_tarihi, type(d_tarihi))
                # if (len(dTarihi) == 0) or (self.valitade_date(dTarihi) == True):
                # print(d_tarihi)
                # if cinsiyet in ["ERKEK", "KIZ", "", None]:
                # print("cinsiyet geçerli")
                if ogrenci_no == None  or (ogrenci_no.isnumeric() == True and ogrenci_no != "0"):
                    # print("öğrenci no Tamam", ogrenci_no, type(ogrenci_no))
                    if sinif == None or sinif in self.sinif:
                        # print("sınıf tamam")
                        if sube == None or sube in self.sube:
                            # print("sube tamam")
                            if  (sinif == None and sube == None) or (sinif in self.sinif and sube in self.sube) or (sinif in self.sinif and sube == None):

                                result = DbQuery.uyeGuncelle(uye=uye)
                                if result == 1:
                                    print("Başarılı")
                                    MessageBOX("BAŞARILI KAYIT", f" {uye.ad} {uye.soyad} adlı üye başarılı bir şeklilde güncellendi.", 1)
                                elif result == 2:
                                    print("Eş kayıt hatası")
                                    MessageBOX("EŞ KAYIT HATASI", " Girilen T.C. numarası veya öğrenci numarası sistemde başka bir üyeye kayıtlı.", 0)
                                elif result == 3:
                                    print("Veritabanı hatası")
                                    MessageBOX("KAYIT HATASI", " Üye güncellenemedi.", 0)
                                else:
                                    print("Bilinmeyen hata")
                                    MessageBOX("KAYIT HATASI", " Bilinmeyen bir hata oluştu.", 0)

                            else:
                                MessageBOX("UYARI", f"""  Sınıf bilgisi girilmeden şube oluşturulamaz.""", 0)

                        else:
                            MessageBOX("UYARI", f""" Şube yalnıca bunlardan biri olmalı :  {self.sube} </h6>  ya da boş bırakılmalı.""", 0)

                    else:
                        MessageBOX("UYARI", f""" Sınıf yalnıca bunlardan biri olmalı :  {self.sinif} </h6>  ya da boş bırakılmalı.""", 0)

                else:
                    MessageBOX("UYARI", f"""Öğrenci no yalnızca sayılardan oluşmalı ve 0'dan büyük olmalı ya da boş bırakılmalı """, 0)

            # else:
            #     MessageBOX("UYARI", f""" Cinsiyet 'ERKEK', 'KIZ' formatında olmalı ya da boş bırakılmalı. """, 0)

            # else:
            #     MessageBOX("UYARI", f""" Doğum tarihi formatı 'Yıl-ay-'gün' şeklinde olmalı ya da boş bırakılmalı.\n (Örnek: 1994-07-25, yıl 4 hane, ay ve gün ise iki hane)""", 0)

            else:
                MessageBOX("UYARI", f""" Kimlik No 11 hane olmalı ve yalnızca rakamdan oluşmalı. (Ya da boş bırakabilirsiniz)""", 0)

        else:
            MessageBOX("UYARI", f""" Ad en az 3 karakter, soyad ise  en az 2 karakter olmalı""", 0)


    def mousePressEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self._move()
            return super().mousePressEvent(event)

    def _move(self):
        window = self.window().windowHandle()
        window.startSystemMove()
# uyg = QApplication(sys.argv)
#     # QApplication.setStyle(QStyleFactory.create('Fusion'))
# QApplication.setHighDpiScaleFactorRoundingPolicy(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
# pen = NewBook()
# uyg.exec()