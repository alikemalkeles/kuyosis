import re
from datetime import datetime
from pathlib import Path

from PyQt6 import QtWidgets, QtCore
from PyQt6.QtCore import QDateTime
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, \
    QLabel, QFrame, QGridLayout, QTableWidget, QTabWidget, QFileDialog
from openpyxl import Workbook, load_workbook
from openpyxl.styles import Alignment, Font, numbers

from query.dbQuery import DbQuery
from customWidget.messageBox import MessageBOX, MessageBOXQuestion
from models.model import *
from customWidget.shadowEffect import BoxShadowEffect

# bu işlem yönetici ayrıcalığı gerektirir. bu işlevi ekle


with open("assets/stil/stil_multiBookMemberRegistration.qss") as file:
    stil = file.read()

class MultiBookMemberRegistrationWindow(QFrame):
    def __init__(self, users):
        super().__init__()

        ## sistemde aktif olan kullanıcı
        self.user = users

        self.setObjectName("mainFrame")
        self.setStyleSheet(stil)
        self.setContentsMargins(0,0,0,0)

        self.layout = QHBoxLayout(self)
        self.layout.setContentsMargins(0,0,0,0)

        self.tabWidget = QTabWidget()

        self.layout.addWidget(self.tabWidget)

        self.kitap_ekle = MultiBookRegisterWin(self.user)
        self.uye_ekle = MultiMemberRegisterWin(self.user)

        self.tabWidget.addTab(self.kitap_ekle, "     ÇOKLU KİTAP EKLE     ")
        self.tabWidget.addTab(self.uye_ekle, "     ÇOKLU ÜYE EKLE     ")

        self.kitap_ekle.setEnabled(self.user.coklu_aktarim_yetkisi)
        self.uye_ekle.setEnabled(self.user.coklu_aktarim_yetkisi)

    
      #  QTimer.singleShot(5000, lambda: self.initialFonk())


    def initialFonkMultiBookMemberWindow(self):
        self.kitap_ekle.initialFonk()
        self.uye_ekle.initialFonk()

        self.kitap_ekle.tableWidgetWidth()
        self.uye_ekle.tableWidgetWidth()

        
class MultiBookRegisterWin(QFrame):
    def __init__(self, users):
        super().__init__()

        self.user = users

        self.setObjectName("mainFrame")
        self.setStyleSheet(stil)
        self.setContentsMargins(15,0,15,8)

         #Ekran çözünürlüğü
        screen = self.screen()
        self.w, self.h = (screen.size().width(), screen.size().height())

        self.database = DbQuery()

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0,0,0,0)

        self.bilgiLabel1 = QLabel()
        self.bilgiLabel2 = QLabel()
        self.bilgiLabel3 = QLabel()

        self.bilgiLabel2.setWordWrap(True)
        self.bilgiLabel3.setWordWrap(True)

        yontem1 = """1. Yöntem: Şablon dosyasına kaydedilen kitap bilgilerini 'Dosya Seç ve Tabloya Aktar' butonu ile 
        seçiniz. Dosyanızı seçtikten sonra dosya içeriği otomatik olarak aşağıdaki tabloya aktarılacaktır. Tablo 
        üzerinde dosyanınızın içeriğini kontrol edip veya düzelterek içeriği veritabanına aktarınız."""
        yontem2 = """2. Yöntem: Kaydedeceğiniz kitapları aşağıdaki tabloya yazıp veritabanına aktarınız. <br></br> 
        <br> <font color = "red"> Not: Tek seferde en fazla 1000 kitap kaydedebilirsiniz. </font> </br>"""

        self.bilgiLabel1.setText("YÖNERGE")
        self.bilgiLabel2.setText(yontem1)
        self.bilgiLabel3.setText(yontem2)

        self.bilgiLabel1.setObjectName("bilgiLabelBaslik")
        self.bilgiLabel2.setObjectName("bilgiLabel")
        self.bilgiLabel3.setObjectName("bilgiLabel")


    # dosya yolu seçme
        self.frameDosya = QFrame()
        self.frameDosya.setObjectName("frameDosya")
        self.frameDosya.setContentsMargins(8,8,8,8)
        self.frameDosya.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frameDosya.setFixedHeight(140)

        self.frameDosyaLayout = QVBoxLayout(self.frameDosya)
        self.frameDosyaLayout.setContentsMargins(0,0,0,0)

        self.dosyaSecBut = QPushButton(" Dosya Seç ve Tabloya Aktar ")
        self.dosyaYolu = QLabel("Dosya yolu: Henüz dosya seçilmedi")

        self.dosyaYolu.setFixedHeight(55)
        self.dosyaSecBut.setFixedHeight(45)

        self.dosyaYolu.setWordWrap(True)

        self.dosyaSecBut.setObjectName("dosyaSecBut")
        self.dosyaYolu.setObjectName("dosyaYolu")

        self.frameDosyaLayout.addWidget(self.dosyaSecBut)
        self.frameDosyaLayout.addWidget(self.dosyaYolu)


    ## Butonlar
        self.frameBut = QFrame()
        self.frameBut.setObjectName("frameBut") 
        self.frameBut.setContentsMargins(4,25,4,2)
        self.frameBut.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frameBut.setFixedHeight(140)

        self.frameButLayout = QHBoxLayout(self.frameBut)
        self.frameButLayout.setContentsMargins(0,0,0,0)

        self.sablonDosyaOlusturBut = QPushButton("Şablon Excel\nDosyası Oluştur")
        self.icerikleriAktarBut = QPushButton("İçerikleri Veritabanına\nAktar")

        self.sablonDosyaOlusturBut.setFixedHeight(105)
        self.icerikleriAktarBut.setFixedHeight(105)

        self.sablonDosyaOlusturBut.setObjectName("sablonDosyaOlusturBut")
        self.icerikleriAktarBut.setObjectName("icerikleriAktarBut")

        self.frameButLayout.addWidget(self.sablonDosyaOlusturBut)
        self.frameButLayout.addWidget(self.icerikleriAktarBut)



        self.tableWidget = QTableWidget()
        self.tableWidget.setColumnCount(7)
        self.tableWidget.setRowCount(1000)
        self.tabloTitle = ["Kitap Adı*", "Yazar", "Yayınevi", "Tür", "Syf. Sayısı", "Raf Konumu", "Açıklama"]
        self.tableWidget.setHorizontalHeaderLabels(self.tabloTitle)
      #  self.tableWidget.horizontalHeader().setFixedHeight(32)
        self.tableWidget.horizontalScrollBar().setStyleSheet(stil)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.tableWidget.setGraphicsEffect(BoxShadowEffect.colorBlue())

        
        
        

        self.layout.addWidget(self.bilgiLabel1,                 0, 0, 1, 4)
        self.layout.addWidget(self.bilgiLabel2,                 1, 0, 1, 4)
        self.layout.addWidget(self.bilgiLabel3,                 2, 0, 1, 4)

        self.layout.addWidget(self.frameDosya,                  0, 4, 3, 3)
        self.layout.addWidget(self.frameBut,                    0, 7, 3, 4)
        self.layout.addWidget(self.tableWidget,                 3, 0, 15, 11)


    # sinyal slotlar
        self.tableWidget.itemChanged.connect(self.karakterBuyutme)
        self.sablonDosyaOlusturBut.clicked.connect(self.sablonDosyaOlusturFonk)
        self.icerikleriAktarBut.clicked.connect(self.tabloIcerikleriniDenetle)
        self.dosyaSecBut.clicked.connect(self.sablondanTabloyaAktar)


      #  self.initialFonk()
        

    def initialFonk(self):
        pass 

        # if self.user.statu == "Kullanıcı":
        #     self.frameDosya.setEnabled(False)
        #     self.frameBut.setEnabled(False)

        # elif self.user.statu == "Yönetici":
        #     self.frameDosya.setEnabled(True)
        #     self.frameBut.setEnabled(True)

    
    def tableWidgetWidth(self):
        table = self.w - self.tableWidget.verticalHeader().width() - 52 - 22 - 22 # ekran - tablo satır numrası genişliği - sol acılır menu - padding

        self.tableWidget.setColumnWidth(0, int((table / 33) * 10))
        self.tableWidget.setColumnWidth(1, int((table / 33) * 6))
        self.tableWidget.setColumnWidth(2, int((table / 33) * 5))
        self.tableWidget.setColumnWidth(3, int((table / 33) * 3))
        self.tableWidget.setColumnWidth(4, int((table / 33) * 2))
        self.tableWidget.setColumnWidth(5, int((table / 33) * 3))
        self.tableWidget.setColumnWidth(6, int((table / 33) * 4))



    # Şablon dosya oluştur
    def sablonDosyaOlusturFonk(self):
        date = QDateTime.currentDateTime()
        # print(str(date.toString('dd-MM-yyyy HH-mm-ss')))
        
            
        home_dir = str(Path.home())

        fnamePath = QFileDialog.getSaveFileName(self, home_dir, f"Toplu Kitap Ekleme Şablonu {str(date.toString('dd-MM-yyyy HH-mm-ss'))}", ".ods")
        print(fnamePath, home_dir, bool(fnamePath))

        if bool(fnamePath[0]) == True:
            work_book = Workbook()  
            work_sheet = work_book.active  
            work_sheet.title = "Kitaplar"
            work_sheet.sheet_properties.pageSetUpPr.fitToPage = True # sayfayı genişliğe sığdır
            work_sheet.page_setup.fitToHeight = False
            work_sheet.page_margins.left = 0.2
            work_sheet.page_margins.right = 0.1
            work_sheet.page_margins.top = 0.4
           
            work_sheet.column_dimensions['A'].width = 55# sütun genişlikleri
            work_sheet.column_dimensions['B'].width = 30
            work_sheet.column_dimensions['C'].width = 20
            work_sheet.column_dimensions['D'].width = 15
            work_sheet.column_dimensions['E'].width = 15
            work_sheet.column_dimensions['F'].width = 20
            work_sheet.column_dimensions['G'].width = 25
            work_sheet

            work_sheet.merge_cells('A1:G1')  # hücre birleştirme
            work_sheet.merge_cells('A2:G2')  # hücre birleştirme


            work_sheet.row_dimensions[1].height = 35
            work_sheet.row_dimensions[2].height = 130

            cell = work_sheet.cell(row=1, column=1)  
            cell.value = "TOPLU KİTAP KAYDI ŞABLONU" 
            cell.font = Font(bold=True, size=15) 
            cell.alignment = Alignment(horizontal='center', vertical='center') 


            aciklama = """ Bilgilendirme:
                        1- Kitap ID ve Karekod numarası otomatik verilecektir.
                        2- Kitap adı boş bırakılamaz.
                        3- Açıklama sütunu hariç tüm sütunlarda büyük harf kullanınız.
                        4- Raf konumu boş bırakıldığında kitabın raf konumu 'GENEL' olarak belirlenecektir.
                        5- Tek seferde en fazla 1000 kitap eklenebilir.
                        6- Şablona yeni sütunlar eklemeyiniz ya da şablondan sütun silmeyiniz. (örneğin tablo başlığı ve bilgilendirme alanını)
                        """
            cellAciklama = work_sheet.cell(row=2, column=1)  
            cellAciklama.value = aciklama 
            cellAciklama.font = Font(bold=False, size=12) 
            cellAciklama.alignment = Alignment(horizontal='left', vertical='center') 


            titleColumn = 1
            work_sheet.row_dimensions[3].height = 30
            for title_ in self.tabloTitle: # tablo başlığını yaz
                baslik = work_sheet.cell(row=3, column=titleColumn)  
                baslik.value = f"{title_}"  
                baslik.font = Font(bold=True)
                baslik.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)  
                titleColumn+=1

            
            work_book.save(f"{fnamePath[0]}.xlsx")
            MessageBOX("BİLGİLENDİRME", f"Şablon başarılı bir şekilde oluşturuldu.", 1)

        else:
            print("dosya yolu alınamadı")
            pass

    # tablo içeriklerini db ye kaydetme
    def tabloIcerikleriniDenetle(self):

        if self.user.coklu_aktarim_yetkisi:
            # işlem öncesi icerikleri aktar butonunu devre dışı bırak
            self.icerikleriAktarBut.setEnabled(False)

            kitapListesi: BookModel = []

            hataliSatir = 0

            karekod_no = DbQuery.otoKitapNo() # bir kere al sonra sırayla devam et

            for row in range(0, self.tableWidget.rowCount()):
            
                self.tableWidget.selectRow(row) # sırayla satır seç
                item = self.tableWidget.selectionModel().selectedIndexes()

                if any([bool(i.data()) for i in item]) == True: # bir tana bile true dönerse satırda veri vardır
                    # print(row + 1, "satırda veri var")

                    # Zorunlu satırları kontrol etme None veya boş gelirse veri None Döndür

                    ad = self.tableWidget.item(row, 0).text() if self.tableWidget.item(row, 0) is not None else ""

                    yazar  = None if (self.tableWidget.item(row, 1) == None
                                      or self.tableWidget.item(row, 1).text().strip()  == "") else self.tableWidget.item(row, 1).text().strip()

                    yayinevi =  None if (self.tableWidget.item(row, 2) == None
                                         or self.tableWidget.item(row, 2).text().strip()  == "") else self.tableWidget.item(row, 2).text().strip()

                    tur = None if (self.tableWidget.item(row, 3) == None
                                   or self.tableWidget.item(row, 3).text().strip()  == "") else self.tableWidget.item(row, 3).text().strip()

                    sayfa = None if (self.tableWidget.item(row, 4) == None
                                     or self.tableWidget.item(row, 4).text().strip()  == "") else self.tableWidget.item(row, 4).text().strip()

                    raf = "GENEL" if (self.tableWidget.item(row, 5) == None
                                   or self.tableWidget.item(row, 5).text().strip()  == "") else self.tableWidget.item(row, 5).text().strip()

                    aciklama = self.tableWidget.item(row, 6).text().strip()   if self.tableWidget.item(row, 6) is not None else ""

                    # sayfa konytrolü None veya "" boş karakerden oluşuyorsa None Döndür

                    print("Yazar veri tipi: ", type(yazar), yazar)


                    if len(ad) >=2:
                        # sayfa sayısı numeric olmalı değil ise doğrudan kayda gec
                        if sayfa is not None:
                            
                            # sayfanın numeric olup olmadığını denteleme
                            if sayfa.isnumeric() == True:

                                if int(sayfa) >0:
                                # karekod no al
                                # Bilgileri modele aktar
                                    kitap = BookModel(
                                        None, 
                                        karekod_no, 
                                        self.karakterBuyut(ad),
                                        self.karakterBuyut(yazar) if yazar is not None else None, # None gelirse Karakter büyütme işlemine tabi değil
                                        self.karakterBuyut(yayinevi) if yayinevi is not None else None,
                                        self.karakterBuyut(tur) if tur is not None else None,
                                        int(sayfa),
                                        aciklama,
                                        raf,
                                        "tarih otomatik verilecek"
                                    )
                                    kitapListesi.append(kitap)
                                # print(kitap.karekod_no, kitap.ad, kitap.yazar, kitap.yayınevi, kitap.tür, kitap.sayfa, kitap.raf, kitap.bilgi)
                                    karekod_no+=1 # karekod numrasını manuel olarak arttır

                                else:
                                # print("sayfa sayısı 0 dan büyük olmalı", row)
                                    MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Sayfa sayısı 0'dan büyük olmalı""", 0)
                                    hataliSatir+=1
                                    self.tableWidget.selectRow(row)
                                    break

                            else:
                                
                                MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Sayfa sayısı yalnızca rakamdan oluşmalı.""", 0) 
                                hataliSatir+=1
                                self.tableWidget.selectRow(row)
                                break


                        # sayfa NOne ise
                        else:
                            # karekod no al
                                # Bilgileri modele aktar
                            kitap = BookModel(
                                        None, 
                                        karekod_no, 
                                        self.karakterBuyut(ad),
                                        self.karakterBuyut(yazar) if yazar is not None else None, # None gelirse Karakter büyütme işlemine tabi değil
                                        self.karakterBuyut(yayinevi) if yayinevi is not None else None,
                                        self.karakterBuyut(tur) if tur is not None else None,
                                        sayfa, 
                                        aciklama,
                                        raf,
                                        "tarih otomatik verilecek"
                                    )
                            kitapListesi.append(kitap)
                                # print(kitap.karekod_no, kitap.ad, kitap.yazar, kitap.yayınevi, kitap.tür, kitap.sayfa, kitap.raf, kitap.bilgi)
                            karekod_no+=1 # karekod numrasını manuel olarak arttır

                    else:

                        MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Kitap adı en az 2 karakterden oluşmalı.""", 0) 
                        hataliSatir+=1
                        self.tableWidget.selectRow(row)
                        break


    # sa    tırlardan herhangi birinde hata var ise kaydetme işlemine devam etme
            if hataliSatir == 0:
                # Kitapları kontrol edip listeye aktardıktan sonra db ye kaydetme
                if len(kitapListesi) >0 :
                    msg = MessageBOXQuestion("ONAY", f" Tabloda yer alan  {len(kitapListesi)} tane kitap veritabanına aktarılacak.\n İşleme devam etmek istiyor musunuz?", 1)
                    msg.okBut.clicked.connect(lambda: self.icerikleriVeritabaninaKaydetFonk(kitapListesi))  
                    msg.iptalBut.clicked.connect(lambda: self.icerikleriAktarBut.setEnabled(True))


                else:
                    MessageBOX("UYARI", """ Tabloda aktarılacak veri bulunamadı.""", 0)
                    self.tableWidget.selectRow(0)
                    # işlem öncesi icerikleri aktar butonunu devre dışı bırak
                    self.icerikleriAktarBut.setEnabled(True)

            else:
                print("hatali satırlar var kontrol et ve yeniden dene")

                # işlem öncesi icerikleri aktar butonunu devre dışı bırak
                self.icerikleriAktarBut.setEnabled(True)

        else:
            MessageBOX("UYARI", """ Bu işlem yönetici ayrıcalığı gerektirir. Yönetici hesabıyla giriş yapınız.""", 0)



    # tablodan gelen düzenli içerikleri toplu olarak db kaydet
    def icerikleriVeritabaninaKaydetFonk(self, kitapListesi):
         # tablonun ilk satırına gitme
        item = self.tableWidget.model().index(0, 0)
        self.tableWidget.scrollTo(item)
        
        kaydedilenler: BookModel = []

        # işlem öncesi icerikleri aktar butonunu devre dışı bırak
        self.icerikleriAktarBut.setEnabled(False)

        for kitap in kitapListesi:
            result = DbQuery.yeniKitapEkle(kitap=kitap)
            
            if result == 1:
                kaydedilenler.append(kitap)
                # self.ornek = MessageBOX("KAYIT BAŞARILI", f" {kitap.ad} adlı kitap başarılı bir şeklilde eklendi.", 1)
            elif result == 2:
                self.ornek = MessageBOX("EŞ KAYIT HATASI", f" {kitap.karekod_no} bu karekod numarası başka bir kitaba kayıtlı olduğu için veritabanına aktarılamadı.", 0)
            elif result == 3:
                self.ornek = MessageBOX("KAYIT HATASI", f" {kitap.ad} adlı kitap bilinmeyen bir sebepten dolayı veritabanına aktarılamadı.", 0)

        
        if len(kaydedilenler) >0:
            self.ornek = MessageBOX("BAŞARILI KAYIT SAYISI", f" {len(kaydedilenler)} kitap başarılı bir şekilde veritabanına aktarıldı.", 1)   
            self.icerikleriAktarBut.setEnabled(True)
        else:
            self.icerikleriAktarBut.setEnabled(True)

        
       


    #şablon dosyadan içerikleri aktarma
    def sablondanTabloyaAktar(self):
       
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Excell (*.xlsx)")
        if fileName:
            print(fileName, _)

            self.dosyaYolu.setText(fileName)


            workbook = load_workbook(filename=fileName)
            # print("sayfa ismi", workbook.sheetnames)
            
            sheet = workbook[workbook.sheetnames[0]]

            row = 0 # satır numarası
            hataliSatirlar = 0 # eksik ya da fazla sutun döndüren satırlar
            icerikDondurenSatirlar = 0 # içerik döndüren satır 0 ise kullanıcıya herhangi bir içerik bulunamadı uyarısı göster

            for rows in sheet.iter_rows(min_row=4, max_row=1000, values_only=True):
                # print(f"{satır}. Satır: ", row, any([bool(i) for i in row]))
                if any([bool(i) for i in rows]): # eğer satır içeriğinde veri varsa döndür 
                    # print([i for i in rows])
                    # sutun sayısı kontrolü
                    icerikDondurenSatirlar+=1 

                    print("tür: ", type(rows[2]), rows[2])

                    if len(rows) == 7:
                        item0 = QtWidgets.QTableWidgetItem(str(rows[0]) if rows[0] is not None else "")
                        self.tableWidget.setItem(row, 0, item0)

                        item1 = QtWidgets.QTableWidgetItem(str(rows[1]) if rows[1] is not None else "")
                        self.tableWidget.setItem(row, 1, item1)

                        item2 = QtWidgets.QTableWidgetItem(str(rows[2]) if rows[2] is not None else "")
                        self.tableWidget.setItem(row, 2, item2)

                        item3 = QtWidgets.QTableWidgetItem(str(rows[3]) if rows[3] is not None else "")
                        self.tableWidget.setItem(row, 3, item3)

                        item4 = QtWidgets.QTableWidgetItem(str(rows[4]) if rows[4] is not None else "")
                        item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 4, item4)

                        item5 = QtWidgets.QTableWidgetItem(str(rows[5]) if rows[5] is not None else "")
                        self.tableWidget.setItem(row, 5, item5)

                        item6 = QtWidgets.QTableWidgetItem(str(rows[6]) if rows[6] is not None else "")
                        self.tableWidget.setItem(row, 6, item6)

                        row+=1



                    else:
                        hataliSatirlar+=1


                
            print("içerik donduren satır: ", icerikDondurenSatirlar)
            if icerikDondurenSatirlar == 0:
                MessageBOX("BİLGİLENDİRME", f" Excel listesinde aktarılack veri bulunamdı.", 0)

            elif hataliSatirlar > 0:
                MessageBOX("BİLGİLENDİRME", 
                           f" Tablonuzdaki sutun sayısı 7'den az ya da fazla. Şablon tabloda sütun silmeyiniz ya da yeni sütunlar eklemeyiniz", 0)




    

        
    # tabloya girilen yazıları büyütme
    def karakterBuyutme(self, row):
        row.setText(self.karakterBuyut(row.text()))


    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()


class MultiMemberRegisterWin(QFrame):
    def __init__(self, users):
        super().__init__()

        self.user = users

        self.setObjectName("mainFrame")
        self.setStyleSheet(stil)
        self.setContentsMargins(15,0,15,8)

         #Ekran çözünürlüğü
        screen = self.screen()
        self.w, self.h = (screen.size().width(), screen.size().height())

        self.database = DbQuery()

        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0,0,0,0)

        self.bilgiLabel1 = QLabel()
        self.bilgiLabel2 = QLabel()
        self.bilgiLabel3 = QLabel()

        self.bilgiLabel2.setWordWrap(True)
        self.bilgiLabel3.setWordWrap(True)

        yontem1 = """ 1. Yöntem: Şablon dosyasına kaydedilen kitap bilgilerini 'Dosya Seç ve Tabloya Aktar' butonu ile seçiniz. Dosyanızı seçtikten sonra dosya içeriği otomatik olarak aşağıdaki tabloya aktarılacaktır. Tablo üzerinde dosyanınızın içeriğini kontrol edip veya düzelterek içeriği veritabanına aktarınız. """
        yontem2 = """ 2. Yöntem: Kaydedeceğiniz kitapları aşağıdaki tabloya yazıp veritabanına aktarınız. <br></br> <br> <font color = "red"> Not: Tek seferde en fazla 1000 üye kaydedebilirsiniz. </font> </br> """

        self.sinif = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "ANA SINIFI", "OKUL ÖNCESİ", "ETÜT", "HAZIRLIK", "SÖZEL", "SAYISAL", "EŞİT AĞIRLIK"]
        self.sube = ["A","B","C","Ç","D","E","F","G","Ğ","H","I","İ","J","K","L","M","N","O","Ö","P","R","S","Ş","T","U","Ü","V","Y","Z", "GECE", "GÜNDÜZ", "İKİNCİ ÖĞRETİM","GENEL", "TEMEL", "SAYISAL", "SÖZEL", "EŞİT AĞIRLIK", "1", "2", "3", "4", "5", "6"]

        self.bilgiLabel1.setText("YÖNERGE")
        self.bilgiLabel2.setText(yontem1)
        self.bilgiLabel3.setText(yontem2)

        self.bilgiLabel1.setObjectName("bilgiLabelBaslik")
        self.bilgiLabel2.setObjectName("bilgiLabel")
        self.bilgiLabel3.setObjectName("bilgiLabel")


    # dosya yolu seçme
        self.frameDosya = QFrame()
        self.frameDosya.setObjectName("frameDosya")
        self.frameDosya.setContentsMargins(8,8,8,8)
        self.frameDosya.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frameDosya.setFixedHeight(140)

        self.frameDosyaLayout = QVBoxLayout(self.frameDosya)
        self.frameDosyaLayout.setContentsMargins(0,0,0,0)

        self.dosyaSecBut = QPushButton(" Dosya Seç ve Tabloya Aktar ")
        self.dosyaYolu = QLabel("Dosya yolu: Henüz dosya seçilmedi")

        self.dosyaYolu.setFixedHeight(55)
        self.dosyaSecBut.setFixedHeight(45)

        self.dosyaYolu.setWordWrap(True)

        self.dosyaSecBut.setObjectName("dosyaSecBut")
        self.dosyaYolu.setObjectName("dosyaYolu")

        self.frameDosyaLayout.addWidget(self.dosyaSecBut)
        self.frameDosyaLayout.addWidget(self.dosyaYolu)


    ## Butonlar
        self.frameBut = QFrame()
        self.frameBut.setObjectName("frameBut") 
        self.frameBut.setContentsMargins(4,25,4,2)
        self.frameBut.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frameBut.setFixedHeight(140)

        self.frameButLayout = QHBoxLayout(self.frameBut)
        self.frameButLayout.setContentsMargins(0,0,0,0)

        self.sablonDosyaOlusturBut = QPushButton("Şablon Excel\nDosyası Oluştur")
        self.icerikleriAktarBut = QPushButton("İçerikleri Veritabanına\nAktar")

        self.sablonDosyaOlusturBut.setFixedHeight(105)
        self.icerikleriAktarBut.setFixedHeight(105)

        self.sablonDosyaOlusturBut.setObjectName("sablonDosyaOlusturBut")
        self.icerikleriAktarBut.setObjectName("icerikleriAktarBut")

        self.frameButLayout.addWidget(self.sablonDosyaOlusturBut)
        self.frameButLayout.addWidget(self.icerikleriAktarBut)



        self.tableWidget = QTableWidget()
        self.tableWidget.setColumnCount(11)
        self.tableWidget.setRowCount(1000)
        self.tabloTitle = ["Üye Kimlik No", "Üye Adı*", "Soyadı*", "D. Tarihi", "Cinsiyeti", "Öğr. No", "Sınıfı", "Şubesi", "Adresi", "Tel. No", "Açıklama"]

        self.tableWidget.setHorizontalHeaderLabels(self.tabloTitle)
        self.tableWidget.horizontalHeader().setFixedHeight(32)
        self.tableWidget.horizontalScrollBar().setStyleSheet(stil)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.tableWidget.setGraphicsEffect(BoxShadowEffect.colorBlue())

        

        

        self.layout.addWidget(self.bilgiLabel1,                 0, 0, 1, 4)
        self.layout.addWidget(self.bilgiLabel2,                 1, 0, 1, 4)
        self.layout.addWidget(self.bilgiLabel3,                 2, 0, 1, 4)

        self.layout.addWidget(self.frameDosya,                  0, 4, 3, 3)
        self.layout.addWidget(self.frameBut,                    0, 7, 3, 4)
        self.layout.addWidget(self.tableWidget,                 3, 0, 15, 11)


    # sinyal slotlar
        self.tableWidget.itemChanged.connect(self.karakterBuyutme)
        self.sablonDosyaOlusturBut.clicked.connect(self.sablonDosyaOlusturFonk)
        self.icerikleriAktarBut.clicked.connect(self.tabloIcerikleriniDenetle)
        self.dosyaSecBut.clicked.connect(self.sablondanTabloyaAktar)


    #    self.initialFonk()


    def initialFonk(self):
        pass
    
        # if self.user.statu == "Kullanıcı":
        #     self.frameDosya.setEnabled(False)
        #     self.frameBut.setEnabled(False)

        # elif self.user.statu == "Yönetici":
        #     self.frameDosya.setEnabled(True)
        #     self.frameBut.setEnabled(True)


    def tableWidgetWidth(self):
        table = self.w - self.tableWidget.verticalHeader().width() - 52 - 22 - 25 # ekran - tablo satır numrası genişliği - sol acılır menu - padding

        self.tableWidget.setColumnWidth(0,  int((table / 33) * 2.2))
        self.tableWidget.setColumnWidth(1,  int((table / 33) * 5))
        self.tableWidget.setColumnWidth(2,  int((table / 33) * 4))
        self.tableWidget.setColumnWidth(3,  int((table / 33) * 2)) # d_tarihi
        self.tableWidget.setColumnWidth(4,  int((table / 33) * 2))
        self.tableWidget.setColumnWidth(5,  int((table / 33) * 2)) # öğrenci no
        self.tableWidget.setColumnWidth(6,  int((table / 33) * 2))
        self.tableWidget.setColumnWidth(7,  int((table / 33) * 2))
        self.tableWidget.setColumnWidth(8,  int((table / 33) * 5)) # adres
        self.tableWidget.setColumnWidth(9,  int((table / 33) * 2.5))
        self.tableWidget.setColumnWidth(10, int((table / 33) * 4.2))

    # Şablon dosya oluştur
    def sablonDosyaOlusturFonk(self):
        date = QDateTime.currentDateTime()
        # print(str(date.toString('dd-MM-yyyy HH-mm-ss')))
        
        
     
        home_dir = str(Path.home())

        fnamePath = QFileDialog.getSaveFileName(self, home_dir, f"Toplu Üye Ekleme Şablonu {str(date.toString('dd-MM-yyyy HH-mm-ss'))}", ".xlsx")
        print(fnamePath, home_dir, bool(fnamePath))

        if bool(fnamePath[0]) == True:
            work_book = Workbook()  
            work_sheet = work_book.active  
            work_sheet.title = "Üyeler"
            work_sheet.sheet_properties.pageSetUpPr.fitToPage = True # sayfayı genişliğe sığdır
            work_sheet.page_setup.fitToHeight = False
            work_sheet.page_margins.left = 0.2
            work_sheet.page_margins.right = 0.1
            work_sheet.page_margins.top = 0.4
           
            work_sheet.column_dimensions['A'].width = 15 # kimlik no
            work_sheet.column_dimensions['B'].width = 30 # ad
            work_sheet.column_dimensions['C'].width = 25 # soyad
            work_sheet.column_dimensions['D'].width = 10 # d tarihi
            work_sheet.column_dimensions['E'].width = 15 # cinsiyet
            work_sheet.column_dimensions['F'].width = 15 # öğrenci no
            work_sheet.column_dimensions['G'].width = 10 # sınıf
            work_sheet.column_dimensions['H'].width = 15 # şube
            work_sheet.column_dimensions['I'].width = 30 # adres
            work_sheet.column_dimensions['J'].width = 15 # tel
            work_sheet.column_dimensions['K'].width = 35 # açıklama

            work_sheet.column_dimensions['D']


            work_sheet.merge_cells('A1:K1')  # hücre birleştirme
            work_sheet.merge_cells('A2:K2')  # hücre birleştirme


            work_sheet.row_dimensions[1].height = 35
            work_sheet.row_dimensions[2].height = 150

            cell = work_sheet.cell(row=1, column=1)  
            cell.value = "TOPLU ÜYE KAYDI ŞABLONU" 
            cell.font = Font(bold=True, size=15) 
            cell.alignment = Alignment(horizontal='center', vertical='center') 


            aciklama = f""" Bilgilendirme:
                        1- Üye ID otomatik verilecektir.
                        2- Zorunlu alanlar(Yıldızlı) boş bırakılmamalı.
                        3- Açıklama sütunu hariç tüm alanlarda büyük harf kullanınız.
                        4- T.C. Kimlik No(11 hane), öğrenci no, sınıf ve telefon(10 hane) no yalnızca rakamdan oluşmalı.
                        5- Doğum Tarihi formatı "yıl-ay-gün" şeklinde olmalıdır. Örneğin: 1994-07-25
                        6- Cinsiyet: "ERKEK" ya da "KIZ" olarak belirtilmeli ya da boş bırakılmalı.
                        7- Seçebileceğiniz sınıf değerleri: {self.sinif}
                        8. Seçebileceğiniz şube değeleri: {self.sube}
                        """
            cellAciklama = work_sheet.cell(row=2, column=1)  
            cellAciklama.value = aciklama 
            cellAciklama.font = Font(bold=False, size=12) 
            cellAciklama.alignment = Alignment(horizontal='left', vertical='center') 


            titleColumn = 1
            work_sheet.row_dimensions[3].height = 30
            for title_ in self.tabloTitle: # tablo başlığını yaz
                baslik = work_sheet.cell(row=3, column=titleColumn)
                baslik.value = f"{title_}"  
                baslik.font = Font(bold=True)
                baslik.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True)
                if title_ == "D. Tarihi":
                    baslik.number_format = 'dd/mm/yy'
                titleColumn+=1


            
            work_book.save(f"{fnamePath[0]}.xlsx")
            MessageBOX("BİLGİLENDİRME", f"Şablon başarılı bir şekilde oluşturuldu.", 1)

        else:
            print("dosya yolu alınamadı")
            pass



    #şablon dosyadan içerikleri tabloya aktarma
    def sablondanTabloyaAktar(self):
       
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","Excell (*.xlsx)")
        if fileName:
            print(fileName, _)

            self.dosyaYolu.setText(fileName)

            workbook = load_workbook(filename=fileName)
            # print("sayfa ismi", workbook.sheetnames)
            
            sheet = workbook[workbook.sheetnames[0]]

            row = 0 # satır numarası
            hataliSatirlar = 0 # eksik ya da fazla sutun döndüren satırlar
            icerikDondurenSatirlar = 0 # içerik döndüren satır 0 ise kullanıcıya herhangi bir içerik bulunamadı uyarısı göster

            for rows in sheet.iter_rows(min_row=4, max_row=1000, values_only=True):
                # print(f"{satır}. Satır: ", row, any([bool(i) for i in row]))
                if any([bool(i) for i in rows]): # eğer satır içeriğinde veri varsa döndür 
                    # print([i for i in rows])
                    # sutun sayısı kontrolü
                    icerikDondurenSatirlar+=1 

                    # print("tür: ", type(rows[2]), rows[2])
                    ["Üye Kimlik No", "Üye Adı*", "Soyadı*", "D. Tarihi", "Cinsiyeti", "Öğr. No", "Sınıfı", "Şubesi", "Adresi", "Tel. No", "Açıklama"]
                    """
                    Null döndürebilecek olanlar:  Kimlik no, D_tarihi, cinsiyet, öğrenci_no, sınıf, şube
                    """
                    print("test :", rows[0], type(rows[0]))

                    if len(rows) == 11:
                        item0 = QtWidgets.QTableWidgetItem(rows[0] if rows[0] is not None else "") # kimlik no (Null döndürebilir)
                        item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 0, item0)

                        item1 = QtWidgets.QTableWidgetItem(rows[1] if rows[1] is not None else "")
                        self.tableWidget.setItem(row, 1, item1)

                        item2 = QtWidgets.QTableWidgetItem(rows[2] if rows[2] is not None else "")
                        self.tableWidget.setItem(row, 2, item2)

                    ## Tarihi denetle
                        d_tarihi = None
                        if type(rows[3]) in (datetime, datetime.date, datetime.time): d_tarihi = rows[3].strftime("%Y-%m-%d")
                        elif rows[3] == None: d_tarihi = ""
                        else: d_tarihi = f"{rows[3]}"

                        item3 = QtWidgets.QTableWidgetItem(d_tarihi) # D_tarihi (Null döndürebilir)
                        item3.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 3, item3)
                        print("Doğum tarihi: ", type(rows[3]), rows[3])

                        item4 = QtWidgets.QTableWidgetItem(f"{rows[4]}" if rows[4] is not None else "") # cinsiyet no (Null döndürebilir)
                        item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 4, item4)

                        item5 = QtWidgets.QTableWidgetItem(f"{rows[5]}" if rows[5] is not None else "") # Öğrenci no (Null döndürebilir)
                        item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 5, item5)

                        item6 = QtWidgets.QTableWidgetItem(f"{rows[6]}" if rows[6] is not None else "") # sınıf (Null döndürebilir)
                        item6.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 6, item6)

                        item7 = QtWidgets.QTableWidgetItem(f"{rows[7]}" if rows[7] is not None else "") # şube (Null döndürebilir)
                        self.tableWidget.setItem(row, 7, item7)

                        item8 = QtWidgets.QTableWidgetItem(str(rows[8]) if rows[8] is not None else "")
                        self.tableWidget.setItem(row, 8, item8)

                        item9 = QtWidgets.QTableWidgetItem(str(rows[9]) if rows[9] is not None else "")
                        item9.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                        self.tableWidget.setItem(row, 9, item9)

                        item10 = QtWidgets.QTableWidgetItem(str(rows[10]) if rows[10] is not None else "")
                        self.tableWidget.setItem(row, 10, item10)

                        row+=1

                    else:
                        hataliSatirlar+=1


            print("içerik donduren satır: ", icerikDondurenSatirlar)
            if icerikDondurenSatirlar == 0:
                MessageBOX("BİLGİLENDİRME", f" Excel listesinde aktarılack veri bulunamdı.", 0)

            elif hataliSatirlar > 0:
                MessageBOX("BİLGİLENDİRME", 
                           f" Tablonuzdaki sutun sayısı 11'den az ya da fazla. Şablon tabloda sütun silmeyiniz ya da yeni sütunlar eklemeyiniz", 0)



    # tablo içeriklerini db ye kaydetme
    def tabloIcerikleriniDenetle(self):

        if self.user.coklu_aktarim_yetkisi:
            # işlem öncesi icerikleri aktar butonunu devre dışı bırak
            self.icerikleriAktarBut.setEnabled(False)

            uyeListesi: MemberModel = []

            hataliSatir = 0

            for row in range(0, self.tableWidget.rowCount()):
            
                self.tableWidget.selectRow(row) # sırayla satır seç
                item = self.tableWidget.selectionModel().selectedIndexes()

                if any([bool(i.data()) for i in item]) == True: # bir tana bile true dönerse satırda veri vardır
                    # print(row + 1, "satırda veri var")

                    # Zorunlu satırları kontrol etme
                    ["Üye Kimlik No-", "Üye Adı*", "Soyadı*", "D. Tarihi-", "Cinsiyeti-", "Öğr. No-", "Sınıfı-", "Şubesi-", "Adresi", "Tel. No.su", "Açıklama"]

                    kimlik_no = "" if self.tableWidget.item(row, 0) == None else self.tableWidget.item(row, 0).text()

                    ad = "" if (self.tableWidget.item(row, 1) == None
                                  or self.tableWidget.item(row, 1).text().strip()  == "") else self.tableWidget.item(row, 1).text().strip()

                    soyad = "" if (self.tableWidget.item(row, 2) == None
                                      or self.tableWidget.item(row, 2).text().strip()  == "") else self.tableWidget.item(row, 2).text().strip()

                    d_tarihi = "" if (self.tableWidget.item(row, 3) == None
                                        or self.tableWidget.item(row, 3).text().strip()  == "") else self.tableWidget.item(row, 3).text().strip()

                    cinsiyet = "" if (self.tableWidget.item(row, 4) == None
                                        or self.tableWidget.item(row, 4).text().strip()  == "") else self.tableWidget.item(row, 4).text().strip()

                    ogrenci_no = "" if (self.tableWidget.item(row, 5) == None
                                          or self.tableWidget.item(row, 5).text().strip()  == "") else self.tableWidget.item(row, 5).text().strip()

                    sinif = "" if (self.tableWidget.item(row, 6) == None
                                     or self.tableWidget.item(row, 6).text().strip()  == "") else self.tableWidget.item(row, 6).text().strip()

                    sube = "" if (self.tableWidget.item(row, 7) == None
                                    or self.tableWidget.item(row, 7).text().strip()  == "") else self.tableWidget.item(row, 7).text().strip()

                    adres = "" if (self.tableWidget.item(row, 8) == None
                                     or self.tableWidget.item(row, 8).text().strip()  == "") else self.tableWidget.item(row, 8).text().strip()

                    tel = "" if (self.tableWidget.item(row, 9) == None
                                   or self.tableWidget.item(row, 9).text().strip()  == "") else self.tableWidget.item(row, 9).text().strip()

                    aciklama = "" if (self.tableWidget.item(row, 10) == None
                                        or self.tableWidget.item(row, 10).text().strip()  == "") else self.tableWidget.item(row, 10).text().strip()

                    if len(ad) and len(soyad) > 0: # ad ve soyad boş bırakılamaz
                        # print("adım 2", kimlik_no, len(kimlik_no), kimlik_no.isnumeric(), type(kimlik_no))
                        if (len(kimlik_no) == 0) or (len(kimlik_no) == 11 and kimlik_no.isnumeric() == True):
                            # print("doğum tarihi", d_tarihi, type(d_tarihi))
                            if (len(d_tarihi) == 0) or (self.valitade_date(d_tarihi) == True):
                                # print(d_tarihi)
                                if cinsiyet in ["ERKEK", "KIZ", "", None]:
                                    # print("cinsiyet geçerli")
                                    if ogrenci_no == ""  or (ogrenci_no.isnumeric() == True and ogrenci_no != "0"):
                                        # print("öğrenci no Tamam", ogrenci_no, type(ogrenci_no))
                                        if sinif == "" or sinif in self.sinif:
                                            # print("sınıf tamam")
                                            if sube == "" or sube in self.sube:
                                                # print("sube tamam")
                                                if  (sinif == "" and sube =="") or (sinif in self.sinif and sube in self.sube) or (sinif in self.sinif and sube == ""):
                                                    print("sinif şube kontrolü tamam")

                                                    """
                                                     Null döndürebilecek olanlar:  Kimlik no, D_tarihi, cinsiyet, öğrenci_no, sınıf, şube
                                                    """
                                                    uye = MemberModel(None, 
                                                                      None if kimlik_no == "" else kimlik_no.strip(),
                                                                      ad.strip(), 
                                                                      soyad.strip(), 
                                                                      None if d_tarihi == "" else d_tarihi.strip(), 
                                                                      None if cinsiyet == "" else cinsiyet.strip(),
                                                                      None if ogrenci_no == "" else ogrenci_no.strip(),
                                                                      None if sinif == "" else sinif.strip(),
                                                                      None if sube == "" else sube.strip(),
                                                                      adres, 
                                                                      tel, 
                                                                      False, # kart
                                                                      aciklama,  
                                                                      "otomatik verilecek" )
                                                    
                                                    uyeListesi.append(uye)

                                                else:
                                                    MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI: </font>  Sınıf bilgisi girilmeden şube oluşturulamaz.""", 0)
                                                    hataliSatir+=1
                                                    self.tableWidget.selectRow(row)
                                                    break

                                            else:
                                                MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI: Şube yalnıca bunlardan biri olmalı : </font> <h6 style="font-size:10px;" > {self.sube} </h6>  ya da boş bırakılmalı.""", 0)
                                                hataliSatir+=1
                                                self.tableWidget.selectRow(row)
                                                break
                                        else:
                                            MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI: Sınıf yalnıca bunlardan biri olmalı : </font> <h6 style="font-size:10px;" > {self.sinif} </h6>  ya da boş bırakılmalı.""", 0) 
                                            hataliSatir+=1
                                            self.tableWidget.selectRow(row)
                                            break
                                    else:
                                       MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Öğrenci no yalnızca sayılardan oluşmalı ve 0'dan büyük olmalı ya da boş bırakılmalı """, 0) 
                                       hataliSatir+=1
                                       self.tableWidget.selectRow(row)
                                       break
                                else:
                                    MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Cinsiyet 'ERKEK', 'KIZ' formatında olmalı ya da boş bırakılmalı. """, 0)
                                    hataliSatir+=1
                                    self.tableWidget.selectRow(row)
                                    break
                            else:
                                MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Doğum tarihi formatı 'Yıl-ay-'gün' şeklinde olmalı ya da boş bırakılmalı.\n (Örnek: 1994-07-25, yıl 4 hane, ay ve gün ise iki hane)""", 0)
                                hataliSatir+=1
                                self.tableWidget.selectRow(row)
                                break
                        else:
                            MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Kimlik No 11 hane olmalı ve yalnızca rakamdan oluşmalı.(Ya da boş bırakabilirsiniz.)""", 0)
                            hataliSatir+=1
                            self.tableWidget.selectRow(row)
                            break
                    else:
                        MessageBOX("UYARI", f"""<font color = "black" weight = "bold"> {row+1}. SATIR HATALI:  </font> Zorunlu alanlar(yıldızlı) boş bırakılmamalı.""", 0) 
                        hataliSatir+=1
                        self.tableWidget.selectRow(row)
                        break


            

    # satırlardan herhangi birinde hata var ise kaydetme işlemine devam etme
            if hataliSatir == 0:
                # Kitapları kontrol edip listeye aktardıktan sonra db ye kaydetme
                if len(uyeListesi) >0 :
                    msg = MessageBOXQuestion("ONAY", f" Tabloda yer alan  {len(uyeListesi)} tane uye veritabanına aktarılacak.\n İşleme devam etmek istiyor musunuz?", 1)
                    msg.okBut.clicked.connect(lambda: self.icerikleriVeritabaninaKaydetFonk(uyeListesi))  
                    msg.iptalBut.clicked.connect(lambda: self.icerikleriAktarBut.setEnabled(True))


                else:
                    MessageBOX("UYARI", """ Tabloda aktarılacak veri bulunamadı.""", 0)
                    self.tableWidget.selectRow(0)
                    # işlem öncesi icerikleri aktar butonunu devre dışı bırak
                    self.icerikleriAktarBut.setEnabled(True)

            else:
                print("hatali satırlar var kontrol et ve yeniden dene")

                # işlem öncesi icerikleri aktar butonunu devre dışı bırak
                self.icerikleriAktarBut.setEnabled(True)

        else:
            MessageBOX("UYARI", """ Bu işlem yönetici ayrıcalığı gerektirir. Yönetici hesabıyla giriş yapınız.""", 0)


        item = self.tableWidget.model().index(0, 0)
        self.tableWidget.scrollTo(item)



    # tablodan gelen düzenli içerikleri toplu olarak db kaydet
    def icerikleriVeritabaninaKaydetFonk(self, uyeListesi):
         # tablonun ilk satırına gitme
        item = self.tableWidget.model().index(0, 0)
        self.tableWidget.scrollTo(item)
        
        kaydedilenler: MemberModel = []

        # işlem öncesi icerikleri aktar butonunu devre dışı bırak
        self.icerikleriAktarBut.setEnabled(False)

        for uye in uyeListesi:
            result = DbQuery.yeniUyeEkle(uye)
            
            if result == 1:
                kaydedilenler.append(uye)
                # self.ornek = MessageBOX("KAYIT BAŞARILI", f" {kitap.ad} adlı kitap başarılı bir şeklilde eklendi.", 1)
            elif result == 2:
                self.ornek = MessageBOX("EŞ KAYIT HATASI", f" {uye.tc} 'T.C. Kimlik' numarası ya da {uye.ogrenci_no} 'Öğrenci No'  başka bir üyemize kayıtlı. (T.C. No ve öğrenci No benzersiz olmalı)", 0)
            elif result == 3:
                self.ornek = MessageBOX("KAYIT HATASI", f" {uye.ad} {uye.soyad} adlı uye bilinmeyen bir sebepten dolayı veritabanına aktarılamadı.", 0)

        
        if len(kaydedilenler) >0:
            self.ornek = MessageBOX("BAŞARILI KAYIT SAYISI", f" {len(kaydedilenler)} uye başarılı bir şekilde veritabanına aktarıldı.", 1)   
            self.icerikleriAktarBut.setEnabled(True)
        else:
            self.icerikleriAktarBut.setEnabled(True)

        
    
        
    # tabloya girilen yazıları büyütme
    def karakterBuyutme(self, row):
        row.setText(self.karakterBuyut(row.text()))


    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()
    

    
    # girilen ifadenin bir tarh olup olmadığını kontrol etme
    def valitade_date(self, date_string):
        # önce format kontrolü yap
        if date_string is not None:
            pattern = "^\d{4}-\d{2}-\d{2}$"
            if re.match(pattern, date_string):
                date_format = "%Y-%m-%d"
                try:
                    date = datetime.strptime(date_string, date_format)
                    return True
                except:
                    print("girilen ifade tarih değil")
                    return False

            else:
                print( "Girilen ifadenin formatı hatalı")
                False

        else:
            False