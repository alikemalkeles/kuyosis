import os
import sqlite3
import time

from PyQt6.QtCore import QStandardPaths

from models.model import Users, BookModel, MemberModel, EnterpriseSettingModel, GivenTakenBookModel


class DbQuery():

    DATABASENAME = "kuyosis.db"
    userLocalPath = QStandardPaths.writableLocation(QStandardPaths.StandardLocation.GenericDataLocation) # os.path.dirname(os.path.abspath(__file__))
    appLocalPath = os.path.join(userLocalPath, "Kuyosis")
    dbLocalPath =  os.path.join(appLocalPath, DATABASENAME) # "sqlDB/database.db"


  #  print("Db Yol: ", dbLocalPath)
    # print(QStandardPaths.writableLocation(QStandardPaths.StandardLocation.AppDataLocation))
    # print(QStandardPaths.writableLocation(QStandardPaths.StandardLocation.AppLocalDataLocation))
    # print(QStandardPaths.writableLocation(QStandardPaths.StandardLocation.GenericDataLocation))
    # print(QStandardPaths.writableLocation(QStandardPaths.StandardLocation.DocumentsLocation)
    
    
    if os.path.exists(appLocalPath):
        database = sqlite3.connect(dbLocalPath, check_same_thread=False)
        imlec = database.cursor()

    else:
        os.makedirs(appLocalPath, exist_ok=True)
        database = sqlite3.connect(dbLocalPath, check_same_thread=False)
        imlec = database.cursor()
    

    @classmethod
    def createDatabaseTables(cls):
        #kullanıcı tablosu
        cls.imlec.execute("""CREATE TABLE IF NOT EXISTS Kullanicilar(
                        kullanici_id INTEGER NOT NULL PRIMARY KEY, 
                        isim TEXT NOT NULL, 
                        statu BOOL NOT NULL, 
                        parola TEXT NOT NULL,
                        eklenme_tarihi TEXT NOT NULL,
                        ekleme BOOL NOT NULL,
                        guncelleme BOOL NOT NULL,
                        silme BOOL NOT NULL,
                        ayar BOOL NOT NULL,
                        raporlama BOOL NOT NULL,
                        istatistik BOOL NOT NULL,
                        coklu_aktarim BOOL NOT NULL,
                        karekod_kimlik BOOL NOT NULL
                )""")

        # bilgi ve yapılandırmalar tablosu
        cls.imlec.execute("""CREATE TABLE IF NOT EXISTS BilgiVeYapilandirmalar(
                        id INTEGER NOT NULL PRIMARY KEY,
                        kurum_adi TEXT NOT NULL, 
                        kurum_adresi TEXT, 
                        yonetici TEXT NOT NULL, 
                        telefon TEXT,
                        alinabilecek_kitap_sayisi INTEGER,
                        kitap_emanet_suresi INTEGER
                )""")
        
        # Log kayıtları
        cls.imlec.execute("""CREATE TABLE IF NOT EXISTS LogKayitlari(
                        kullanici_id INTEGER NOT NULL,
                        kullanici_adi TEXT NOT NULL,
                        statu TEXT NOT NULL,
                        giris_tarihi TEXT NOT NULL
                )""")

        # kitaplar tablosu
        cls.imlec.execute("""CREATE TABLE IF NOT EXISTS Kitaplar(
                        id INTEGER NOT NULL PRIMARY KEY,
                        karekod_no INTEGER UNIQUE, 
                        kitap_adi TEXT NOT NULL,
                        yazar TEXT,
                        yayinevi TEXT,
                        tur TEXT,
                        sayfa INTEGER,
                        bilgi TEXT,
                        raf TEXT,
                        eklenme_tarihi TEXT NOT NULL
                        
                )""")

        # Uyeler tablosu
        cls.imlec.execute("""CREATE TABLE IF NOT EXISTS Uyeler(
                        id INTEGER NOT NULL PRIMARY KEY,
                        uye_tc INTEGER UNIQUE, 
                        uye_adi TEXT NOT NULL,
                        uye_soyadi TEXT NOT NULL,
                        d_tarihi TEXT,
                        cinsiyet TEXT,
                        ogrenci_no INTEGER UNIQUE, 
                        sinif INTEGER,
                        sube TEXT,
                        adres TEXT,
                        telefon TEXT,
                        kart_durumu TEXT,
                        bilgi TEXT,
                        eklenme_tarihi TEXT NOT NULL
                        
                )""")
        
        # Kitap tarnsfer tablosu
        cls.imlec.execute("""CREATE TABLE IF NOT EXISTS KitapTransfer(
                        rowID INTEGER NOT NULL PRIMARY KEY,
                        kitap_id INTEGER NOT NULl, 
                        uye_id INTEGER NOT NULL, 
                        cikis_tarihi TEXT,
                        donus_tarihi TEXT,
                        cikis_saati TEXT,
                        donus_saati TEXT,
                        durum BOOL NOT NULL
                )""")

        # Tetikleyiciler
        cls.imlec.execute(""" 
                            CREATE TRIGGER IF NOT EXISTS kitap_silme 
                                AFTER DELETE ON Kitaplar 
                                FOR EACH ROW BEGIN DELETE FROM KitapTransfer 
                                WHERE (kitap_id=OLD.id AND durum='1'); END
                           """)
        
        cls.imlec.execute("""
                                CREATE TRIGGER IF NOT EXISTS uye_silme 
                                    AFTER DELETE ON Uyeler 
                                    FOR EACH ROW BEGIN DELETE FROM KitapTransfer 
                                    WHERE (uye_id=OLD.id AND durum='1'); END
                            """)

        cls.database.commit()
        
    
## veritabanını kapat
    @classmethod
    def databaseClosed(cls):
        cls.database.close()





#################### KULLANICI SORGULARI   ############################################3

# Kullanıcı ve yönetici listesini alma
    @classmethod
    def kullaniciListesi(cls)->list:

        hesaplar = cls.imlec.execute("SELECT * FROM Kullanicilar")
        
        kullanicilar: Users = []

        for hesap in hesaplar:
            kullanicilar.append(
                Users(
                    hesap[0], hesap[1], hesap[2], hesap[3], hesap[4], hesap[5], hesap[6], hesap[7], 
                    hesap[8], hesap[9], hesap[10], hesap[11], hesap[12] )
                                )

        return kullanicilar



    
## siteme yeni kullanıcı ekleme
    @classmethod
    def yeniKullaniciEkle(cls, user: Users):

        print("db ye gelen: ", user.isim)
        
        try:
            cls.imlec.execute("""INSERT INTO Kullanicilar 
                    (kullanici_id, isim, statu, parola, eklenme_tarihi, ekleme, guncelleme, silme, ayar, raporlama, istatistik, coklu_aktarim, karekod_kimlik) 
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)""", 

                (user.id, user.isim, user.statu, user.parola, user.eklenme_tarihi, 
                user.ekleme_yetkisi, user.guncelleme_yetkisi, user.silme_yetkisi, user.ayar_yetkisi,
                user.raporlama_yetkisi, user.istatistik_yetkisi, user.coklu_aktarim_yetkisi, user.karekod_kimlik_yetkisi
                )
            )
            cls.database.commit()
            return 1
        
        except sqlite3.IntegrityError:
            return 2
        
        except:
            return 0


    ## siteme yeni kullanıcı ekleme
    @classmethod
    def kullaniciGuncelle(cls, user: Users):

        try:
            cls.imlec.execute("""UPDATE Kullanicilar SET 
                                    isim=?, ekleme=?, guncelleme=?, silme=?, ayar=?, 
                                    raporlama=?, istatistik=?, coklu_aktarim=?, 
                                    karekod_kimlik=? WHERE kullanici_id=?""", 
                    (user.isim, user.ekleme_yetkisi, user.guncelleme_yetkisi, user.silme_yetkisi, user.ayar_yetkisi,
                     user.raporlama_yetkisi, user.istatistik_yetkisi, user.coklu_aktarim_yetkisi, user.karekod_kimlik_yetkisi, int(user.id)))
            cls.database.commit()
            return 1
        
        except sqlite3.IntegrityError:
            return 2
        
        except:
            return 0
        

## kullanıcı sil
    @classmethod
    def kullaniciSil(cls, id_):
        try:
            cls.imlec.execute(f"""DELETE FROM Kullanicilar WHERE kullanici_id=?""", (id_,))
            cls.database.commit()
            return 1
        except:
            return 0
        


## kullanıcı parola güncelleme
    @classmethod
    def kullaniciParolaGuncelle(cls, parola, id_):
        try:
            cls.imlec.execute("""UPDATE Kullanicilar SET parola=?  WHERE kullanici_id=?""", (parola, int(id_)))
            cls.database.commit()
            return 1
        except:
            return 0
        

        
## kullanıcı adı güncelleme fonksiyonu
    @classmethod
    def kullaniciAdiGuncelle(cls, ad, id_):
        
        try:
            cls.imlec.execute("""UPDATE Kullanicilar SET isim=?  WHERE kullanici_id=?""", (ad, int(id_)))
            cls.database.commit()
            return 1
        except:
            return 0



# Yönetici hesabı devretme
    @classmethod
    def yoneticiHesabiniDevret(cls, id_, ad, parola, eski_yonetici_id):
        # statu sabit
        try:
            cls.imlec.execute("""UPDATE Kullanicilar SET kullanici_id=?,  ad=?, parola=?
                                WHERE kullanici_id=?""", 
                                (int(id_), ad, parola, eski_yonetici_id)
                                )
            
            cls.database.commit()
            return 1
        
        except sqlite3.IntegrityError:
            return 2
        
        except:
            return 0



## Kullanıcı id veya şifre denetleme ve sisteme giriş yapma
    @classmethod
    def girisKontrol(cls, id_, parola_):
        try:
           # cls.imlec.execute("""SELECT kullanici_id, parola FROM Kullanicilar WHERE kullanici_id=?""", (id_, parola))
            return cls.imlec.execute(f"SELECT * FROM Kullanicilar WHERE kullanici_id=? AND parola=?",(id_, parola_)).fetchone()
            
        except:
            None



## sisteme giriş yapanların log kayıtları
    @classmethod
    def girisLog(cls, id_, ad, statu, tarih_saat):
        try:
            cls.imlec.execute("INSERT INTO LogKayitlari (kullanici_id, kullanici_adi, statu, giris_tarihi) VALUES(?,?,?, ?)", (id_, ad, statu ,tarih_saat))
            cls.database.commit()
            return 1
        except:
            return 0
        
## Kullanıcı giriş kayıtlarını alma (log)
    @classmethod
    def logKayitlari(cls):
        try:
            return cls.imlec.execute(f"SELECT * FROM LogKayitlari ORDER BY rowid DESC LIMIT 5000").fetchall()

        except:
            [] #  hata olursa boş liste döndür

    ## Kullanıcı giriş kayıtlarını alma (yalnızca kullanıcıyı listeler)
    @classmethod
    def logKayitlariUser(cls, kullanici_id):
        try:
            return cls.imlec.execute(f"SELECT * FROM LogKayitlari WHERE kullanici_id=? ORDER BY rowid DESC  LIMIT 5000", (kullanici_id,)).fetchall()
            
        except:
            [] #  hata olursa boş liste döndür



################# KİTAP SORGULARI  ##############################################




## Kitap ksrekod  numarası otomatik kontrol
    @classmethod
    def otoKitapNo(cls):
        
        result = cls.imlec.execute("SELECT MAX(karekod_no) AS “MAX” FROM Kitaplar").fetchone()
        if result[0] == None:
            return 100001
        else:
            return result[0] + 1
        


## Yeni Kitap Ekle
    @classmethod
    def yeniKitapEkle(cls, kitap: BookModel):

        try:
            cls.imlec.execute("""INSERT INTO Kitaplar
                        (id, karekod_no, kitap_adi, yazar, yayinevi, tur, sayfa, bilgi, raf, eklenme_tarihi) 
                         VALUES(?,?,?,?,?,?,?,?,?, datetime('now', 'localtime'))""", 
                        (None, kitap.karekod_no, kitap.ad, kitap.yazar, kitap.yayınevi, kitap.tür, kitap.sayfa, kitap.bilgi, kitap.raf)
            ) 

            cls.database.commit()

            return 1
        
        except sqlite3.IntegrityError:
            return 2
        
        except sqlite3.OperationalError:
            return 3
        
        except:
            return 3
        
## Kitap Güncelle
    @classmethod
    def kitapGuncelle(cls, kitap: BookModel):

        print(kitap.ad)
        
        try:
            cls.imlec.execute("""UPDATE Kitaplar SET karekod_no=?, 
                                                      kitap_adi=?,  
                                                      yazar=?,  
                                                      yayinevi=?, 
                                                      tur=?,  
                                                      sayfa=?,  
                                                      bilgi=?,  
                                                      raf=? 
                                                      WHERE id=?""", 
        (kitap.karekod_no, kitap.ad,  kitap.yazar,  kitap.yayınevi,  kitap.tür,  kitap.sayfa,  kitap.bilgi,  kitap.raf,   int(kitap.id)))
            cls.database.commit()
            # print("güncellendi")

            return 1
        
        except sqlite3.IntegrityError:
            return 2
        
        except sqlite3.OperationalError:
            return 3
        
        except:
            return 3
       
## ComboBox secim (BU fonksiyon Ayarlar sayfasındaki kitap güncelleme ekranındanda da kullanılıyor)
    @classmethod
    def comboBox1Secim(cls, katogori):

        return  cls.imlec.execute(f"SELECT DISTINCT {katogori} FROM kitaplar").fetchall()
       # print("combo1 secim: ___", result.fetchall())

    @classmethod
    def comboBox2Secim(cls, katogori, deger):

        if deger == "":
            result = cls.imlec.execute(f"""SELECT * FROM kitaplar WHERE {katogori} IS NULL """)
            kitaplar = []

            for i in result.fetchall():
                # print(i)
                kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))

            return kitaplar  ## BooksMOdel olarak içerikleri döndür

        else:

            result = cls.imlec.execute(f"""SELECT * FROM kitaplar WHERE {katogori}=? """, (deger,))

            kitaplar = []

            for i in result.fetchall():
            # print(i)
                kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
        
            return kitaplar  ## BooksMOdel olarak içerikleri döndür


## tümünü listeleme
    @classmethod
    def tumKitaplar(cls):
        result = cls.imlec.execute(f"""SELECT * FROM Kitaplar ORDER BY kitap_adi ASC LIMIT 50000; """)
    
        kitaplar = []

        for i in result.fetchall():
            # print(i)
            kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
        
    
        return kitaplar  ## BooksMOdel olarak içerikleri döndür



## Numara veya kitap adına göre arama
    @classmethod
    def kitapNoAdAra(cls, deger, katogori):
        """ ["ID","Karekod No", "Kitap Adı", "Tüm Alanlar"] """
        if katogori == 0:
            result = cls.imlec.execute(f"""SELECT * FROM Kitaplar WHERE id=? ORDER BY kitap_adi ASC LIMIT 1000; """, (deger,))

            kitaplar = []

            for i in result.fetchall():
                kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
            return kitaplar 

        elif katogori == 1:
            result = cls.imlec.execute(f"""SELECT * FROM Kitaplar WHERE karekod_no=? ORDER BY kitap_adi ASC LIMIT 1000; """, (deger,))

            kitaplar = []

            for i in result.fetchall():
                kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
            return kitaplar 

        elif katogori == 2:
            result = cls.imlec.execute(f"""SELECT * FROM Kitaplar WHERE kitap_adi LIKE ? ORDER BY kitap_adi ASC LIMIT 1000; """, ('%'+deger+'%',))

            kitaplar = []

            for i in result.fetchall():
                kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
            return kitaplar 

        elif katogori == 3:
            result = cls.imlec.execute(f"""SELECT * FROM Kitaplar WHERE 
                (id LIKE ?) OR (karekod_no LIKE ?) OR (kitap_adi LIKE ?)  ORDER BY kitap_adi ASC LIMIT 1000; """, ('%'+deger+'%', '%'+deger+'%', '%'+deger+'%'))
            kitaplar = []

            for i in result.fetchall():
                kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
            return kitaplar 

    

## Emanetteki kitaplar Kitaplar emanettekileri listeleme

    @classmethod
    def emanettekiKitaplar(cls):
        result = cls.imlec.execute("""SELECT * FROM Kitaplar WHERE id IN (SELECT kitap_id FROM KitapTransfer WHERE durum='0');""")

        kitaplar = []

        for i in result.fetchall():
            # print(i)
            kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
        
        return kitaplar

    # Emanetteki kitapların ve mevcut okuyanların ID numrasını döndürürü. Kitap lıp verme ve kitaplar sayfasında
    @classmethod
    def emanettekiKitapUyeID(cls):

        try:
            result = cls.imlec.execute("""SELECT kitap_id, uye_id FROM KitapTransfer WHERE donus_tarihi IS NULL;""").fetchall()
            kitaplar = []
            uyeler = []

            for kitapId, uyeId in result:
                kitaplar.append(kitapId)
                uyeler.append(uyeId)

            return [kitaplar, uyeler]

        except Exception as hata:
            print("HATA::: EmanettekiKitapUyeID fonksiyonu.  ", hata)
            return None


    
## kitap silme
    @classmethod
    def kitapSil(cls, no):
        print("alınan no: ", no)

        try:
            cls.imlec.execute(f"""DELETE FROM Kitaplar WHERE id=? """, (no,))  
            cls.database.commit()
            return 1
        
        except:  return 2


    # Silme işlemi tablo bazında gerçekleşecektir.
    @classmethod
    def tumKitaplariSil(cls):
        # Emanetteki kitaplar ve okuyan üyeler
        try:
            cls.imlec.execute("DROP TABLE IF EXISTS KitapTransfer")
            cls.imlec.execute("DROP TABLE IF EXISTS Kitaplar")
            cls.database.commit()
            return True
        except Exception as hata:
            print("Kitap tablosu silme işleminde hata oluştu: ", hata)
            return False

           



############### ÜYE SORGULARI #######################333

## Yeni üye Ekle
    @classmethod
    def yeniUyeEkle(cls, uye: MemberModel):
        
        try:
            cls.imlec.execute("""INSERT INTO Uyeler
                        (id, uye_tc, uye_adi, uye_soyadi, d_tarihi, cinsiyet, ogrenci_no, sinif, sube, adres, telefon, kart_durumu, bilgi, eklenme_tarihi) 
                         VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?, datetime('now', 'localtime'))""", 
                        (None, uye.tc, uye.ad, uye.soyad, uye.d_tarihi, uye.cinsiyet, uye.ogrenci_no, uye.sinif, uye.sube, uye.adres, uye.tel, uye.kart_durumu, uye.bilgi)
            ) 

            cls.database.commit()

            return 1 # başrılı
        
        except sqlite3.IntegrityError:
            return 2 # eş kayıt
        
        except sqlite3.OperationalError:
            return 3   # hata
        
        except:
            return 3 # hata
        
# Üye güncelle
    @classmethod
    def uyeGuncelle(cls, uye: MemberModel):

        try:
            cls.imlec.execute("""UPDATE Uyeler SET uye_tc=?, 
                                                    uye_adi=?, 
                                                    uye_soyadi=?, 
                                                    d_tarihi=?, 
                                                    cinsiyet=?, 
                                                    ogrenci_no=?, 
                                                    sinif=?, 
                                                    sube=?, 
                                                    adres=?, 
                                                    telefon=?, 
                                                    kart_durumu=?, 
                                                    bilgi=? 
                                                    WHERE id=?""", 
                (uye.tc, uye.ad, uye.soyad, uye.d_tarihi, uye.cinsiyet, uye.ogrenci_no, uye.sinif, uye.sube, uye.adres, uye.tel, uye.kart_durumu, uye.bilgi, int(uye.id)))
            
            cls.database.commit()
            print("güncellendi")

            return 1
        
        except sqlite3.IntegrityError:
            return 2 # eş kayıt
        
        except sqlite3.OperationalError:
            return 3
        
        except:
            return 4
        

## tümünü listeleme
    @classmethod
    def tumUyeler(cls):
        result = cls.imlec.execute(f"""SELECT * FROM Uyeler ORDER BY uye_adi ASC""")
    
        uyeler = []

        for i in result.fetchall():
            # print("dogum ", i[3])
            uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
    
        return uyeler  ## BooksMOdel olarak içerikleri döndür

    @classmethod
    def tumUyeleriSil(cls):
        # Emanetteki kitaplar ve okuyan üyeler
        try:
            cls.imlec.execute("DROP TABLE IF EXISTS KitapTransfer")
            cls.imlec.execute("DROP TABLE IF EXISTS Uyeler")
            cls.database.commit()
            return True
        except Exception as hata:
            print("Uye tablosu silme işleminde hata oluştu: ", hata)
            return False
    


##  okuyan üyeler
    @classmethod
    def okuyanUyeler(cls):
        result = cls.imlec.execute("""
                    SELECT 
                        Uyeler.id,
                        Uyeler.uye_tc,
                        Uyeler.uye_adi,
                        Uyeler.uye_soyadi,
                        Uyeler.d_tarihi,
                        Uyeler.cinsiyet,
                        Uyeler.ogrenci_no,
                        Uyeler.sinif,
                        Uyeler.sube,
                        Uyeler.adres,
                        Uyeler.telefon,
                        Uyeler.kart_durumu,
                        Uyeler.bilgi,
                        Uyeler.eklenme_tarihi 
                    FROM 
                        KitapTransfer
                    INNER JOIN 
                        Uyeler ON Uyeler.id = KitapTransfer.uye_id 
                    WHERE 
                        donus_tarihi is NULL
                    ORDER BY 
                        uye_adi ASC;
                    """)

        uyeler = []
        for i in result.fetchall():
            # print(i)
            uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
        
        return uyeler 
    
# Üyenin okudukları memberWindows ta kullanılıyor
    @classmethod
    def uyeninOkuduklari(cls, uye_id):
        result = cls.imlec.execute(f"""
                                        SELECT * 
                                        FROM
                                            KitapTransfer 
                                        INNER JOIN 
                                            Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
                                        WHERE 
                                            uye_id=? AND donus_tarihi IS NULL ; """, (uye_id,))

        kitaplar = []

        for i in result.fetchall():
            # print(i)
            kitaplar.append(BookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9]))
        
        return kitaplar


    # Emanete kitap verirken üyenin okyup okumadığını kontrol etme için sayısal değere geri döndürür
    @classmethod
    def uyeKacTaneOkuyor(cls, uye_id):
        try:
            result = cls.imlec.execute(f"""
                                        SELECT durum
                                        FROM
                                            KitapTransfer 
                                        WHERE 
                                            uye_id=?; """, (uye_id,))

            return [i[0] for i in result.fetchall()].count(0)
        except:
            return 100





## Üyenin okuma geçmişi
    @classmethod
    def uyeninOkumaGecmisi(cls, uye_id):
        return cls.imlec.execute(f"""SELECT 
                                        Kitaplar.id, 
                                        Kitaplar.kitap_adi, 
                                        Kitaplar.yazar,
                                        Kitaplar.sayfa,
                                        KitapTransfer.cikis_tarihi,
                                        KitapTransfer.donus_tarihi,
                                        KitapTransfer.cikis_saati,
                                        KitapTransfer.donus_saati
                                    
                                    FROM KitapTransfer
                                    INNER JOIN Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
                                    WHERE 
                                        KitapTransfer.uye_id=?
                                    ORDER BY KitapTransfer.donus_tarihi ASC LIMIT 1000;
                            """, (uye_id,)).fetchall()


## üye silme
    @classmethod
    def uyeSil(cls, no):
        print("alınan no: ", no)

        try:
            cls.imlec.execute(f"""DELETE FROM Uyeler WHERE id={no} """) 
            cls.database.commit()
            return 1
        
        except:  return 2

## ComboBox secim
    @classmethod
    def comboBox1SecimUye(cls, katogori):
        # katogori sınıff şube bilgisi ise ayrı sorgu döndür
        if katogori == "Sınıf/Şube":
            return cls.imlec.execute(f"SELECT DISTINCT sinif, sube FROM Uyeler").fetchall()

        else:
            return cls.imlec.execute(f"SELECT DISTINCT {katogori} FROM Uyeler") .fetchall()

    @classmethod
    def comboBox2SecimUye(cls, katagori, deger):
        # eğere katogori sınıf/sube ise sorguyu değiştir
        if katagori == "Sınıf/Şube":
            print("Deger: ", deger, type(deger))
        ## Deger NULL gelirse( None) (Null değerleri boş string olarak comboBoxta gösteriyorum)
            sinif, sube = deger.split("/")
            print("Sınıf sube: ", sinif, sube)
            # sube/sınıf veya sadece sube nin boş olduğu durumlarda 
            if bool(sinif) == False:
                
                result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE sinif IS NULL AND sube IS NULL """)

                uyeler = []

                for i in result.fetchall():
                # print("dogum ", i[3])
                    uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))

                return uyeler  ## BooksMOdel olarak içerikleri döndür
            
            elif bool(sinif) == True and bool(sube) == False:
                result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE sinif=? AND sube IS NULL """, (sinif,))

                uyeler = []

                for i in result.fetchall():
                # print("dogum ", i[3])
                    uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))


                return uyeler  ## BooksMOdel olarak içerikl

            else:
                sinif, sube = deger.split("/")
                print("Sınıf sube: Null", sinif, sube)
                result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE sinif=? AND sube=? """, (sinif, sube))
                uyeler = []

                for i in result.fetchall():
                # print("dogum ", i[3])
                    uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))


                return uyeler  ## BooksMOdel olarak içerikleri döndür

        elif katagori == "Kimlik durumu":
            print("Kimlik durumu sorgusu")
            data  = "0" if deger == "ÜYE KARTI YOK" else "1"
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE kart_durumu=? """, (data,))

            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
        
    
            return uyeler  ## BooksMOdel olarak içerikleri döndür

        else:
            ## Deger NULL gelirse( None) (Null değerleri boş string olarak comboBoxta gösteriyorum)
            
            if deger == "":
                result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE {katagori} IS NULL """)

                uyeler = []

                for i in result.fetchall():
                # print("dogum ", i[3])
                    uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))


                return uyeler  ## BooksMOdel olarak içerikleri döndür
                
            else:
                result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE {katagori}==? """, (deger,))

                uyeler = []

                for i in result.fetchall():
                # print("dogum ", i[3])
                    uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))


                return uyeler  ## BooksMOdel olarak içerikleri döndür
        

## Üye adı, soyadı, tc kimlik no, telefon, ID, öğrenci no
    @classmethod
    def uyeIdAdSoyadTcNoAra(cls, deger, limit, katagori):
        """ ["ID","Ad", "Soyad", "Öğrenci No", "T.C. No", "Tel No", "Tüm Alanlar"] """
        if katagori == 0:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE id=? LIMIT ? ; """, (deger, limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür
        
        elif katagori == 1:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE uye_adi LIKE ? LIMIT ? ; """, ('%'+deger+'%', limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür

        elif katagori == 2:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE uye_soyadi LIKE ? LIMIT ? ; """, ('%'+deger+'%', limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür

        elif katagori == 3:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE ogrenci_no=? LIMIT ? ; """, (deger,  limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür
        
        elif katagori == 4:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE uye_tc=? LIMIT ? ; """, (deger, limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür
        
        elif katagori == 5:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE telefon=? LIMIT ? ; """, (deger, limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür

        elif katagori == 6:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE 
                                                (id LIKE ?) OR 
                                                (uye_tc LIKE ?) OR 
                                                (uye_adi LIKE ?) OR 
                                                (uye_soyadi LIKE ?) OR 
                                                (ogrenci_no LIKE ?) OR
                                                (telefon LIKE ?)
                                                LIMIT ?;
                                                """, ('%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür

        
        # Tüm alanlar yalnızca tel no yok (Kitap verme sayfası üye aramada tel no araması aktif değil)
        elif katagori == 7:
            result = cls.imlec.execute(f"""SELECT * FROM Uyeler WHERE 
                                                (id LIKE ?) OR 
                                                (uye_tc LIKE ?) OR 
                                                (uye_adi LIKE ?) OR 
                                                (uye_soyadi LIKE ?) OR 
                                                (ogrenci_no LIKE ?) 
                                                LIMIT ?;
                                                """, ('%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', limit))
            uyeler = []

            for i in result.fetchall():
            # print("dogum ", i[3])
                uyeler.append(MemberModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], i[10], i[11], i[12], i[13]))
            return uyeler  ## BooksMOdel olarak içerikleri döndür


    
 
    



############ Kitap alıp verme sorguları ##########################33

    # Kitap ödünç verme sorgusu
    @classmethod
    def kitabiEmaneteVer(cls, rowID, kitap_id, uye_id):

        try:
            cls.imlec.execute("""INSERT INTO KitapTransfer
                        (rowID, kitap_id, uye_id, cikis_tarihi, donus_tarihi, cikis_saati, donus_saati, durum) 
                         VALUES(?, ?, ?,  date('now', 'localtime'), ?, time('now', 'localtime'), ?, ?)""",
                        (rowID, kitap_id, uye_id, None, None, False)
            )

            cls.database.commit()

            return 1 # başrılı

        except sqlite3.OperationalError:
            return 2 # eş kayıt

        except:
            return 3   # hata
        
    
    # Kitap teslim alma sorgusu
    @classmethod
    def kitabiTeslimAl(cls, rowID):
        try:
            cls.imlec.execute("""UPDATE KitapTransfer SET durum=?, donus_tarihi=date('now', 'localtime'), donus_saati=time('now', 'localtime') WHERE rowID=? """, 
                               (True, rowID)) 

            cls.database.commit()

            return 1
        
        except sqlite3.OperationalError:
            return 2 # veritabanı hatası
        
        except:
            return 3   # hata
        

    # KitapTransfer tablosundaki son kaydın rowID'sini alma _____ İPtal edildi
    # def kitapTransferMaxRowId():
    #     try:
    #         result = cls.imlec.execute("""
    #                             SELECT MAX(rowID) FROM KitapTransfer;
    #                            """).fetchone()
    #         #print("Kitap transfer:: ROW ", result)
    #
    #         if result[0] == None:
    #             return None
    #         else:
    #             return result[0] + 1
    #     except:
    #         return None
        

    # Günlük verilen kitap listesi (Kitap + üye birleştirilmiş tablo döndür)   +++++
    @classmethod
    def emaneteVerilenGunlukKitaplar(cls, teslim_suresi):
        
        result = cls.imlec.execute(f"""SELECT 
                                        KitapTransfer.rowID,
                                        Kitaplar.id, 
                                        Kitaplar.karekod_no, 
                                        Kitaplar.kitap_adi,
                                        Kitaplar.yazar,
                                    
                                        Uyeler.id, 
                                        Uyeler.uye_tc, 
                                        Uyeler.uye_adi,
                                        Uyeler.uye_soyadi,
                                        Uyeler.ogrenci_no,
                                    
                                        KitapTransfer.cikis_tarihi,
                                        KitapTransfer.donus_tarihi,
                                        date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'),
                                        KitapTransfer.cikis_saati,
                                        KitapTransfer.donus_saati
            
                                    FROM KitapTransfer
									INNER JOIN Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
									INNER JOIN Uyeler ON Uyeler.id = KitapTransfer.uye_id
                                    WHERE 
                                        
                                        KitapTransfer.cikis_tarihi == date('now','localtime') 

                                    ORDER BY KitapTransfer.cikis_saati DESC LIMIT 1000;
                            """)
        #(KitapTransfer.cikis_tarihi LIKE '%date('now', 'localtime')%')
       # KitapTransfer.cikis_tarihi=date('now', 'localtime')
        
        gunlukEmanetListesi: GivenTakenBookModel = []
        
        for i in result.fetchall():
            
            model = GivenTakenBookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], 
                                               emanete_verilme_tarihi=i[10], 
                                               teslim_tarihi="",
                                               teslim_suresi=teslim_suresi, 
                                               son_teslim_tarihi=i[12],
                                               verilme_saati=i[13],
                                               teslim_saati=i[14]
                                               )
            gunlukEmanetListesi.append(model)
           
        return gunlukEmanetListesi
    

    # Günlük olarak emanetten alınan kitap listesi (Kitap + üye birleştirilmiş tablo döndür)   +++++
    @classmethod
    def emanettenAlinanGunlukKitaplar(cls, teslim_suresi):
        
        result = cls.imlec.execute(f"""SELECT 
                                        KitapTransfer.rowID,
                                        Kitaplar.id, 
                                        Kitaplar.karekod_no, 
                                        Kitaplar.kitap_adi,
                                        Kitaplar.yazar,
                                    
                                        Uyeler.id, 
                                        Uyeler.uye_tc, 
                                        Uyeler.uye_adi,
                                        Uyeler.uye_soyadi,
                                        Uyeler.ogrenci_no,
                                    
                                        KitapTransfer.cikis_tarihi,
                                        KitapTransfer.donus_tarihi,
                                        date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'),
                                        KitapTransfer.cikis_saati,
                                        KitapTransfer.donus_saati
            
                                    FROM KitapTransfer
									INNER JOIN Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
									INNER JOIN Uyeler ON Uyeler.id = KitapTransfer.uye_id
                                    WHERE 
                                        KitapTransfer.donus_tarihi == date('now','localtime') 
                                    ORDER BY KitapTransfer.donus_saati DESC LIMIT 1000;
                                        """)
        #(KitapTransfer.cikis_tarihi LIKE '%date('now', 'localtime')%')
       # KitapTransfer.cikis_tarihi=date('now', 'localtime')
        
        gunlukEmanetListesi: GivenTakenBookModel = []
        
        
        for i in result.fetchall():
            
            model = GivenTakenBookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], 
                                               emanete_verilme_tarihi=i[10], 
                                               teslim_tarihi="",
                                               teslim_suresi=teslim_suresi, 
                                               son_teslim_tarihi=i[12],
                                               verilme_saati=i[13],
                                               teslim_saati=i[14]
                                               )
            gunlukEmanetListesi.append(model)
           
        return gunlukEmanetListesi
    

    # Emanete verilen kitaplar içinde arama yapma (Kitap + üye birleştirilmiş tablo döndür)   ++++
    @classmethod
    def emaneteVerilenKitaplardaAra(cls, deger, teslim_suresi, katagori):
        """ ["Kitap ID","Karekod No", "Kitap Adı", "Üye ID", "Üye Adı", "Üye Soyadı", "Öğrenci No", "T.C Kimlik No", "Tüm Alanlar"] """

        if katagori == 0:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Kitaplar.id=?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, (deger,))

        elif katagori == 1:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Kitaplar.karekod_no=?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, (deger,))


        elif katagori == 2:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Kitaplar.kitap_adi LIKE ?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, ('%'+deger+'%',))

        elif katagori == 3:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Uyeler.id=?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, (deger,))

        elif katagori == 4:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Uyeler.uye_adi LIKE ?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, ('%'+deger+'%',))

        elif katagori == 5:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Uyeler.uye_soyadi LIKE ?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, ('%'+deger+'%',))

        elif katagori == 6:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Uyeler.ogrenci_no=?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, (deger,))

        elif katagori == 7:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND  Uyeler.uye_tc=?
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """, (deger,))

        elif katagori == 8:
            result = cls.imlec.execute(f"""SELECT 
                KitapTransfer.rowID, Kitaplar.id, Kitaplar.karekod_no, Kitaplar.kitap_adi, Kitaplar.yazar,
                Uyeler.id, Uyeler.uye_tc, Uyeler.uye_adi, Uyeler.uye_soyadi, Uyeler.ogrenci_no,
                KitapTransfer.cikis_tarihi, KitapTransfer.donus_tarihi, date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'), KitapTransfer.cikis_saati, KitapTransfer.donus_saati
                FROM Kitaplar, Uyeler, KitapTransfer 
                WHERE KitapTransfer.kitap_id=Kitaplar.id AND KitapTransfer.uye_id=Uyeler.id AND KitapTransfer.durum='0' AND 
                (
                    (Kitaplar.id LIKE ?) OR (Kitaplar.karekod_no LIKE ?) OR (Kitaplar.kitap_adi LIKE ?) OR 
                    (Uyeler.id LIKE ?) OR (Uyeler.uye_adi LIKE ?) OR (Uyeler.uye_soyadi LIKE ?) OR (Uyeler.ogrenci_no LIKE ?) OR (Uyeler.uye_tc LIKE ?)

                ) 
                ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 1000;  """,
                    ('%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%', '%'+deger+'%',)
                )
        
        emaneteVerilenlerdeAra: GivenTakenBookModel = []
        for i in result.fetchall():
            model = GivenTakenBookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], 
                                               emanete_verilme_tarihi=i[10], 
                                               teslim_tarihi="",
                                               teslim_suresi=teslim_suresi, 
                                               son_teslim_tarihi=i[12],
                                               verilme_saati=i[13],
                                               teslim_saati=i[14]
                                               )
            emaneteVerilenlerdeAra.append(model)
        return emaneteVerilenlerdeAra
        

    # Emanete verillen tüm kitaplar (Kitap + üye birleştirilmiş tablo döndür)  ++++++
    @classmethod
    def emaneteVerilenTumKitaplar(cls, teslim_suresi):
        result = cls.imlec.execute(f"""SELECT 
                                        KitapTransfer.rowID,
                                        Kitaplar.id, 
                                        Kitaplar.karekod_no, 
                                        Kitaplar.kitap_adi,
                                        Kitaplar.yazar,
                                    
                                        Uyeler.id, 
                                        Uyeler.uye_tc, 
                                        Uyeler.uye_adi,
                                        Uyeler.uye_soyadi,
                                        Uyeler.ogrenci_no,
                                    
                                        KitapTransfer.cikis_tarihi,
                                        KitapTransfer.donus_tarihi,
                                        date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'),
                                        KitapTransfer.cikis_saati,
                                        KitapTransfer.donus_saati
                                    FROM KitapTransfer
                                    INNER JOIN Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
                                    INNER JOIN Uyeler ON Uyeler.id = KitapTransfer.uye_id
                                    WHERE 
                                        KitapTransfer.durum='0' 
                                    ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 5000;   
                                """)
        
        emaneteVerilenTumKitaplarListesi: GivenTakenBookModel = []
       
        for i in result.fetchall():
            
            model = GivenTakenBookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], 
                                               emanete_verilme_tarihi=i[10], 
                                               teslim_tarihi="",
                                               teslim_suresi=teslim_suresi, 
                                               son_teslim_tarihi=i[12],
                                               verilme_saati=i[13],
                                               teslim_saati=i[14]
                                               )
            emaneteVerilenTumKitaplarListesi.append(model)
           
        return emaneteVerilenTumKitaplarListesi
    


    # Emanete verillen tüm kitaplar (Kitap + üye birleştirilmiş tablo döndür)   ++++
    @classmethod
    def emanetSuresiGecmisKitaplar(cls, teslim_suresi):
        result = cls.imlec.execute(f"""SELECT 
                                        KitapTransfer.rowID,
                                        Kitaplar.id, 
                                        Kitaplar.karekod_no, 
                                        Kitaplar.kitap_adi,
                                        Kitaplar.yazar,
                                    
                                        Uyeler.id, 
                                        Uyeler.uye_tc, 
                                        Uyeler.uye_adi,
                                        Uyeler.uye_soyadi,
                                        Uyeler.ogrenci_no,
                                    
                                        KitapTransfer.cikis_tarihi,
                                        KitapTransfer.donus_tarihi,
                                        date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'),
                                        KitapTransfer.cikis_saati,
                                        KitapTransfer.donus_saati
            
                                    FROM KitapTransfer
                                    INNER JOIN Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
                                    INNER JOIN Uyeler ON Uyeler.id = KitapTransfer.uye_id
                                    WHERE 
                                        KitapTransfer.durum='0' AND KitapTransfer.cikis_tarihi < date('now','-{teslim_suresi} days') 
                                    ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 2000;
                                """)
        
        emanetSuresiGecmisKitaplarListesi: GivenTakenBookModel = []

        for i in result.fetchall():
            
            model = GivenTakenBookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], 
                                               emanete_verilme_tarihi=i[10], 
                                               teslim_tarihi="",
                                               teslim_suresi=teslim_suresi, 
                                               son_teslim_tarihi=i[12],
                                               verilme_saati=i[13],
                                               teslim_saati=i[14]
                                               )
            emanetSuresiGecmisKitaplarListesi.append(model)
           
        return emanetSuresiGecmisKitaplarListesi


    #alıp verilen kitapalrı tarihr göre fitreleme
    @classmethod
    def tumAlipVerilenKitaplariListele(cls, teslim_suresi, baslangic_tarihi, bitis_tarihi):

        result = cls.imlec.execute(f"""SELECT 
                                        KitapTransfer.rowID,
                                        
                                        Kitaplar.id, 
                                        Kitaplar.karekod_no, 
                                        Kitaplar.kitap_adi,
                                        Kitaplar.yazar,
                                    
                                        Uyeler.id, 
                                        Uyeler.uye_tc, 
                                        Uyeler.uye_adi,
                                        Uyeler.uye_soyadi,
                                        Uyeler.ogrenci_no,
                                    
                                        KitapTransfer.cikis_tarihi,
                                        KitapTransfer.donus_tarihi,
                                        date(KitapTransfer.cikis_tarihi, '+{teslim_suresi} days'),
                                        KitapTransfer.cikis_saati,
                                        KitapTransfer.donus_saati
            
                                    FROM KitapTransfer
                                    INNER JOIN Kitaplar ON Kitaplar.id = KitapTransfer.kitap_id
                                    INNER JOIN  Uyeler ON Uyeler.id = KitapTransfer.uye_id
                                    WHERE 
                                         (KitapTransfer.cikis_tarihi BETWEEN ? AND ?)  OR (KitapTransfer.donus_tarihi BETWEEN ? AND ?)

                                    ORDER BY KitapTransfer.cikis_tarihi DESC LIMIT 5000;
                                        """, (baslangic_tarihi, bitis_tarihi, baslangic_tarihi, bitis_tarihi))
        #(KitapTransfer.cikis_tarihi LIKE '%date('now', 'localtime')%')
       # KitapTransfer.cikis_tarihi=date('now', 'localtime')
        
        gunlukEmanetListesi: GivenTakenBookModel = []
        
        
        for i in result.fetchall():
            
            model = GivenTakenBookModel(i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8], i[9], 
                                               emanete_verilme_tarihi=i[10], 
                                               teslim_tarihi=i[11],
                                               teslim_suresi=teslim_suresi, 
                                               son_teslim_tarihi=i[12],
                                               verilme_saati=i[13],
                                               teslim_saati=i[14]
                                               )
            gunlukEmanetListesi.append(model)
           
        return gunlukEmanetListesi


    # Kitabı okunmamış olarak geri alma
    @classmethod
    def kitabiOkunmadanAl(cls, kitap_id):
        try:
            cls.imlec.execute("DELETE FROM KitapTransfer WHERE kitap_id=? AND durum=?", (str(kitap_id), False))

            cls.database.commit()

            return 1 # başrılı

        except:
            return 2


    @classmethod
    def kitapAlipVermeGecmisiniSil(cls):
        # Emanetteki kitaplar ve okuyan üyeler
        try:
            cls.imlec.execute("DROP TABLE IF EXISTS KitapTransfer")
            cls.database.commit()
            return True
        except Exception as hata:
            print("Kitap alışverişi silme işleminde hata oluştu: ", hata)
            return False




######### AYARLAR VE YAPILANDIRMALAR ###########333
    @classmethod
    def ayarlarVeYapilandirmalar(cls):
        result = cls.imlec.execute("SELECT * FROM BilgiVeYapilandirmalar").fetchone()

        ayarlar = EnterpriseSettingModel(None, "", "", "", "", "1", "15") #varsayılan değer

        # print("result",result, bool(result))

        if bool(result):
            ayarlar = EnterpriseSettingModel(result[0], result[1], result[2], result[3], result[4], result[5], result[6])
        
        return ayarlar


    @classmethod
    def kurumBilgileriniOlustur(cls, kurum_adi, kurum_adresi, kurum_yoneticisi, kurum_telefonu, alinabilecek_kitap_sayisi, emanet_suresi):
        # try:
            cls.imlec.execute("""INSERT INTO BilgiVeYapilandirmalar VALUES(?, ?, ?, ?, ?, ?, ?)""", 
                               (1, kurum_adi, kurum_adresi, kurum_yoneticisi, kurum_telefonu, alinabilecek_kitap_sayisi, emanet_suresi))
            cls.database.commit()
            return 1
        # except:
        #     return 2

    @classmethod
    def kurumBilgileriniGuncelle(cls, kurum_adi, kurum_adresi, kurum_yoneticisi, kurum_telefonu, alinabilecek_kitap_sayisi, emanet_suresi, rowid):

        cls.imlec.execute(""" UPDATE BilgiVeYapilandirmalar SET 
                                                                kurum_adi=?, 
                                                                kurum_adresi=?, 
                                                                yonetici=?, 
                                                                telefon=?, 
                                                                alinabilecek_kitap_sayisi=?, 
                                                                kitap_emanet_suresi=?
                                                              WHERE rowID=? """, (
            kurum_adi, kurum_adresi, kurum_yoneticisi, kurum_telefonu, alinabilecek_kitap_sayisi, emanet_suresi, rowid
                                                             ))
        cls.database.commit()


    
    # kayıtlı sınıfların listesini alma
    @classmethod
    def kayitliSiniflarinListesi(cls):
        result = cls.imlec.execute(""" 
                        SELECT DISTINCT sinif FROM Uyeler ORDER BY sinif;
        """).fetchall()

        return result


    # yazar adı güncelle
    @classmethod
    def yazarGuncelle(cls, eski_ad, yeni_ad):
    
        if eski_ad == "":
            try:
                cls.imlec.execute(f"""UPDATE Kitaplar SET yazar=?  WHERE yazar IS NULL """, (yeni_ad,))
                cls.database.commit()
                return 1 # başrılı
            except sqlite3.OperationalError:
                return 2 # eş kayıt
            except:
                return 3   # hata

        else:
            try:
                cls.imlec.execute(f"""UPDATE Kitaplar SET yazar=?  WHERE yazar=? """, (yeni_ad, eski_ad))
                cls.database.commit()
                return 1 # başrılı
            except sqlite3.OperationalError:
                return 2 # eş kayıt
            except:
                return 3   # hata



# yayınevi güncelle
    @classmethod
    def yayineviGuncelle(cls, eski_ad, yeni_ad):

        print("Eski ad yayınevi:", eski_ad)
        # Null olanları kontrol etme
        if eski_ad == "":

            try:
                cls.imlec.execute(f"""UPDATE Kitaplar SET yayinevi=?  WHERE yayinevi IS NULL """, (yeni_ad,))

                cls.database.commit()

                return 1 # başrılı

            except sqlite3.OperationalError:
                return 2 # eş kayıt

            except:
                return 3   # hata
        else:
            try:
                cls.imlec.execute(f"""UPDATE Kitaplar SET yayinevi=?  WHERE yayinevi=? """, (yeni_ad, eski_ad))
                cls.database.commit()
                return 1 # başrılı
            except sqlite3.OperationalError:
                return 2 # eş kayıt

            except:
                return 3   # hata

    
    # raf güncelle
    @classmethod
    def rafGuncelle(cls, eski_ad, yeni_ad):

        if eski_ad == "":
            try:
                cls.imlec.execute(f"""UPDATE Kitaplar SET raf=?  WHERE raf IS NULL """, (yeni_ad,))

                cls.database.commit()

                return 1 # başrılı

            except sqlite3.OperationalError:
                return 2 # eş kayıt

            except:
                return 3   # hata

        else:
            try:
                cls.imlec.execute(f"""UPDATE Kitaplar SET raf=?  WHERE raf=? """, (yeni_ad, eski_ad))

                cls.database.commit()

                return 1 # başrılı

            except sqlite3.OperationalError:
                return 2 # eş kayıt

            except:
                return 3   # hata
        


    # sınıf yükseltme döngü ile kullanılacak
    @classmethod
    def sinifYuksetltme(cls, eski_sinif, yeni_sinif):
        # cls.imlec.execute(f"""UPDATE Uyeler SET sinif=?  WHERE sinif=? """, (yeni_sinif, eski_sinif)) 

        # cls.database.commit()
        try:
            cls.imlec.execute(f"""UPDATE Uyeler SET sinif=?  WHERE sinif=? """, (yeni_sinif, eski_sinif)) 

            cls.database.commit()
            
            return 1 # başrılı
        
        except sqlite3.OperationalError:
            return 2 # eş kayıt
        
        except:
            return 3   # hata


    # sinif yükseltme işleminde mezuniyet fonksiyonu
    @classmethod
    def sinifSil(cls, sinif):
        try:
            cls.imlec.execute(f"""DELETE FROM Uyeler  WHERE sinif=? """, (sinif,)) 

            cls.database.commit()
            
            return 1 # başrılı
        
        except:
            return 2


########## İstatistik işlemler sorgusu ###################3

    # En cok okunan kitaplar listesi
    @classmethod
    def enCokOkunanKitaplarListesi(cls, tarih):
 
        return cls.imlec.execute("""
                                       SELECT
                                            Kitaplar.kitap_adi,
                                        	COUNT(*)
                                        FROM
                                        	KitapTransfer
                                        INNER JOIN Kitaplar ON
                                        	KitapTransfer.kitap_id = Kitaplar.id
                                        WHERE
                                            KitapTransfer.cikis_tarihi >= ?
                                        GROUP BY
                                        	KitapTransfer.kitap_id
                                        ORDER BY
                                        	COUNT(*) DESC LIMIT 50;
                                                                    """, (tarih,)).fetchall()
    
    


    # En cok okuyan üyeler listesi
    @classmethod
    def enCokOkuyanUyelerListesi(cls, tarih):
        return cls.imlec.execute("""
                                       SELECT
                                            Uyeler.uye_adi,
                                            Uyeler.uye_soyadi,
                                        	COUNT(*)
                                        FROM
                                        	KitapTransfer
                                        INNER JOIN Uyeler ON
                                        	KitapTransfer.uye_id = Uyeler.id
                                        WHERE
                                            KitapTransfer.cikis_tarihi >= ?
                                        GROUP BY
                                        	KitapTransfer.uye_id
                                        ORDER BY
                                        	COUNT(*) DESC LIMIT 50;
                                                                    """, (tarih,)).fetchall()
    

     # Cinsiyete göre okuma sayısı

    @classmethod
    def cinsiyeteGoreOkumaSayisi(cls, tarih):
        result = cls.imlec.execute(""" 
                                        SELECT
                                            Uyeler.cinsiyet,
                                        	COUNT(*)
                                        FROM
                                        	KitapTransfer
                                        INNER JOIN Uyeler ON
                                        	KitapTransfer.uye_id = Uyeler.id  and (Uyeler.cinsiyet IS NOT NULL)
                                        WHERE
                                            KitapTransfer.cikis_tarihi >= ?
                                        GROUP BY
                                        	Uyeler.cinsiyet;
                                        
                                         """, (tarih,)).fetchall()
        
        # print("result cinsiyet: ", result, len(result))
        if len(result) == 2:
            countList = {"ERKEK": 1, "KIZ": 1}
            for i in result:
                if "ERKEK" in i:
                    # print(f"{i} içinde var erkek {i[1]}")
                    countList["ERKEK"] = i[1]
                if "KIZ" in i:
                    # print(f"{i} içinde var kız {i[1]}")
                    countList["KIZ"] = i[1]

            # print(countList)
            return countList
        
        else:
            return {"ERKEK": 1, "KIZ": 1}

         
    


    # Aylara göre okuma sayısı
    @classmethod
    def aylaraGoreOkumaSayisi(cls, tarih):
        result = cls.imlec.execute(""" 
                                SELECT 
                                    cikis_tarihi 
                                FROM 
                                    KitapTransfer
                                WHERE
                                     KitapTransfer.cikis_tarihi >=  ?;
                                """, (tarih,)).fetchall()
        ayListesi = [i[0].split("-")[1] for i in result] # DB den gelen sonuçtan yalnızca ayları listeye yaz ve ay listesinde ara
        aylar = {}
        # print(ayListesi)

        # print(ayListesi.count("07"), ayListesi.count("06"), len(result))

        for isim, numara in [["ocak", "01"], ["şubat", "02"],["mart", "03"],["nisan", "04"],["mayıs", "05"],["haziran", "06"],["temmuz", "07"],["ağustos", "08"],["eylül", "09"],["ekim", "10"],["kasım", "11"],["aralık", "12"]]:
            aylar[isim] = ayListesi.count(numara)

        #print("Aylar: ",  aylar)
        return aylar
    


    # sınıf seviyesine göre okuma sayısı
    @classmethod
    def siniflaraGoreOkumaSayisi(cls, tarih):
         
        result = cls.imlec.execute(""" SELECT
                                            Uyeler.sinif,
                                        	COUNT(*)
                                        FROM
                                        	KitapTransfer
                                    
                                        INNER JOIN Uyeler ON
                                        	KitapTransfer.uye_id = Uyeler.id  and (Uyeler.sinif IS NOT NULL)
                                        WHERE
                                            KitapTransfer.cikis_tarihi >= ?
                                        GROUP BY
                                        	Uyeler.sinif
                                         """, (tarih,)).fetchall()
        # print("sınıf: ", result)
        siniflar = {1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0, 11:0, 12:0}

        for sinif in result:
            siniflar[sinif[0]] = sinif[1]
        # sinifListesi = [str(i[0]) for i in result] # DB den gelen sonuçtan yalnızca ayları listeye yaz ve ay listesinde ara
      
      #  print("siniflar: ", siniflar)
        return siniflar
    

    # uye sayısı
    @classmethod
    def uyeSayisi(cls):
        return cls.imlec.execute(""" SELECT COUNT(id)
                                    FROM 
                                        Uyeler
                        
                                    """).fetchone()[0]
        # print("result:..", result[0])
    

    # Kitap sayısı
    @classmethod
    def kitapSayisi(cls):
        return cls.imlec.execute(""" SELECT COUNT(id) FROM Kitaplar""", ).fetchone()[0]
        # print("result:..", result[0])

    
    #alıp verilen kitap sayısı
    @classmethod
    def verilenKitapSayisi(cls, tarih):
        return cls.imlec.execute(""" SELECT COUNT(rowID) 
                                    FROM 
                                        KitapTransfer
                                    WHERE
                                        KitapTransfer.cikis_tarihi >= ?
                                    """, (tarih,)).fetchone()[0]

        

    ## Yedekleme oluştur
    @classmethod
    def yedeklemeOlustur(cls, yedekleme_konumu):
        try:
            yedekleme_path = os.path.join(yedekleme_konumu, "kuyosis_yedek.db")
            yedek_db = sqlite3.connect(yedekleme_path)

            with yedek_db:
                cls.database.backup(yedek_db)

            yedek_db.close()

            return [True, "Mesaj iletilmedi"]

        except Exception as e:

            return [False, e]


    # Yedeği geri yükle
    @classmethod
    def yedegiGeriYukle(cls, yedek_dosya_konumu, yedek_dosya):
        try:
            ## Yedeklemeyi yüklemeden önce yedek dosyanın içeriğinde tablo olup olmadığını kontrol et
            yedek_dosya_yolu = os.path.join(yedek_dosya_konumu, yedek_dosya)
            yedek_db = sqlite3.connect(yedek_dosya_yolu)

            sorgu = cls.imlec.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name; ")

            #print("içinde var mı", db_tablolar, yedekteki_tablolar, db_tablolar.issubset(yedekteki_tablolar))
            # Yedek dosya içinde tablolar var mı? (tabloloları karşılaştırma)

            if set(["Kullanicilar", "BilgiVeYapilandirmalar", "LogKayitlari", "Kitaplar", "Uyeler", "KitapTransfer"]).issubset(set([i[0] for i in sorgu.fetchall()])):
                print("yedekte bilgi var")
                
                with cls.database:
                    yedek_db.backup(cls.database)

                yedek_db.close()

                return [1, "Yedek veritabanı başarılı bir şekilde geri yüklendi."]

            else:

                return [0, "Yedek dosyada veri bulunamadı. Bu nedenle yedekleme geri yüklenemedi."]

        except Exception as e:
            
            return [0, "Bilinmeyen bir hata oluştu."]

    
