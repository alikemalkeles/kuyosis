from PyQt6.QtWidgets import QMainWindow, QStackedWidget

from userWindow.adminUserWindow import *
from bookWindow.booksWindow import *
from customWidget.calendarWindow import *
from query.dbQuery import *
from memberWindow.membersWindow import *
from multiRegistrationWindow.multiBookMemberRegistrationWindow import *
from settingWindows.settingWindow import *
from takenGivenWindow.takenGivenBookWindow import *
from usageReportsWindow.usageReportsWindow import *



with open("assets/stil/stil_mainWindow.qss", "r") as file:
    stil = file.read()

# DbQuery.createDatabaseTables()
# s = DbQuery.dbLocalPath
# print("database: ", s)


class MainWindow(QMainWindow):
    def __init__(self, user, userList, loginRecords):
        super().__init__()

        ## sistemde aktif oturumu olan kullanıcı
        self.user = user
        self.userList = userList  # userList   # KUllnıcılar listesi
        self.loginRecodrs = loginRecords

        self.setObjectName("mainWindow")
        self.setStyleSheet(stil)

        # Ekran çözünürlüğü
        screen = self.screen()
        self.w, self.h = (screen.size().width(), screen.size().height())

        ## Ekranın en fazla 30% oranında küçülmesini sağla
        self.w_percent, self.h_percent = (int(self.w / 100) * 80, int(self.h / 100) * 80)
        print("yuzde", self.w_percent, self.h_percent)

        self.setWindowTitle("KÜTÜPHANE YÖNETİM SİSTEMİ")

        # Yan menu acılma miktarı
        self.MENUSIZEOPEN = 250

        # palet = QtGui.QPalette()
        # print(palet.light().color().getRgb())

        self.MENUWIDTH = 52  # 350   ikon cercevesi 40x40 ikon ise 28x28

        ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        self.setContentsMargins(0, 0, 0, 0)

        self.setMinimumSize(self.w_percent, self.h_percent)

        # self.show()

        self.showMaximized()

        # self.showNormal()

        self.centralWid = QWidget()
        self.centralWid.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)
        self.centralWid.setContentsMargins(0, 0, 0, 0)
        #  self.centralWid.setObjectName("centralWid")

        self.setCentralWidget(self.centralWid)

        self.mainLayout = QHBoxLayout()
        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setSpacing(0)

        # self.setLayout(self.mainLayout)
        self.centralWid.setLayout(self.mainLayout)

        self.menuFrame = QFrame()
        self.windowLayout = QVBoxLayout()

        #  self.menuFrame.setFixedWidth(56)
        self.menuFrame.setObjectName("menuFrame")
        self.menuFrame.setFixedWidth(52)

        self.tabBarFrame = QFrame()
        self.stackWidget = QStackedWidget()
        self.windowLayout.setContentsMargins(0, 0, 0, 0)

        self.stackWidget.setContentsMargins(0, 0, 0, 0)
        self.tabBarFrame.setObjectName("tabBarFrame")
        self.tabBarFrame.setFixedHeight(35)

        self.windowLayout.addWidget(self.tabBarFrame)
        self.windowLayout.addWidget(self.stackWidget)

        self.mainLayout.addWidget(self.menuFrame)
        self.mainLayout.addLayout(self.windowLayout)

        ## MenuFrame ve ögeleri ve bu ögeye bağlı vertical layout yer alacak
        self.menuFrameLayout = QVBoxLayout(self.menuFrame)
        self.menuFrameLayout.setContentsMargins(0, 6, 8, 8)
        self.menuFrameLayout.setSpacing(0)

        self.menuBut1 = QPushButton()
        
        self.menuBut2 = QPushButton("KİTAP ALIP VERME")
        self.menuBut3 = QPushButton("ÜYELER")
        self.menuBut4 = QPushButton("KİTAPLAR")
        self.menuBut5 = QPushButton("AYARLAR")
        self.menuBut6 = QPushButton("İSTATİSTİK")
        self.menuBut7 = QPushButton("ÇOKLU KİTAP-ÜYE KAYDI")
        self.menuBut8 = QPushButton("HESAP")

        self.menuBut1.setObjectName("menuBut1")
        self.menuBut2.setObjectName("menuBut2")
        self.menuBut3.setObjectName("menuBut3")
        self.menuBut4.setObjectName("menuBut4")
        self.menuBut5.setObjectName("menuBut5")
        self.menuBut6.setObjectName("menuBut6")
        self.menuBut7.setObjectName("menuBut7")
        self.menuBut8.setObjectName("menuBut8")

        self.menuBut1.setFixedSize(40, 40)

        self.menuBut2.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut3.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut4.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut5.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut6.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut7.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut7.setFixedSize(self.MENUSIZEOPEN + 10, 48)
        self.menuBut8.setFixedSize(self.MENUSIZEOPEN + 10, 48)

        self.menuBut1.setContentsMargins(0, 0, 0, 0)
        self.menuBut2.setContentsMargins(0, 0, 0, 0)
        self.menuBut3.setContentsMargins(5, 0, 0, 0)
        self.menuBut4.setContentsMargins(5, 0, 0, 0)
        self.menuBut5.setContentsMargins(5, 0, 0, 0)
        self.menuBut6.setContentsMargins(5, 0, 0, 0)
        self.menuBut7.setContentsMargins(5, 0, 0, 0)
        self.menuBut7.setContentsMargins(5, 0, 0, 0)

        self.menuBut2.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)

        self.menuFrameLayout.addWidget(self.menuBut1, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.menuFrameLayout.addSpacing(50)
        self.menuFrameLayout.addWidget(self.menuBut2)
        self.menuFrameLayout.addSpacing(10)
        self.menuFrameLayout.addWidget(self.menuBut4)
        self.menuFrameLayout.addSpacing(10)
        self.menuFrameLayout.addWidget(self.menuBut3)

        self.menuFrameLayout.addSpacing(10)
        self.menuFrameLayout.addWidget(self.menuBut6)
        self.menuFrameLayout.addSpacing(10)
        self.menuFrameLayout.addWidget(self.menuBut7)
        self.menuFrameLayout.addSpacing(10)
        self.menuFrameLayout.addWidget(self.menuBut5)
        self.menuFrameLayout.addStretch()

        self.menuFrameLayout.addWidget(self.menuBut8)
        self.menuFrameLayout.addSpacing(15)

        ## tabBarLayout  ogeleri (pencere kapatma düğmeleri)
        self.tabBarLayout = QHBoxLayout(self.tabBarFrame)  # self.tabBarFrame
        self.tabBarLayout.setContentsMargins(0, 2, 5, 2)

        self.titleLogo = QLabel()
        self.titleWin = QLabel("")
        self.date = QPushButton("-")
        self.userAccount = QPushButton()
        self.closeBut = QPushButton()
        self.resizeBut = QPushButton()
        self.minimizedBut = QPushButton()

        self.titleLogo.setObjectName("titleLogo")
        self.titleWin.setObjectName("titleWin")
        self.date.setObjectName("date")
        self.userAccount.setObjectName("userAccount")
        self.closeBut.setObjectName("closeBut")
        self.resizeBut.setObjectName("resizeBut")
        self.minimizedBut.setObjectName("minimizedBut")
        

        self.titleLogo.setFixedSize(100, 35)
        self.titleWin.setFixedHeight(35)
        self.date.setFixedHeight(25)
        self.userAccount.setFixedHeight(32)
        self.closeBut.setFixedSize(28, 25)
        self.resizeBut.setFixedSize(28, 24)
        self.minimizedBut.setFixedSize(28, 24)

        self.tabBarLayout.addWidget(self.titleLogo)
        self.tabBarLayout.addStretch()
        # self.tabBarLayout.addWidget(self.titleWin)
        self.tabBarLayout.addStretch()
        self.tabBarLayout.addWidget(self.date)
        self.tabBarLayout.addSpacing(20)
        self.tabBarLayout.addWidget(self.userAccount)
        self.tabBarLayout.addSpacing(15)
        self.tabBarLayout.addWidget(self.minimizedBut)
        self.tabBarLayout.addWidget(self.resizeBut)
        self.tabBarLayout.addWidget(self.closeBut, alignment=QtCore.Qt.AlignmentFlag.AlignVCenter)

        ## Yan uygulamalar

        self.bookWindow = BooksWindow(self.user)

        self.memberWindow = MembersWindow(self.user)

        self.takenGivenWindow = TakenGivenWindow(user=self.user)

        self.usageReportsWindow = UsageReportsWindow(self.user)

        self.multiBookMemberRegistrationWindow = MultiBookMemberRegistrationWindow(self.user)

        self.settingWindow = SettingWindow(self.user)

        # user = Users(1, "ALİ KEMAL KELEŞ", True, "12345", "12.15.2023 - 15.10", False, True, False, False, False, False, False, True)
        self.adminUserWindow = AdminUserWindow(self.user, self.w, self.h, self.userList, self.loginRecodrs)

        self.stackWidget.addWidget(self.bookWindow)
        self.stackWidget.addWidget(self.memberWindow)
        self.stackWidget.addWidget(self.takenGivenWindow)
        self.stackWidget.addWidget(self.settingWindow)
        self.stackWidget.addWidget(self.usageReportsWindow)
        self.stackWidget.addWidget(self.multiBookMemberRegistrationWindow)
        self.stackWidget.addWidget(self.adminUserWindow)

        ## Sinyal Slotlar
        ""
        self.minimizedBut.clicked.connect(self.showMinimized)
        self.resizeBut.clicked.connect(self.windowResizeFonk)
        self.closeBut.clicked.connect(self.appClose)
        self.menuBut1.clicked.connect(self.menuOpenCloseFonk)
        #  self.userAccount.clicked.connect(self.userWindow)
        #  self.date.clicked.connect(self.calendarDialog)

        self.menuBut2.clicked.connect(self.menuBut2WindowOpenFonk)
        self.menuBut3.clicked.connect(self.menuBut3WindowOpenFonk)
        self.menuBut4.clicked.connect(self.menuBut4WindowOpenFonk)
        self.menuBut5.clicked.connect(self.menuBut5WindowOpenFonk)
        self.menuBut6.clicked.connect(self.menuBut6WindowOpenFonk)
        self.menuBut7.clicked.connect(self.menuBut7WindowOpenFonk)
        self.menuBut8.clicked.connect(self.menuBut8WindowOpenFonk)

        self.bookWindow.sinyal.connect(self.cokluEklemeWinFonk)
        self.memberWindow.sinyal.connect(self.cokluEklemeWinFonk)
        self.adminUserWindow.sinyal.connect(self.userNewData)


        self.initializedFonks()  # acılıs uygulanalarını başlatma

    # Tüm modüllerin başlangıc fonksiyonu
    def initializedFonks(self):

        self.initialFonkMainWindow()

        self.bookWindow.initialFonkBooksWindow()
        self.memberWindow.initialFonkMemberWindow()

        self.takenGivenWindow.initialFonkGivenWindow()
        self.takenGivenWindow.initialFonkTakenWindow()
        

        QTimer.singleShot(1000, lambda: self.takenGivenWindow.initialFonkTakenGivenWindow())  # Gecikme 1 sn
        QTimer.singleShot(3000, lambda: self.settingWindow.initialFonkSettingWindow())  # Gecikme 3.5 sn

        self.multiBookMemberRegistrationWindow.initialFonkMultiBookMemberWindow()

    ## Açılış Uygulamaları
    def initialFonkMainWindow(self):
        ## acılışta kitap fonk aktif
        self.menuBut2WindowOpenFonk()
        self.setDataUser()

    ## tarih bilgilerini yazdır
    def setDataUser(self):
        date = QDate.currentDate()
        # print(date.toString('dd.MM.yyyy'))
        self.date.setText(f"  {str(date.toString('dd.MM.yyyy'))}  ")

        self.userAccount.setText(f"""{"Yönetici" if self.user.statu == True else "Kullanıcı"}:  {self.user.isim}""")

    # Coklu Kitap ve coklu üye penceresi ekleme sinyali alma
    def cokluEklemeWinFonk(self, value):
        # sinyalden 0 alınırsa coklu kitap ekleme penceresinde 0 ıncı Taba git
        # print("sinyal alındı", value)
        self.menuBut7WindowOpenFonk()
        self.multiBookMemberRegistrationWindow.tabWidget.setCurrentIndex(value)

    ## Takvim penceresi 
    def calendarDialog(self):
        kullaniciPencersiKonumu = (
            self.userAccount.pos().x() + self.pos().x(), self.userAccount.pos().y() + self.pos().y())
        self.calendarWin = CalendarWindow(kullaniciPencersiKonumu)

    ## kullanıcı bilgileri değiştirilince değişen argumanların sinyalini yakalayıp kullaıcı butonuna yansıtma
    def userNewData(self, value):
        print("Yeni kullanıcı adı alındı___________: ", value)
        self.user = value
        self.userAccount.setText(
            f"""  {"Yönetici: " if self.user.statu == True else "Kullanıcı: "}:  {self.user.isim}""")

    @pyqtSlot()
    def menuOpenCloseFonk(self):

        ANIMATIONTIME = 200
        MENUSIZECLOSE = 52
        # Menuyu aç
        if self.MENUWIDTH == MENUSIZECLOSE:
            #     self.MENUWIDTH = 350
            # ANIMATION
            self.animation = QtCore.QPropertyAnimation(self.menuFrame, b"minimumWidth")
            self.animation.setDuration(ANIMATIONTIME)
            self.animation.setStartValue(MENUSIZECLOSE)
            self.animation.setEndValue(self.MENUSIZEOPEN)
            self.animation.start()

            self.menuBut1.setStyleSheet("""
                QPushButton#menuBut1{
                    background-image: url("./assets/icon/menu-close.png");
                    background-position: center;
                    background-repeat: no-repeat;
                    background-color: transparent;
                    outline: 0px;
                    color: black;
                    }""")

            #     QTimer.singleShot(405, lambda: self.menuBut2.setFixedWidth)        
            # print("animation")
            self.MENUWIDTH = self.MENUSIZEOPEN
        # menu kapat
        else:
            self.animation = QtCore.QPropertyAnimation(self.menuFrame, b"minimumWidth")
            self.animation.setDuration(ANIMATIONTIME)
            self.animation.setStartValue(self.MENUSIZEOPEN)
            self.animation.setEndValue(MENUSIZECLOSE)
            self.animation.start()

            self.MENUWIDTH = MENUSIZECLOSE

            self.menuBut1.setStyleSheet("""
                QPushButton#menuBut1{
                    background-image: url("./assets/icon/menu-open2.png");
                    background-position: center;
                    background-repeat: no-repeat;
                    background-color: transparent;
                    outline: 0px;
                    color: black;
                    }""")

    ## pencereyi yeniden boyutlandırma
    def windowResizeFonk(self):

        if self.isMaximized() == True:
            self.showNormal()
        else:
            self.showMaximized()
        # screen = self.screen()
        # print(screen.size().width())
        # self.setFixedSize(screen.size().width(), screen.size().height())
        # print(self.isMaximized())

        # if self.isMaximized() == True:
        #     self.showNormal()

        #     self.setFixedSize(int((screen.size().width() / 10 ) * 8), int((screen.size().height() / 10) * 8))

        #     desktop =  QtGui.QGuiApplication.primaryScreen().availableGeometry() 
        #     w, h = desktop.width(), desktop.height()
        #     self.move(w//2 - self.width()//2, h//2 - self.height()//2)
        #     print(self.width(), "normale döner")

        # else:
        #     print("Tam ekran değil")
        #     screen = self.screen()
        #     self.showMaximized()
        # self.setMaximumSize(screen.size().width(), screen.size().height())
        # self.setFixedSize(screen.size().width(), screen.size().height())

    # Kitap alıp verme
    @pyqtSlot()
    def menuBut2WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(2)
        self.menuBut2.setObjectName("menuBut2A")
        self.menuBut3.setObjectName("menuBut3P")
        self.menuBut4.setObjectName("menuBut4P")
        self.menuBut5.setObjectName("menuBut5P")
        self.menuBut6.setObjectName("menuBut6P")
        self.menuBut7.setObjectName("menuBut7P")
        self.menuBut8.setObjectName("menuBut8P")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)

    ## Üyeler
    @pyqtSlot()
    def menuBut3WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(1)
        self.menuBut2.setObjectName("menuBut2P")
        self.menuBut3.setObjectName("menuBut3A")
        self.menuBut4.setObjectName("menuBut4P")
        self.menuBut5.setObjectName("menuBut5P")
        self.menuBut6.setObjectName("menuBut6P")
        self.menuBut7.setObjectName("menuBut7P")
        self.menuBut8.setObjectName("menuBut8P")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)

    ## Kitaplar
    @pyqtSlot()
    def menuBut4WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(0)
        self.menuBut2.setObjectName("menuBut2P")
        self.menuBut3.setObjectName("menuBut3P")
        self.menuBut4.setObjectName("menuBut4A")
        self.menuBut5.setObjectName("menuBut5P")
        self.menuBut6.setObjectName("menuBut6P")
        self.menuBut7.setObjectName("menuBut7P")
        self.menuBut8.setObjectName("menuBut8P")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)

    # ayarlar sayfası
    @pyqtSlot()
    def menuBut5WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(3)
        self.menuBut2.setObjectName("menuBut2P")
        self.menuBut3.setObjectName("menuBut3P")
        self.menuBut4.setObjectName("menuBut4P")
        self.menuBut5.setObjectName("menuBut5A")
        self.menuBut6.setObjectName("menuBut6P")
        self.menuBut7.setObjectName("menuBut7P")
        self.menuBut8.setObjectName("menuBut8P")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)
    @pyqtSlot()
    def menuBut6WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(4)
        self.menuBut2.setObjectName("menuBut2P")
        self.menuBut3.setObjectName("menuBut3P")
        self.menuBut4.setObjectName("menuBut4P")
        self.menuBut5.setObjectName("menuBut5P")
        self.menuBut6.setObjectName("menuBut6A")
        self.menuBut7.setObjectName("menuBut7P")
        self.menuBut8.setObjectName("menuBut8P")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)

    # bu penere başka pencere üzerinden cağrılacaği için duruma göre tabBar sırasını arguman olarak alır
    @pyqtSlot()
    def menuBut7WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(5)

        self.menuBut2.setObjectName("menuBut2P")
        self.menuBut3.setObjectName("menuBut3P")
        self.menuBut4.setObjectName("menuBut4P")
        self.menuBut5.setObjectName("menuBut5P")
        self.menuBut6.setObjectName("menuBut6P")
        self.menuBut7.setObjectName("menuBut7A")
        self.menuBut8.setObjectName("menuBut8P")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)

    # bu penere başka pencere üzerinden cağrılacaği için duruma göre tabBar sırasını arguman olarak alır
    @pyqtSlot()
    def menuBut8WindowOpenFonk(self):
        self.stackWidget.setCurrentIndex(6)

        self.menuBut2.setObjectName("menuBut2P")
        self.menuBut3.setObjectName("menuBut3P")
        self.menuBut4.setObjectName("menuBut4P")
        self.menuBut5.setObjectName("menuBut5P")
        self.menuBut6.setObjectName("menuBut6P")
        self.menuBut7.setObjectName("menuBut7P")
        self.menuBut8.setObjectName("menuBut8A")
        for w in self.menuBut2, self.menuBut3, self.menuBut4, self.menuBut5, self.menuBut6, self.menuBut7, self.menuBut8:
            w.setStyleSheet(stil)

    def mousePressEvent(self, event):

        if event.button() == Qt.MouseButton.LeftButton:
            self._move()
            return super().mousePressEvent(event)

    def _move(self):
        s = self.date.geometry()
        # print("takvim konum", s, "pencere konumu: ", self.geometry())

        window = self.window().windowHandle()
        window.startSystemMove()

        # print(window)
    @pyqtSlot()
    def appClose(self):
        DbQuery.databaseClosed()
        self.close()

    # ## veritabanını çıkışta kapatma
    #     def closeEvent(self, event):
    #         DbQuery().databaseClosed()
    #         self.close()

    ## uygulamayı yeniden başlatma


        # print(status)

# if __name__ == "__main__":
#     print(sys.argv)

#     uyg = QApplication(sys.argv)
#     # uyg.setHighDpiScaleFactorRoundingPolicy(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.Round)
#     # uyg.setAttribute(QtCore.Qt.ApplicationAttribute.AA_UseStyleSheetPropagationInWidgetStyles)


#     pen = MainWindow()

#     uyg.exec()
