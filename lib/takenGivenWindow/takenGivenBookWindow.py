# -*- coding: utf-8 -*-
import sys
import time
from pathlib import Path

from PyQt6 import QtCore, QtWidgets, QtGui
from PyQt6.QtCore import Qt, QTimer, QDate, QDateTime, QThread, pyqtSignal, pyqtSlot
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, \
    QLabel, QFrame, QLineEdit, QGroupBox, QGridLayout, QTableWidget, \
    QSplitter, QTabWidget, QAbstractItemView, QFileDialog, QRadioButton, \
    QStackedWidget, QDateEdit, QComboBox
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font
from openpyxl.styles.borders import Border, Side, BORDER_THIN

from query.dbQuery import DbQuery
from customWidget.messageBox import MessageBOX, MessageBOXQuestion, MessageBoxGiven, MessageBoxTaken
from models.model import *
from settingWindows.settings import SettingsConf
from customWidget.shadowEffect import BoxShadowEffect

with open("./assets/stil/stil_takeGiven.qss") as file:
    stil = file.read()




## Kitap alıp verme
class TakenGivenWindow(QFrame):
    work_requested = pyqtSignal(int)

    def __init__(self, user: Users):
        super().__init__()

        self.user = user
        
        self.database = DbQuery()
        #emanet süresi
        self.borrowingTime = 0
        # üyenin en fazla kaç kitap alabileceğini tutan değişken (değer db den cekilecek ve ayarlar sayfasından değiştirilebilecek)
        self.takenMaximumBooks = 1

        #Ekran çözünürlüğü
        screen = self.screen()
        #  print(screen.size().width(), screen.size().height())
        self.w, self.h = (screen.size().width(), screen.size().height())

        # ComboBox ve lineedit widget yükseklikleri
        self.h1 = 40 if self.h > 900 else 36 # combobox ve lineedit
        self.h2 = 38 if self.h > 900 else 32 # BAzı butonlar alıp verme butonu
        self.h3 = 30 if self.h > 900 else 28 # BAzı butonlar
        self.h4 = 35 if self.h > 900 else 32 # BAzı butonlar



        self.setObjectName("mainFrame")
        self.setStyleSheet(stil)

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(8,4,8,8)

        self.setLayout(self.layout)

        self.tabWidget = QTabWidget(self)

    
        self.layout.addWidget(self.tabWidget)

        ## GivenBook
        self.givenWindowFrame = QFrame()
        self.takenWindowFRame = QFrame()

        self.givenWindowFrame.setObjectName("mainFrame")
        self.takenWindowFRame.setObjectName("mainFrame")


        self.tabWidget.addTab(self.givenWindowFrame,   "KİTAP VER")
        self.tabWidget.addTab(self.takenWindowFRame,   "KİTAP AL")



##############################################################  Kitap Verme (GivenWindow) ###################################################3
        # Kitap arama sonuclarını tutan değişken
        self.booksSearchListGiven:  BookModel = []
        # Üye sonuçalrını tutan değişken
        self.membersSearchListGiven: MemberModel = []
        # Günlük verilen kitaplar listesi
        self.dailyGivenBooksList: GivenTakenBookModel = [] # günlük verilen kitaplar listesi

        self.readerMemberIdListGiven: list = []  # okuyan üyelerin id si
        self.borrowedBooksIdListGiven: list = [] # emanetteki kitapların id sini tututor

        
        # Kitap verme sayfasında aranan kitap ve aranan üyeyi tutan değişkenler
        # Kitap teslim alındığı zaman kitap verme ekranında kitap ve üye arama listesinden kaldırmak içn kullanılacak
        self.searchIndexMemberGiven = -1
        self.searchIndexBookGiven = -1

        self.mainLayoutGiven = QVBoxLayout(self.givenWindowFrame)
        self.mainLayoutGiven.setContentsMargins(0,0,0,0)


        self.splitterGiven = QSplitter()
        self.splitterGiven.setOrientation(Qt.Orientation.Vertical)
        self.splitterGiven.setContentsMargins(0, 0, 0, 0)
        self.splitterGiven.setStretchFactor(4, 2)
       
  
        self.mainLayoutGiven.addWidget(self.splitterGiven)
        

      # Üye ve kitap arama yapılacak alan
        self.frameGiven = QFrame() # üst kısım
        self.givenBookTableDaily = QTableWidget()
        
        self.frameGiven.setContentsMargins(0, 0, 0, 0)
        self.givenBookTableDaily.setContentsMargins(0, 0, 0, 0)

        self.frameGiven.setObjectName("takenGiven")
      #  self.givenBookTableDaily.setMinimumHeight(int((self.h / 2) - 250))

        self.splitterGiven.addWidget(self.frameGiven)
        self.splitterGiven.addWidget(self.givenBookTableDaily)


        # Üst üye ve kitap arama cercevesi
        self.ustLayoutGiven = QGridLayout(self.frameGiven)
        self.ustLayoutGiven.setContentsMargins(0, 0, 0, 6)


        self.groupBoxBookGiven = QGroupBox()
        self.groupBoxMemberGiven = QGroupBox()

        self.groupBoxBookGiven.setContentsMargins(0, 0, 0, 0)
        self.groupBoxMemberGiven.setContentsMargins(0, 0, 0, 0)

       

        self.groupBoxBookGiven.setTitle("Kitap ara (ID, Karekod No, Ad)")
        self.groupBoxMemberGiven.setTitle("Üye ara (ID, Ad, Soyad, Kimlik No, Öğrenci No)")


        self.ustLayoutGiven.addWidget(self.groupBoxBookGiven,     0, 0, 5, 5)
        self.ustLayoutGiven.addWidget(self.groupBoxMemberGiven,   0, 5, 5, 5)

    ### groupBoxBook ögeleri
        self.groupBoxBookLayoutGiven = QGridLayout()
        self.groupBoxBookLayoutGiven.setContentsMargins(0, 18, 0, 8)
        self.groupBoxBookLayoutGiven.setSpacing(0)

        self.lineEditBooksGiven = QLineEdit()
        self.searchFilterComboBoxBook = QComboBox()
        self.booksSearchButGiven = QPushButton()
        self.tableBookGiven = QTableWidget()

        self.lineEditBooksGiven.setObjectName("lineEdit")
        self.booksSearchButGiven.setObjectName("searchBut")
        self.searchFilterComboBoxBook.setObjectName("searchFilter")

        self.searchFilterComboBoxBook.setFixedHeight(self.h1)
        self.lineEditBooksGiven.setFixedHeight(self.h1)
        self.booksSearchButGiven.setFixedHeight(self.h1)
        
        self.searchFilterComboBoxBook.addItems(["ID","Karekod No", "Kitap Adı", "Tüm Alanlar"])
        self.searchFilterComboBoxBook.setEditable(True)
        self.searchFilterComboBoxBook.lineEdit().setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.searchFilterComboBoxBook.lineEdit().setReadOnly(True)
        self.searchFilterComboBoxBook.setCurrentIndex(3)
        self.lineEditBooksGiven.setClearButtonEnabled(True)
        self.lineEditBooksGiven.setPlaceholderText("Kitap ID, karekod no, kitap adı adı ile arama yapabilirsiniz")
     #   self.lineEditBooksGiven.setGraphicsEffect(BoxShadowEffect.colorBlue())

        self.tabloBooksTitleGiven = ["ID", "Karekod No", "Kitap Adı"]
        
        self.tableBookGiven.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.tableBookGiven.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.SingleSelection) # tekli satır seçme
        self.tableBookGiven.setColumnCount(3)
        self.tableBookGiven.setRowCount(40)
      #  self.tableBookGiven.setHorizontalHeader()
      #  self.tableBookGiven.horizontalScrollBar().setStyleSheet(stil)
        self.tableBookGiven.setAlternatingRowColors(True)
        self.tableBookGiven.setHorizontalHeaderLabels(self.tabloBooksTitleGiven)
        self.tableBookGiven.setCornerButtonEnabled(False)

      #  self.tableBookGiven.setShowGrid(False)

        self.tableBookGiven.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma


    

        self.groupBoxBookLayoutGiven.addWidget(self.lineEditBooksGiven,        0, 2, 1, 5)
        self.groupBoxBookLayoutGiven.addWidget(self.searchFilterComboBoxBook,  0, 7, 1, 2)
        self.groupBoxBookLayoutGiven.addWidget(self.booksSearchButGiven,       0, 9, 1, 1)
        self.groupBoxBookLayoutGiven.setVerticalSpacing(6)
        self.groupBoxBookLayoutGiven.addWidget(self.tableBookGiven,            2, 0, 1, 11)

        self.groupBoxBookGiven.setLayout(self.groupBoxBookLayoutGiven)



    # groupBoxNember ögeleri
        self.groupBoxMemberLayoutGiven = QGridLayout()
        self.groupBoxMemberLayoutGiven.setContentsMargins(8, 18, 0, 8)
        self.groupBoxMemberLayoutGiven.setSpacing(0)

        self.lineEditMemberGiven = QLineEdit()
        self.searchFilterComboBoxMember = QComboBox()
        self.memberSearchButGiven = QPushButton()
        self.tableMemberGiven = QTableWidget()

        self.lineEditMemberGiven.setObjectName("lineEdit")
        self.memberSearchButGiven.setObjectName("searchBut")
        self.searchFilterComboBoxMember.setObjectName("searchFilter")

        self.lineEditMemberGiven.setFixedHeight(self.h1)
        self.memberSearchButGiven.setFixedSize(40, self.h1)
        self.searchFilterComboBoxMember.setFixedHeight(self.h1)

        self.lineEditMemberGiven.setClearButtonEnabled(True)
       # self.lineEditMemberGiven.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.searchFilterComboBoxMember.addItems(["ID", "Üye Adı", "Soyadı", "Öğr. No", "Kimlik No", "Tüm Alanlar"])
        self.searchFilterComboBoxMember.setEditable(True)
        self.searchFilterComboBoxMember.lineEdit().setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.searchFilterComboBoxMember.lineEdit().setReadOnly(True)
        self.searchFilterComboBoxMember.setCurrentIndex(5)
        self.lineEditMemberGiven.setPlaceholderText("ID, ad, soyad, öğrenci no, T.C. no, tel no ile arama yapabilirisiniz.")

        
        self.tabloMemberTitleGiven = ["ID", "Kimlik No", "Üye Adı", "Soyadı", "Öğr. No"]
        self.tableMemberGiven.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.tableMemberGiven.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.SingleSelection) # tekli satır seçme
        self.tableMemberGiven.setColumnCount(5)
        self.tableMemberGiven.setRowCount(40)
      #  self.tableMemberGiven.horizontalHeader().setFixedHeight(32)
        # self.tableMembGivener.horizontalScrollBar().setStyleSheet(stil)
        self.tableMemberGiven.setAlternatingRowColors(True)
        self.tableMemberGiven.setHorizontalHeaderLabels(self.tabloMemberTitleGiven)

        self.tableMemberGiven.setGraphicsEffect(BoxShadowEffect.colorBlue())

        self.tableMemberGiven.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma


        self.groupBoxMemberLayoutGiven.addWidget(self.lineEditMemberGiven,        0, 2, 1, 5)
        self.groupBoxMemberLayoutGiven.addWidget(self.searchFilterComboBoxMember, 0, 7, 1, 2)
        self.groupBoxMemberLayoutGiven.addWidget(self.memberSearchButGiven,       0, 9, 1, 1)
        self.groupBoxMemberLayoutGiven.setVerticalSpacing(6)
        self.groupBoxMemberLayoutGiven.addWidget(self.tableMemberGiven,           2, 0, 1, 11)

        self.groupBoxMemberGiven.setLayout(self.groupBoxMemberLayoutGiven)


    # Üst cerveye bağlı kitap ver ve kitap al 
        
        
        self.givenBut = QPushButton("    ÖDÜNÇ VER      ")
        self.givenBookCancelBut = QPushButton("İşlemi iptal et")
        self.label1Given =  QLabel("Bugün Verilen Kitaplar")  #  QLabel(f"""Emanete verilme: <font color = "#339af0">{self.todayDate}</font>""")
        self.label2Given =  QLabel()

        # self.label3Given =  QLabel(f""" <font color = "red" weight = "bold"> {self.borrowingTime} </font> günlük süreyle""")  #QLabel(f"""Son teslim: <font color = "#339af0">{QDate.currentDate().addDays(self.borrowingTime).toString('dd.MM.yyyy')}</font>""")
        # self.label4Given = QLabel(f"""<font color = "red" weight = "bold"> {QDate.currentDate().addDays(self.borrowingTime).toString('dd.MM.yyyy')} </font> tarihine kadar""")

        self.givenBut.setGraphicsEffect(BoxShadowEffect.colorTeal())

        self.givenBookCancelBut.setToolTip("Verilen kitabı iptal et")


        self.givenBut.setObjectName("takenGivenBut")
        self.givenBookCancelBut.setObjectName("givenBookCancelBut")
        self.label1Given.setObjectName("label1")
        self.label2Given.setObjectName("label2")
        # self.label3Given.setObjectName("label3")
        # self.label4Given.setObjectName("label3")

        self.label2Given.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

        self.label1Given.setAlignment(Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignBottom)
        self.label2Given.setAlignment(Qt.AlignmentFlag.AlignCenter)
        # self.label3Given.setAlignment(Qt.AlignmentFlag.AlignCenter)
        # self.label4Given.setAlignment(Qt.AlignmentFlag.AlignCenter)

       
        self.givenBut.setFixedSize(220, self.h2)
        self.givenBookCancelBut.setFixedHeight(self.h3)
        self.label1Given.setFixedHeight(38)
        self.label1Given.setFixedHeight(38)

        self.ustLayoutGiven.addWidget(self.label1Given,             6, 0, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.ustLayoutGiven.addWidget(self.label2Given,             6, 3, 1, 4, alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignBottom)
        # self.ustLayoutGiven.addWidget(self.label4Given,           6, 6, 1, 1)
        self.ustLayoutGiven.addWidget(self.givenBookCancelBut,      6, 7, 1, 1,  alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.ustLayoutGiven.addWidget(self.givenBut,                6, 8, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignRight)



## Altta yer alan TableWidget günlük verilen kitap listesini tutacak
        self.givenBookTableDaily.setColumnCount(11)
        self.givenBookTableDaily.setRowCount(50)
        titleGiven = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
        self.givenBookTableDaily.setHorizontalHeaderLabels(titleGiven)
        self.givenBookTableDaily.horizontalHeader().setFixedHeight(32)
        self.givenBookTableDaily.horizontalScrollBar().setStyleSheet(stil)
        self.givenBookTableDaily.setAlternatingRowColors(True)
        self.givenBookTableDaily.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.givenBookTableDaily.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.SingleSelection) # tekli satır seçme
        self.givenBookTableDaily.setGraphicsEffect(BoxShadowEffect.colorBlue())


        self.tableBookGiven.setGraphicsEffect(BoxShadowEffect.colorBlue())

        self.givenBookTableDaily.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma


## Sinya slotlar
        self.booksSearchButGiven.clicked.connect(self.bookNoNameSearchFonkGiven)
        self.lineEditBooksGiven.textChanged.connect(lambda: self.lineEditBooksGiven.setText(self.karakterBuyut(self.lineEditBooksGiven.text())))
        self.lineEditBooksGiven.returnPressed.connect(self.bookNoNameSearchFonkGiven)
        self.memberSearchButGiven.clicked.connect(self.memberNoNameSearchFonkGiven)
        self.lineEditMemberGiven.textChanged.connect(lambda: self.lineEditMemberGiven.setText(self.karakterBuyut(self.lineEditMemberGiven.text())))
        self.lineEditMemberGiven.returnPressed.connect(self.memberNoNameSearchFonkGiven)
        self.givenBut.clicked.connect(self.bookGivenFonk)
       # self.tableBookGiven.cellClicked.connect(self.selectBookNameGiven) devre dışı bırakıldı.
       # self.tableMemberGiven.cellClicked.connect(self.selectMemberNameGiven) devre dışı bırakıldı.

    


##############################################################  Kitap Alma (TakenWindow) ###################################################3
        # Kitap arama sonuclarını tutan değişken
        self.booksSearchListTaken:  GivenTakenBookModel = []
        # Günlük geri alınan kitapların listesi
        self.dailyTakenBooksList: GivenTakenBookModel = []
       
        # kitap geri verme sayfasında secilen kitabın indexini  tut.
        # tutulan indexe göre geri alınan ögeyi tablodan ve listeden sil(bookList)
        self.bookTakenListChoiceIndex = 0

        self.mainLayoutTaken = QVBoxLayout(self.takenWindowFRame)
        self.mainLayoutTaken.setContentsMargins(0,0,0,0)


        self.splitterTaken = QSplitter()
        self.splitterTaken.setOrientation(Qt.Orientation.Vertical)
        self.splitterTaken.setStretchFactor(4, 2)
        self.splitterTaken.setContentsMargins(0, 0, 0, 0)
        

        self.mainLayoutTaken.addWidget(self.splitterTaken)
        

    # kitap arama yapılacak alan (Emanete verilen kitapların içinde arama yapılacak)
        self.frameTaken = QFrame() # üst kısım
        self.takenAlt = QStackedWidget() # alt kısım; günlük alınanlar ve alıp verme geçmişi

        self.takenAlt.setMinimumHeight(int((self.h / 2) - 250))
        
        self.frameTaken.setContentsMargins(0, 15, 0, 5)
        self.frameTaken.setObjectName("takenGiven")

        self.takenAlt.setContentsMargins(0, 0, 0, 0)
        self.takenAlt.setObjectName("takenGiven")
       

        self.splitterTaken.addWidget(self.frameTaken)
        self.splitterTaken.addWidget(self.takenAlt)
    

        self.takenBookTableDaily = QTableWidget() #alt kısım günlük alınan kitaplar
        self.takenGivenBookHistoryView = QFrame() # alt kısım kitap alıp verme geçmişi başlangıçta gizli


        self.takenGivenBookHistoryView.setContentsMargins(0,0,0,0)


        self.takenAlt.addWidget(self.takenBookTableDaily)
        self.takenAlt.addWidget(self.takenGivenBookHistoryView)

        


# Üst üye ve kitap arama cercevesi
        self.ustLayoutTaken = QGridLayout(self.frameTaken)
        self.ustLayoutTaken.setContentsMargins(0, 0, 0, 0)
        self.ustLayoutTaken.setHorizontalSpacing(0)
        self.ustLayoutTaken.setVerticalSpacing(8)
       

        self.borrewedAllBooksListBut = QRadioButton()
        self.timeOutBooksListBut = QRadioButton()
        self.csvToBut = QPushButton("")
        self.lineEditBooksTaken = QLineEdit()
        self.searchFilterComboBoxTaken = QComboBox()
        self.booksSearchButTaken = QPushButton()
        self.tableBookTaken = QTableWidget()


        self.lineEditBooksTaken.setPlaceholderText("Kitap adı ile arama yapabilirsiniz")        
        self.borrewedAllBooksListBut.setText("Emanetteki Kitaplar      ")
        self.timeOutBooksListBut.setText("Teslim Süresi Geçmiş Kitaplar")
        
        
        self.csvToBut.setObjectName("csvToBut")
        self.lineEditBooksTaken.setObjectName("lineEdit")
        self.booksSearchButTaken.setObjectName("searchBut")
        self.searchFilterComboBoxTaken.setObjectName("searchFilter")

        self.borrewedAllBooksListBut.setFixedHeight(self.h1)
        self.timeOutBooksListBut.setFixedHeight(self.h1)
        self.lineEditBooksTaken.setFixedHeight(self.h1)
        self.booksSearchButTaken.setFixedHeight(self.h2)
        self.csvToBut.setFixedHeight(self.h2)
        self.searchFilterComboBoxTaken.setFixedHeight(self.h1)  
        
        
        self.searchFilterComboBoxTaken.addItems(["Kitap ID","Karekod No", "Kitap Adı", "Üye ID", "Üye Adı", "Üye Soyadı", "Öğrenci No", "T.C. Kimlik No", "Tüm Alanlar"])
        self.searchFilterComboBoxTaken.setEditable(True)
        self.searchFilterComboBoxTaken.lineEdit().setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.searchFilterComboBoxTaken.lineEdit().setReadOnly(True)
        self.searchFilterComboBoxTaken.setCurrentIndex(2)
        self.csvToBut.setToolTip("Tabloyu xlsx dosyasına aktar")
        self.lineEditBooksTaken.setClearButtonEnabled(True)

       

        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
        self.tableBookTaken.setColumnCount(11)
        self.tableBookTaken.setRowCount(50)
        self.tableBookTaken.setHorizontalHeaderLabels(title)
        self.tableBookTaken.horizontalScrollBar().setStyleSheet(stil)
        self.tableBookTaken.setAlternatingRowColors(True)
        self.tableBookTaken.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.tableBookTaken.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.SingleSelection) # tekli satır seçme
        self.tableBookTaken.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma
        self.tableBookTaken.setGraphicsEffect(BoxShadowEffect.colorBlue())
    

        self.ustLayoutTaken.addWidget(self.borrewedAllBooksListBut,          0, 2, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.ustLayoutTaken.addWidget(self.lineEditBooksTaken,               0, 5,  1, 6)
        self.ustLayoutTaken.addWidget(self.searchFilterComboBoxTaken,        0, 11, 1, 2)
        self.ustLayoutTaken.addWidget(self.booksSearchButTaken,              0, 13, 1, 1)
        self.ustLayoutTaken.addWidget(self.timeOutBooksListBut,              0, 14, 1, 3)
        self.ustLayoutTaken.addWidget(self.csvToBut,                         0, 17, 1, 1)
        self.ustLayoutTaken.addWidget(self.tableBookTaken,                   1, 0, 1, 18)

        for i in range(0, 18):
            self.ustLayoutTaken.setColumnStretch(i, 1)
        
        

    

    # Üst cerveye bağlı kitap ver ve kitap al butonları ve günlük verilen kitap listesi ile alıp verilen tüm kitapları listeleme geçiş butonu
        
        self.takenBut = QPushButton("KİTABI GERİ AL")
        self.label1Taken =  QLabel("ÜYELERDEN TESLİM ALINAN KİTAPLAR (GÜNLÜK)")  

        # Günlük alınan kitapları görüntüle sayfası geçiş butonu ve alıp verilen kitap geçmişi kitaplar listesini görüntüle
        self.takenDailyBookWinOpenBut = QPushButton()
        self.takenGivenHistoryWinOpenBut = QPushButton()
        

        self.takenDailyBookWinOpenBut.setText("     Bügün Alınan Kitaplar     ")
        self.takenGivenHistoryWinOpenBut.setText("  Alıp Verilen Tüm Kitaplar   ")
        self.takenBut.setGraphicsEffect(BoxShadowEffect.colorTeal())

        self.takenBut.setObjectName("takenGivenBut")
        self.label1Taken.setObjectName("label1")
        self.takenDailyBookWinOpenBut.setObjectName("takenGivenWinOpen")
        self.takenGivenHistoryWinOpenBut.setObjectName("takenGivenWinOpen")
      
        self.label1Taken.setAlignment(Qt.AlignmentFlag.AlignCenter)
        
        self.takenBut.setFixedSize(220, self.h2)
        self.label1Taken.setFixedHeight(38)
        self.takenDailyBookWinOpenBut.setFixedHeight(self.h3)
        self.takenGivenHistoryWinOpenBut.setFixedHeight(self.h3)

        self.ustLayoutTaken.addWidget(self.takenDailyBookWinOpenBut,           6, 0, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.ustLayoutTaken.addWidget(self.takenGivenHistoryWinOpenBut,        6, 2, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.ustLayoutTaken.addWidget(self.takenBut,                           6, 16, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignCenter)
        

    ## Altta yer alan TableWidget günlük verilen kitap listesini
    ## Ve daha önceden verilen kitapların tarihe göre listelenmesini tutacak olacak table widget yer alacak
        
    # Günlük alınan kitaplar
        titleB = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
        self.takenBookTableDaily.setColumnCount(11)
        self.takenBookTableDaily.setRowCount(50)
        self.takenBookTableDaily.setHorizontalHeaderLabels(titleB)
        self.takenBookTableDaily.horizontalScrollBar().setStyleSheet(stil)
        self.takenBookTableDaily.setAlternatingRowColors(True)
        self.takenBookTableDaily.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme
        self.takenBookTableDaily.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers) # satırı düzenlenemez yapma
        self.takenBookTableDaily.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.SingleSelection) # tekli satır seçme
        self.takenBookTableDaily.setGraphicsEffect(BoxShadowEffect.colorBlue())


    # Alıp verilen kitaplar geçmişi self.takenGivenBookHistoryView = QFrame()
        
        self.takenGivenBookHistoryViewLayout = QGridLayout(self.takenGivenBookHistoryView)
        self.takenGivenBookHistoryViewLayout.setContentsMargins(0,0,0,0)


        self.listeleBut = QPushButton("")

        self.takenGivenBookHistoryViewLabel = QLabel()

        self.dateedit1 = QDateEdit(calendarPopup=True)
        self.dateedit1.setDateTime(QtCore.QDateTime.currentDateTime())
        self.dateedit1.setMaximumDateTime(QDateTime.currentDateTime())

        self.dateedit2 = QDateEdit(calendarPopup=True)
        self.dateedit2.setDateTime(QDateTime.currentDateTime())
        self.dateedit2.setMaximumDateTime(QDateTime.currentDateTime())

        self.takenGivenBookHistoryViewTable = QTableWidget()


        self.dateedit1.setFixedHeight(self.h4)
        self.dateedit2.setFixedHeight(self.h4)
        self.listeleBut.setFixedSize(45, self.h4)

        self.listeleBut.setObjectName("listeleBut")
        self.listeleBut.setToolTip("Seçili tarihleri listele")

        self.takenGivenBookHistoryViewLabel.setText("  Seçilen tarih aralığında kitap geçmişini listeleme (En fazla 5 bin kitap listelenir.)")



        titleC = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Teslim T."]
        self.takenGivenBookHistoryViewTable.setColumnCount(11)
        self.takenGivenBookHistoryViewTable.setRowCount(50)
        self.takenGivenBookHistoryViewTable.setHorizontalHeaderLabels(titleC)
        self.takenGivenBookHistoryViewTable.horizontalScrollBar().setStyleSheet(stil)
        self.takenGivenBookHistoryViewTable.setAlternatingRowColors(True)
        self.takenGivenBookHistoryViewTable.setSelectionBehavior(QtWidgets.QTableView.SelectionBehavior.SelectRows) # satırın tamamını seçme     
        self.takenGivenBookHistoryViewTable.setGraphicsEffect(BoxShadowEffect.colorBlue())

        
        self.takenGivenBookHistoryViewLayout.addWidget(self.takenGivenBookHistoryViewLabel,         0, 0, 1, 4)
        self.takenGivenBookHistoryViewLayout.addWidget(self.dateedit1,                              0, 4, 1, 1)
        self.takenGivenBookHistoryViewLayout.addWidget(self.dateedit2,                              0, 5, 1, 1)
        self.takenGivenBookHistoryViewLayout.addWidget(self.listeleBut,                             0, 6, 1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.takenGivenBookHistoryViewLayout.addWidget(self.takenGivenBookHistoryViewTable,         1, 0, 5, 10)



## Sinyal slotlar
        self.booksSearchButTaken.clicked.connect(self.bookNoNameSearchFonkTaken)
        self.lineEditBooksTaken.textChanged.connect(lambda: self.lineEditBooksTaken.setText(self.karakterBuyut(self.lineEditBooksTaken.text())))
        self.lineEditBooksTaken.returnPressed.connect(self.bookNoNameSearchFonkTaken)
        self.csvToBut.clicked.connect(self.tabloXlsxAktarFonk)
        self.searchFilterComboBoxBook.currentIndexChanged.connect(self.seacrhFilterBookFonk)
        self.searchFilterComboBoxMember.currentIndexChanged.connect(self.seacrhFilterMemberFonk)
        self.givenBookCancelBut.clicked.connect(self.bookGivenCancelFonk)


        self.takenBut.clicked.connect(self.bookTakenFonk)
        self.borrewedAllBooksListBut.clicked.connect(self.borrewedAllList)
        self.timeOutBooksListBut.clicked.connect(self.timeOutBookList)
        self.takenDailyBookWinOpenBut.clicked.connect(self.takenGivenWinOpenDailyStil)
        self.takenGivenHistoryWinOpenBut.clicked.connect(self.takenGivenWinOpenAllStil)
        self.listeleBut.clicked.connect(self.takenGivenBookHistoryViewFonk)
        self.searchFilterComboBoxTaken.currentIndexChanged.connect(self.seacrhFilterFonkTaken)





    # Kitap verme verme ve kitap alma butonu animasyonları
        self.timerTaken = QTimer()
        self.timerTaken.timeout.connect(self.messageFonkTaken)
        self.takenTimerSayac = 0

        self.timerGiven = QTimer()
        self.timerGiven.timeout.connect(self.messageFonkGiven)
        self.givenTimerSayac = 0


    #Kitap sayfası table widget sutun genişliği
    def tableGivenBookWidth(self):
        # tabe widget sutun genişliklerini hesapla 
        table = self.tableBookGiven.width()
    
        self.tableBookGiven.setColumnWidth(0, int(((table) / 14) * 2))
        self.tableBookGiven.setColumnWidth(1, int(((table) / 14) * 3))
        self.tableBookGiven.setColumnWidth(2, int(((table) / 14) * 9) - 50)
        
    

    def tableGivenMemberWidth(self):
        # tabe widget sutun genişliklerini hesapla 
        table = self.tableMemberGiven.width()
    
        self.tableMemberGiven.setColumnWidth(0, int(((table) / 12) * 1))
        self.tableMemberGiven.setColumnWidth(1, int(((table) / 12) * 2))
        self.tableMemberGiven.setColumnWidth(2, int(((table) / 12) * 4))
        self.tableMemberGiven.setColumnWidth(3, int(((table) / 12) * 3))
        self.tableMemberGiven.setColumnWidth(4, int(((table) / 12) * 2) - 50)

    
    def tableBookGivenDailyWidth(self):
        table = self.givenBookTableDaily.width()

        self.givenBookTableDaily.setColumnWidth(0,  int(((table) / 47) * 2) - 5)
        self.givenBookTableDaily.setColumnWidth(1,  int(((table) / 47) * 3) - 5)
        self.givenBookTableDaily.setColumnWidth(2,  int(((table) / 47) * 9))
        self.givenBookTableDaily.setColumnWidth(3,  int(((table) / 47) * 6) - 5)
        self.givenBookTableDaily.setColumnWidth(4,  int(((table) / 47) * 2))
        self.givenBookTableDaily.setColumnWidth(5,  int(((table) / 47) * 3))
        self.givenBookTableDaily.setColumnWidth(6,  int(((table) / 47) * 5))
        self.givenBookTableDaily.setColumnWidth(7,  int(((table) / 47) * 4) - 5)
        self.givenBookTableDaily.setColumnWidth(8,  int(((table) / 47) * 3) - 5)
        self.givenBookTableDaily.setColumnWidth(9,  int(((table) / 47) * 5) - 10)
        self.givenBookTableDaily.setColumnWidth(10, int(((table) / 47) * 5) - 10)

    
    def tableBookTakenWidth(self):
        table = self.w - self.tableBookTaken.verticalHeader().width() - 52 - 22 - 16 # ekran - tablo satır numrası genişliği - sol acılır menu - padding

        self.tableBookTaken.setColumnWidth(0,  int((table / 47) * 2))
        self.tableBookTaken.setColumnWidth(1,  int((table / 47) * 3))
        self.tableBookTaken.setColumnWidth(2,  int((table / 47) * 9))
        self.tableBookTaken.setColumnWidth(3,  int((table / 47) * 6))
        self.tableBookTaken.setColumnWidth(4,  int((table / 47) * 2))
        self.tableBookTaken.setColumnWidth(5,  int((table / 47) * 3))
        self.tableBookTaken.setColumnWidth(6,  int((table / 47) * 5))
        self.tableBookTaken.setColumnWidth(7,  int((table / 47) * 4))
        self.tableBookTaken.setColumnWidth(8,  int((table / 47) * 3))
        self.tableBookTaken.setColumnWidth(9,  int((table / 47) * 5))
        self.tableBookTaken.setColumnWidth(10, int((table / 47) * 5))

        print("tableBookTakenWidth ", table)

    def takenBookTableDailyWidth(self):
        table = self.w - self.tableBookTaken.verticalHeader().width() - 52 - 22 - 16 # ekran - tablo satır numrası genişliği - sol acılır menu - padding

        self.takenBookTableDaily.setColumnWidth(0,  int((table / 33) * 2))
        self.takenBookTableDaily.setColumnWidth(1,  int((table / 33) * 2))
        self.takenBookTableDaily.setColumnWidth(2,  int((table / 33) * 7))
        self.takenBookTableDaily.setColumnWidth(3,  int((table / 33) * 4))
        self.takenBookTableDaily.setColumnWidth(4,  int((table / 33) * 2)) # üye ıd
        self.takenBookTableDaily.setColumnWidth(5,  int((table / 33) * 2))
        self.takenBookTableDaily.setColumnWidth(6,  int((table / 33) * 4))
        self.takenBookTableDaily.setColumnWidth(7,  int((table / 33) * 3))
        self.takenBookTableDaily.setColumnWidth(8,  int((table / 33) * 2))
        self.takenBookTableDaily.setColumnWidth(9,  int((table / 33) * 3))
        self.takenBookTableDaily.setColumnWidth(10, int((table / 33) * 2))

    
    def takenGivenBookHistoryViewWidth(self):
        table = self.w - self.tableBookTaken.verticalHeader().width() - 52 - 22 - 16 # ekran - tablo satır numrası genişliği - sol acılır menu - padding

        self.takenGivenBookHistoryViewTable.setColumnWidth(0,  int((table / 33) * 2))
        self.takenGivenBookHistoryViewTable.setColumnWidth(1,  int((table / 33) * 2))
        self.takenGivenBookHistoryViewTable.setColumnWidth(2,  int((table / 33) * 7))
        self.takenGivenBookHistoryViewTable.setColumnWidth(3,  int((table / 33) * 4))
        self.takenGivenBookHistoryViewTable.setColumnWidth(4,  int((table / 33) * 2)) # üye ıd
        self.takenGivenBookHistoryViewTable.setColumnWidth(5,  int((table / 33) * 2))
        self.takenGivenBookHistoryViewTable.setColumnWidth(6,  int((table / 33) * 4))
        self.takenGivenBookHistoryViewTable.setColumnWidth(7,  int((table / 33) * 3))
        self.takenGivenBookHistoryViewTable.setColumnWidth(8,  int((table / 33) * 2))
        self.takenGivenBookHistoryViewTable.setColumnWidth(9,  int((table / 33) * 3))
        self.takenGivenBookHistoryViewTable.setColumnWidth(10, int((table / 33) * 2))


    def initialFonkTakenGivenWindow(self):
        
        self.csvToBut.setEnabled(self.user.raporlama_yetkisi)

        self.tableGivenBookWidth()
        self.tableGivenMemberWidth()
        self.tableBookGivenDailyWidth()
        self.tableBookTakenWidth()
        self.takenBookTableDailyWidth()
        self.takenGivenBookHistoryViewWidth()

        self.takenGivenWinOpenDailyStil()


      #  kitap verme kitap ara
        group_name = "comboBoxIndex"
        setting1 = SettingsConf.getValueConf(group_name=group_name, key="comboBoxBookGiven")
        if setting1 is None:
            self.searchFilterComboBoxBook.setCurrentIndex(3)
        else:
            self.searchFilterComboBoxBook.setCurrentIndex(int(setting1))
     #   print("settings takenGiven: ", setting1)

    #   # Kitap verme Üye
        setting2 = SettingsConf.getValueConf(group_name=group_name, key="comboBoxMemberGiven")
        if setting2 is None:
            self.searchFilterComboBoxMember.setCurrentIndex(5)

        else:
            self.searchFilterComboBoxMember.setCurrentIndex(int(setting2))
      #  print("settings takenGiven: ", setting2)

        setting3 = SettingsConf.getValueConf(group_name=group_name, key="comboBoxTaken")
        if setting3 is None:
            self.searchFilterComboBoxTaken.setCurrentIndex(2)
        else:
            self.searchFilterComboBoxTaken.setCurrentIndex(int(setting3))
      #  print("settings takenGivenTaken: ", setting3)
        


        
# Kitap Arama filtresi seçince
    def seacrhFilterBookFonk(self):
       # print(self.searchFilterComboBoxBook.currentIndex())
        """["ID No","Karekod No", "Kitap Adı", "Tüm Alanlar"]"""

        group_name = "comboBoxIndex"
        settings = SettingsConf.setValueConf(group_name=group_name, key="comboBoxBookGiven", value=self.searchFilterComboBoxBook.currentIndex())
        print("ayar kaydedildi: ", self.searchFilterComboBoxBook.currentIndex())

        if self.searchFilterComboBoxBook.currentIndex() == 0:
          #  print("id")
            self.lineEditBooksGiven.setPlaceholderText("Kitap ID ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxBook.currentIndex() == 1:
         #   print("karekod_")
            self.lineEditBooksGiven.setPlaceholderText("Karekod no ile arama yapabilirsiniz")
        
        elif self.searchFilterComboBoxBook.currentIndex() == 2:
           # print("kitap adı")
            self.lineEditBooksGiven.setPlaceholderText("Kitap adı ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxBook.currentIndex() == 3:
          #  print("tüm alanlar")
            self.lineEditBooksGiven.setPlaceholderText("Kitap ID, karekod no, kitap adı adı ile arama yapabilirsiniz")

# Üye Arama filtresi seçince
    def seacrhFilterMemberFonk(self):
        # bazı ögeler filtrelenmeyecegi icin filitrelenmeyen ögeleri kaldırma
       
        """["ID","Ad", "Soyad", "Öğrenci No", "T.C. No", "Tel No", "Tüm Alanlar"]"""

        group_name = "comboBoxIndex"
        settings = SettingsConf.setValueConf(group_name=group_name, key="comboBoxMemberGiven", value=self.searchFilterComboBoxMember.currentIndex())
       # print("ayar kaydedildi: ", self.searchFilterComboBoxMember.currentIndex())

        if self.searchFilterComboBoxMember.currentIndex() == 0:
            self.lineEditMemberGiven.setPlaceholderText("Üye ID ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxMember.currentIndex() == 1:
            self.lineEditMemberGiven.setPlaceholderText("Üye adı ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxMember.currentIndex() == 2:
            self.lineEditMemberGiven.setPlaceholderText("Üye soyadı ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxMember.currentIndex() == 3:
            self.lineEditMemberGiven.setPlaceholderText("Öğrenci no ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxMember.currentIndex() == 4:
            self.lineEditMemberGiven.setPlaceholderText("T.C. Kimlik no ile arama yapabilirsiniz")

        # elif self.searchFilterComboBox.currentIndex() == 5:  # Kitap verme sayfası üye aramada tel no araması devre dışı
        #     self.lineEditMember.setPlaceholderText("Telefon no ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxMember.currentIndex() == 5:
            self.lineEditMemberGiven.setPlaceholderText("ID, ad, soyad, öğrenci no, kimlik no, tel no ile arama yapabilirsiniz")

    # Arama filtresi seçince (Kitabı geri alma sayfası)
    def seacrhFilterFonkTaken(self):
       # print(self.searchFilterComboBox.currentIndex())
        """ ["Kitap ID","Karekod No", "Kitap Adı", "Üye ID", "Üye Adı", "Üye Soyadı", "Öğrenci No", "T.C Kimlik No", "Tüm Alanlar"] """

        group_name = "comboBoxIndex"
        settings = SettingsConf.setValueConf(group_name=group_name, key="comboBoxTaken", value=self.searchFilterComboBoxTaken.currentIndex())
     #   print("ayar kaydedildi: ", self.searchFilterComboBoxTaken.currentIndex())

        if self.searchFilterComboBoxTaken.currentIndex() == 0:
            self.lineEditBooksTaken.setPlaceholderText("Kitap ID ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 1:
            self.lineEditBooksTaken.setPlaceholderText("Karekod no ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 2:
            self.lineEditBooksTaken.setPlaceholderText("Kitap adı ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 3:
            self.lineEditBooksTaken.setPlaceholderText("Üye ID ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 4:
            self.lineEditBooksTaken.setPlaceholderText("Üye adı adı ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 5:
            self.lineEditBooksTaken.setPlaceholderText("Üye soyadı ile arama yapabilirsiniz")
        
        elif self.searchFilterComboBoxTaken.currentIndex() == 6:
            self.lineEditBooksTaken.setPlaceholderText("Öğrenci no ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 7:
            self.lineEditBooksTaken.setPlaceholderText("T.C. kimlik no ile arama yapabilirsiniz")

        elif self.searchFilterComboBoxTaken.currentIndex() == 8:
            self.lineEditBooksTaken.setPlaceholderText("ID, karekod, kitap adı, üye adı, soyadı, öğrenci no, kimik no ile arama ayapabilirisiniz")






######################  Kitap Verme (GivenWindow) Fonksiyonları ##################################

    # başlangıc fonksiyonları ve işlemleri yalnızca program calıştığında bir kere çağrılacak +++
    def initialFonkGivenWindow(self):
        # emanet süresi ve kişinin alabileceği kitap miktarını belirle
        result = DbQuery.ayarlarVeYapilandirmalar()
        self.borrowingTime = int(result.kitap_emanet_suresi)
        self.takenMaximumBooks = int(result.alinabilecek_kitap_sayisi)

        self.initialFonkGivenWindow2()

        self.emanettekiKitapUyeNo()

        # günlük verilen kitap listesinde kitap varsa verilen kitabı iptal etme butonunun aktif et
        self.label2Given.setText("Verilen kitabı gün içinde yönetici hesabıyla iptal edebilirsiniz") # Herkese görünsün
        if self.user.statu:
            if self.dailyGivenBooksList:
                self.givenBookCancelBut.setHidden(False)
            else:
                self.givenBookCancelBut.setHidden(True)
        else:
            # Boş ise gizle
            self.givenBookCancelBut.setHidden(True)



    def initialFonkGivenWindow2(self):
        # günlük alınan kitapların listesi

        # start_time = time.time()
        self.dailyGivenBooksList =  DbQuery.emaneteVerilenGunlukKitaplar(teslim_suresi=self.borrowingTime)
        # print(f"Emanete verme sorgu süresi2: {(time.time() - start_time)}")

        self.givenBookTableDailyResultWrite()



    # Okuyan üyeleri ve emanetteki kitapları db den alıp değişkenlere gönderir
    def emanettekiKitapUyeNo(self):

        result = DbQuery.emanettekiKitapUyeID()
        if result is not None:
           # print("emanettekiKitapUyeID: ", result, self.borrowedBooksIdListGiven, type(self.borrowedBooksIdListGiven))
            self.borrowedBooksIdListGiven = result[0]
            self.readerMemberIdListGiven = result[1]

        else:
            self.borrowedBooksIdListGiven = None
            self.readerMemberIdListGiven = None
            print("Db den veri alınalanmadı")

        
    # kitap transfer başarılı işlem bildirimi
    def messageFonkGiven(self):
        self.givenTimerSayac+= 2.5
        self.givenBut.setText("ÖDÜNÇ VERİLİYOR")
        self.givenBut.setEnabled(False) # işl3m devam ederken butonu kullınm dışı bırak

        value = (self.givenTimerSayac / 100)

       # print("Value:", value)

        self.givenBut.setStyleSheet(f""" 
        background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, 
                    stop:{value - 0.001} rgba(71, 118, 230, 255), 
                    stop:{value} rgba(71, 118, 230, 255), 
                    stop:{value} rgba(250, 250, 250, 255) );
                border-radius: 8px;
        """)

        if self.givenTimerSayac == 100 :
            self.givenTimerSayac = 0
            self.givenBut.setText("")
            self.givenBut.setStyleSheet("""  background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                                        stop:0 #57cc99, stop: 0.5 #80ed99, stop:1 #9ef01a); 
                                        background-image: url("assets/icon/check-mark.png");
                                        background-position: center center;
                                        background-repeat: no-repeat;
                                        """)
            
            self.timerGiven.stop()

            QTimer.singleShot(200, self.messageFonkStopGiven)

        
    def messageFonkStopGiven(self):
        self.givenBut.setStyleSheet("""background: qlineargradient(x1:0, y1:1, x2:1, y2:1,
                                        stop:0 #2682ff, stop: 0.4 #4f94f4, stop:1 #6aa9ff);""")
        self.givenBut.setEnabled(True)
        self.givenBut.setText("      ÖDÜNÇ VER      ")

        self.givenBookTableDailyResultWrite() # günlük verilen kitaplar listesini yeniden yaz
        self.emanettekiKitapUyeNo() # Emanetteki kitaplar ve okuyan üyeler listesini döndürür
        self.tableBookSearchResultsWriteGiven(self.booksSearchListGiven) # Kitap verme kitap arama sonuçlarını yeniden yaz (Verilen kitap kitap kırmızı işaretlenir)


    # Kitap secilince kitpa ismi label1 gönder


    ## Kitap emanete verme 
    def bookGivenFonk(self):

        # secilen satır numaralarını al
        bookIndex = -1
        memberIndex = -1


        indexesB = self.tableBookGiven.selectionModel().selectedRows(column=0) # seçilen satırı döndür
        for index in indexesB:
            if index.data() != None: # eğer boş satır seçilirse 
                bookIndex = index.row()
                self.searchIndexBookGiven = bookIndex
            else:
                bookIndex = -1

        indexesM = self.tableMemberGiven.selectionModel().selectedRows(column=0) # seçilen satır döndür
        for index in indexesM:
            if index.data() != None: # eğer boş satır seçilirse 
                memberIndex = index.row()
                self.searchIndexMemberGiven = memberIndex
            else:
                memberIndex = -1

        # satır seçilip secilmediğini kontrol et
        if bookIndex != -1:

            # print("kitap seçilmiş", bookIndex)
            if memberIndex != -1:

                if self.borrowedBooksIdListGiven or self.readerMemberIdListGiven is not None: # Emanettekiler ve okuyan üyeler listesi No gelirse kitap verme

                    if not self.booksSearchListGiven[bookIndex].id in self.borrowedBooksIdListGiven:

                        if self.readerMemberIdListGiven.count(self.membersSearchListGiven[memberIndex].id) < self.takenMaximumBooks: # Üye kaç tane okuyor

                            self.message = MessageBoxGiven(
                                borrowingTime=f"{self.borrowingTime}",
                                deadline=QDate.currentDate().addDays(self.borrowingTime).toString('dd.MM.yyyy'),
                                book=self.booksSearchListGiven[bookIndex],
                                member=self.membersSearchListGiven[memberIndex],
                                buttonHeight=self.h4
                                )
                            self.message.okeyBut.clicked.connect(lambda: self.bookGivenFonk2(self.booksSearchListGiven[bookIndex], self.membersSearchListGiven[memberIndex]))
                            self.message.exec()


                        else:
                            MessageBOX("UYARI", f" Bir üye en fazla {self.takenMaximumBooks} kitap alabilir. \n(Not: Üyenin alabileceği kitap sayısınını yönetici hesabıyla değiştirebilirsiniz.)", 0)

                    else:
                        MessageBOX("UYARI", f" {self.booksSearchListGiven[bookIndex].ad} adlı kitap şu an bir başka üye tarafından okunmaktadır.", 0)

                else:
                    MessageBOX("HATA", f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için kitap verilemiyor. Programı kapatıp tekrar deneyiniz.", 0)

            else:
                MessageBOX("UYARI", " Öncelikle listeden bir üye seçiniz.", 0) 

        else:
            MessageBOX("UYARI", " Öncelikle listeden  bir kitap seçniz", 0) 

    def bookGivenFonk2(self, book, member):

        start_time = time.time()

        result = DbQuery.kitabiEmaneteVer(None,  book.id, member.id)

        if result == 1:

            self.timerGiven.start(20)

            date = QDate.currentDate()
            time_ = QDateTime.currentDateTime().toString('hh:mm:ss')

            self.dailyGivenBooksList.insert(0, GivenTakenBookModel(
                rowid=None,
                kitap_id=book.id,
                karekod=book.karekod_no,
                kitap_adi=book.ad,
                yazar=book.yazar,
                uye_id=member.id,
                tc=member.tc,
                uye_adi=member.ad,
                soyadi=member.soyad,
                ogrenci_no=member.ogrenci_no,
                emanete_verilme_tarihi=date.toString('yyyy.MM.dd'),
                teslim_tarihi="",
                teslim_suresi=self.borrowingTime,
                son_teslim_tarihi=date.addDays(self.borrowingTime).toString('yyyy.MM.dd'),
                verilme_saati=time_,
                teslim_saati=time_
            ))

            print(f"Emanete verme sorgu süresi 1: {(time.time() - start_time)}")

            # günlük verilen kitap listesinde kitap varsa verilen kitabı iptal etme butonunun aktif et
            self.label2Given.setText("") # Bir kere görünecek başlangıcta
            if self.dailyGivenBooksList:
                self.givenBookCancelBut.setHidden(False)
            else:
                self.givenBookCancelBut.setHidden(True)


        elif result == 2:
            MessageBOX("UYARI", " Veritabanı hatası oluştu.", 0)
        else:
            MessageBOX("UYARI", " Bilinmeyen bir hata oluştu.", 0)






    # Günlük verilen herhangi bir kitabı iptal edip geri al
    def bookGivenCancelFonk(self):
        secilenSatirIndex = -1

        indexes = self.givenBookTableDaily.selectionModel().selectedRows(column=0) # seçilen satırı döndür
        for index in indexes:
            if index.data() != None: # eğer boş satır seçilirse
                secilenSatirIndex = index.row()
            else:
                secilenSatirIndex = -1

        if self.user.statu:
            if secilenSatirIndex != -1:
                secilenSatir = self.dailyGivenBooksList[secilenSatirIndex]

                print("iptal edilecek kitap id: ", secilenSatir.kitap_id)

                if self.borrowedBooksIdListGiven is not None:
                    if secilenSatir.kitap_id in self.borrowedBooksIdListGiven:
                      #  print("silinecek satir işlem ID: ", secilenSatir.rowID)
                        msg = MessageBOXQuestion("UYARI", f"    Gün içinde {secilenSatir.verilme_saati} saatinde yapılan kitap verme işlemini iptal etmek üzeresiniz. İptal işleminden sonra kitap okunmamış olarak kabul edilip kayıtlardan silinecek.\n\n    İşleme devam edilsin mi?")
                        msg.okBut.clicked.connect(lambda: self.bookGivenCancelFonk2(secilenSatir))
                    else:
                        MessageBOX("UYARI", " Bu kitap geri alındığı için iptal edilemez. Yalnızca emanetteki kitaplar iptal edilebilirsiniz.", 0)
                else:
                    MessageBOX("HATA", f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için iptal işlemi gerçekleştirilemiyor. Programı kapatıp tekrar deneyiniz.", 0)
            else:
                MessageBOX("UYARI", " Öncelikle iptal etmek istediğiniz işlemi aşağıdaki günlük verilen kitap listesinden seçininiz", 0)
        else:
            MessageBOX("UYARI", " Bu işlem yönetici ayrıcalığı gerektirir", 0)

    def bookGivenCancelFonk2(self, secilen_kitap: GivenTakenBookModel):
        result = DbQuery.kitabiOkunmadanAl(kitap_id=secilen_kitap.kitap_id)
        if result == 1:

            self.emanettekiKitapUyeNo()
            self.tableBookSearchResultsWriteGiven(self.booksSearchListGiven)

            # Kitap alındıktan sonra günlük verilen kitapları yeniden listeleme db den yeniden cek
            self.initialFonkGivenWindow2()


            # Emanetten alınan kitaplar listesi kitap arama listesini tekrar güncelle
            # Liste içinde "self.booksSearchListTaken" bulunuyorsa kitap, kitap_id ye göre listeden çıkar

            emanetteki = [emanettekiler.kitap_id for emanettekiler in self.booksSearchListTaken]
           # print("Emanetteki kitap RowID: ", emanetteki, "secilen kitap rowID: ", book.rowID)
            if secilen_kitap.kitap_id in emanetteki:
                self.booksSearchListTaken.pop(emanetteki.index(secilen_kitap.kitap_id))
                ## sonuca göre tablodaki satır sayısını belirle
                self.tableBookTaken.clear()
                if len(self.booksSearchListTaken) >= 50:
                    self.tableBookTaken.setRowCount(len(self.booksSearchListTaken))
                else:
                    self.tableBookTaken.setRowCount(50)

                self.tableBookSearchResultsWriteTaken()
                title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
                self.tableBookTaken.setHorizontalHeaderLabels(title)

          #  print("Emanetteki kitap RowID: ", emanetteki)

            MessageBOX("BİLGİLENDİRME", " Seçtiğiniz kitap verme işlemi iptal edilmiştir. Kitabı üyeden almayı unutmayınız.", 1)

        else:
            MessageBOX("UYARI", " Bilinmeyen bir hata oluştu tekrar deneyiniz", 0)



## Kitap no ve kitap adı ara
    def bookNoNameSearchFonkGiven(self):
        rowColor = QtGui.QColor(133, 174, 255)

        ## eğer arama yeri boş gelirse 
        ara =  self.lineEditBooksGiven.text()

        if self.borrowedBooksIdListGiven is not None:
            if len(ara) > 0:

                self.booksSearchListGiven = DbQuery.kitapNoAdAra(ara, self.searchFilterComboBoxBook.currentIndex())

            ## sonuca göre tablodaki satır sayısını belirle
                if len(self.booksSearchListGiven) >= 50:
                    self.tableBookGiven.setRowCount(len(self.booksSearchListGiven))
                else:
                    self.tableBookGiven.setRowCount(50)

                self.tableBookSearchResultsWriteGiven(self.booksSearchListGiven)

            else:
                self.tableBookGiven.clear()
                self.tableBookGiven.setHorizontalHeaderLabels(self.tabloBooksTitleGiven)

            # tablonun ilk satırına gitme
            item = self.tableBookGiven.model().index(0, 0)
            self.tableBookGiven.scrollTo(item)

        else:
            MessageBOX("HATA", f" Emanetteki kitaplar ve okuyan üyeler listesi veritabanından alınamadığı için kitaplar listelenemiyor. Programı kapatıp tekrar deneyiniz.", 0)




    ## Kitap arama sonuçlarını tabloya yaz
    def tableBookSearchResultsWriteGiven(self, kitapListesi: BookModel):

        rowColor = QtGui.QColor(255, 50, 0)
        self.tableBookGiven.clear()
        row = 0

        if self.borrowedBooksIdListGiven is not None:
            for i in kitapListesi:
            # kitap emanet listesi içind evar ise renkli göster
                if i.id in self.borrowedBooksIdListGiven:
                    item0 = QtWidgets.QTableWidgetItem(str(i.id))
                    item0.setForeground(rowColor)
                    item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableBookGiven.setItem(row, 0, item0)

                    item1 = QtWidgets.QTableWidgetItem(str(i.karekod_no))
                    item1.setForeground(rowColor)
                    item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableBookGiven.setItem(row, 1, item1)

                    item2 = QtWidgets.QTableWidgetItem(i.ad)
                    item2.setForeground(rowColor)
                    self.tableBookGiven.setItem(row, 2, item2)

            # item3 = QtWidgets.QTableWidgetItem(i.yazar)
            # item3.setForeground(rowColor)
            # self.tableBookGiven.setItem(row, 3, item3)

                    row+=1
                else:
                    item0 = QtWidgets.QTableWidgetItem(str(i.id))
            # item0.setForeground(rowColor)
                    item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableBookGiven.setItem(row, 0, item0)

                    item1 = QtWidgets.QTableWidgetItem(str(i.karekod_no))
            # item1.setForeground(rowColor)
                    item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
                    self.tableBookGiven.setItem(row, 1, item1)

                    item2 = QtWidgets.QTableWidgetItem(i.ad)
            # item2.setForeground(rowColor)
                    self.tableBookGiven.setItem(row, 2, item2)

            # item3 = QtWidgets.QTableWidgetItem(i.yazar)
            # # item3.setForeground(rowColor)
            # self.tableBookGiven.setItem(row, 3, item3)

                    row+=1

                self.tableBookGiven.setHorizontalHeaderLabels(self.tabloBooksTitleGiven)


    ## Üye Id tc ad soyad no arama yap
    def memberNoNameSearchFonkGiven(self):
        rowColor = QtGui.QColor(133, 174, 255)
        ## eğer arama yeri boş gelirse 
        ara = self.lineEditMemberGiven.text()

        if len(ara) > 0:
            # Kitap verme üye aramasında bazı ögeler aranmayacagı için secim index no da değişiklik yap
            cvr = {0:0, 1:1, 2:2, 3:3, 4:4, 5:7}
            katogoriIndex = self.searchFilterComboBoxMember.currentIndex()
            self.membersSearchListGiven = DbQuery.uyeIdAdSoyadTcNoAra(ara, 1000, cvr[katogoriIndex]) # arama limi 1000 bin olarak kısıtlandı

            ## sonuca göre tablodaki satır sayısını belirle
            if len(self.membersSearchListGiven) >= 50:
                self.tableMemberGiven.setRowCount(len(self.membersSearchListGiven))
            else:
                self.tableMemberGiven.setRowCount(50)

            self.tableMemberSearchResultsWriteGiven(self.membersSearchListGiven)

        else:
            self.tableMemberGiven.clear()
            self.tableMemberGiven.setHorizontalHeaderLabels(self.tabloMemberTitleGiven)

        # tablonun ilk satırına gitme
        item = self.tableMemberGiven.model().index(0, 0)
        self.tableMemberGiven.scrollTo(item)

    ## uyeListesi değişkeni üzerinde yer alan ögeleri tabloya yazma
    def tableMemberSearchResultsWriteGiven(self, uyeListesi: MemberModel):

        rowColor = QtGui.QColor(0, 0, 0)

        self.tableMemberGiven.clear()
        row = 0
        for i in uyeListesi:

           
            item0 = QtWidgets.QTableWidgetItem(str(i.id))
            item0.setForeground(rowColor)
            item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableMemberGiven.setItem(row, 0, item0)

            item1 = QtWidgets.QTableWidgetItem("" if i.tc == None else f"{str(i.tc)}")
            item1.setForeground(rowColor)
            item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableMemberGiven.setItem(row, 1, item1)

            item2 = QtWidgets.QTableWidgetItem(i.ad)
            item2.setForeground(rowColor)
            self.tableMemberGiven.setItem(row, 2, item2)

            item3 = QtWidgets.QTableWidgetItem(i.soyad)
            item3.setForeground(rowColor)
            self.tableMemberGiven.setItem(row, 3, item3)

            item6 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
            item6.setForeground(rowColor)
            item6.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableMemberGiven.setItem(row, 4, item6)

            row+=1
        
        self.tableMemberGiven.setHorizontalHeaderLabels(self.tabloMemberTitleGiven)


    # Günlük verilen kitapların listesini tabloya yaz
    def givenBookTableDailyResultWrite(self):
        if len(self.dailyGivenBooksList) >= 50: self.givenBookTableDaily.setRowCount(10000 if len(self.dailyGivenBooksList) >10000 else len(self.dailyGivenBooksList))

        else: self.givenBookTableDaily.setRowCount(50)
       
        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]        
        self.givenBookTableDaily.clear()
        row = 0
        for i in self.dailyGivenBooksList[0:10000]:

            item0 = QtWidgets.QTableWidgetItem(str(i.kitap_id))
            item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 0, item0)

            item1 = QtWidgets.QTableWidgetItem("" if i.karekod_no == None else f"{str(i.karekod_no)}")
            item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 1, item1)

            item2 = QtWidgets.QTableWidgetItem(i.kitap_adi)
            self.givenBookTableDaily.setItem(row, 2, item2)

            item3 = QtWidgets.QTableWidgetItem(i.yazar)
            self.givenBookTableDaily.setItem(row, 3, item3)

            item4 = QtWidgets.QTableWidgetItem(str(i.uye_id))
            item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 4, item4)

            item5 = QtWidgets.QTableWidgetItem("" if i.tc == None else str(i.tc))
            item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 5, item5)

            item6 = QtWidgets.QTableWidgetItem(str(i.uye_adi))
            self.givenBookTableDaily.setItem(row, 6, item6)

            item7 = QtWidgets.QTableWidgetItem(str(i.soyad))
            self.givenBookTableDaily.setItem(row, 7, item7)

            item8 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
            item8.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 8, item8)

            item9 = QtWidgets.QTableWidgetItem(f"{str(i.emanete_verilme_tarihi)}  {i.verilme_saati}")
            item9.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 9, item9)

            item10 = QtWidgets.QTableWidgetItem(str(i.son_teslim_tarihi))
            item10.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.givenBookTableDaily.setItem(row, 10, item10)

            row+=1
        
        self.givenBookTableDaily.setHorizontalHeaderLabels(title)

         


###################### Emanetten Kitap Alma (TakenWindow) Fonksiyonları ##################################

    # başlangıc fonksiyonları ve işlemleri mainWindow üzerinden çağrılacak yalnızca


    def initialFonkTakenWindow(self): # Başlangıcta çalıştır yalnızca
        start_time = time.time()
        self.dailyTakenBooksList = DbQuery.emanettenAlinanGunlukKitaplar(self.borrowingTime)
       # print(f"Emanetten alma sorgu süresi2: {(time.time() - start_time)}")
        self.dailyTakenBooksListResultWrite()

        #günlük alınan kitaplar tablosunun ilk satırına gitme
        item = self.takenBookTableDaily.model().index(0, 0)
        self.takenBookTableDaily.scrollTo(item)



    # günlük alınan kitaplar ve alıp verilen tüm kitaplar penceresini (stackedWidget) kontrol eden fonksiyon (stil kontrolü)
    def takenGivenWinOpenDailyStil(self):
        self.takenAlt.setCurrentIndex(0)
       
        self.takenDailyBookWinOpenBut.setStyleSheet("border: 1px solid #339af0; border-radius: 8px; color: black; height: 22px; font-weight: 300;")
        self.takenGivenHistoryWinOpenBut.setStyleSheet("border: 0px solid #339af0; border-radius: 8px; color: #666666; height: 22px; font-weight: 300;")

    
    # günlük alınan kitaplar ve alıp verilen tüm kitaplar penceresini (stackedWidget) kontrol eden fonksiyon (stil kontrolü)
    def takenGivenWinOpenAllStil(self):
  
        self.takenAlt.setCurrentIndex(1)
        
        self.takenDailyBookWinOpenBut.setStyleSheet("border: 0px solid #339af0; border-radius: 8px; color: #666666; height: 22px; font-weight: 300;")
        self.takenGivenHistoryWinOpenBut.setStyleSheet("border: 1px solid #339af0; border-radius: 8px; color: black; height: 22px; font-weight: 300;")


    # seçilen tarihlerde kitap alıp vere geçişinde arama yap
    def takenGivenBookHistoryViewFonk(self):
        date1 = self.dateedit1.date().toString(('yyyy-MM-dd'))
        date2 = self.dateedit2.date().toString(('yyyy-MM-dd'))
       # print("date1:", date1.toString(('yyyy-MM-dd')), "  date2: ", self.dateedit2.date().toString(('yyyy-MM-dd')))
        print(date1, date2)

        date = DbQuery.tumAlipVerilenKitaplariListele(self.borrowingTime, date1, date2)
       # print("date: ", date)

        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Teslim Tarihi"]
        # self.takenBookTableDaily.clear()

        self.takenGivenBookHistoryViewTable.clear()

        if len(date) >= 50: self.takenGivenBookHistoryViewTable.setRowCount(len(date))

        else: self.takenGivenBookHistoryViewTable.setRowCount(50)

        row = 0
        for i in date:
            
            item0 = QtWidgets.QTableWidgetItem(str(i.kitap_id))
            item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 0, item0)

            item1 = QtWidgets.QTableWidgetItem("" if i.karekod_no == None else f"{str(i.karekod_no)}")
            item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 1, item1)

            item2 = QtWidgets.QTableWidgetItem(i.kitap_adi)
            self.takenGivenBookHistoryViewTable.setItem(row, 2, item2)

            item3 = QtWidgets.QTableWidgetItem(i.yazar)
            self.takenGivenBookHistoryViewTable.setItem(row, 3, item3)

            item4 = QtWidgets.QTableWidgetItem(str(i.uye_id))
            item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 4, item4)

            item5 = QtWidgets.QTableWidgetItem("" if i.tc == None else str(i.tc))
            item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 5, item5)

            item6 = QtWidgets.QTableWidgetItem(str(i.uye_adi))
            self.takenGivenBookHistoryViewTable.setItem(row, 6, item6)

            item7 = QtWidgets.QTableWidgetItem(str(i.soyad))
            self.takenGivenBookHistoryViewTable.setItem(row, 7, item7)

            item8 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
            item8.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 8, item8)

            item9 = QtWidgets.QTableWidgetItem(f"{str(i.emanete_verilme_tarihi)}")
            item9.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 9, item9)

            item10 = QtWidgets.QTableWidgetItem(f"{'' if i.teslim_tarihi  == None else i.teslim_tarihi}")
            item10.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenGivenBookHistoryViewTable.setItem(row, 10, item10)

            row+=1
        
        self.takenGivenBookHistoryViewTable.setHorizontalHeaderLabels(title)



    # kitap transfer başarılı işlem bildirimi
    def messageFonkTaken(self):
       
        self.takenTimerSayac+= 2.5
        self.takenBut.setText("KİTAP ALINIYOR...")
        self.takenBut.setEnabled(False)

        value = (self.takenTimerSayac / 100)
        
        self.takenBut.setStyleSheet(f""" 
        background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, 
                    stop:{value - 0.001} rgba(71, 118, 230, 255), 
                    stop:{value} rgba(71, 118, 230, 255), 
                    stop:{value} rgba(250, 250, 250, 255) );
                border-radius: 8px;
        """)

        if self.takenTimerSayac == 100 : 
            self.takenTimerSayac = 0
            self.takenBut.setText("")
            self.takenBut.setStyleSheet("""  background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                                        stop:0 #57cc99, stop: 0.5 #80ed99, stop:1 #9ef01a); 
                                        background-image: url("assets/icon/check-mark.png");
                                        background-position: center center;
                                        background-repeat: no-repeat;
                                        """)
            
            self.timerTaken.stop()

            QTimer.singleShot(200, self.messageStopFonkTaken)
       

    def messageStopFonkTaken(self):
        self.takenBut.setStyleSheet("""background: qlineargradient(x1:0, y1:1, x2:1, y2:1,
                                        stop:0 #2682ff, stop: 0.4 #4f94f4, stop:1 #6aa9ff);""")
        self.takenBut.setEnabled(True)
        self.takenBut.setText("KİTABI GERİ AL")

        self.dailyTakenBooksListResultWrite() # Günlük alınan kitaplar listesi
        self.emanettekiKitapUyeNo() # Okuyan üyeler ve okunan kitaplar listesi (yalnızca id)
        self.tableBookSearchResultsWriteGiven(self.booksSearchListGiven) # Kitap verme sayfası kitap arama sonuçlarını yeniden yaz

       
    
    ## Kitabı emANETTTEN geri alma
    def bookTakenFonk(self):
        # secilen satır numaralarını al
        bookIndex = -1
        
        indexesB = self.tableBookTaken.selectionModel().selectedRows(column=0) # seçilen satırı döndür
        for index in indexesB:
            if index.data() != None: # eğer boş satır seçilirse 
                bookIndex = index.row()
            else:
                bookIndex = -1

        # satır seçilip secilmediğini kontrol et
        if bookIndex != -1:

            self.message = MessageBoxTaken(booksTaken=self.booksSearchListTaken[bookIndex], buttonHeight=self.h4)
            self.message.okeyBut.clicked.connect(lambda: self.bookTakenFonk2(self.booksSearchListTaken[bookIndex].rowID, bookIndex))

            self.message.exec()

        else:
            MessageBOX("UYARI", " Öncelikle listeden  bir kitap seçniz", 0) 

    def bookTakenFonk2(self, rowID, index):

        self.start_time = time.time()

        result = DbQuery.kitabiTeslimAl(rowID=rowID)

        # kitap secim indexi teslim alındıktan sonra kitabı listeden silmek için kullanılacak liste.pop(kitap secim index)
        if result == 1:
            self.timerTaken.start(20)

            alinan_kitap = self.booksSearchListTaken[index]
            self.dailyTakenBooksList.insert(0,
                                            GivenTakenBookModel(
                                                rowid=rowID,
                                                kitap_id=alinan_kitap.kitap_id,
                                                karekod=alinan_kitap.karekod_no,
                                                kitap_adi=alinan_kitap.kitap_adi,
                                                yazar=alinan_kitap.yazar,
                                                uye_id=alinan_kitap.uye_id,
                                                tc=alinan_kitap.tc,
                                                uye_adi=alinan_kitap.uye_adi,
                                                soyadi=alinan_kitap.soyad,
                                                ogrenci_no=alinan_kitap.ogrenci_no,
                                                emanete_verilme_tarihi=alinan_kitap.emanete_verilme_tarihi,
                                                teslim_tarihi=alinan_kitap.teslim_tarihi,
                                                teslim_suresi=self.borrowingTime,
                                                son_teslim_tarihi=alinan_kitap.son_teslim_tarihi,
                                                verilme_saati=alinan_kitap.verilme_saati,
                                                teslim_saati=QDateTime.currentDateTime().toString('hh:mm:ss')
                                            )
                                        )


            #günlük alınan kitaplar tablosunun ilk satırına gitme
            item = self.takenBookTableDaily.model().index(0, 0)
            self.takenBookTableDaily.scrollTo(item)

          # 3. Geri alınan kitabı aranan kitaplar(teslim süsersi gecenler ve emanetteki tüm kitaplar listesi) listesinden cıkar.
             # Bu liste bookListTaken: GivenTakenModel listesidir.
             # Amaç anlık olarak listeden cıkarılmasını sağlamak. Kullanıcı yeniden arama yapınca ya da emanetteki kitapları listeleyince zaten ögeler db den alınacak.

           # print("tip:", type(self.booksSearchListTaken))
            self.booksSearchListTaken.pop(index)
            ## sonuca göre tablodaki satır sayısını belirle
            self.tableBookTaken.clear()
            if len(self.booksSearchListTaken) >= 50:
                self.tableBookTaken.setRowCount(len(self.booksSearchListTaken))
            else:
                self.tableBookTaken.setRowCount(50)

            self.tableBookSearchResultsWriteTaken()
            title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
            self.tableBookTaken.setHorizontalHeaderLabels(title)

            print("Emanetten kitap alma süresi: ", time.time() - self.start_time)

        elif result == 2:
            MessageBOX("UYARI", " Veritabanı hatası oluştu.", 0)
        else:
            MessageBOX("UYARI", " Bilinmeyen bir hata oluştu.", 0)

    
    #Teslim etme Süresi geçen kitaplar
    def timeOutBookList(self):
        
        self.tableBookTaken.clear()
        
        self.booksSearchListTaken = DbQuery.emanetSuresiGecmisKitaplar(self.borrowingTime)
        
        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.booksSearchListTaken) >= 50:
            self.tableBookTaken.setRowCount(len(self.booksSearchListTaken))
        else:
            self.tableBookTaken.setRowCount(50)
        self.tableBookSearchResultsWriteTaken()
    
        
        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]

        self.tableBookTaken.setHorizontalHeaderLabels(title)

        # tablonun ilk satırına gitme
        item = self.tableBookTaken.model().index(0, 0)
        self.tableBookTaken.scrollTo(item)


    #Tüm emanettekiler
    def borrewedAllList(self):
        
        self.tableBookTaken.clear()
        
        self.booksSearchListTaken = DbQuery.emaneteVerilenTumKitaplar(self.borrowingTime)
        
        
        ## sonuca göre tablodaki satır sayısını belirle
        if len(self.booksSearchListTaken) >= 50:
            self.tableBookTaken.setRowCount(len(self.booksSearchListTaken))
        else:
            self.tableBookTaken.setRowCount(50)
        self.tableBookSearchResultsWriteTaken()
    
        
        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]

        self.tableBookTaken.setHorizontalHeaderLabels(title)

        item = self.tableBookTaken.model().index(0, 0)
        self.tableBookTaken.scrollTo(item)


    ## Kitap no ve kitap adı ara
    def bookNoNameSearchFonkTaken(self):
        # Arama yapılacağı zaman QradioButtonlardaki seçimi kaldır(Emanetteki kitapları listele ve süresi gecmiş kitaplarını listele butonu)
        self.borrewedAllBooksListBut.setAutoExclusive(False)
        self.timeOutBooksListBut.setAutoExclusive(False)
        self.borrewedAllBooksListBut.setChecked(False)
        self.timeOutBooksListBut.setChecked(False)
        self.borrewedAllBooksListBut.setAutoExclusive(True)
        self.timeOutBooksListBut.setAutoExclusive(True)
        
        print("kitap alınma arama", self.borrewedAllBooksListBut.isChecked(), self.timeOutBooksListBut.isChecked())

        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
        ## eğer arama yeri boş gelirse 
        ara =  self.lineEditBooksTaken.text()
        if len(ara) > 0:
            katogori = self.searchFilterComboBoxTaken.currentIndex()

            self.booksSearchListTaken = DbQuery.emaneteVerilenKitaplardaAra(ara, self.borrowingTime, katagori=katogori)
          
            ## sonuca göre tablodaki satır sayısını belirle
            if len(self.booksSearchListTaken) >= 50:
                self.tableBookTaken.setRowCount(len(self.booksSearchListTaken))
            else:
                self.tableBookTaken.setRowCount(50)

            self.tableBookSearchResultsWriteTaken()

        else:
            self.tableBookTaken.clear()

            self.tableBookTaken.setHorizontalHeaderLabels(title)

        # tablonun ilk satırına gitme
        item = self.tableBookTaken.model().index(0, 0)
        self.tableBookTaken.scrollTo(item)



    ## Kitap arama sonuçlarını tabloya yaz
    def tableBookSearchResultsWriteTaken(self):

        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Son Teslim T."]
        self.tableBookTaken.clear()
        row = 0
        for i in self.booksSearchListTaken:

            item0 = QtWidgets.QTableWidgetItem(str(i.kitap_id))
            item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 0, item0)

            item1 = QtWidgets.QTableWidgetItem("" if i.karekod_no == None else f"{str(i.karekod_no)}")
            item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 1, item1)

            item2 = QtWidgets.QTableWidgetItem(i.kitap_adi)
            self.tableBookTaken.setItem(row, 2, item2)

            item3 = QtWidgets.QTableWidgetItem(i.yazar)
            self.tableBookTaken.setItem(row, 3, item3)

            item4 = QtWidgets.QTableWidgetItem(str(i.uye_id))
            item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 4, item4)

            item5 = QtWidgets.QTableWidgetItem("" if i.tc == None else str(i.tc))
            item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 5, item5)

            item6 = QtWidgets.QTableWidgetItem(str(i.uye_adi))
            self.tableBookTaken.setItem(row, 6, item6)

            item7 = QtWidgets.QTableWidgetItem(str(i.soyad))
            self.tableBookTaken.setItem(row, 7, item7)

            item8 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
            item8.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 8, item8)

            item9 = QtWidgets.QTableWidgetItem(f"{str(i.emanete_verilme_tarihi)}  {i.verilme_saati}")
            item9.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 9, item9)

            item10 = QtWidgets.QTableWidgetItem(str(i.son_teslim_tarihi))
            item10.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.tableBookTaken.setItem(row, 10, item10)

            row+=1
        
        self.tableBookTaken.setHorizontalHeaderLabels(title)



    ## Emanetten gelen günlük kitap listesini gunluk tabloya yaz
    def dailyTakenBooksListResultWrite(self):
        title = ["Kitap ID", "Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Ü. Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Üyeye Verilme T.", "Dönüş Saati"]
        # self.takenBookTableDaily.clear()

        if len(self.dailyTakenBooksList) >= 50:
            self.takenBookTableDaily.setRowCount(10000 if len(self.dailyTakenBooksList) >10000 else len(self.dailyTakenBooksList))

        else:
            self.takenBookTableDaily.setRowCount(50)


        row = 0
        for i in self.dailyTakenBooksList[0:10000]:

            item0 = QtWidgets.QTableWidgetItem(str(i.kitap_id))
            item0.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 0, item0)

            item1 = QtWidgets.QTableWidgetItem("" if i.karekod_no == None else f"{str(i.karekod_no)}")
            item1.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 1, item1)

            item2 = QtWidgets.QTableWidgetItem(i.kitap_adi)
            self.takenBookTableDaily.setItem(row, 2, item2)

            item3 = QtWidgets.QTableWidgetItem(i.yazar)
            self.takenBookTableDaily.setItem(row, 3, item3)

            item4 = QtWidgets.QTableWidgetItem(str(i.uye_id))
            item4.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 4, item4)

            item5 = QtWidgets.QTableWidgetItem("" if i.tc == None else str(i.tc))
            item5.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 5, item5)

            item6 = QtWidgets.QTableWidgetItem(str(i.uye_adi))
            self.takenBookTableDaily.setItem(row, 6, item6)

            item7 = QtWidgets.QTableWidgetItem(str(i.soyad))
            self.takenBookTableDaily.setItem(row, 7, item7)

            item8 = QtWidgets.QTableWidgetItem("" if i.ogrenci_no == None else str(i.ogrenci_no))
            item8.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 8, item8)

            item9 = QtWidgets.QTableWidgetItem(f"{str(i.emanete_verilme_tarihi)}  {i.verilme_saati}")
            item9.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 9, item9)

            item10 = QtWidgets.QTableWidgetItem(str(i.teslim_saati))
            item10.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
            self.takenBookTableDaily.setItem(row, 10, item10)

            row+=1
        
        self.takenBookTableDaily.setHorizontalHeaderLabels(title)



    ## Emnate verilen kitapları ve süresi geçmiş kitapları xelsx olarak aktar
    def tabloXlsxAktarFonk(self):
        # Radio BUtonların seçimine göre rapor ekranının başlığını ve dosya ismini belirle
        if self.borrewedAllBooksListBut.isChecked():
            self.tabloXlsxFonk("ÜYELERE EMANET VERİLMİŞ TÜM KİTAPLARIN LİSTESİ", "Emanete verilmiş kitaplar")
        elif self.timeOutBooksListBut.isChecked():
            self.tabloXlsxFonk("TESLİM SÜRESİ GEÇMİŞ KİTAPLARIN LİSTESİ", "Teslim süresi geçmiş kitaplar")
        elif self.borrewedAllBooksListBut.isChecked() == False and self.timeOutBooksListBut.isChecked() == False:
            self.tabloXlsxFonk("ÜYELERE EMANET VERİLMİŞ TÜM KİTAPLARIN LİSTESİ", "Emanete verilmiş kitaplar")

    # Emanetteki kitaplar ve süresi geçmiş olanları liste olarak dışarı aktar
    def tabloXlsxFonk(self, title, fileName):

        kurumBilgileri = DbQuery.ayarlarVeYapilandirmalar()
        date = QDate.currentDate()
        sorgu_tarihi = date.toString('dd.MM.yyyy')
        

        if len(self.booksSearchListTaken) > 0:
            ## modeli listeye aktar
            home_dir = str(Path.home())

            tablo = ["SIRA", "KAREKOD", "KİTAP ADI", "ÜYE ADI", "SOYADI", "ÜYE NO", "ALINMA TARİHİ", "SON T. TARİHİ"]
        
            fnamePath = QFileDialog.getSaveFileName(self, home_dir, fileName, ".xlsx")
            print(fnamePath, home_dir, bool(fnamePath))

            if bool(fnamePath[0]) == True:

                thin_border = Border(
                            left=Side(border_style=BORDER_THIN, color='00000000'),
                            right=Side(border_style=BORDER_THIN, color='00000000'),
                            top=Side(border_style=BORDER_THIN, color='00000000'),
                            bottom=Side(border_style=BORDER_THIN, color='00000000')
                            )

                work_book = Workbook()  
                work_sheet = work_book.active  
                work_sheet.page_setup.orientation = work_sheet.ORIENTATION_PORTRAIT
                work_sheet.page_setup.paperSize = work_sheet.PAPERSIZE_A4
                work_sheet.sheet_properties.pageSetUpPr.fitToPage = True # sayfayı genişliğe sığdır
                work_sheet.page_setup.fitToHeight = False
                work_sheet.page_setup.fitToWidth = True
                work_sheet.page_margins.left = 0.2
                work_sheet.page_margins.right = 0.1
                work_sheet.page_margins.top = 0.1
                work_sheet.page_margins.bottom = 0.1
               
                work_sheet.column_dimensions['A'].width = 8 # sütun genişlikleri
                work_sheet.column_dimensions['B'].width = 11 # sütun genişlikleri
                work_sheet.column_dimensions['C'].width = 50
                work_sheet.column_dimensions['D'].width = 18
                work_sheet.column_dimensions['E'].width = 12
                work_sheet.column_dimensions['F'].width = 12
                work_sheet.column_dimensions['G'].width = 16
                work_sheet.column_dimensions['H'].width = 16

                work_sheet.merge_cells('A1:H2')  # hücre birleştirme
  
            # kurum bilgileri ve kurum adı
                work_sheet.row_dimensions[1].height = 20
                work_sheet.row_dimensions[2].height = 25
                cell = work_sheet.cell(row=1, column=1)  
                cell.value = f"{kurumBilgileri.kurum_adi}\n {title}"
                cell.font = Font(bold=True, size=15) 
                cell.alignment = Alignment(horizontal='center', vertical='center', wrapText=True) 

            ## tablonun başlıkları
                titleColumn = 1
                work_sheet.row_dimensions[3].height = 30
                for title_ in tablo: # tablo başlığını yaz
                    baslik = work_sheet.cell(row=3, column=titleColumn)  
                    baslik.value = f"{title_}"  
                    baslik.font = Font(bold=True, size=10)
                    baslik.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                    titleColumn+=1


                titleRow = 4
                
                siraNumarasi = 1
                for kitaplar in self.booksSearchListTaken:
                   # print("bookListTaken", kitaplar)
                    work_sheet.row_dimensions[titleRow].height = 25

                    siraNo = work_sheet.cell(row=titleRow, column=1)  
                    siraNo.font = Font(bold=True, size=10)
                    siraNo.value = f"{siraNumarasi}"  
                    siraNo.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True) 
                    siraNo.border = thin_border

                    karekodNo = work_sheet.cell(row=titleRow, column=2)  
                    karekodNo.font = Font(bold=False, size=10)
                    karekodNo.value = f"{kitaplar.karekod_no}"  
                    karekodNo.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True) 
                    karekodNo.border = thin_border

                    kitapAdi = work_sheet.cell(row=titleRow, column=3) 
                    kitapAdi.font = Font(bold=False, size=10) 
                    kitapAdi.value = f"{kitaplar.kitap_adi}"  
                    kitapAdi.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True) 
                    kitapAdi.border = thin_border

                    uyeAdi = work_sheet.cell(row=titleRow, column=4)  
                    uyeAdi.font = Font(bold=False, size=10)
                    uyeAdi.value = f"{kitaplar.uye_adi}"  
                    uyeAdi.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True)  
                    uyeAdi.border = thin_border

                    soyadi = work_sheet.cell(row=titleRow, column=5)  
                    soyadi.font = Font(bold=False, size=10)
                    soyadi.value = f"{kitaplar.soyad}"  
                    soyadi.alignment = Alignment(horizontal='left', vertical='center', justifyLastLine=True, wrapText=True)  
                    soyadi.border = thin_border

                    ogrenciNo = work_sheet.cell(row=titleRow, column=6) 
                    ogrenciNo.font = Font(bold=False, size=10) 
                    ogrenciNo.value = f"""{"" if kitaplar.ogrenci_no == None else f"{kitaplar.ogrenci_no}"}"""  
                    ogrenciNo.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True) 
                    ogrenciNo.border = thin_border 

                    verilmeTarihi = work_sheet.cell(row=titleRow, column=7)  
                    verilmeTarihi.font = Font(bold=False, size=10)
                    verilmeTarihi.value = f"{kitaplar.emanete_verilme_tarihi}"  
                    verilmeTarihi.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                    verilmeTarihi.border = thin_border

                    teslimTarihi = work_sheet.cell(row=titleRow, column=8)  
                    teslimTarihi.font = Font(bold=False, size=10)
                    teslimTarihi.value = f"{kitaplar.son_teslim_tarihi}"  
                    teslimTarihi.alignment = Alignment(horizontal='center', vertical='center', justifyLastLine=True, wrapText=True)  
                    teslimTarihi.border = thin_border

                    titleRow+=1

                    siraNumarasi+=1


                work_sheet.merge_cells(f"G{titleRow + 1}:H{titleRow + 1}")  # hücre birleştirme ( kitap listesini yazdıktan sonra 1 satır boşluk bırakıp kurum yöneticini yaz)
                work_sheet.row_dimensions[titleRow].height = 25

                work_sheet.row_dimensions[titleRow + 1].height = 50
                cell2 = work_sheet.cell(row=titleRow + 1, column=7)  
                cell2.value = f"{sorgu_tarihi}\n{kurumBilgileri.kurum_yoneticisi}\nKurum Yöneticisi"  
                cell2.font = Font(bold=True, size=10)
                cell2.alignment = Alignment(horizontal='center', vertical='center', wrapText=True)


                work_book.save(f"{fnamePath[0]}.xlsx")
                print(("tabloya yazdık"))
                MessageBOX("BİLGİLENDİRME", " Tablodaki içerikler .xlsx dosyasına başarılı bir şekilde aktarıldı.", 1)

            else:
                print("dosya yolu alınamadı")
                pass

        else:
            MessageBOX("UYARI", " Aktarılacak veri bulunmadı", 0)
            print("öncelikle üye listele")

## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()


    def restart(self):
        QtCore.QCoreApplication.quit()
        QtCore.QProcess.startDetached(sys.executable, sys.argv)




       


