
from PyQt6.QtGui import QColor
from PyQt6.QtWidgets import QGraphicsDropShadowEffect


class BoxShadowEffect():

    def colorBlue():
        efect = QGraphicsDropShadowEffect()
        efect.setBlurRadius(10)
        efect.setOffset(0,0)
        efect.setColor(QColor(124,140,180))
        return efect

    
    def colorBlueDark():
        efect = QGraphicsDropShadowEffect()
        efect.setBlurRadius(10)
        efect.setOffset(0,0)
        efect.setColor(QColor(0,139,255))
        return efect
    
    
    def colorTeal():
        efect = QGraphicsDropShadowEffect()
        efect.setBlurRadius(15)
        efect.setOffset(0,0)
        efect.setColor(QColor(108,138,139))
        return efect

    def colorTealDark():
        efect = QGraphicsDropShadowEffect()
        efect.setBlurRadius(10)
        efect.setOffset(0,0)
        efect.setColor(QColor(42,196,199))
        return efect


    def colorRed():
        efect = QGraphicsDropShadowEffect()
        efect.setBlurRadius(10)
        efect.setOffset(0,0)
        efect.setColor(QColor(255,133,126))
        return efect

   

    