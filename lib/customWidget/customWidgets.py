from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import Qt, QPoint
from PyQt6.QtGui import *
from PyQt6.QtWidgets import *


class CustomButton(QCheckBox):
    def __init__(self,
        width_ = 60,
        background_color = QColor(210, 210, 210),
        circle_color = QColor(245,245,245),
        active_background_color = QColor(0, 226,166),
        active_circle_color = QColor(255, 255, 255),
    ):
        QPushButton.__init__(self)
      
       
        self.width_ = width_
        self.background_color = background_color
        self.circle_color = circle_color
        self.active_background_color = active_background_color
        self.active_circle_color = active_circle_color

        self.setFixedSize(self.width_, int(self.width_ / 2))
        
        self.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
         
        self.setContentsMargins(0,0,0,0)
        self.setSizePolicy(QtWidgets.QSizePolicy.Policy.Ignored,
                            QtWidgets.QSizePolicy.Policy.Ignored)

        self.setStyleSheet("background-color: transparent; border: none; color: transparent;")

    #     self.stateChanged.connect(self.durum)

    # def durum(self):
    #     print(self.isChecked())

    # tıklama alanını genişlet
    def hitButton(self, pos= QPoint):
        return  self.contentsRect().contains(pos)

    def setFixedWidth(self, value):
        self.width_ = value
        self.setFixedSize(self.width_, int(self.width_ / 2))
        self.update()

       
    def paintEvent(self, e):

        p = QPainter()
        p.begin(self)
        p.setRenderHint(QPainter.RenderHint.Antialiasing)
        p.setPen(Qt.PenStyle.NoPen)

        if not self.isChecked():
            p.setBrush(self.background_color)
            p.drawRoundedRect(0, 0, self.width_, int(self.width_ / 2), float(self.width_ / 4) , float(self.width_ / 4))

            p.setBrush(self.circle_color)
            p.drawRoundedRect(3, 3, int(self.width_ / 2) - 6, int(self.width_ / 2) - 6, float(self.width_ / 2), float(self.width_ / 2))
        else:
            p.setBrush(self.active_background_color)
            p.drawRoundedRect(0, 0, self.width_, int(self.width_ / 2), float(self.width_ / 4) , float(self.width_ / 4))

            p.setBrush(self.active_circle_color)
            p.drawRoundedRect(int(self.width_ / 2) + 3, 3, int(self.width_ / 2) - 6, int(self.width_ / 2) - 6, float(self.width_ / 2), float(self.width_ / 2))


        p.end()






class CustomListTile(QFrame):
    def __init__(self,
            height=30,
            label1="1",
            label2="Sınıflar",
            label3="Yükseltme sonrası yeni sınıf seviyesi",
            label4="4",
            durum=1
    ):
        super().__init__()


        self.setContentsMargins(0,0,0,0)
        self.setFixedHeight(height)
        self.setStyleSheet("""background-color: transparent;
                            border-radius: 8px;

                            """)

        self.setSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Expanding)

        self.layout = QHBoxLayout(self)
        self.layout.setContentsMargins(0,0,0,0)

        label1 = QLabel(label1)
        label1.setSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Expanding)
        label1.setFixedSize(55, height)
        label1.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignVCenter)

        if durum == 1:
            label1.setStyleSheet("""background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                            stop:0 #339af0, stop: 0.5 #66b6f8, stop:1 #dae4fd);
                            border-radius: 8px;
                            color: white;
                            font-size: 22px;
                            font-weight: 800;
                            """)
        else:
            label1.setStyleSheet("""background: qlineargradient(x1:0, y1:0, x2:1, y2:1,
                            stop:0 #fd4c0b, stop: 0.5 #fe7948, stop:1 #ffa482);
                            border-radius: 8px;
                            color: white;
                            font-size: 22px;
                            font-weight: 800;
                            """)

        label2 = QLabel(label2)
        label2.setFixedHeight(height)
        label2.setStyleSheet("""background-color: transparent; border: none; font-size: 17px; font-weight: 500; """)

        label3 = QLabel(label3)
        label3.setFixedHeight(height)
        label3.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)
        label3.setStyleSheet("""background-color: transparent; border: none; font-size: 17px; font-weight: 500; """)

        label4 = QLabel(label4)
        label4.setFixedSize(55, height)
        label4.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignVCenter)
        label4.setStyleSheet("""background-color: transparent; border: none; font-size: 25px; font-weight: 800; color: #339af0; """)

        label5 = QLabel("")
        label5.setFixedSize(55, height)
        label5.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)

        if durum == 1:
            label5.setStyleSheet("""background-image: url("assets/icon/arrow-up.png");
                                background-position: left;
                                background-repeat: no-repeat;
                                background-color: transparent;""")
        else:
            label5.setStyleSheet("""background-image: url("assets/icon/delete-friend.png");
                                background-position: left;
                                background-repeat: no-repeat;
                                background-color: transparent;""")
        
        self.layout.addWidget(label1)
        self.layout.addWidget(label2)
        self.layout.addWidget(label3)
        self.layout.addWidget(label4)
        self.layout.addWidget(label5)



class ItemWidget(QGridLayout):
    def __init__(self, title_, subtitle_, durum=False):
        super().__init__()

        # self.setSizePolicy(QSizePolicy.Policy.Maximum, QSizePolicy.Policy.Minimum)
       # self.setStyleSheet("background-color: transparent; color: black;")

        self.durum = durum

        self.setContentsMargins(8, 4, 8, 4)
        self.setSpacing(0)
        self.setHorizontalSpacing(15)
        

        title = QLabel(f"{title_}")
        subtitle = QLabel(f"{subtitle_}")
        self.durumWidget = CustomButton(width_=55, active_background_color= QColor(93, 175, 243))
        self.durumWidget.setChecked(self.durum)

       # durumWidget.clicked.connect(self.durum)

        # title.setStyleSheet("font-weight: 600px; color: #4d4d4d;")
        # subtitle.setStyleSheet("font-weight: 100px; color: #b3b3b3;") 

        title.setObjectName("titleCustom")
        subtitle.setObjectName("subtitleCustom")

        subtitle.setWordWrap(True)
        
     #   self.durumWidget.clicked.connect(lambda: self.checked_)

        self.addWidget(title,             0, 0, 1, 6)
        self.addWidget(subtitle,          1, 0, 3, 6)
        self.addWidget(self.durumWidget,  0, 6, 4, 1)

    def isChecked_(self)->bool:
        return self.durumWidget.isChecked()

   

    def setChecked_(self, chacked: bool):
        self.durumWidget.setChecked(chacked)
        
    

    def setEnabled_(self, enabled: bool):
        self.durumWidget.setEnabled(False)



# class CustomListWidget(QWidget):
#     def __init__(self, oge, deger):
#         super().__init__()
#
#         self.setContentsMargins(0,0,0,0)
#
#         self.setFixedHeight(75)
#
#         self.layout = QHBoxLayout()
#         # self.layout.setContentsMargins(0,0,0,0)
#
#         self.label1 = QLabel(oge)
#         self.label2 = QLabel(deger)
#
#         self.label2.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
#         self.label1.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
#
#         self.label1.setStyleSheet(""" font-size: 16px; font-weight: 500; color: #686868; """)
#         self.label2.setStyleSheet(""" font-size: 18px; font-weight: 700; color: #00b9c1; """)
#
#         self.label2.setFixedWidth(50)
#         self.label2.setFixedHeight(75)
#
#
#         self.layout.addWidget(self.label2)
#         self.layout.addWidget(self.label1)
#
#         self.setLayout(self.layout)

# class MainWindow(QMainWindow):
#     def __init__(self):
#         super().__init__()

#       #  self.setMinimumSize(300,300)

#         self.widget  = QWidget()
#         self.layout = QGridLayout(self.widget)

#         self.setCentralWidget(self.widget)

#         self.buton1 = CustomButton(width_=55)
#         self.buton2 = CustomButton()

#         self.buton1.setObjectName("buton1")

#         self.layout.addWidget(self.buton1)
#         self.layout.addWidget(self.buton2)

#        # self.buton1.stateChanged.connect(self.checkButton)
#         self.buton1.clicked.connect(self.checkButton)

#     def checkButton(self, olay):
#         print("Object: ", self.sender().objectName())
#         print("Check: ", olay)
        

# if  __name__=="__main__":
#     app = QApplication(sys.argv)
#     screen = MainWindow()
#     screen.show()
#     sys.exit(app.exec())