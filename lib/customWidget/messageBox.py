from PyQt6 import QtCore
from PyQt6.QtCore import Qt, QTimer
from PyQt6.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QPushButton, QLabel, QFrame, QWidget, QGridLayout, \
    QSpacerItem, QSizePolicy

from models.model import MemberModel, BookModel, GivenTakenBookModel
from customWidget.shadowEffect import BoxShadowEffect

with open("./assets/stil/stil_messageBox.qss") as file:
    stil = file.read()

# Kitap alıp verme onay pencereleri
class MessageBoxGiven(QDialog):
    def __init__(self, borrowingTime, deadline, book: BookModel, member: MemberModel, buttonHeight):
        super().__init__()

        self.book = book
        self.member = member
        self.borrowingTime = borrowingTime
        self.deadline = deadline
        self.h1 = buttonHeight

        self.setStyleSheet(stil)
        self.show()

        self.setContentsMargins(0,0,0,0)
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)
      #  self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal) # Pencere odak noktasını değiştirme

        # layout
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        self.setLayout(layout)

        self.mainFrame = QFrame()

        layout.addWidget(self.mainFrame)

        self.mainGridLayout = QGridLayout(self.mainFrame)


        self.title = QLabel("KİTAP TESLİM ONAYI")
        self.iconLabel = QLabel("")
        self.bookLabel = QLabel(f"{self.book.ad}")
        self.memberLabel = QLabel(f"{self.member.ad} {self.member.soyad}")
        self.borrowingTimeLabel = QLabel(f"{self.borrowingTime} gün süreyle")
        self.deadlineLabel = QLabel(f"{self.deadline} tarihine kadar     ")
        self.subtitle = QLabel("Kitap üyeye verilecek, işleme devam edilsin mi?")
        self.okeyBut = QPushButton("    Evet    ")
        self.cancelBut = QPushButton("    İptal    ")

        self.title.setObjectName("title")
        self.iconLabel.setObjectName("iconTakenGiven")
        self.bookLabel.setObjectName("book")
        self.memberLabel.setObjectName("member")
        self.borrowingTimeLabel.setObjectName("borrowing")
        self.deadlineLabel.setObjectName("deadline")
        self.subtitle.setObjectName("subtitle")
        self.okeyBut.setObjectName("okey")
        self.cancelBut.setObjectName("cancel")

        self.bookLabel.setMinimumHeight(40)
        self.memberLabel.setMinimumHeight(40)
        self.borrowingTimeLabel.setFixedHeight(22)
        self.deadlineLabel.setFixedHeight(22)
        self.okeyBut.setFixedHeight(self.h1 + 3)
        self.subtitle.setFixedHeight(30)
        self.cancelBut.setFixedHeight(self.h1 + 3)
        self.iconLabel.setFixedSize(120, 110)

        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.subtitle.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.okeyBut.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)
        self.cancelBut.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)

        self.bookLabel.setWordWrap(True)
        self.memberLabel.setWordWrap(True)

        self.mainGridLayout.addWidget(self.title,               0, 0, 1, 8)
        self.mainGridLayout.addItem(QSpacerItem(10, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 1, 0, 1, 8)
        self.mainGridLayout.addWidget(self.iconLabel,           2, 0, 3, 2)
        self.mainGridLayout.addWidget(self.bookLabel,           2, 2, 1, 6)
        self.mainGridLayout.addWidget(self.memberLabel,         3, 2, 1, 6)
        self.mainGridLayout.addItem(QSpacerItem(10, 6, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 4, 0, 1, 6)
        self.mainGridLayout.addWidget(self.borrowingTimeLabel,  5, 2, 1, 3)
        self.mainGridLayout.addWidget(self.deadlineLabel,       5, 5, 1, 3)
        self.mainGridLayout.addItem(QSpacerItem(10, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 6, 0, 1, 8)
        self.mainGridLayout.addWidget(self.subtitle,            7, 0, 1, 8)
        self.mainGridLayout.addItem(QSpacerItem(10, 12, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 8, 0, 1, 8)
        self.mainGridLayout.addWidget(self.cancelBut,           9, 4, 1, 2)
        self.mainGridLayout.addWidget(self.okeyBut,             9, 6, 1, 2)


        # slot
        self.cancelBut.clicked.connect(self.close)

        self.okeyBut.clicked.connect(self.close)

        # Mesaj kutusu 15 sn sonra kapansın
        QTimer.singleShot(20000, lambda: self.close())


class MessageBoxTaken(QDialog):
    def __init__(self, booksTaken: GivenTakenBookModel, buttonHeight):
        super().__init__()

        self.booksTaken = booksTaken
        self.h1 = buttonHeight

        self.setStyleSheet(stil)
        self.show()

        self.setContentsMargins(0,0,0,0)
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)
        #  self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal) # Pencere odak noktasını değiştirme

        # layout
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0,0,0,0)
        self.setLayout(layout)

        self.mainFrame = QFrame()

        layout.addWidget(self.mainFrame)

        self.mainGridLayout = QGridLayout(self.mainFrame)


        self.title = QLabel("KİTABI GERİ ALMA ONAYI")
        self.iconLabel = QLabel("")
        self.bookLabel = QLabel(f"{self.booksTaken.kitap_adi}")
        self.memberLabel = QLabel(f"{self.booksTaken.uye_adi} {self.booksTaken.soyad}")
       # self.noReadingBut = CustomButton(width_=46, active_background_color = QColor(255, 7, 0))
       # self.noReadingLabel = QLabel("Okunmamış olarak geri al (Kayıtlardan silinecek)")
        self.subtitle = QLabel("       Kitap üyeden geri alınacak, işleme devam edilsin mi?       ")
        self.okeyBut = QPushButton("    Evet    ")
        self.cancelBut = QPushButton("    İptal    ")

        self.title.setObjectName("title")
        self.iconLabel.setObjectName("iconTakenGiven")
        self.bookLabel.setObjectName("book")
        self.memberLabel.setObjectName("member")
       # self.noReadingLabel.setObjectName("noReadingLabel")
        self.subtitle.setObjectName("subtitle")
        self.okeyBut.setObjectName("okey")
        self.cancelBut.setObjectName("cancel")

        self.bookLabel.setMinimumHeight(40)
        self.memberLabel.setMinimumHeight(40)

        self.okeyBut.setFixedHeight(self.h1 + 3)
        self.subtitle.setFixedHeight(30)
        self.cancelBut.setFixedHeight(self.h1 + 3)
        self.iconLabel.setFixedSize(120, 110)
       # self.noReadingBut.setFixedSize(55, self.h1 + 3)

        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.subtitle.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.okeyBut.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)
        self.cancelBut.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Fixed)

        self.bookLabel.setWordWrap(True)
        self.memberLabel.setWordWrap(True)
       # self.noReadingLabel.setWordWrap(True)

        self.mainGridLayout.addWidget(self.title,               0, 0, 1, 8)
        self.mainGridLayout.addItem(QSpacerItem(10, 14, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 1, 0, 1, 8)
        self.mainGridLayout.addWidget(self.iconLabel,           2, 0, 3, 2)
        self.mainGridLayout.addWidget(self.bookLabel,           2, 2, 1, 6)
        self.mainGridLayout.addWidget(self.memberLabel,         3, 2, 1, 6)
        self.mainGridLayout.addItem(QSpacerItem(10, 4, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 4, 0, 1, 6)
       # self.mainGridLayout.addWidget(self.noReadingBut,        5, 2, 1, 1,  QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)
       # self.mainGridLayout.addWidget(self.noReadingLabel,      5, 3, 1, 5)
        self.mainGridLayout.addItem(QSpacerItem(10, 12, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 6, 0, 1, 8)
        self.mainGridLayout.addWidget(self.subtitle,            7, 0, 1, 8)
        self.mainGridLayout.addItem(QSpacerItem(10, 8, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 8, 0, 1, 8)
        self.mainGridLayout.addWidget(self.cancelBut,           9, 4, 1, 2)
        self.mainGridLayout.addWidget(self.okeyBut,             9, 6, 1, 2)


        # slot


        self.okeyBut.clicked.connect(self.close)
        self.cancelBut.clicked.connect(self.close)

        # Mesaj kutusu 15 sn sonra kapansın
        QTimer.singleShot(20000, lambda: self.close())





class MessageBOX(QWidget):
    def __init__(self, title="UYARI", subtitle = "", tip=0):
        super().__init__()

        self.baslik = title
        self.icerik = subtitle
        self.tip = tip #0 ise uyarı, 1 ise başarılı işlem ikonu göster

        ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal) # Pencere odak noktasını değiştirme

        qr = self.frameGeometry()
        cp = self.screen().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

        self.setFixedSize(500, 250)


        self.frame = QFrame(self)
        self.frame.setFixedSize(500,250)

        self.layout = QHBoxLayout(self.frame)

        self.icon = QLabel()
       # self.icon.setObjectName("icon")
        self.icon.setFixedSize(110, 110)

        
        self.layoutRight = QVBoxLayout()

        self.title = QLabel(self.frame)
        self.subTitle = QLabel(self.icerik)
        self.okBut = QPushButton("Tamam")

        self.title.setText(self.baslik)
        self.title.setFixedSize(480, 60)
        self.title.setWordWrap(True)

        self.subTitle.setWordWrap(True)

        self.title.setObjectName("title")
        self.subTitle.setObjectName("subtitle")
        self.okBut.setObjectName("okey")
        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.okBut.setFixedSize(100, 38)

       # self.layoutRight.addWidget(self.title)
        self.layoutRight.addWidget(self.subTitle)
        self.layoutRight.addWidget(self.okBut,      alignment=Qt.AlignmentFlag.AlignRight)

        self.okBut.setGraphicsEffect(BoxShadowEffect.colorTeal())

        self.layout.addWidget(self.icon)
        self.layout.addLayout(self.layoutRight)

        

        self.show()

## Sinyal slotlar

        self.okBut.clicked.connect(self.closeBut)

        QTimer.singleShot(15000, lambda: self.close())



        if self.tip == 0:
            self.icon.setObjectName("iconWarning")
            self.setStyleSheet(stil)

        elif self.tip ==1:
            self.icon.setObjectName("iconChecked")
            self.setStyleSheet(stil)

    def closeBut(self):
        self.close()


## Onay dialog penceresi
class MessageBOXQuestion(QWidget):
    def __init__(self, title="UYARI", subtitle = "",  tip=0):
        super().__init__()


        self.baslik = title
        self.icerik = subtitle
        self.tip = tip #0 ise uyarı, 1 ise başarılı işlem ikonu göster

        self.setStyleSheet(stil)

        ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)

        self.setFixedSize(550, 250)
      
        self.frame = QFrame(self)
        self.frame.setFixedSize(550, 250)
        self.frame.setContentsMargins(0,0,0,0)

        self.icon = QLabel()
        self.icon.setFixedSize(110, 110)


        self.layout = QGridLayout(self.frame)
        self.layout.setContentsMargins(8,4,8,8)

        self.title = QLabel(self.frame)
        self.subTitle = QLabel(self.icerik)
        self.okBut = QPushButton("Evet")
        self.iptalBut = QPushButton("İptal")

        self.title.setText(self.baslik)
        self.title.setFixedSize(460, 65)
        self.title.setWordWrap(True)
        self.subTitle.setWordWrap(True)

        self.okBut.setGraphicsEffect(BoxShadowEffect.colorTeal())
        self.iptalBut.setGraphicsEffect(BoxShadowEffect.colorTeal())

        self.title.setObjectName("title")
        self.subTitle.setObjectName("subtitle")
        self.iptalBut.setObjectName("cancel")
        self.okBut.setObjectName("okey")
       
        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.okBut.setFixedSize(100, 38)
        self.iptalBut.setFixedSize(100, 38)


        self.layout.addWidget(self.title,       0, 0, 1, 4,   alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.layout.addWidget(self.icon,        1, 0, 3, 1)
        self.layout.addWidget(self.subTitle,    1, 1, 3, 3,   alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.layout.addWidget(self.iptalBut,    4, 2, 1, 1)
        self.layout.addWidget(self.okBut,       4, 3, 1, 1)

        self.show()

## Sinyal slotlar

        self.iptalBut.clicked.connect(lambda: self.close())
        self.okBut.clicked.connect(lambda: self.close())

        QTimer.singleShot(15000, lambda: self.close())


        if self.tip == 0:
            self.icon.setObjectName("iconWarning")
            self.setStyleSheet(stil)

        elif self.tip ==1:
            self.icon.setObjectName("iconChecked")
            self.setStyleSheet(stil)



    def closeFonk(self):
        QTimer.singleShot(1000, lambda: self.close()) 

    


## Onay dialog penceresi
