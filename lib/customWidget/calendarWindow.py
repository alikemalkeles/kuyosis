from PyQt6 import QtCore
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import (QCalendarWidget,
                             QGridLayout, QFrame, QPushButton)

with open("assets/stil/stil_mainWindow.qss") as file:
    stil = file.read()


class CalendarWindow(QFrame):
    def __init__(self, position):
        super().__init__()

        self.move(position[0] - 300, position[1] + 35)

        self.setContentsMargins(0,0,0,0)
        self.setStyleSheet(stil)

        WINDOWS_WIDTH = 550
        WINDOWS_HEIGHT = 400

        self.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)

         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)


        self.calendarWindow = QFrame(self)
        self.calendarWindow.setFixedSize(WINDOWS_WIDTH, WINDOWS_HEIGHT)
        self.calendarWindow.setObjectName("calendarWindow")
        self.calendarWindow.setContentsMargins(8,8,8,8)


        self.calendarWidgetLayout = QGridLayout(self.calendarWindow)
        self.calendarWidgetLayout.setContentsMargins(0, 0, 0, 0)

        self.closeBut = QPushButton()
        self.calendar = QCalendarWidget(self)
        
        self.calendar.setVerticalHeaderFormat(QCalendarWidget.VerticalHeaderFormat.NoVerticalHeader)
       
       

        self.closeBut.setFixedSize(24, 24)
       
      
       

        self.closeBut.setObjectName("closeBut")
    
       
       
      



        # 7 sutun
        self.calendarWidgetLayout.addWidget(self.closeBut,          0, 6, 1, 1,  alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignTop)
        self.calendarWidgetLayout.addWidget(self.calendar,          1, 0, 12, 7)
       

    # sinyal slotlar
        self.closeBut.clicked.connect(self.close)


        self.show()


# app = QApplication(sys.argv)
# ex = CalendarWindow()
# sys.exit(app.exec())
