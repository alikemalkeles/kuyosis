import json
import os
import sys
import time
import urllib.error
import urllib.request
from datetime import datetime
from pathlib import Path

import certifi
from PyQt6 import QtCore
from PyQt6.QtCore import Qt, QTimer
from PyQt6.QtGui import QColor, QRegularExpressionValidator
from PyQt6.QtWidgets import QApplication, QVBoxLayout, QHBoxLayout, \
    QPushButton, QLineEdit, QFrame, QWidget, QLabel, QGridLayout, QComboBox, \
    QScrollArea, QSizePolicy, QListWidget, QListWidgetItem, QFileDialog

from customWidget.customWidgets import CustomButton, CustomListTile
from query.dbQuery import DbQuery
from settingWindows.downloadWindow import *
from customWidget.messageBox import MessageBOX, MessageBOXQuestion
from models.model import *
from customWidget.shadowEffect import BoxShadowEffect
from settingWindows.settings import *
with open("./assets/stil/stil_setting.qss") as file:
    stil = file.read()

class SettingWindow(QFrame):
    def __init__(self, user: Users):
        super().__init__()

        self.user = user
        self.settingsInfo = EnterpriseSettingModel("", "", "", "", "", "", "") # none dönmesin diye boş model verildi

        self.anahtar = True # Kurum bilgilerini güncelleme alanı kontrol anahtarı


        # Sınıf yükseltmede sınıfları tutan değişken
        self.sinifListeleri = set()


        self.versiyon = 1.2
        self.developer = "Ali Kemal KELEŞ"
        self.developerPosta = "alikemalkeles@yaani.com"
        self.bibliography = """İkonlar: https://www.flaticon.com\nVektörler: https://www.freepik.com"""
        self.yeniSurumAdresi = 'https://gitlab.com/alikemalkeles/kuyosis/-/raw/main/kuyosis_version.json?ref_type=heads' #gitlab adresi

        self.setObjectName("settingFrame")
        self.setStyleSheet(stil)


        #Ekran çözünürlüğü
        screen = self.screen()
      #  print(screen.size().width(), screen.size().height())
        self.w, self.h = (screen.size().width(), screen.size().height())

    # ComboBox ve lineedit widget yükseklikleri
        self.h1 = 38 if self.h > 900 else 32 #  buton yüksekliği
       



    #Ana layout (ScrollBAraera aktif olabilmesi için layout yerine bir widgete bağlandı)
        self.scrollLayout = QVBoxLayout()
        self.scrollLayout.setContentsMargins(0,0,0,0)

        self.setLayout(self.scrollLayout)


        self.widget = QWidget()
        self.widget.setContentsMargins(0,0,0,0)
     #   self.widget.setSizePolicy(QSizePolicy.Policy.Preferred,  QSizePolicy.Policy.Fixed)



        self.scrollAreaWid = QScrollArea()
        self.scrollAreaWid.setWidgetResizable(True)
        self.scrollAreaWid.setLayoutDirection(Qt.LayoutDirection.LeftToRight)   #setWidget(self.mainFrame)
        self.scrollAreaWid.setWidget(self.widget)   #   setLayout(self.mainLayout)
        self.scrollAreaWid.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.scrollAreaWid.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        
        

        self.scrollLayout.addWidget(self.scrollAreaWid)




        self.mainLayout = QGridLayout(self.widget)
        self.mainLayout.setContentsMargins(18, 16, 18, 12)
        self.mainLayout.setAlignment(Qt.AlignmentFlag.AlignTop) # ogeleri yukarı hizalama
    

        

        self.bilgiGuncelleBut = QPushButton("Düzenle")
        self.bilgiGuncelleBut.setFixedSize(145, 35)
        self.bilgiGuncelleBut.setObjectName("bilgileriniGuncelleBut")

        self.statuMesaji = QLabel()
        self.label0 = QLabel("BİLGİ VE YAPILANDIRMALAR")
        self.label1 = QLabel("KURUM BİLGİLERİ")
        self.label2 = QLabel("YAPILANDIRMALAR")
        self.label6 = QLabel("İÇERİK SİLME")
        self.label3 = QLabel("UYGULAMA BİLGİLERİ VE GÜNCELLEMELER")
        self.label5 = QLabel("YEDEKLEME")
        self.label4 = QLabel("İÇERİK GÜNCELLEME VE SINIF YÜKSELTME")

        self.frame1 = QFrame()
        self.frame2 = QFrame()
        self.frame3 = QFrame()
        self.frame4 = QFrame()
        self.frame5 = QFrame()
        self.frame6 = QFrame() # içerik silme
     

        self.label0.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.frame1.setFixedHeight(180)
        self.frame2.setFixedHeight(90)
        self.frame3.setFixedHeight(270)

        self.frame5.setFixedHeight(120)
        self.frame6.setFixedHeight(155)

        self.frame1.setGraphicsEffect(BoxShadowEffect.colorBlueDark())
        self.frame2.setGraphicsEffect(BoxShadowEffect.colorBlueDark())
        self.frame3.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame4.setGraphicsEffect(BoxShadowEffect.colorTealDark())
        self.frame5.setGraphicsEffect(BoxShadowEffect.colorTealDark())
        self.frame6.setGraphicsEffect(BoxShadowEffect.colorTealDark())

        self.statuMesaji.setObjectName("statuMesaji")
        self.label0.setObjectName("labelSetting")
        self.label1.setObjectName("label")
        self.label2.setObjectName("label")
        self.label3.setObjectName("label")
        self.label4.setObjectName("label")
        self.label5.setObjectName("label")
        self.label6.setObjectName("label")

        self.statuMesaji.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.label0.setFixedHeight(30)
        self.label1.setFixedHeight(60)
        self.label2.setFixedHeight(60)
        self.label3.setFixedHeight(60)
        self.label4.setFixedHeight(80)
        self.label5.setFixedHeight(80)
        self.label5.setFixedHeight(80)

        self.frame1.setObjectName("frame")
        self.frame2.setObjectName("frame")
        self.frame3.setObjectName("frame")
        self.frame4.setObjectName("frame")
        self.frame5.setObjectName("frame")
        self.frame6.setObjectName("frame")

        self.mainLayout.addWidget(self.label0,            0, 0, 1, 10)
       
        self.mainLayout.addWidget(self.label1,            1, 0, 1, 3)
        self.mainLayout.addWidget(self.statuMesaji,       1, 3, 1, 4)
        self.mainLayout.addWidget(self.bilgiGuncelleBut,  1, 9, 1, 1)
        self.mainLayout.addWidget(self.frame1,            2, 0, 2, 10)

        self.mainLayout.addWidget(self.label2,            4, 0, 1, 10)
        self.mainLayout.addWidget(self.frame2,            5, 0, 2, 10)

        self.mainLayout.addWidget(self.label4,            8, 0, 1, 10)
        self.mainLayout.addWidget(self.frame4,            9, 0, 4, 10)

        self.mainLayout.addWidget(self.label6,            14, 0, 1, 10)
        self.mainLayout.addWidget(self.frame6,            15, 0, 2, 10)  # içerik silme

        self.mainLayout.addWidget(self.label5,            18, 0, 1, 10)
        self.mainLayout.addWidget(self.frame5,            19, 0, 2, 10)

        self.mainLayout.addWidget(self.label3,           21, 0, 1, 10)
        self.mainLayout.addWidget(self.frame3,           22, 0, 4, 10)
      

       

    # Kurum Bilgileri 
        self.kurumBilgileriLayout = QGridLayout(self.frame1)

        self.labelKurum1 = QLabel("Kurum Adı: ")
        self.labelKurum2 = QLabel("Kurum Adresi: ")
        self.labelKurum3 = QLabel("Kurum Yöneticisi: ")
        self.labelKurum4 = QLabel("Kurum Telefonu: ")


        self.labelKurum1.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelKurum2.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelKurum3.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelKurum4.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)

        self.lineEditKurum1 = QLineEdit()
        self.lineEditKurum2 = QLineEdit()
        self.lineEditKurum3 = QLineEdit()
        self.lineEditKurum4 = QLineEdit()


        self.lineEditKurum1.textChanged.connect(lambda: self.lineEditKurum1.setText(self.karakterBuyut(self.lineEditKurum1.text())))
        self.lineEditKurum2.textChanged.connect(lambda: self.lineEditKurum2.setText(self.karakterBuyut(self.lineEditKurum2.text())))
        self.lineEditKurum3.textChanged.connect(lambda: self.lineEditKurum3.setText(self.karakterBuyut(self.lineEditKurum3.text())))
        self.lineEditKurum4.textChanged.connect(lambda: self.lineEditKurum4.setText(self.karakterBuyut(self.lineEditKurum4.text())))

        rx1 = QtCore.QRegularExpression("[0-9]{11}")
        v1 = QRegularExpressionValidator(rx1, self.lineEditKurum4)
        self.lineEditKurum4.setValidator(v1)

        self.lineEditKurum1.setMaxLength(70)
        self.lineEditKurum2.setMaxLength(70)
        self.lineEditKurum3.setMaxLength(40)

        self.lineEditKurum1.setEnabled(False) # Başlangıcta düzenlenemez yapma
        self.lineEditKurum2.setEnabled(False)
        self.lineEditKurum3.setEnabled(False)
        self.lineEditKurum4.setEnabled(False)

        
        self.lineEditKurum1.setFixedHeight(self.h1)
        self.lineEditKurum2.setFixedHeight(self.h1)
        self.lineEditKurum3.setFixedHeight(self.h1)
        self.lineEditKurum4.setFixedHeight(self.h1)

        

        self.labelKurum1.setObjectName("labelOrtak")
        self.labelKurum2.setObjectName("labelOrtak")
        self.labelKurum3.setObjectName("labelOrtak")
        self.labelKurum4.setObjectName("labelOrtak")

        self.kurumBilgileriLayout.addWidget(self.labelKurum1,             0, 0, 1, 1)
        self.kurumBilgileriLayout.addWidget(self.lineEditKurum1,          0, 1, 1, 3)

        self.kurumBilgileriLayout.addWidget(self.labelKurum2,             1, 0, 1, 1)
        self.kurumBilgileriLayout.addWidget(self.lineEditKurum2,          1, 1, 1, 3)

        self.kurumBilgileriLayout.addWidget(self.labelKurum3,             0, 4, 1, 1)
        self.kurumBilgileriLayout.addWidget(self.lineEditKurum3,          0, 5, 1, 3)

        self.kurumBilgileriLayout.addWidget(self.labelKurum4,             1, 4, 1, 1)
        self.kurumBilgileriLayout.addWidget(self.lineEditKurum4,          1, 5, 1, 2)



    # UYgulamayla ilgili yapılandırmalar
        self.uygulamaYapilandirmaLayout = QGridLayout(self.frame2)

        self.labelYapilandirma1 = QLabel("Üyenin alabileceği kitap sayısı")
        self.labelYapilandirma2 = QLabel("Kitap emanet süresi (Gün)")

        self.labelYapilandirma1.setObjectName("labelOrtak")
        self.labelYapilandirma2.setObjectName("labelOrtak")

        self.labelYapilandirma1.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelYapilandirma2.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignVCenter)

        self.comboYapilandirma1 = QComboBox()
        self.comboYapilandirma2 = QComboBox()

        self.comboYapilandirma1.setEnabled(False)
        self.comboYapilandirma2.setEnabled(False)

        self.comboYapilandirma1.setFixedSize(100,  self.h1)
        self.comboYapilandirma2.setFixedSize(100,  self.h1)

        self.comboYapilandirma1.addItems([str(i) for i in range(1, 11)])
        self.comboYapilandirma2.addItems([str(i) for i in range(1, 61)])

    

        self.uygulamaYapilandirmaLayout.addWidget(self.labelYapilandirma1,    0, 0, 1, 1)
        self.uygulamaYapilandirmaLayout.addWidget(self.comboYapilandirma1,    0, 1, 1, 2)

        self.uygulamaYapilandirmaLayout.addWidget(self.labelYapilandirma2,    0, 4, 1, 1)
        self.uygulamaYapilandirmaLayout.addWidget(self.comboYapilandirma2,    0, 5, 1, 2)


    # İçerik silme
        self.icerikSilmeLayout = QGridLayout(self.frame6)
        self.icerikSilmeLayout.setContentsMargins(15, 12, 15, 12)

        self.labelIcerik7 = QLabel("""<font color = "red"> Yönetici ayrıcalığı gerektirir </font>""")
        self.labelIcerik8 = QLabel("""Bilgilendirme: Emanette kitap bulunması durumunda silme işlemi gerçekleştirilemez. Ayrıca kitapları veya üyeleri sildiğinizde kitap alıp verme geçmişi de otomatik olarak silinecektir.""")

        self.kitaplariSilBut = QPushButton("Tüm Kitapları Sil")
        self.uyeleriSilBut = QPushButton("Tüm Üyeleri Sil")
        self.kitapGecmisiniSilBut = QPushButton("Tüm Kitap Alıp Verme\nGeçmişini Sil")

        self.labelIcerik7.setObjectName("yedeklemeLabel")
        self.kitaplariSilBut.setObjectName("icerikListeleBut")
        self.uyeleriSilBut.setObjectName("icerikListeleBut")
        self.kitapGecmisiniSilBut.setObjectName("icerikListeleBut")

        self.labelIcerik7.setFixedHeight(self.h1)
        self.uyeleriSilBut.setFixedHeight(self.h1)
        self.kitaplariSilBut.setFixedHeight(self.h1)
        self.kitapGecmisiniSilBut.setFixedHeight(self.h1)


        self.icerikSilmeLayout.addWidget(self.labelIcerik7,           0, 0, 1, 7)
        self.icerikSilmeLayout.addWidget(self.labelIcerik8,           1, 0, 1, 7)

        self.icerikSilmeLayout.addWidget(self.kitaplariSilBut,        2, 0, 1, 1)
        self.icerikSilmeLayout.addWidget(self.uyeleriSilBut,          2, 3, 1, 1)
        self.icerikSilmeLayout.addWidget(self.kitapGecmisiniSilBut,   2, 6, 1, 1)





    # Toplu veri günceleme ve sınıf yükseltme asistanı
        self.icerikSinifYukseltmeLayout = QGridLayout(self.frame4)
        self.icerikSinifYukseltmeLayout.setContentsMargins(15, 12, 15, 12)

        self.labelIcerik1 = QLabel("Yayınevi Güncelleme")

        self.comboBoxIcerik1 = QComboBox()
        self.butonIcerikListele1 = QPushButton("Yayınevlerini Listele")
        self.lineEditIcerik1 = QLineEdit()
        self.butonIcerikGuncelle1 = QPushButton("Yayınevi Güncelle")

        self.comboBoxIcerik1.setFixedHeight(self.h1)
        self.butonIcerikListele1.setFixedHeight(self.h1)
        self.lineEditIcerik1.setFixedHeight(self.h1)
        self.butonIcerikGuncelle1.setFixedHeight(self.h1)

        self.lineEditIcerik1.setMaxLength(60)
        self.lineEditIcerik1.setClearButtonEnabled(True)
        self.lineEditIcerik1.setPlaceholderText("Yayınevi için yeni değer giriniz...")
        self.comboBoxIcerik1.setPlaceholderText("Öncelikle yayınevlerini listeleyiniz")

        self.labelIcerik1.setObjectName("labelIcerik")
        self.butonIcerikListele1.setObjectName("icerikListeleBut")
        self.lineEditIcerik1.setObjectName("lineEditIcerik")
        self.butonIcerikGuncelle1.setObjectName("butonStandart")


        self.labelIcerik2 = QLabel("Yazar Güncelleme")

        self.comboBoxIcerik2 = QComboBox()
        self.butonIcerikListele2 = QPushButton("Yazarları Listele")
        self.lineEditIcerik2 = QLineEdit()
        self.butonIcerikGuncelle2 = QPushButton("Yazar Güncelle")

        self.comboBoxIcerik2.setFixedHeight(self.h1)
        self.butonIcerikListele2.setFixedHeight(self.h1)
        self.lineEditIcerik2.setFixedHeight(self.h1)
        self.butonIcerikGuncelle2.setFixedHeight(self.h1)

        self.lineEditIcerik2.setMaxLength(60)
        self.lineEditIcerik2.setClearButtonEnabled(True)
        self.lineEditIcerik2.setPlaceholderText("Yazar adı için yeni değer giriniz...")
        self.comboBoxIcerik2.setPlaceholderText("Öncelikle yazarları listeleyiniz")

        self.labelIcerik2.setObjectName("labelIcerik")
        self.butonIcerikListele2.setObjectName("icerikListeleBut")
        self.lineEditIcerik2.setObjectName("lineEditIcerik")
        self.butonIcerikGuncelle2.setObjectName("butonStandart")


        self.labelIcerik3 = QLabel("Raf Konumu Güncelleme")

        self.comboBoxIcerik3 = QComboBox()
        self.butonIcerikListele3 = QPushButton("Raf Konumlarını Listele")
        self.lineEditIcerik3 = QLineEdit()
        self.butonIcerikGuncelle3 = QPushButton("Raf Konumu Güncelle")

        self.comboBoxIcerik3.setFixedHeight(self.h1)
        self.butonIcerikListele3.setFixedHeight(self.h1)
        self.lineEditIcerik3.setFixedHeight(self.h1)
        self.butonIcerikGuncelle3.setFixedHeight(self.h1)

        self.lineEditIcerik3.setMaxLength(60)
        self.lineEditIcerik3.setClearButtonEnabled(True)
        self.lineEditIcerik3.setPlaceholderText("Raf konumu için yeni değer giriniz...")
        self.comboBoxIcerik3.setPlaceholderText("Öncelikle raf konumlarını listeleyiniz")

        self.labelIcerik3.setObjectName("labelIcerik")
        self.butonIcerikListele3.setObjectName("icerikListeleBut")
        self.lineEditIcerik3.setObjectName("lineEditIcerik")
        self.butonIcerikGuncelle3.setObjectName("butonStandart")

        self.comboBoxIcerik1.setInsertPolicy(QComboBox.InsertPolicy.InsertAlphabetically)
        self.comboBoxIcerik2.setInsertPolicy(QComboBox.InsertPolicy.InsertAlphabetically)
        self.comboBoxIcerik3.setInsertPolicy(QComboBox.InsertPolicy.InsertAlphabetically)

    # sınıf atlatma
        self.labelIcerik4 = QLabel("""<font color = "red" weight = "bold"> Sınıf Yükseltme Yardımcısı </font>""")

        self.butonIcerikListele4 = QPushButton("Sınıfları Listele")
        self.labelIcerik5 = QLabel() #"Veritabanında Kayıtlı Sınıflar"
        self.mezunLabel1 = QLabel("""<font color = "red" weight = "bold"> MEZUN OLACAK SINIFI SEÇİNİZ  (Mezun olarak seçilen sınıfa ait tüm bilgiler veritabanından kaldırılacaktır. Ayrıca sınıf bilgisi eksik üyelere sınıf yükseltme ve mezuniyet işlemleri uygulanamaz.) Not: Yükseltme işleminden önce yedekleme almanız önerilir. </font> """)
        self.mezunSinifSecimi = QComboBox()
        self.sonucTablosu = QListWidget()
        self.labelIcerik6 = QLabel("   Sınıf Yükseltme Tablosu")
        self.butonSinifYukselt = QPushButton("     Sınıfları Yükselt     ")

        self.labelIcerik4.setObjectName("labelIcerik")
        self.butonIcerikListele4.setObjectName("icerikListeleBut")
        self.labelIcerik5.setObjectName("labelIcerikAlt")
        self.mezunLabel1.setObjectName("labelIcerikAlt")
        self.sonucTablosu.setObjectName("sonucListesi")
        self.labelIcerik6.setObjectName("labelIcerik")
        self.butonSinifYukselt.setObjectName("butonStandart")

        self.butonIcerikListele4.setFixedHeight(self.h1)
        self.mezunSinifSecimi.setFixedSize(60, self.h1)
        
        self.mezunLabel1.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.butonSinifYukselt.setFixedHeight(self.h1)
        self.mezunLabel1.setWordWrap(True)
        self.labelIcerik5.setWordWrap(True)
        
    

        self.labelIcerik5.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        

        self.icerikSinifYukseltmeLayout.setSpacing(15)

        self.butonIcerikGuncelle1.setGraphicsEffect(BoxShadowEffect.colorTeal())
        self.butonIcerikGuncelle2.setGraphicsEffect(BoxShadowEffect.colorTeal())
        self.butonIcerikGuncelle3.setGraphicsEffect(BoxShadowEffect.colorTeal())
        self.butonSinifYukselt.setGraphicsEffect(BoxShadowEffect.colorTeal())
        

        self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik1,           0, 0, 1, 8)
        self.icerikSinifYukseltmeLayout.addWidget(self.comboBoxIcerik1,        1, 0, 1, 3)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikListele1,    1, 3, 1, 1)
        self.icerikSinifYukseltmeLayout.addWidget(self.lineEditIcerik1,        1, 4, 1, 3)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikGuncelle1,   1, 7, 1, 1)

        self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik2,           2, 0, 1, 8)
        self.icerikSinifYukseltmeLayout.addWidget(self.comboBoxIcerik2,        3, 0, 1, 3)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikListele2,    3, 3, 1, 1)
        self.icerikSinifYukseltmeLayout.addWidget(self.lineEditIcerik2,        3, 4, 1, 3)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikGuncelle2,   3, 7, 1, 1)

        self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik3,           4, 0, 1, 8)
        self.icerikSinifYukseltmeLayout.addWidget(self.comboBoxIcerik3,        5, 0, 1, 3)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikListele3,    5, 3, 1, 1)
        self.icerikSinifYukseltmeLayout.addWidget(self.lineEditIcerik3,        5, 4, 1, 3)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikGuncelle3,   5, 7, 1, 1)

        bosluk = QFrame()
        bosluk.setFixedHeight(20)
        self.icerikSinifYukseltmeLayout.addWidget(bosluk,                      6, 0, 1, 8)

        self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik4,           7, 0, 1, 4)
        self.icerikSinifYukseltmeLayout.addWidget(self.butonIcerikListele4,    8, 0, 1, 1)
        

        # sistemde kayıtlı sınıflar SwitchButton OLarak HBoxLayouta yazılacak fonksiyon üzerinden widget eklenecek (kayitliSinifListesiFonk(self))
        self.kayitliSiniflarListesiLayout = QHBoxLayout()
        self.kayitliSiniflarListesiLayout.setContentsMargins(0,0,0,0)
        self.kayitliSiniflarListesiLayout.setSpacing(0)
        self.kayitliSiniflarListesiLayout.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)

        self.icerikSinifYukseltmeLayout.addLayout(self.kayitliSiniflarListesiLayout,    11, 0, 1, 8)


    # Yedekleme 
        self.yedeklemeLayout = QGridLayout(self.frame5)
        self.yedeklemeLayout.setContentsMargins(15, 12, 15, 12)

        self.yedeklemeLabelTitle1 = QLabel("Yedekleme konumu (varsayılan konum)")
        self.yedeklemeLabelTitle2 = QLabel("En son yedekleme")

        self.yedeklemeDosyaYolu = QLabel("Henüz yedekleme konumu seçilmedi")
        self.yedeklemeYoluSec = QPushButton("    Yeni Yedekleme Konumu Seç    ")
        self.yedeklemeTarihi = QLabel("Henüz yedekleme yapılmadı")
        self.yedegiGeriYukle = QPushButton("     Yedeği Geri Yükle     ")
        self.simdiYedekle = QPushButton("                Şimdi Yedekle                ")

        self.yedeklemeYoluSec.setFixedHeight(25)
        self.yedegiGeriYukle.setFixedHeight(25)
        self.simdiYedekle.setFixedHeight(self.h1)

        self.yedegiGeriYukle.setEnabled(False)  # Başlangıcta pasif

        self.yedeklemeTarihi.setWordWrap(True)
       # self.yedeklemeDosyaYolu.setWordWrap(True)


        self.yedeklemeLabelTitle1.setObjectName("yedeklemeLabel")
        self.yedeklemeLabelTitle2.setObjectName("yedeklemeLabel")
        self.yedeklemeDosyaYolu.setObjectName("yedeklemeYolTarih")
        self.yedeklemeYoluSec.setObjectName("butonMinimal")
        self.yedeklemeTarihi.setObjectName("yedeklemeYolTarih")
        self.yedegiGeriYukle.setObjectName("butonMinimal")
        self.simdiYedekle.setObjectName("butonStandart")
        


        self.yedeklemeLayout.addWidget(self.yedeklemeLabelTitle1,     0, 0, 1, 4, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.yedeklemeLayout.addWidget(self.yedeklemeLabelTitle2,     0, 4, 1, 4, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.yedeklemeLayout.addWidget(self.yedeklemeDosyaYolu,       1, 0, 1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.yedeklemeLayout.addWidget(self.yedeklemeYoluSec,         1, 2, 1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.yedeklemeLayout.addWidget(self.yedeklemeTarihi,          1, 4, 1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.yedeklemeLayout.addWidget(self.yedegiGeriYukle,          1, 5, 1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.yedeklemeLayout.addWidget(self.simdiYedekle,             0, 6, 2, 2, alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignCenter)





    # Uygulama bilgileri
        self.uygulamaBilgileriLayout = QGridLayout(self.frame3)


        self.labelBilgi1 = QLabel("Uygulama Sürümü: ")

        self.surumKontrolBut = QPushButton("Güncellemeleri Kontrol Et")
        self.surumKontrolLabel = QLabel("Yeni sürüm henüz kontrol edilmedi.")

        self.labelBilgi2 = QLabel("Geliştirici: ")
        self.labelBilgi3 = QLabel("Destek ve Talep: ")
        self.labelBilgi4 = QLabel("Uygulama ikon ve vektör kaynakçası: ")

        
        self.labelBilgi1.setObjectName("labelOrtak")
        self.labelBilgi2.setObjectName("labelOrtak")
        self.labelBilgi3.setObjectName("labelOrtak")
        self.labelBilgi4.setObjectName("labelOrtak")
        self.surumKontrolBut.setObjectName("butonMinimal")
        self.surumKontrolLabel.setObjectName("surumKontrolLabel")

        self.surumKontrolBut.setFixedSize(200, 25)


        self.labelBilgi1.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelBilgi2.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelBilgi3.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelBilgi4.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.surumKontrolLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
       


        self.labelBilgi1_ = QLabel(str(self.versiyon))
        self.labelBilgi2_ = QLabel(self.developer)
        self.labelBilgi3_ = QLabel(self.developerPosta)
        self.labelBilgi4_ = QLabel(self.bibliography)

        self.labelBilgi1_.setObjectName("labelBilgi")
        self.labelBilgi2_.setObjectName("labelBilgi")
        self.labelBilgi3_.setObjectName("labelBilgi")
        self.labelBilgi4_.setObjectName("labelBilgi")

        self.labelBilgi1_.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelBilgi2_.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelBilgi3_.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
        self.labelBilgi4_.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)


 
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi1,        0, 0, 1, 1)
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi1_,       0, 1, 1, 1)
        self.uygulamaBilgileriLayout.addWidget(self.surumKontrolBut,    1, 1, 1, 1,    QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignTop)
      #  self.uygulamaBilgileriLayout.addWidget(self.surumKontrolLabel,  1, 2, 1, 4,    QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignTop)

        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi2,        2, 0, 1, 1)
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi2_,       2, 1, 1, 5)
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi3,        3, 0, 1, 1)
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi3_,       3, 1, 1, 5)
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi4,        4, 0, 1, 1)
        self.uygulamaBilgileriLayout.addWidget(self.labelBilgi4_,       4, 1, 1, 5)


    # sinyal ve slotlar
        self.bilgiGuncelleBut.clicked.connect(self.kurumBilgileriniGuncelleFonk)
        self.butonIcerikListele4.clicked.connect(self.kayitliSinifListesiFonk)
        self.mezunSinifSecimi.currentIndexChanged.connect(self.sinifAtlatmaSonucTablosu)

        self.lineEditIcerik1.textChanged.connect(lambda: self.lineEditIcerik1.setText(self.karakterBuyut(self.lineEditIcerik1.text())))
        self.lineEditIcerik2.textChanged.connect(lambda: self.lineEditIcerik2.setText(self.karakterBuyut(self.lineEditIcerik2.text())))
        self.lineEditIcerik3.textChanged.connect(lambda: self.lineEditIcerik3.setText(self.karakterBuyut(self.lineEditIcerik3.text())))

        self.kitaplariSilBut.clicked.connect(self.tumKitaplariSil)
        self.uyeleriSilBut.clicked.connect(self.tumUyeleriSil)
        self.kitapGecmisiniSilBut.clicked.connect(self.kitapAlipVermeGecmisiniSil_)

        self.butonIcerikListele1.clicked.connect(lambda: self.yazarYayineviRafSorgula("yayinevi"))
        self.butonIcerikListele2.clicked.connect(lambda: self.yazarYayineviRafSorgula("yazar"))
        self.butonIcerikListele3.clicked.connect(lambda: self.yazarYayineviRafSorgula("raf"))

        self.butonIcerikGuncelle1.clicked.connect(self.yayineviGuncellemeFonk)
        self.butonIcerikGuncelle2.clicked.connect(self.yazarGuncellemeFonk)
        self.butonIcerikGuncelle3.clicked.connect(self.rafGuncellemeFonk)

        self.butonSinifYukselt.clicked.connect(self.sinifYukseltmeFonk)

        self.yedeklemeYoluSec.clicked.connect(self.yedeklemeKonumuSec)

        self.surumKontrolBut.clicked.connect(self.yeniSurumKontrolu)

        self.simdiYedekle.clicked.connect(self.yedekleFonk)

        self.yedegiGeriYukle.clicked.connect(self.yedegiGeriYukleFonk)
        


      #  self.initialFonk()

       

    def initialFonkSettingWindow(self):
        
        self.enterpriseInfoSettingGetFonk()
        self.yedeklemeBilgileri()

        # yönetici denetimi yap
        # eğer sistem kullanıcısının rolü yönetici ise butonları aktif et
        self.widget.setEnabled(self.user.ayar_yetkisi) # Ana widgeti devre dışı bırak
        self.frame6.setEnabled(self.user.statu)


    # Tüm kitapları silme
    def tumKitaplariSil(self):
        kitapSayisi = DbQuery.kitapSayisi()
        if kitapSayisi is not None:
            if kitapSayisi > 0:
                okunanKitaplar = DbQuery.emanettekiKitapUyeID()

                if okunanKitaplar is not None:
                    if len(okunanKitaplar[0]) == 0:

                        msg = MessageBOXQuestion("BİLGİLENDİRME", f" Tüm kitaplar silinecek. Ayrıca kitap alıp verme tablosundaki tüm kayıtlar da silinecek.\n\n İşleme devam edilsin mi?", 0)
                        msg.okBut.clicked.connect(lambda: self.tumKitaplariSil2())

                    else:
                        MessageBOX(title="UYARI", subtitle=f"  Emanette kitaplar oduğu için silme işlemi gerçekleştirilemiyor. Öncelikle emanetteki kitapları teslim alınız.\n\n  Emanetteki kitap sayısı: {len(okunanKitaplar[0])}", tip=0)
                else:
                    MessageBOX(title="HATA", subtitle="Okunan kitaplar listesi alınamadığı için işlem gerçekleştirilemiyor.", tip=0)

            else:
                MessageBOX(title="BİLGİLENDİRME", subtitle=" Kayıtlı kitabınız bulunamadı.", tip=0)

        else:
            MessageBOX(title="HATA", subtitle=" Kitap sayısı alınamadı.", tip=0)

    def tumKitaplariSil2(self):
        result = DbQuery.tumKitaplariSil()
        if result:
            DbQuery.createDatabaseTables()
            MessageBOX(title="BİLGİLENDİRME", subtitle="Tüm Kitaplar ve kitap alıp verme geçmişi başarılı bir şekilde silindi. Uygulama yeniden başlatılacak", tip=1)

            QTimer.singleShot(3500, lambda: self.restartApp()) # uygulamayı yeniden başlat

        else:
            MessageBOX(title="HATA", subtitle="Bilinmeyen bir hata oluştu daha sonra tekrar deneyiniz.", tip=0)



    def tumUyeleriSil(self):
        uyeSayisi = DbQuery.uyeSayisi()
        if uyeSayisi is not None:
            if uyeSayisi > 0:
                okuyanUyeler = DbQuery.emanettekiKitapUyeID()

                if okuyanUyeler is not None:
                    if len(okuyanUyeler[1]) == 0:

                        msg = MessageBOXQuestion("BİLGİLENDİRME", f" Tüm üyeler silinecek. Ayrıca kitap alıp verme tablosundaki tüm kayıtlar da silinecek.\n\n İşleme devam edilsin mi?", 0)
                        msg.okBut.clicked.connect(lambda: self.tumUyeleriSil2())

                    else:
                        MessageBOX(title="UYARI", subtitle=f"  Kitabını teslim etmeyen üyeler oduğu için silme işlemi gerçekleştirilemiyor. Öncelikle üyedeki kitapları teslim alınız.\n\n  Okuyan üye sayısı: {len(okuyanUyeler[1])}", tip=0)
                else:
                    MessageBOX(title="HATA", subtitle="Okuyan üyeler listesi alınamadığı için işlem gerçekleştirilemiyor.", tip=0)

            else:
                MessageBOX(title="BİLGİLENDİRME", subtitle=" Kayıtlı üye bulunamadı.", tip=0)

        else:
            MessageBOX(title="HATA", subtitle=" Üye sayısı alınamadı.", tip=0)

    def tumUyeleriSil2(self):
        result = DbQuery.tumUyeleriSil()
        if result:
            DbQuery.createDatabaseTables()
            MessageBOX(title="BİLGİLENDİRME", subtitle="Tüm üyeler ve üyelerin kitap alıp verme geçmişi başarılı bir şekilde silindi. Uygulama yeniden başlatılacak", tip=1)

            QTimer.singleShot(3500, lambda: self.restartApp()) # uygulamayı yeniden başlat

        else:
            MessageBOX(title="HATA", subtitle="Bilinmeyen bir hata oluştu daha sonra tekrar deneyiniz.", tip=0)



    def kitapAlipVermeGecmisiniSil_(self):
        kitapAlipVermeSayisi = DbQuery.verilenKitapSayisi("2000-01-01")
        if kitapAlipVermeSayisi is not None:
            if kitapAlipVermeSayisi > 0:
                okunanKitaplar = DbQuery.emanettekiKitapUyeID()

                if okunanKitaplar is not None:
                    if len(okunanKitaplar[0]) == 0:

                        msg = MessageBOXQuestion("BİLGİLENDİRME", f" Kitap alıp verme geçmişi silinecek.\n\n İşleme devam edilsin mi?", 0)
                        msg.okBut.clicked.connect(lambda: self.kitapAlipVermeGecmisiniSil2())

                    else:
                        MessageBOX(title="UYARI", subtitle=f"  Teslim edilmeyen kitaplar  olduğu için silme işlemi gerçekleştirilemiyor. Öncelikle emanetteki kitapları teslim alınız.\n\n  Emanetteki kitap sayısı: {len(okunanKitaplar[0])}", tip=0)
                else:
                    MessageBOX(title="HATA", subtitle="Okunan kitaplar listesi alınamadığı için işlem gerçekleştirilemiyor.", tip=0)

            else:
                MessageBOX(title="BİLGİLENDİRME", subtitle=" Kayıt bulunamadı", tip=0)

        else:
            MessageBOX(title="HATA", subtitle=" Kitap sayısı alınamadı.", tip=0)



    def kitapAlipVermeGecmisiniSil2(self):
        result = DbQuery.kitapAlipVermeGecmisiniSil()
        if result:
            DbQuery.createDatabaseTables()
            MessageBOX(title="BİLGİLENDİRME", subtitle="Kitap alıp verme geçmişi başarılı bir şekilde silindi. Uygulama yeniden başlatılacak", tip=1)

            QTimer.singleShot(3500, lambda: self.restartApp()) # uygulamayı yeniden başlat

        else:
            MessageBOX(title="HATA", subtitle="Bilinmeyen bir hata oluştu daha sonra tekrar deneyiniz.", tip=0)



    # vt de kayıtlı sınıfları gösterme
    def kayitliSinifListesiFonk(self):
        self.butonIcerikListele4.setHidden(True) # Sınıfları listelemeyi bir kere akt,f et. sınıflar listendikten sonra isteleme butonunu devre dışı bırak

        self.sinifListeleri.clear()

        result = DbQuery.kayitliSiniflarinListesi()
        # sınıf listelerinden yalnızca numeric olanları yükselt(Ana sınıfı veya okul öncesi sinifları dışarıda bırak)
        
        for i in result:
            if i[0] is not None:
                if str(i[0]).isnumeric():
                    self.sinifListeleri.add(i[0])
                    

        # self.kayitliSiniflarListesiLayout layout üzerindeki tüm widgetleri yok etme
        for i in reversed(range(self.kayitliSiniflarListesiLayout.count())): 
            widgetToRemove = self.kayitliSiniflarListesiLayout.itemAt(i).widget()
            # remove it from the layout list
            self.kayitliSiniflarListesiLayout.removeWidget(widgetToRemove)
            # remove it from the gui
            widgetToRemove.setParent(None)


        for sinif in self.sinifListeleri:
            frame = QFrame()
            frame.setContentsMargins(0,0,0,0)
            frame.setStyleSheet("background-color: transparent; color: none; border: none;")
            frame.setFixedSize(52, 60)
        
            l = QVBoxLayout(frame)
            l.setContentsMargins(0,0,0,0)
            l.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
            l.setSpacing(0)


            label = QLabel(f"{str(sinif)}. Sınıf")
            label.setSizePolicy(QSizePolicy.Policy.MinimumExpanding, QSizePolicy.Policy.MinimumExpanding)
            label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignVCenter)
            label.setFixedHeight(22)
            label.setStyleSheet("font-weight: 200px; font-size: 13px;")
            
            w = CustomButton(width_=50, active_background_color=QColor(42, 196, 199))
            w.setChecked(True)
            w.setObjectName(str(sinif))

            l.addWidget(w,       QtCore.Qt.AlignmentFlag.AlignHCenter | QtCore.Qt.AlignmentFlag.AlignVCenter)
            l.addWidget(label,   QtCore.Qt.AlignmentFlag.AlignHCenter | QtCore.Qt.AlignmentFlag.AlignVCenter)
            
            self.kayitliSiniflarListesiLayout.addWidget(frame, QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignVCenter)
            
            w.clicked.connect(self.secilenSiniflarFonk)

        print("Swich layout: 3_", self.kayitliSiniflarListesiLayout.count())

        if len(self.sinifListeleri) == 0:
            self.labelIcerik5.setText("Veritabınına kayıtlı sınıf bulunamadı (Ana sınıfı, okul öncesi gibi sınflar listelenmez)")
            self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik5,                    10, 0, 1, 8)
        else: 
            self.labelIcerik5.setText(""" Otomatik olarak yükseltebileceğiniz sınıflar veritabanından alınarak başarılı bir şekilde listelendi. (Yükseltme işlemi yapmayacağınız sınıfın seçinini kaldırabilirsiniz.)""")
            self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik5,                    10, 0, 1, 8)
            
            self.icerikSinifYukseltmeLayout.addWidget(self.mezunLabel1,                     12, 1, 1, 7)
            self.icerikSinifYukseltmeLayout.addWidget(self.mezunSinifSecimi,                12, 0, 1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
            self.icerikSinifYukseltmeLayout.addWidget(self.labelIcerik6,                    14, 0, 1, 7)
            self.icerikSinifYukseltmeLayout.addWidget(self.sonucTablosu,                    15, 0, 5, 4)
            self.icerikSinifYukseltmeLayout.addWidget(self.butonSinifYukselt,               15, 7, 5, 1, alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignBottom)


            # En yüksek sınıfı (mezun olacak sınıf) ComBoxa gönderme
            cvr = sorted(list(self.sinifListeleri), reverse=True)
            self.mezunSinifSecimi.clear() # sınıf seçimini önce temizle
            self.mezunSinifSecimi.addItems([str(i) for i in cvr] + ["MEZUN YOK"])

            self.sinifAtlatmaSonucTablosu()

            self.sonucTablosu.setFixedHeight((int(len(self.sinifListeleri) * 40) + 5))



       

    # sınıf atlatma secimi yapılınca secim özet tablosunu ekrana yazdırma
    def sinifAtlatmaSonucTablosu(self):

        self.sonucTablosu.clear() #baslangıcta sıfırla
        self.sonucTablosu.setSpacing(2)
        self.sonucTablosu.setEnabled(False)

        for secilenSiniflar in sorted(self.sinifListeleri):
            
            if self.mezunSinifSecimi.currentText() == str(secilenSiniflar):
                item = QListWidgetItem(self.sonucTablosu)
                self.sonucTablosu.addItem(item)

                row = CustomListTile(height=32, label1=f" {secilenSiniflar}.", label3="Mezun olacak(Kayıtlar silinecek)", label4="*", durum=0)
                item.setSizeHint(row.minimumSizeHint())

                self.sonucTablosu.setItemWidget(item, row)

                #self.sonucTablosu.addItem(f"Mevcut {secilenSiniflar}. Sınıfların Atlatma Sonrası Yeni Sınıf Seviyesi =  Mezun olacak (Kayıtlar silinecek)        ")
            else:
                item = QListWidgetItem(self.sonucTablosu)
                self.sonucTablosu.addItem(item)

                row = CustomListTile(height=32, label1=f" {secilenSiniflar}.", label4=f"{secilenSiniflar + 1}", durum=1)
                item.setSizeHint(row.minimumSizeHint())

                self.sonucTablosu.setItemWidget(item, row)
                #self.sonucTablosu.addItem(f"Mevcut {secilenSiniflar}. Sınıfların Atlatma Sonrası Yeni Sınıf Seviyesi  =  {secilenSiniflar + 1}        ")




    # listelenen sınıfların CheckBOx tarafından seçilince sinif listelerini güncelleme
    def secilenSiniflarFonk(self, olay):
        secim = self.sender()
        print(secim.objectName(), olay, type(olay), "secim")
        
        if olay == True:
            self.sinifListeleri.add(int(secim.objectName()))

            # En yüksek sınıfı (mezun olacak sınıf) ComBoxa gönderme
            cvr = sorted(list(self.sinifListeleri), reverse=True)

            print(cvr, "yeni liste True")

            self.mezunSinifSecimi.clear()
            self.mezunSinifSecimi.addItems([str(i) for i in cvr] + ["MEZUN YOK"])

            self.sinifAtlatmaSonucTablosu()

        else:
            self.sinifListeleri.discard(int(secim.objectName()))

            # En yüksek sınıfı (mezun olacak sınıf) ComBoxa gönderme
            cvr = sorted(list(self.sinifListeleri), reverse=True)

            print(cvr, "yeni liste False")

            self.mezunSinifSecimi.clear()
            self.mezunSinifSecimi.addItems([str(i) for i in cvr] + ["MEZUN YOK"])

            self.sinifAtlatmaSonucTablosu()




    # yazar, yayinevi, raf konumu kayıtlı icerikleri comBoxa yazdırma sorgusu
    def yazarYayineviRafSorgula(self, kategori):
        
        if kategori == "yazar":
            result = DbQuery.comboBox1Secim("yazar")
            yazarListesi = []
            if result:
                for i in result:
                  #  print("yazar: ", i)
                    if i: # i none değilse
                        yazarListesi.append(i[0])
            if len(yazarListesi) >0:

                self.comboBoxIcerik2.clear()
                self.comboBoxIcerik2.addItems(yazarListesi)
                self.comboBoxIcerik2.setPlaceholderText("Yazar seçiniz") # secim kutusu mesajını değiştir

        elif kategori == "yayinevi":
            result = DbQuery.comboBox1Secim("yayinevi")
            yayinevi = []
            if result:
                for i in result:
                   # print("yayinevi: ", i)
                    if i: # i none değilse
                        yayinevi.append(i[0])
            if len(yayinevi) >0:

                self.comboBoxIcerik1.clear()
                self.comboBoxIcerik1.addItems(yayinevi)
                self.comboBoxIcerik1.setPlaceholderText("Yayınevi seçiniz") # secim kutusu mesajını değiştir

        elif kategori == "raf":
            result = DbQuery.comboBox1Secim("raf")
            raf = []
            if result:
                for i in result:
                   # print("yazar: ", i)
                    if i: # i none değilse
                        raf.append(i[0])
            if len(raf) >0:

                self.comboBoxIcerik3.clear()
                self.comboBoxIcerik3.addItems(raf)
                self.comboBoxIcerik3.setPlaceholderText("Raf konumu seçiniz") # secim kutusu mesajını değiştir

        self.comboBoxIcerik1.model().sort(0)
        self.comboBoxIcerik2.model().sort(0)
        self.comboBoxIcerik3.model().sort(0)



    # yazar adı güncelleme
    def yazarGuncellemeFonk(self):
        eski_ad = self.comboBoxIcerik2.currentText()
        yeni_ad = self.lineEditIcerik2.text()
        
        # yeni ad boş olmasın
        if self.comboBoxIcerik2.currentIndex() != -1:
            if len(yeni_ad) > 2:
                print("yeni ad", yeni_ad, type(yeni_ad), len(yeni_ad), "eski ad: ", eski_ad, type(eski_ad), self.comboBoxIcerik2.currentIndex())
                msg = MessageBOXQuestion("BİLGİLENDİRME", f" '{eski_ad}'  yazar adı  '{yeni_ad}'  ile değiştirilecek. (Bu işlem geri alınamaz.)\nİşleme devam edilsin mi?", 0)
                msg.okBut.clicked.connect(lambda: self.yazarGuncellemeFonk_(eski_ad, yeni_ad))   

            else:
                MessageBOX(title="UYARI", subtitle="Yazar yeni isim alanı boş geçilemez. Lütfen bir isim giriniz. (En az 3 karakter)", tip=0)
        else:
            MessageBOX(title="UYARI", subtitle="Öncelikle yazar seçiniz.", tip=0)
      #  result = DbQuery.yazarGuncelle()



    def yazarGuncellemeFonk_(self, eski_ad, yeni_ad):
        result = DbQuery.yazarGuncelle(eski_ad, yeni_ad)
        
        if result == 1:
            MessageBOX(title="BİLGİLENDİRME", subtitle="Güncelleme işlemi başarılı bir şekilde gerçekleştirildi.", tip=1)
            self.yazarYayineviRafSorgula("yazar")
            

        else:
            MessageBOX(title="UYARI", subtitle="Bilinmeyen bir hata oluştu. Lütfen tekrar deneyiniz ya da geliştirici ile iletişim kurunuz.", tip=0)


    
    # yayınevi güncelle
    def yayineviGuncellemeFonk(self):
        eski_ad = self.comboBoxIcerik1.currentText()
        yeni_ad = self.lineEditIcerik1.text()
        
        # yeni ad boş olmasın
        if self.comboBoxIcerik1.currentIndex() != -1:
            if len(yeni_ad) > 2:
                print("yeni ad", yeni_ad, type(yeni_ad), len(yeni_ad), "eski ad: ", eski_ad, type(eski_ad), self.comboBoxIcerik1.currentIndex())
                msg = MessageBOXQuestion("BİLGİLENDİRME", f" '{eski_ad}'  yayınevi  '{yeni_ad}'  ile değiştirilecek. (Bu işlem geri alınamaz.)\nİşleme devam edilsin mi?", 0)
                msg.okBut.clicked.connect(lambda: self.yayineviGuncellemeFonk_(eski_ad, yeni_ad))   

            else:
                MessageBOX(title="UYARI", subtitle="Yayınevi alanı boş geçilemez. Lütfen bir isim giriniz. (En az 3 karakter)", tip=0)
        else:
            MessageBOX(title="UYARI", subtitle="Öncelikle yayınevi seçiniz.", tip=0)
      #  result = DbQuery.yazarGuncelle()



    def yayineviGuncellemeFonk_(self, eski_ad, yeni_ad):
        result = DbQuery.yayineviGuncelle(eski_ad, yeni_ad)
        
        if result == 1:
            MessageBOX(title="BİLGİLENDİRME", subtitle="Güncelleme işlemi başarılı bir şekilde gerçekleştirildi.", tip=1)
            self.yazarYayineviRafSorgula("yayinevi")
            

        else:
            MessageBOX(title="UYARI", subtitle="Bilinmeyen bir hata oluştu. Lütfen tekrar deneyiniz ya da geliştirici ile iletişim kurunuz.", tip=0)



    # yayınevi güncelle
    def rafGuncellemeFonk(self):
        eski_ad = self.comboBoxIcerik3.currentText()
        yeni_ad = self.lineEditIcerik3.text()
        
        # yeni ad boş olmasın
        if self.comboBoxIcerik3.currentIndex() != -1:
            if len(yeni_ad) > 2:
                print("yeni ad", yeni_ad, type(yeni_ad), len(yeni_ad), "eski ad: ", eski_ad, type(eski_ad), self.comboBoxIcerik3.currentIndex())
                msg = MessageBOXQuestion("BİLGİLENDİRME", f" '{eski_ad}'  raf konumu  '{yeni_ad}'  ile değiştirilecek. (Bu işlem geri alınamaz.)\nİşleme devam edilsin mi?", 0)
                msg.okBut.clicked.connect(lambda: self.rafGuncellemeFonk_(eski_ad, yeni_ad))   

            else:
                MessageBOX(title="UYARI", subtitle="Raf konumu alanı boş geçilemez. Lütfen bir raf konumu  giriniz. (En az 3 karakter)", tip=0)
        else:
            MessageBOX(title="UYARI", subtitle="Öncelikle raf konumu seçiniz.", tip=0)
      #  result = DbQuery.yazarGuncelle()



    def rafGuncellemeFonk_(self, eski_ad, yeni_ad):
        result = DbQuery.rafGuncelle(eski_ad, yeni_ad)
        
        if result == 1:
            MessageBOX(title="BİLGİLENDİRME", subtitle="Güncelleme işlemi başarılı bir şekilde gerçekleştirildi.", tip=1)
            self.yazarYayineviRafSorgula("raf")
            

        else:
            MessageBOX(title="UYARI", subtitle="Bilinmeyen bir hata oluştu. Lütfen tekrar deneyiniz ya da geliştirici ile iletişim kurunuz.", tip=0)


    # sınıf yükseltme fonksiyonu
    def sinifYukseltmeFonk(self):
        sinifListeleri = list(self.sinifListeleri) 
        sinifListeleri.sort(reverse=True)

        mezunSinif = self.mezunSinifSecimi.currentText() # str

        if len(sinifListeleri) >0:
            # 1. İşlem öncelikle mezun sınıf silinecek
           
            m = "Yükseltme işlemi başlatılacaktır. (Bu işlem biraz zaman alabilir.)\nNOT: YÜKSELTME İŞLEMİ ÖNCESİNDE YEDEKLEME YAPILMASI ÖNERİLİR.\nYükseltme ve mezuniyet işlemine devam edilsin mi?"
            msg = MessageBOXQuestion("BİLGİLENDİRME", m, 0)
            msg.okBut.clicked.connect(lambda: self.sinifYukseltmeFonk2(sinifListeleri, mezunSinif))  

        else:
            MessageBOX(title="UYARI", subtitle="Yükselticek sınıf bulunamadı. Öncelikle sınıf seçiniz", tip=0)


        
    def sinifYukseltmeFonk2(self, sinif_listeleri, mezunSinif):

        if mezunSinif == "MEZUN YOK": # mezun vermeden yükselt 
            print("sınıf listeleri:__", sinif_listeleri)
            
            yukseltmeYapilamayanlar = []
            basariliKayitlar = []

            for i in sinif_listeleri:
                result = DbQuery.sinifYuksetltme(int(i), int(i) + 1)
                print("veritabanı sonucu: ", result)
                time.sleep(0.1)
                
                if result == 1:
                    print(f"{i}.sınıf başarılı")
                    basariliKayitlar.append(i)

                else:
                    print("başarız işlem", i, result)
                    yukseltmeYapilamayanlar.append(i)

            if len(yukseltmeYapilamayanlar) == 0:
                MessageBOX(title="BAŞARILI İŞLEM", subtitle="Seçtiğiniz sınıfların tamamı başarılı bir şekilde yükseltildi", tip=1)

                self.kayitliSinifListesiFonk() # işlem başarılı olunca sınıf listelerini yeniden uygula
            
            elif len(yukseltmeYapilamayanlar) >0 and len(basariliKayitlar) >0:
                MessageBOX(title="BAŞARILI İŞLEM", subtitle=f"Yükseltme işlemi kısmen tamamlandı.\nYükseltilen sınıflar {basariliKayitlar}\nYükseltme işlemi yapılamayan sınıflar {yukseltmeYapilamayanlar}", tip=0)

                self.kayitliSinifListesiFonk() # işlem başarılı olunca sınıf listelerini yeniden uygula
            
            elif len(basariliKayitlar) == 0:
                MessageBOX(title="İŞLEM TAMAMLANAMDI", subtitle="Yükseltme işlemi başarısız oldu.", tip=0)

                
        else: # mezuniyet uygula ve yükselt
            self.butonSinifYukselt.setText(" Mezuniyet işleniyor...  ")
            self.butonSinifYukselt.setEnabled(False)

            QApplication.processEvents()
            time.sleep(0.5)

            yukseltmeYapilamayanlar = []
            basariliKayitlar = []
            # mezun sınıfı sil

        # Mezuniyet işlemi yapmadan önce mezun olacak sınıfın üyelerinin kitaplarını teslim etmeleri gerekiyor.
        # Öncelikle mezun sınıfın üyelerinden kitapları al

           

            okuyan_siniflar = [i.sinif for i in DbQuery.okuyanUyeler()]
            print("okuyan siniflar", okuyan_siniflar)

            if int(mezunSinif) in okuyan_siniflar:
                print("Okuyan sınıflar var silinemez")
                MessageBOX(title="İŞLEME DEVAM EDİLEMİYOR", subtitle=f" Mezun olacak {mezunSinif}. sınıflardan bazı üyeler aldıkları kitapları teslim etmediği için işleme devam edilemiyor. Üyeler sayfasından okuyan üyeleri listeleyebilirsiniz)", tip=0)

                self.butonSinifYukselt.setText("     Sınıfları Yükselt     ")
                self.butonSinifYukselt.setEnabled(True)

            else:
                print("Mezzuniyete devam edilebilir")


                resultM = DbQuery.sinifSil(mezunSinif)
                print("sınıf listeleri mezunlu", sinif_listeleri, resultM)

                self.butonSinifYukselt.setText(" Sınıflar yükseltiliyor... ")
                QApplication.processEvents()
                time.sleep(0.5)

                if resultM == 1: # mezun işlemi başarılı ise işleme devam et değilse işlemi iptal et
                    for i in sinif_listeleri:
                        result = DbQuery.sinifYuksetltme(int(i), int(i) + 1)
                        print("veritabanı sonucu Mezunlu: ", result)
                        time.sleep(0.1)
                        if result == 1:
                            print(f"{i}.sınıf başarılı")
                            basariliKayitlar.append(i)
                        else:
                            print("başarız işlem", i, result)
                            yukseltmeYapilamayanlar.append(i)

                    if len(yukseltmeYapilamayanlar) == 0:
                        MessageBOX(title="BAŞARILI İŞLEM", subtitle="Seçtiğiniz sınıfların tamamı başarılı bir şekilde yükseltildi", tip=1)
                        self.kayitliSinifListesiFonk() # işlem başarılı olunca sınıf listelerini yeniden uygula

                    elif len(yukseltmeYapilamayanlar) >0 and len(basariliKayitlar) >0:
                        MessageBOX(title="BAŞARILI İŞLEM", subtitle=f"Yükseltme işlemi kısmen tamamlandı.\nYükseltilen sınıflar {basariliKayitlar}\nYükseltme işlemi yapılamayan sınıflar {yukseltmeYapilamayanlar}", tip=0)
                        self.kayitliSinifListesiFonk() # işlem başarılı olunca sınıf listelerini yeniden uygula

                    elif len(basariliKayitlar) == 0:
                        MessageBOX(title="İŞLEM TAMAMLANAMDI", subtitle="Yükseltme işlemi başarısız oldu.", tip=0)
                        self.kayitliSinifListesiFonk() # işlem başarılı olunca sınıf listelerini yeniden uygula
                else:
                    MessageBOX(title="İŞLEM TAMAMLANAMDI", subtitle="Yükseltme işlemi mezuniyet işlemlerinin gerçekleştirilememesinden dolayı başarısız oldu.", tip=0)


                self.butonSinifYukselt.setText("     Sınıfları Yükselt     ")
                self.butonSinifYukselt.setEnabled(True)


    # kullanıcı bilgilerini ve ayarlarını sistemden çek
    def enterpriseInfoSettingGetFonk(self):
        self.settingsInfo = DbQuery.ayarlarVeYapilandirmalar()


        self.lineEditKurum1.setText(self.settingsInfo.kurum_adi)
        self.lineEditKurum2.setText(self.settingsInfo.kurum_adresi)
        self.lineEditKurum3.setText(self.settingsInfo.kurum_yoneticisi)
        self.lineEditKurum4.setText(self.settingsInfo.kurum_tel)

        # print("bool:", bool(self.settingsInfo.alinabilecek_kitap_sayisi), self.settingsInfo.alinabilecek_kitap_sayisi, self.settingsInfo.kitap_emanet_suresi)

        ## SEcim kutularına secim gönder
        self.comboYapilandirma2.setCurrentText(str(self.settingsInfo.kitap_emanet_suresi) if bool(self.settingsInfo.kitap_emanet_suresi) == True else "15")
        self.comboYapilandirma1.setCurrentText(str(self.settingsInfo.alinabilecek_kitap_sayisi) if bool(self.settingsInfo.alinabilecek_kitap_sayisi) == True else "1")
        
        

    # kurum bilgilerini guncelleme ekranı aktif etme
    def kurumBilgileriniGuncelleFonk(self):
        
        
        if self.anahtar: # anahtar True ise düzenlenebilir (acılısta anantar true )
            stilA = """ QLineEdit {border: 2px solid #DAE4FD; background-color: transparent; border-radius: 8px; 
                        font-size: 17px; color: black; font-weight: 500;padding-left: 8px; } """
            self.lineEditKurum1.setEnabled(self.anahtar)
            self.lineEditKurum2.setEnabled(self.anahtar)
            self.lineEditKurum3.setEnabled(self.anahtar)
            self.lineEditKurum4.setEnabled(self.anahtar)

            self.comboYapilandirma1.setEnabled(self.anahtar)
            self.comboYapilandirma2.setEnabled(self.anahtar)

            self.lineEditKurum1.setStyleSheet(stilA)
            self.lineEditKurum2.setStyleSheet(stilA)
            self.lineEditKurum3.setStyleSheet(stilA)
            self.lineEditKurum4.setStyleSheet(stilA)

            self.bilgiGuncelleBut.setStyleSheet("""QPushButton#bilgileriniGuncelleBut {background-image: url("assets/icon/save-32X32.svg");
                background-position: left; background-repeat: no-repeat; background-color: transparent; outline: 0px; font-size: 16px; font-weight: 700; color: #2ac4c7;
                } """)
            self.bilgiGuncelleBut.setText("Kaydet")
            
            self.anahtar = False   

        else: # 
            self.lineEditKurum1.setEnabled(self.anahtar)
            self.lineEditKurum2.setEnabled(self.anahtar)
            self.lineEditKurum3.setEnabled(self.anahtar)
            self.lineEditKurum4.setEnabled(self.anahtar)

            self.comboYapilandirma1.setEnabled(self.anahtar)
            self.comboYapilandirma2.setEnabled(self.anahtar)

            self.lineEditKurum1.setStyleSheet(stil)
            self.lineEditKurum2.setStyleSheet(stil)
            self.lineEditKurum3.setStyleSheet(stil)
            self.lineEditKurum4.setStyleSheet(stil)

            self.bilgiGuncelleBut.setStyleSheet("""QPushButton#bilgileriniGuncelleBut {background-image: url("assets/icon/edit-32x32.svg");
        background-position: left; background-repeat: no-repeat; background-color: transparent; outline: 0px; font-size: 16px; font-weight: 700; color: #686868;} """)
            self.bilgiGuncelleBut.setText("Düzenle")

            self.anahtar = True

            if self.settingsInfo.rowID == None:
                msg = MessageBOXQuestion("UYARI", " Yaptığınız değişikliklerin sisteme yansıması için uygulamanın yeniden başlatılması gerekmektedir.\n Devam edilsin mi?", 1)
                msg.okBut.clicked.connect(lambda: self.kurumBilgileriniGuncelleFonk2(1))    
                  
            else:
                msg = MessageBOXQuestion("UYARI", " Yaptığınız değişikliklerin sisteme yansıması için uygulamanın yeniden başlatılması gerekmektedir.\n Devam edilsin mi?", 1)
                msg.okBut.clicked.connect(lambda: self.kurumBilgileriniGuncelleFonk2(2))
                


    ## sistemde yapılan değişiklikleri db ye aktar
    def kurumBilgileriniGuncelleFonk2(self, durum):
        rowid = self.settingsInfo.rowID
        kurum_adi = self.lineEditKurum1.text()
        kurum_adresi = self.lineEditKurum2.text()
        yonetici = self.lineEditKurum3.text()
        telefon = self.lineEditKurum4.text()
        alinabilecek_kitap = self.comboYapilandirma1.currentText()
        emanet_suresi = self.comboYapilandirma2.currentText()

        if durum == 1:
            DbQuery.kurumBilgileriniOlustur(kurum_adi, kurum_adresi, yonetici, telefon, alinabilecek_kitap, emanet_suresi)
            self.restartApp()

        elif durum == 2:
            DbQuery.kurumBilgileriniGuncelle(kurum_adi, kurum_adresi, yonetici, telefon, alinabilecek_kitap, emanet_suresi, rowid)
            self.restartApp()


    ## Yedekleme bilgilerini veritabanından cekme
    def yedeklemeBilgileri(self):

        # Ayarları sistemden cek eğer setting üzerine kayıtlı adres yok ise varsayılan konumu gönder
        konum = SettingsConf.getValueConf("backupSettings", "backupPath")

        yedeke_dosya_adi = "kuyosis_yedek.db"
        
        if konum:
            self.yedeklemeDosyaYolu.setText(f"{konum}")
            self.yedeklemeLabelTitle1.setText("Yedekleme konumu")

            # yedek dosyanın oluşturulma tarihini al
            yol = os.path.join(konum, yedeke_dosya_adi)

            if os.path.exists(yol):
                #dosya oluşturma tarihi
                time_stamp = os.stat(yol).st_mtime
                date_time = datetime.fromtimestamp(time_stamp)

                self.yedeklemeTarihi.setText(f"""{date_time.strftime("%H.%M - %d.%m.%Y")}\n{yedeke_dosya_adi}""")

                #Yedegi geri yukleme butonunu aktif et
                self.yedegiGeriYukle.setEnabled(True)

            else:
                self.yedeklemeTarihi.setText("Henüz yedekleme yapılmadı")
                self.yedegiGeriYukle.setEnabled(False)
        
        else:
            # Yedekleme varsayılan konumu
            yedeklemeVarsayilanKonum = DbQuery.appLocalPath
            print("yedekleme konumu: ", yedeklemeVarsayilanKonum)
            self.yedeklemeDosyaYolu.setText(yedeklemeVarsayilanKonum)

            # yedek dosyanın oluşturulma tarihini al
            yol = os.path.join(yedeklemeVarsayilanKonum, yedeke_dosya_adi)

            if os.path.exists(yol):
                #dosya oluşturma tarihi
                time_stamp = os.stat(yol).st_mtime
                date_time = datetime.fromtimestamp(time_stamp)
                

                self.yedeklemeTarihi.setText(f"""{date_time.strftime("%H.%M - %m/%d/%Y")}\n{yedeke_dosya_adi}""")

                #Yedegi geri yukleme butonunu aktif et
                self.yedegiGeriYukle.setEnabled(True)
       
            else:
                self.yedeklemeTarihi.setText("Henüz yedekleme yapılmadı")
                self.yedegiGeriYukle.setEnabled(False)
           


    # yedekleme konumu sec ve consf dosyasına aktar
    def yedeklemeKonumuSec(self):
    
        home_dir = str(Path.home())
        fnamePath = QFileDialog.getExistingDirectory(self, 'Yedekleme konumu seç')
        print(fnamePath, home_dir, bool(fnamePath))

        if bool(fnamePath) == True:
            self.yedeklemeDosyaYolu.setText(f"{fnamePath}")
            # Başlığı da değiştir
            self.yedeklemeLabelTitle1.setText("Yedekleme konumu")

            # Yedekleme konumunu conf dosyasına yaz
            
            SettingsConf.setValueConf("backupSettings", "backupPath", f"{fnamePath}")

            #Yedekleme bilgilerini güncelle
            self.yedeklemeBilgileri()


        else:
            pass


    # Yedekleme Yap
    def yedekleFonk(self):
        yedeke_dosya_adi = "kuyosis_yedek.db"
        dosya_konumu = self.yedeklemeDosyaYolu.text()

        # Yedekleme konumunu doğrula
        if os.path.exists(dosya_konumu):
            self.simdiYedekle.setText("   Yedekleniyor...   ")

            QApplication.processEvents()

            result = DbQuery.yedeklemeOlustur(dosya_konumu)
            if result[0]:
                self.simdiYedekle.setText("    Şimdi Yedekle    ")

                MessageBOX(title="BAŞARILI İŞLEM", subtitle=f"Veritabanı başarılı bir şekilde yedeklendi.\nYedekleme konumu:\n{dosya_konumu}", tip=1)

                self.yedeklemeBilgileri() # yedekleme bilgilerini güncellemek için

            else:
                self.simdiYedekle.setText("    Şimdi Yedekle    ")

                MessageBOX(title="HATA", subtitle=f"Veritabanı yedeklenemedi. \n {result[1]}", tip=0)


        else:
            MessageBOX(title="UYARI", subtitle="Belirlenen yedekleme konumu bulunamadı. Lütfen yedekleme konumunu yeniden seçiniz", tip=0)


    def yedegiGeriYukleFonk(self):
        m ="Yedek veritabanı dosyası geri yüklenecek. Yükleme işleminden sonra uygulama yeniden başlatılacak.\nİşleme devam edilsin mi?"
        msg = MessageBOXQuestion("BİLGİLENDİRME", m, 0)
        msg.okBut.clicked.connect(self.yedegiGeriYukleFonk_)  

    # Yedeklemeyi geri yükleme
    def yedegiGeriYukleFonk_(self):
        yedeke_dosya_adi = "kuyosis_yedek.db"
        dosya_konumu = self.yedeklemeDosyaYolu.text()

        if os.path.exists(os.path.join(dosya_konumu, yedeke_dosya_adi)):
            geri_yukle = DbQuery.yedegiGeriYukle(dosya_konumu, yedeke_dosya_adi)

            if geri_yukle[0] == 1:
                MessageBOX(title="BAŞARILI İŞLEM", subtitle=f"{geri_yukle[1]}", tip=1)

                QTimer.singleShot(2000, lambda: self.restartApp()) 

            else:
                MessageBOX(title="HATA", subtitle=f"{geri_yukle[1]}", tip=0)

        else:
            MessageBOX(title="UYARI", subtitle=f"{os.path.join(dosya_konumu, yedeke_dosya_adi)}\n Belirlenen konumda yedekleme dosyası bulunamadı.", tip=0)



    # Yeni sürüm kontrolü
    def yeniSurumKontrolu(self):

        try: 
            response = urllib.request.urlopen(self.yeniSurumAdresi, cafile=certifi.where())
            
            content = response.read() 
            content = content.decode('utf-8') 


            self.lineEditKurum1.setEnabled(self.anahtar)
            
            try:
                json_file = json.loads(content) 
                print(type(json_file)) 
                print(json_file["versiyon"], type(json_file["versiyon"]), type(self.versiyon))

                # QDesktopServices.openUrl(QUrl(self.yeniSurumAdresi))

                # sürüm kontrol numarsına göre güncellemeyi yönet
                if json_file['versiyon'] == self.versiyon:
                    print("Sürümler eşit")
                    MessageBOX(title="UYARI", subtitle=f" Programınız günceldir", tip=1)
                
                    self.surumKontrolLabel.setText("Güncel sürümü kullanmaktasınız.")

            
                elif json_file['versiyon'] > self.versiyon:
                    print("Yeni sürüm mevcut")

                    self.dowloandWindow = DownloadWindow(json_file)
                    self.dowloandWindow.show()

                    self.surumKontrolLabel.setText("Yeni bir güncelleme bulundu.")

            except:
                MessageBOX(title="UYARI", subtitle="Dosya ayrıştırma hatası oluştu. Lütfen geliştiriciyle iletişim kurunuz.", tip=0)


            

        except urllib.error.HTTPError as e: 
            print(f"HTTP Error: {e.code} {e.reason}")
            MessageBOX(title="UYARI", subtitle=f"Sunucusuyla bağlantı kurulamadı. Daha sonra tekrar deneyiniz. Sorun halen daha devam ediyorsa geliştiriciye bildiriniz.\nHata: {e.code} {e.reason}", tip=0)

        except urllib.error.URLError as e: 
            print(f"URL Error: {e.reason}")
            MessageBOX(title="UYARI", subtitle=f"Sunucusuyla bağlantı kurulamadı. İnternet erişiminizi kontrol ediniz. Sorun halen daha devam ediyorsa geliştiriciye bildiriniz.\nHata: {e.reason}", tip=0)

        except:
            MessageBOX(title="UYARI", subtitle="Bilinmeyen bir hata oluştu. Lütfen geliştiriciyle iletişim kurunuz.", tip=0)


## uygulamayı yeniden başlatma
    def restartApp(self):
        print("sys argv", sys.argv)
        QtCore.QCoreApplication.quit()
        QtCore.QProcess.startDetached(sys.executable, sys.argv)


    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()       
        
        



