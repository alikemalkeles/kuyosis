from PyQt6.QtCore import QSettings

class SettingsConf():

    __APPNAME = "kuyosis"
    __FILENAME = "settings"

    global _setting
    _setting = QSettings(__APPNAME, __FILENAME)

    

    def setValueConf(group_name, key, value):
        _setting.beginGroup(group_name)
        _setting.setValue(key, value)
        _setting.endGroup()
    


    def getValueConf(group_name, key):
        _setting.beginGroup(group_name)
        value = _setting.value(key)
        _setting.endGroup()
        # print("value conf_____:", value)

        return value


