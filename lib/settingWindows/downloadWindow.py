import subprocess
import webbrowser

from PyQt6 import QtCore
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QGridLayout, QPushButton, QLabel, QFrame, QDialog, QListWidget, QListWidgetItem

from customWidget.messageBox import MessageBOX
from customWidget.shadowEffect import BoxShadowEffect

with open("assets/stil/stil_downloadWindow.qss", "r") as file:
    stil = file.read()

class DownloadWindow(QDialog):
    def __init__(self, file: dict):
        super().__init__()

        self.content = file

        self.setStyleSheet(stil)
        self.setFixedSize(650, 550)
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)
        self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)
        self.setContentsMargins(0,0,0,0)

        self.mainFrame = QFrame(self)
        self.mainFrame.setObjectName("mainFrameDownload")
        self.mainFrame.setContentsMargins(8,8,8,8)
        self.mainFrame.setFixedSize(650, 550)


    # Ana LAyout ögeleri
        self.mainLayout = QGridLayout(self.mainFrame)
        self.mainLayout.setContentsMargins(0, 0, 0, 0)

        self.closeBut = QPushButton()
        self.titleLabel = QLabel("YENİ SÜRÜM BULUNDU")

        self.iconLabel = QLabel()
        self.nameLabel = QLabel()
        self.dateLabel = QLabel()

        self.newsLabel = QLabel("Yenilikler")

        self.newsPanel = QListWidget()

        self.infoLabel = QLabel()

        self.olderVersiyonBut = QLabel() # QPushButton("Eski Sürümleri Görüntüle")
        self.olderVersiyonBut.setOpenExternalLinks(True)
        self.olderVersiyonBut.setText (f"""<a href=\"{self.content["eski_versiyon_adresi"]}\" >Eski Sürümler</a>""");


        self.updateBut = QPushButton("Web Tarayıcıda İndir")

        self.closeBut.setFixedSize(24, 24)
        self.newsPanel.setWordWrap(True)
        self.titleLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignCenter)
        self.iconLabel.setFixedSize(50,75)
        self.newsPanel.setSpacing(5)
        self.newsLabel.setFixedHeight(20)
        self.infoLabel.setWordWrap(True)
        self.titleLabel.setFixedHeight(60)
        self.newsPanel.setFixedSize(632, 205)
    

        self.updateBut.setGraphicsEffect(BoxShadowEffect.colorTeal())
        #self.newsPanel.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        
        
        self.closeBut.setObjectName("closeBut")
        self.titleLabel.setObjectName("titleLabel")
        self.iconLabel.setObjectName("iconLabel")
        self.nameLabel.setObjectName("nameLabel")
        self.dateLabel.setObjectName("dateLabel")
        self.newsLabel.setObjectName("newsLabel")
        self.infoLabel.setObjectName("infoLabel")
        self.olderVersiyonBut.setObjectName("olderVersiyonBut")
        self.updateBut.setObjectName("updateBut")


        self.mainLayout.addWidget(self.titleLabel,          0, 1, 1, 3)
        self.mainLayout.addWidget(self.closeBut,            0, 4, 1, 1,    alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignTop)
    #    self.mainLayout.setRowStretch(1, 1)
        self.mainLayout.addWidget(self.iconLabel,           1, 0, 1, 1,    alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        self.mainLayout.addWidget(self.nameLabel,           1, 1, 1, 1)
        self.mainLayout.addWidget(self.dateLabel,           1, 3, 1, 2,    alignment=QtCore.Qt.AlignmentFlag.AlignRight)
        self.mainLayout.addWidget(self.newsLabel,           2, 0, 1, 5,    alignment=QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.mainLayout.addWidget(self.newsPanel,           3, 0, 5, 5)
        self.mainLayout.addWidget(self.infoLabel,           8, 0, 2, 5)
        self.mainLayout.addWidget(self.olderVersiyonBut,   10, 0, 1, 2)
        self.mainLayout.addWidget(self.updateBut,          10, 3, 1, 2)


    # sinyal slotlar
        self.closeBut.clicked.connect(self.close)
        self.updateBut.clicked.connect(self.updateInstallFonk)
       # self.olderVersiyonBut.clicked.connect(self.oldVersionListFonk)


        self.initialFonk()
        

    def initialFonk(self):
        self.nameLabel.setText(f"""Küyosis {self.content["versiyon"]}""")
        self.dateLabel.setText(f"""Güncellenme tarihi: {self.content["guncelleme_tarihi"]}""")
        
        for item in self.content["versiyon_ozellikleri"]:
            self.newsPanel.addItem(QListWidgetItem(QIcon("assets/icon/madde.png"), item))

        self.infoLabel.setText(f"""<font color = "red" weight = "bold"> Bilgilendirme: </font> {self.content["versiyon_iletisi"]}""")


    # Güncellemyi indir
    def updateInstallFonk(self):
        self.close()
        try:
            rapor = subprocess.Popen(['firefox', self.content["indirme_adresi"]])
            
            print("_________indirme adresi subprocess__________")
        except Exception as hata:
            MessageBOX("HATA", " Bilinmeyen bir hata oluştu. Lütfen geliştiriciyle iletişim kurunuz.", 0)
            print(hata)

    # eski sürümleri listele
    def oldVersionListFonk(self):

        try:
          #  rapor = subprocess.Popen(['firefox', self.content["eski_versiyon_adresi"]])
            webbrowser.open(self.content["eski_versiyon_adresi"], 0, True)
        except Exception as hata:
            MessageBOX("HATA", " Bilinmeyen bir hata oluştu. Lütfen geliştiriciyle iletişim kurunuz.", 0)
            print(hata)

        self.close()