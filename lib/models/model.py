

## kitap modeli
class BookModel():
    def __init__(self, id_, karekod, kitap_adi, yazar, yayinevi, tür, sayfa, bilgi, raf, tarih):
        self.id = id_
        self.karekod_no = karekod
        self.ad = kitap_adi
        self.yazar = yazar
        self.yayınevi = yayinevi
        self.tür = tür
        self.sayfa = sayfa
        self.bilgi = bilgi
        self.raf = raf
        self.tarih = tarih

   


## üye modeli
class MemberModel():
    def __init__(self, id_, uye_tc, uye_adi, uye_soyadi, d_tarihi, cinsiyet, ogrenci_no, sinif, sube, adres, tel, kart_durumu, bilgi, eklenme_tarihi):
        self.id = id_
        self.tc = uye_tc
        self.ad = uye_adi
        self.soyad = uye_soyadi
        self.d_tarihi = d_tarihi
        self.cinsiyet = cinsiyet
        self.ogrenci_no = ogrenci_no
        self.sinif = sinif
        self.sube = sube
        self.adres = adres
        self.tel = tel
        self.kart_durumu = kart_durumu
        self.bilgi = bilgi
        self.eklenme_tarihi = eklenme_tarihi

    


class Users():
    def __init__(self, 
        id, isim, statu, parola, eklenme_tarihi,
        ekleme_yetkisi, guncelleme_yetkisi, silme_yetkisi, ayar_yetkisi, raporlama_yetkisi, istatistik_yetkisi, coklu_aktarim_yetkisi, karekod_kimlik_yetkisi
        ):
        self.id: int = id
        self.isim: str = isim
        self.statu: bool = statu
        self.parola: str = parola
        self.eklenme_tarihi: str = eklenme_tarihi
        self.ekleme_yetkisi: bool = ekleme_yetkisi
        self.guncelleme_yetkisi: bool = guncelleme_yetkisi
        self.silme_yetkisi: bool = silme_yetkisi
        self.ayar_yetkisi: bool = ayar_yetkisi
        self.raporlama_yetkisi: bool = raporlama_yetkisi
        self.istatistik_yetkisi: bool = istatistik_yetkisi
        self.coklu_aktarim_yetkisi: bool = coklu_aktarim_yetkisi
        self.karekod_kimlik_yetkisi: bool = karekod_kimlik_yetkisi

# Günlük verilen kitapların listesini tutan model (["Kitap ID", "K. Karekod No", "Kitap Adı", "Yazar", "Üye ID", "Üye Kimlik No", "Üye Adı", "Soyadı","Öğrenci No", "Süre", "Son teslim Tarihi"])
class GivenTakenBookModel():
    def __init__(self, rowid, kitap_id, karekod, kitap_adi, yazar, uye_id, tc, uye_adi, soyadi, ogrenci_no, emanete_verilme_tarihi, teslim_tarihi, teslim_suresi, son_teslim_tarihi, verilme_saati, teslim_saati):
        self.rowID = rowid
        self.kitap_id = kitap_id
        self.karekod_no = karekod
        self.kitap_adi = kitap_adi
        self.yazar = yazar

        self.uye_id = uye_id
        self.tc = tc
        self.uye_adi = uye_adi
        self.soyad = soyadi
        self.ogrenci_no = ogrenci_no

        self.emanete_verilme_tarihi = emanete_verilme_tarihi
        self.teslim_tarihi = teslim_tarihi
        self.teslim_suresi = teslim_suresi
        self.son_teslim_tarihi = son_teslim_tarihi
        self.verilme_saati = verilme_saati
        self.teslim_saati = teslim_saati


#Kurum bilgileri ve yapılandırmaları tutan model
class EnterpriseSettingModel():
    def __init__(self, rowid, kurum_adi, kurum_adresi, kurum_yoneticisi, kurum_tel, alinabilecek_kitap_sayisi, emanet_suresi):
        self.rowID = rowid
        self.kurum_adi = kurum_adi
        self.kurum_adresi = kurum_adresi
        self.kurum_yoneticisi = kurum_yoneticisi
        self.kurum_tel = kurum_tel
        self.alinabilecek_kitap_sayisi = alinabilecek_kitap_sayisi
        self.kitap_emanet_suresi = emanet_suresi
