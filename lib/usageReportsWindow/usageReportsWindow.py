from datetime import datetime  # kod sürelerini ölcmek için

from PyQt6 import QtCore
from PyQt6.QtCharts import QChart, QChartView, QPieSeries, QBarSet, QPercentBarSeries, QBarCategoryAxis, \
    QAbstractBarSeries, QValueAxis
from PyQt6.QtCore import Qt, QSize, QTimer, QDate
from PyQt6.QtGui import QColor
from PyQt6.QtGui import QPainter
from PyQt6.QtWidgets import QPushButton, QFrame, QGridLayout, QLabel, \
    QListWidget, QVBoxLayout, QHBoxLayout, QListWidgetItem, QWidget, QScrollArea, QDateEdit, QCalendarWidget

from query.dbQuery import DbQuery
from models.model import Users
from customWidget.shadowEffect import BoxShadowEffect


gifIconPath = "icon/progres1.gif"

with open("assets/stil/stil_usageReports.qss") as file:
    stil = file.read()


class UsageReportsWindow(QFrame):
    def __init__(self, user: Users) -> None:
        super().__init__()

        self.user = user

        self.database = DbQuery()

        self.setContentsMargins(0,0,0,0)
        self.setStyleSheet(stil)

        self.scrollLayout = QVBoxLayout()
        self.scrollLayout.setContentsMargins(0,0,0,0)

        self.setLayout(self.scrollLayout)

        #Ekran çözünürlüğü
        screen = self.screen()
        #  print(screen.size().width(), screen.size().height())
        self.w, self.h = (screen.size().width(), screen.size().height())

        # ComboBox ve lineedit widget yükseklikleri
        self.h1 = 28 if self.h > 900 else 24 #  buton yüksekliği

    #Ana layout (ScrollBAraera aktif olabilmesi için layout yerine bir widgete bağlandı)
        widget = QWidget()
        self.mainLayout = QGridLayout(widget)
        self.mainLayout.setContentsMargins(8, 0, 8, 8)
        self.mainLayout.setAlignment(Qt.AlignmentFlag.AlignTop)
   #     self.mainLayout.setSizeConstraint(QGridLayout.SizeConstraint.SetMinimumSize)


        self.scrollAreaWid = QScrollArea()
        self.scrollAreaWid.setWidgetResizable(True)
        self.scrollAreaWid.setLayoutDirection(Qt.LayoutDirection.LeftToRight)   #setWidget(self.mainFrame)
        self.scrollAreaWid.setWidget(widget)   #   setLayout(self.mainLayout)
        self.scrollAreaWid.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.scrollAreaWid.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)

        self.scrollLayout.addWidget(self.scrollAreaWid)

        

     
        self.frame1 = QFrame()
        self.frame2 = QFrame()
        self.frame3 = QFrame()


        self.title1 = QLabel("En Çok Okunan Kitaplar (Okunma Sayısı + Kitap Adı) (en fazla 50 kitap)")
        self.title2 = QLabel("En çok Okuyan Üyeler (Okuma Sayısı + Üye Adı)")

        self.frame4 = QFrame() #
        self.frame5 = QFrame()

        layout4 = QHBoxLayout(self.frame4)
        layout5 = QHBoxLayout(self.frame5)

        layout4.setContentsMargins(8, 2, 0, 2)
        layout5.setContentsMargins(8, 2, 0, 2)

        self.frame4List = QListWidget()
        self.frame5List = QListWidget()

        layout4.addWidget(self.frame4List)
        layout5.addWidget(self.frame5List)

        self.title6 = QLabel("Cinsiyete göre okuma oranı")
        self.title7 = QLabel("Sınıf seviyesine göre okuma oranı")
        self.title8 = QLabel("Aylara göre okuma oranı")

        self.frame6 = QFrame()
        self.frame7 = QFrame()
        self.frame8 = QFrame()


        self.frame1.setObjectName("frame")
        self.frame2.setObjectName("frame")
        self.frame3.setObjectName("frame")
        self.frame4.setObjectName("frame")
        self.frame5.setObjectName("frame")
        self.frame6.setObjectName("frame")
        self.frame7.setObjectName("frame")
        self.frame8.setObjectName("frame")

        self.frame4.setContentsMargins(8, 10, 6, 8)
        self.frame5.setContentsMargins(8, 10, 6, 8)
        self.frame6.setContentsMargins(0,0,0,0)
        self.frame7.setContentsMargins(0,0,0,0)


        self.frame1.setContentsMargins(4, 4, 4, 4)
        self.frame2.setContentsMargins(4, 4, 4, 4)
        self.frame3.setContentsMargins(4, 4, 4, 4)


        self.frame1.setFixedHeight(150)
        self.frame2.setFixedHeight(150)
        self.frame3.setFixedHeight(150)
        self.frame4.setFixedHeight(380)
        self.frame5.setFixedHeight(380)
        self.frame6.setFixedHeight(400)
        self.frame7.setFixedHeight(400)
        self.frame8.setFixedHeight(400)

        self.frame1.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame2.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame3.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame4.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame5.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame6.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame7.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.frame8.setGraphicsEffect(BoxShadowEffect.colorBlue())


        # ana başlık
        self.title = QLabel("KÜTÜPHANE KULLANIM İSTATİSTİKLERİ")
        self.title.setObjectName("title")
        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)


        self.dataUpdateBut = QPushButton()
        self.dataUpdateBut.setText(" Veri Topla")
        self.dataUpdateBut.setObjectName("dataUpdate")
        self.dataUpdateBut.setFixedSize(165, 32)

        self.islemItetisi = QLabel()

        self.dateFilter = QDateEdit()
        self.dateFilter.setCalendarPopup(True)
        self.dateFilter.setFixedHeight(self.h1)



        self.titleDate = QLabel("Başlangıç tarihi ")
        self.titleDate.setObjectName("titleDate")
        self.titleDate.setWordWrap(True)
        self.titleDate.setFixedHeight(self.h1)


        self.mainLayout.addWidget(self.title,                 0, 5, 1,  5)
        self.mainLayout.addWidget(self.titleDate,             0, 11, 1, 1,  alignment=Qt.AlignmentFlag.AlignRight | Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addWidget(self.dateFilter,            0, 12, 1, 1,  alignment=Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignVCenter)
        self.mainLayout.addWidget(self.islemItetisi,          0, 13, 1, 1)
        self.mainLayout.addWidget(self.dataUpdateBut,         0, 14, 1, 1)


        self.mainLayout.addWidget(self.frame1,  1, 0, 1, 5)
        self.mainLayout.addWidget(self.frame2,  1, 5, 1, 5)
        self.mainLayout.addWidget(self.frame3,  1, 10, 1, 5)

        self.mainLayout.addWidget(self.title6,  2, 0, 1, 3)
        self.mainLayout.addWidget(self.title8,  2, 3, 1, 6)
        self.mainLayout.addWidget(self.title7,  2, 9, 1, 6)
        self.mainLayout.addWidget(self.frame6,  3, 0, 3, 3)
        self.mainLayout.addWidget(self.frame8,  3, 3, 3, 6)
        self.mainLayout.addWidget(self.frame7,  3, 9, 3, 6)

        self.mainLayout.addWidget(self.title1,  6, 0, 1, 8)
        self.mainLayout.addWidget(self.title2,  6, 8, 1, 7)
        self.mainLayout.addWidget(self.frame4,  7, 0, 3, 8)
        self.mainLayout.addWidget(self.frame5,  7, 8, 3, 7)

        # sutunları gerdirme factoru eşit dağılım
        for column in range(0, 15):
           # print("strech: ", column)
            self.mainLayout.setColumnStretch(column, 2)

        self.mainLayout.setSpacing(5)



        #FRame1 Toplam kitap sayısı
        self.grid1 = QGridLayout(self.frame1)
        self.grid1.setContentsMargins(0,0,0,0)


        self.labelIcon1 = QLabel("")
        self.labelTitle1 = QLabel("Toplam Kitap Sayısı")
        self.labelValue1 = QLabel("")

        self.labelIcon1.setFixedWidth(150)
        self.labelTitle1.setWordWrap(True)
        self.labelTitle1.adjustSize()

        self.labelValue1.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.labelTitle1.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.labelIcon1.setObjectName("labelIcon1")
        self.labelTitle1.setObjectName("labelTitle1")
        self.labelValue1.setObjectName("labelValue1")

        self.grid1.addWidget(self.labelIcon1,    0, 0, 2, 1)
        self.grid1.addWidget(self.labelTitle1,   0, 1, 1, 3)
        self.grid1.addWidget(self.labelValue1,   1, 1, 1, 3)



        #Frame2 Toplam Üye sayısı
        self.grid2 = QGridLayout(self.frame2)
        self.grid2.setContentsMargins(0,0,0,0)

        self.labelIcon2 = QLabel("")
        self.labelTitle2 = QLabel("Toplam Üye Sayısı")
        self.labelValue2 = QLabel("")

        self.labelIcon2.setFixedWidth(150)
        self.labelTitle2.setWordWrap(True)

        self.labelValue2.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.labelTitle2.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.labelTitle2.adjustSize()
       

        self.labelIcon2.setObjectName("labelIcon2")
        self.labelTitle2.setObjectName("labelTitle2")
        self.labelValue2.setObjectName("labelValue2")

        self.grid2.addWidget(self.labelIcon2,    0, 0, 2, 1)
        self.grid2.addWidget(self.labelTitle2,   0, 1, 1, 3)
        self.grid2.addWidget(self.labelValue2,   1, 1, 1, 3)



        #Frame3  Toplam verilen alınan kitap sayısı
        self.grid3 = QGridLayout(self.frame3)
        self.grid3.setContentsMargins(0,0,0,0)

        self.labelIcon3 = QLabel("")
        self.labelTitle3 = QLabel("Emanete Verilen Kitap Sayısı (Seçili tarihten sonra)")
        self.labelValue3 = QLabel("")

        self.labelIcon3.setFixedWidth(150)
        self.labelTitle3.setWordWrap(True)

        self.labelValue3.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.labelTitle3.adjustSize()

        self.labelIcon3.setObjectName("labelIcon3")
        self.labelTitle3.setObjectName("labelTitle3")
        self.labelValue3.setObjectName("labelValue3")

        self.grid3.addWidget(self.labelIcon3,    0, 0, 2, 1)
        self.grid3.addWidget(self.labelTitle3,   0, 1, 1, 3)
        self.grid3.addWidget(self.labelValue3,   1, 1, 1, 3)




    # Okumanın cinsiyete göre sonuclarını döndürür.
        # Okuma oranları frame 6
        pieLayout = QHBoxLayout()
        pieLayout.setContentsMargins(0,0,0,0)
        
        self.seriesC = QPieSeries()
        # self.seriesC.append("Erkek", resultC["erkek"])
        # self.seriesC.append("Kız", resultC["kız"])
        # series.setLabelsPosition(QPieSlice.)
        for slice in self.seriesC.slices():
            slice.setLabel("{:.2f}%".format(100 * slice.percentage()))

        self.chartC = QChart()
        self.chartC.addSeries(self.seriesC)
        self.chartC.createDefaultAxes()
        self.chartC.setAnimationOptions(QChart.AnimationOption.SeriesAnimations)
        self.chartC.setTitle("ERKEK & KIZ")
        self.chartC.setBackgroundBrush(QColor(255,255,255, 0))

        chartview = QChartView(self.chartC)
        chartview.setRenderHint(QPainter.RenderHint.Antialiasing)
        chartview.setBackgroundBrush(QColor(255,255,255, 0))

        pieLayout.addWidget(chartview)
        self.frame6.setLayout(pieLayout)



    # sınıf seviyesine göre okuma
        barSetLayout1 = QHBoxLayout()
        barSetLayout1.setContentsMargins(4,4,4,4)

        self.set0S = QBarSet("")
        self.set1S = QBarSet("")

        self.set1S.setColor(QColor(255,255,255, 0))
        self.set0S.setColor(QColor(0,185,193, 250))
        self.set0S.setLabelColor(QColor(0, 0, 0, 250))

        seriesS = QPercentBarSeries()
        seriesS.append(self.set0S)
        seriesS.append(self.set1S)
        seriesS.setBarWidth(0.9)
     #   seriesS.setLabelsVisible(True)
        seriesS.setLabelsPosition(QAbstractBarSeries.LabelsPosition.LabelsOutsideEnd)
        
        chartS = QChart()
        chartS.addSeries(seriesS)
        chartS.setTheme(QChart.ChartTheme.ChartThemeLight)
        chartS.setBackgroundBrush(QColor(255,255,255, 255))
        chartS.setAnimationOptions(QChart.AnimationOption.SeriesAnimations)


        categories = ["1. S", "2. S", "3. S", "4. S", "5. S", "6. S","7. S", "8. S", "9. S", "10. S", "11. S", "12. S"]


        axis = QBarCategoryAxis()
        axisV = QValueAxis()

        axisV.setRange(0, 100)
        axisV.setTickCount(11)

        axis.append(categories)

        chartS.addAxis(axis, Qt.AlignmentFlag.AlignBottom)
        chartS.addAxis(axisV, Qt.AlignmentFlag.AlignLeft)
        seriesS.attachAxis(axis)
        seriesS.attachAxis(axisV)
        chartS.legend().setVisible(True)
        chartS.legend().setAlignment(Qt.AlignmentFlag.AlignBottom)
        self.chart_viewS = QChartView(chartS)
        
        
        barSetLayout1.addWidget(self.chart_viewS)
        self.frame7.setLayout(barSetLayout1)




    # okumanın aylara göre sayısı
        barSetLayout2 = QHBoxLayout()
        barSetLayout2.setContentsMargins(4,4,4,4)

        self.set0A = QBarSet("")
        self.set1A = QBarSet("")
        self.set1A.setColor(QColor(255,255,255, 0))
        self.set0A.setLabelColor(QColor(0, 0, 0, 250))


        seriesA = QPercentBarSeries()
        seriesA.append(self.set0A)
        seriesA.append(self.set1A)
        seriesA.setBarWidth(0.9)
      #  seriesA.setLabelsVisible(True)
       # seriesA.setLabelsPrecision(3)
       # seriesA.setLabelsAngle(0.5)
        seriesA.setLabelsPosition(QAbstractBarSeries.LabelsPosition.LabelsOutsideEnd)

        chartA = QChart()
        chartA.addSeries(seriesA)
        chartA.setTheme(QChart.ChartTheme.ChartThemeLight)
        chartA.setBackgroundBrush(QColor(255,255,255, 250))
        chartA.setAnimationOptions(QChart.AnimationOption.SeriesAnimations)

        
        categories = ["Oca", "Şub", "Mar", "Nis", "May", "Haz","Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"]
        axis = QBarCategoryAxis()
        axisV = QValueAxis()
        axisV.setRange(0, 100)
        axisV.setTickCount(11)
        axis.append(categories)
        chartA.addAxis(axis, Qt.AlignmentFlag.AlignBottom)
        chartA.addAxis(axisV, Qt.AlignmentFlag.AlignLeft)
        seriesA.attachAxis(axis)
        seriesA.attachAxis(axisV)
        chartA.legend().setVisible(True)
        chartA.legend().setAlignment(Qt.AlignmentFlag.AlignBottom)
        chart_viewA = QChartView(chartA)
        
        barSetLayout2.addWidget(chart_viewA)
        self.frame8.setLayout(barSetLayout2)


        self.dataUpdateBut.clicked.connect(self.usageReports)
        self.dataUpdateBut.setEnabled(self.user.istatistik_yetkisi)

        self.initialDate()

       
    # tarih seçme
    def initialDate(self):

       # print("tarih: ", self.dateFilter.date().toPyDate())
        simdiki_tarih = QDate.currentDate()
        self.dateFilter.setMaximumDate(simdiki_tarih)
        # print("Simdiki ay ", simdiki_tarih.month())
        # Tarih başlangıcı eğitim ögretim yılı başlangıcı kabul edilecek yani 1 Eylül
        # 1-8 ayları arasında ise bir önceki yıl değilse içinde bulunan yıl tarih başlangıç yılı seçilecek

        try:
            if simdiki_tarih.month() >= 1 and  simdiki_tarih.month() <= 8:
                self.dateFilter.setDate(QDate(simdiki_tarih.year() - 1, 9, 1))
            else:
                self.dateFilter.setDate(QDate(simdiki_tarih.year(), 9, 1))
        except:
            self.dateFilter.setDate(simdiki_tarih)



        
    def yuzdeKaci(self, bir_sayi, toplam_sayi):
        if bir_sayi >0 and toplam_sayi >0:
              a = (bir_sayi / toplam_sayi) * 100
            #   print("yuzde", a)
              return  a
        else:
            return 0
        

    def usageReports(self):
        # yenileme butonuna iki defa basılmasını önlemek için initialFonk çağırıldığında butonu devre dışı bırak
        self.dataUpdateBut.hide()
        self.dataUpdateBut.blockSignals(True)
        
        #Veri Topla butonu ilk kez basıldıktan sonra Verileri yenile olarak ismi güncellenecek
        self.islemItetisi.setText("Veri Toplanıyor...")

        QTimer.singleShot(50, lambda: self.usageReports2())


        

    def usageReports2(self):
        start = datetime.now()

        self.cinsiyeteGoreOkumaFonk()
        self.aylaraGoreOkumaFonk()
        self.sinifSeviyesineGoreOkuma()

        self.kitapSayisiFonk()
        self.uyeSayisiFonk()
        self.verilenKitapSayisiFonk()

        self.enCokOkunanKitapFonk()
        self.enCokOkuyanUyelerFonk() 

        end = datetime.now()
        td = (end - start).total_seconds() * 10**3
        print(f"Çalışma süresi: {td:.03f}ms")

        self.dataUpdateBut.show()

        self.islemItetisi.setText("")
        self.dataUpdateBut.setText("  Verileri Yenile")
        self.dataUpdateBut.blockSignals(False)
        
        
    

## sonucları db den alıp widgetlere gönderme
    def enCokOkunanKitapFonk(self):
        self.frame4List.clear()
        # En cok okunan kitaplar listesi
        result = DbQuery.enCokOkunanKitaplarListesi(tarih=self.dateFilter.date().toPyDate())
        #En cok okunan kitaplar (en cok 30) frame4
        for ad, okumaSayisi in result:
            # print(i)
            item = QListWidgetItem(f"""  {okumaSayisi}    {ad}""")
            # item.sizeHint()
            item.setSizeHint(QSize(0, 22))
            self.frame4List.addItem(item)


    def enCokOkuyanUyelerFonk(self):
        self.frame5List.clear()
        # En cok okuyan üyeler listesi
        result = DbQuery.enCokOkuyanUyelerListesi(tarih=self.dateFilter.date().toPyDate())
        #En cok okuyan üyeler
        for  ad, soyad, okumaSayisi in result:
            # print(i)
            item = QListWidgetItem(f"""  {okumaSayisi}    {ad} {soyad}""")
            # item.sizeHint()
            item.setSizeHint(QSize(0, 22))
            self.frame5List.addItem(item)


    def cinsiyeteGoreOkumaFonk(self):
        resultC = DbQuery.cinsiyeteGoreOkumaSayisi(tarih=self.dateFilter.date().toPyDate())
        # print(resultC)
        # self.seriesC.remove(self.seriesC.slices()[0])
        for slice in self.seriesC.slices(): # veri varsa grafikte öncelikle veriyi sil
            self.seriesC.remove(slice)

        for key, value in resultC.items():
            self.seriesC.append(f"{key}", value)
    
        self.seriesC.setLabelsVisible(True)
        
        # series.setLabelsPosition(QPieSlice.)
        for slice in self.seriesC.slices():
            slice.setLabel("{:.2f}%".format(100 * slice.percentage()))

        self.chartC.legend().markers(self.seriesC)[0].setLabel("Erkek")
        self.chartC.legend().markers(self.seriesC)[1].setLabel("Kız")
        self.chartC.setBackgroundBrush(QColor(255, 255, 255))
        self.chartC.legend().setVisible(True)
        self.chartC.legend().setAlignment(QtCore.Qt.AlignmentFlag.AlignBottom)

    


    def aylaraGoreOkumaFonk(self):
        # okumanın aylara göre sayısı
        result = DbQuery.aylaraGoreOkumaSayisi(tarih=self.dateFilter.date().toPyDate())

        sayilar = [i for i in result.values()]
        oranlar = [self.yuzdeKaci(i, sum(sayilar)) for i in sayilar]

        # print("Aylara göre okuma sayısı : ", sayilar, sum(sayilar))


        self.set0A.remove(0, self.set0A.count())
        self.set1A.remove(0, self.set1A.count())
        
        self.set0A.append(oranlar)
        self.set1A.append([100-i for i in oranlar]) # oranları 100 cıkarıp üst gizli bara aktar



        

    
    def sinifSeviyesineGoreOkuma(self):
        # okumanın aylara göre sayısı
        result = DbQuery.siniflaraGoreOkumaSayisi(tarih=self.dateFilter.date().toPyDate())

        sayilar = [i for i in result.values()]
        oranlar = [self.yuzdeKaci(i, sum(sayilar)) for i in sayilar]
        print(oranlar , sayilar)

        self.set0S.remove(0, self.set0S.count())
        self.set1S.remove(0, self.set1S.count())
        
        self.set0S.append(oranlar)
        self.set1S.append([100-i for i in oranlar]) # oranları 100 cıkarıp üst gizli bara aktar
    
        


    def kitapSayisiFonk(self):
        # kitap sayısı
        result1 = DbQuery.kitapSayisi()
        self.labelValue1.setText(str(result1))

    def uyeSayisiFonk(self):
        #uye sayısı
        result2 = DbQuery.uyeSayisi()
        self.labelValue2.setText(str(result2))

    def verilenKitapSayisiFonk(self):
        # alıp verilen kitap sayısı
        result3 = DbQuery.verilenKitapSayisi(tarih=self.dateFilter.date().toPyDate())
        self.labelValue3.setText(str(result3))