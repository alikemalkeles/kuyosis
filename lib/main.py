# -*- coding: utf-8 -*-

import base64
import gettext
import os

from PyQt6 import QtCore
from PyQt6.QtCore import Qt, QTimer, QDateTime, pyqtSlot
from PyQt6.QtGui import QRegularExpressionValidator, QIcon
from PyQt6.QtWidgets import QApplication, QVBoxLayout, QHBoxLayout, QPushButton, QStackedWidget, \
    QLabel, QFrame, QLineEdit, QComboBox, QWidget



from query.dbQuery import *
from mainWindow.mainWindow import *
from customWidget.messageBox import *
from models.model import *
from customWidget.shadowEffect import *

with open("assets/stil/stil_main.qss", "r") as file:
    stil = file.read()


class ParolaGirisPen(QWidget):
    def __init__(self):
        super().__init__()

        WINDOW_WIDTH = 880
        WINDOW_HEIGHT = 530
        GORSEL_WIDTH = 530
        GORSEL_HEIGHT = 530
        FRAME_WIDTH = WINDOW_WIDTH - GORSEL_WIDTH
        FRAME_HEIGHT = WINDOW_HEIGHT  # margin bottom 

        # sistem renk paleti açıktan koyuya doğru 
        # 9896f1
        # 7c73e6
        # 6643b5  

        # sistemde kaıtlı kullanıcıların listes
        self.user = None
        self.kullaniciListesi: Users = []
        self.logKayitlari = []

        self.setWindowTitle("KÜYÖSİS")

        self.tarih = QDateTime.currentDateTime().toString('dd.MM.yyyy')
        self.saat = QDateTime.currentDateTime().toString('hh.mm')

        print(self.tarih, self.saat)

        self.setStyleSheet(stil)

        self.setWindowIcon(QIcon("/opt/kuyosis/assets/icon/kuyosis.png"))

        self.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)

        ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

        self.stacked_widget = QStackedWidget(self)
        self.stacked_widget.setCurrentIndex(0)
        self.stacked_widget.setContentsMargins(0, 0, 0, 0)
    

        ## Kullanıcı giriş penceresi ana cereceve

        self.loginPageFrame = QFrame()
        self.loginPageFrame.setObjectName("anaFrame")
        self.loginPageFrame.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)

        self.loginPageLayout = QHBoxLayout(self.loginPageFrame)
        self.loginPageLayout.setSpacing(0)
        self.loginPageLayout.setContentsMargins(0, 0, 0, 0)

        self.gorsel = QLabel()
        self.gorsel.setFixedSize(GORSEL_WIDTH, GORSEL_HEIGHT)
        self.gorsel.setObjectName("gorsel")

        # kullanıcı giriş ve ID etiketi ana cercevesi
        self.frameParolaUser = QFrame()
        self.frameParolaUser.setObjectName("frameParolaUser")
        self.frameParolaUser.setFixedSize(FRAME_WIDTH, FRAME_HEIGHT)

        ## frameye baglı layout bu layouta bağlı olacaklar
        self.widgetLayout = QVBoxLayout(self.frameParolaUser)

        self.kurum_ismi = QLabel()
        self.kurum_ismi.setObjectName("kurum_ismi")
        self.kurum_ismi.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.kurum_ismi.setWordWrap(True)

        ## Uygulama iconu
        self.appIcon = QLabel()
        self.appIcon.setFixedSize(FRAME_WIDTH, 95)
        self.appIcon.setObjectName("appIcon")
        # self.appIcon.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.etiket = QLabel("GİRİŞ")
        self.girisIDEtiket = QLabel("Kullanıcı")
        self.girisID = QComboBox()
        self.girisParolaEtiket = QLabel("Parola")
        self.girisParola = QLineEdit()
        self.labelMesaj = QLabel("")
        self.girisButon = QPushButton("Giriş Yap")

        self.etiket.setObjectName("etiket")
        self.girisIDEtiket.setObjectName("girisIDEtiket")
        self.girisID.setObjectName("comboBox")
        self.girisParolaEtiket.setObjectName("girisParolaEtiket")
        self.girisParola.setObjectName("girisParola")
        self.labelMesaj.setObjectName("labelMesaj")
        self.girisButon.setObjectName("genel")

        self.etiket.setAlignment(Qt.AlignmentFlag.AlignLeft)
        # self.girisIDEtiket.setAlignment(Qt.AlignmentFlag.AlignLeft)
        self.girisParolaEtiket.setAlignment(Qt.AlignmentFlag.AlignLeft)
        self.labelMesaj.setAlignment(Qt.AlignmentFlag.AlignLeft)

        #  self.girisID.setAlignment(Qt.AlignmentFlag.AlignLeft)
        self.girisParola.setAlignment(Qt.AlignmentFlag.AlignLeft)

        self.girisParola.setEchoMode(QLineEdit.EchoMode.Password)

        self.girisID.setFixedHeight(35)
        self.girisParola.setFixedHeight(45)

        rx1 = QtCore.QRegularExpression("[0-9]{11}")
        v1 = QRegularExpressionValidator(rx1, self.girisID)
        self.girisID.setValidator(v1)

        self.girisButon.setGraphicsEffect(BoxShadowEffect.colorTeal())

        #  self.girisID.setMaxLength(11)
        self.girisParola.setMaxLength(15)

        #   self.girisID.setPlaceholderText("11 haneli kimlik no giriniz...")
        self.girisParola.setPlaceholderText("Parola giriniz...")

        self.iptalButon = QPushButton()
        self.iptalButon.setFixedSize(24, 24)
        self.iptalButon.setObjectName("closeBut")

        self.widgetLayout.addWidget(self.iptalButon, alignment=Qt.AlignmentFlag.AlignRight)

        self.widgetLayout.addWidget(self.kurum_ismi)
        self.widgetLayout.addWidget(self.appIcon)
        self.widgetLayout.addStretch()
        self.widgetLayout.addWidget(self.etiket)
        self.widgetLayout.addWidget(self.girisIDEtiket)
        self.widgetLayout.addWidget(self.girisID)
        self.widgetLayout.addWidget(self.girisParolaEtiket)
        self.widgetLayout.addWidget(self.girisParola)
        self.widgetLayout.addWidget(self.labelMesaj)
        self.widgetLayout.addStretch()
        self.widgetLayout.addWidget(self.girisButon)
        # self.widgetLayout.addStretch()

        self.loginPageLayout.addWidget(self.gorsel, alignment=Qt.AlignmentFlag.AlignCenter)
        self.loginPageLayout.addWidget(self.frameParolaUser, alignment=Qt.AlignmentFlag.AlignCenter)
        # self.loginPageLayout.addStretch()

        ## Yeni kullanıcı ekleme cercevesi

        self.loginCreatePageFrame = QFrame()
        self.loginCreatePageFrame.setObjectName("anaFrameYeni")
        self.loginCreatePageFrame.setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT)

        self.loginCreatePageLayout = QHBoxLayout(self.loginCreatePageFrame)
        self.loginCreatePageLayout.setSpacing(0)
        self.loginCreatePageLayout.setContentsMargins(0, 0, 0, 0)

        # sol taraftaki görsel 530 x 530
        self.gorsel = QLabel()
        self.gorsel.setFixedSize(GORSEL_WIDTH, GORSEL_HEIGHT)
        self.gorsel.setObjectName("gorselYeni")

        # kullanıcı giriş ve ID etiketi ana cercevesi
        self.frameCreateParolaUser = QFrame()
        self.frameCreateParolaUser.setObjectName("frameParolaUserYeni")
        self.frameCreateParolaUser.setFixedSize(FRAME_WIDTH, FRAME_HEIGHT)

        ## frameye baglı layout bu layouta bağlı olacaklar
        self.widgetLayoutYeni = QVBoxLayout(self.frameCreateParolaUser)
        self.widgetLayoutYeni.setContentsMargins(0, 0, 0, 0)

        self.iptalButonY = QPushButton()
        self.iptalButonY.setFixedSize(24, 24)
        self.iptalButonY.setObjectName("closeBut")

        self.etiketYeni = QLabel("YÖNETİCİ HESABI")

        self.kullaniciIdEtiket = QLabel("Kimlik no")
        self.kullaniciId = QLineEdit()

        self.isimEtiket = QLabel("Ad Soyad")
        self.isim = QLineEdit()

        self.statuEtiket = QLabel("Statü")
        self.statu = QComboBox()

        self.parolaEtiket = QLabel("Parola")
        self.parola = QLineEdit()

        self.parolaTekrarEtiket = QLabel("Parolayı Doğrulayınız")
        self.parolaTekrar = QLineEdit()

        self.yeniKullaniciKaydetBut = QPushButton("Kaydet")
        self.yeniKullaniciKaydetBut.setObjectName("genel")

        self.statu.addItems(["Yönetici", "Kullanıcı"])
        self.isim.setMaxLength(25)
        self.parola.setMaxLength(15)
        self.parolaTekrar.setMaxLength(15)
        self.parola.setEchoMode(QLineEdit.EchoMode.Password)
        self.parolaTekrar.setEchoMode(QLineEdit.EchoMode.Password)

        rx = QtCore.QRegularExpression("[0-9]{11}")
        v = QRegularExpressionValidator(rx, self.kullaniciId)
        self.kullaniciId.setValidator(v)

        self.etiketYeni.setObjectName("etiketYeni")

        self.kullaniciIdEtiket.setObjectName("etiketY")
        self.kullaniciId.setObjectName("girisY")
        self.isimEtiket.setObjectName("etiketY")
        self.isim.setObjectName("girisY")
        self.statuEtiket.setObjectName("etiketY")
        self.statu.setObjectName("girisY")
        self.parolaEtiket.setObjectName("etiketY")
        self.parola.setObjectName("girisY")
        self.parolaTekrarEtiket.setObjectName("etiketY")
        self.parolaTekrar.setObjectName("girisY")

        self.widgetLayoutYeni.addWidget(self.iptalButonY,
                                        alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignTop)
        self.widgetLayoutYeni.addStretch()
        self.widgetLayoutYeni.addWidget(self.etiketYeni)
        self.widgetLayoutYeni.addStretch()
        #   self.widgetLayoutYeni.addWidget(self.kullaniciIdEtiket)
        # self.widgetLayoutYeni.addWidget(self.kullaniciId)
        self.widgetLayoutYeni.addWidget(self.isimEtiket)
        self.widgetLayoutYeni.addWidget(self.isim)
        # self.widgetLayoutYeni.addWidget(self.statuEtiket)
        # self.widgetLayoutYeni.addWidget(self.statu)
        self.widgetLayoutYeni.addWidget(self.parolaEtiket)
        self.widgetLayoutYeni.addWidget(self.parola)
        self.widgetLayoutYeni.addWidget(self.parolaTekrarEtiket)
        self.widgetLayoutYeni.addWidget(self.parolaTekrar)
        self.widgetLayoutYeni.addStretch()
        self.widgetLayoutYeni.addWidget(self.yeniKullaniciKaydetBut)

        self.loginCreatePageLayout.addWidget(self.gorsel)
        self.loginCreatePageLayout.addWidget(self.frameCreateParolaUser)

        self.stacked_widget.addWidget(self.loginPageFrame)
        self.stacked_widget.addWidget(self.loginCreatePageFrame)

        ## sinyal slotlar
        self.iptalButon.clicked.connect(self.closeFonk)
        self.iptalButonY.clicked.connect(self.closeFonk)
        self.isim.textChanged.connect(lambda: self.isim.setText(self.karakterBuyut(self.isim.text())))

        self.yeniKullaniciKaydetBut.clicked.connect(self.yeniKullaniciEkleFonk)
        self.girisButon.clicked.connect(self.girisKontrolFonk)

        self.girisParola.returnPressed.connect(self.girisKontrolFonk)

        ## veritabanını başlat (Tabloları oluşturmak için)
        DbQuery.createDatabaseTables()
        ## baslangıc fonksiyonları
        self.kullaniciListeleFonk()
        self.kurumAdiFonk()

    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i", "İ").upper()

    ## parolayı base64 ile kodlama
    def parolaEnc(self, string):
        try:
            string_bytes = string.encode('utf-8')
            b64_bytes = base64.b64encode(string_bytes)
            b64_string = b64_bytes.decode('utf-8')
            # print("Base64 Encoded String: ", b64_string)
            return b64_string
        except:
            return False

    # kurum ismi
    def kurumAdiFonk(self):
        kurum_ismi = DbQuery.ayarlarVeYapilandirmalar().kurum_adi
        self.kurum_ismi.setText(kurum_ismi)

    ## veritabanını başlat kullanıcı yoksa kullanıcı ekleme penceresine yönlendirme ve kullanıcıları listeye gönderme
    def kullaniciListeleFonk(self):
        # database ornekle
        self.kullaniciListesi = DbQuery.kullaniciListesi()
        print(f"Kullanici var mı {bool(self.kullaniciListesi)} - {self.kullaniciListesi}")

        # Eğer sistemde kayıtlı kullanıcı yoksa yeni kullanıcı ekleme widgetine gec(stacked widget 1)
        if bool(self.kullaniciListesi) == False:
            self.stacked_widget.setCurrentIndex(1)
        else:
            self.stacked_widget.setCurrentIndex(0)

            # kullanıcı varsa comboBox a gönder
            for kullanici in self.kullaniciListesi:
                self.girisID.addItem(kullanici.isim)

    # Giris kontrol fonksiyonu
    @pyqtSlot()
    def girisKontrolFonk(self):
        print("kullanici sayısı: ", len(self.kullaniciListesi))

        # combox Da en az bir kişi olmalı
        if not self.girisID.currentIndex() == -1:
            girisId = self.kullaniciListesi[self.girisID.currentIndex()].id
            parola = self.girisParola.text()

            # print(girisId, " bilgiler:", "  index: ", self.girisID.currentIndex())

            if self.parolaEnc(parola):
                result = DbQuery.girisKontrol(girisId, self.parolaEnc(parola))
                # print("giris kontrol:  ", result)
                if result == None:
                    self.labelMesaj.setText("Parola hatalı")
                    self.girisParola.setText("")
                    QTimer.singleShot(3000, lambda: self.labelMesaj.setText(""))

                else:
                    # Giriş yapan kullanıcı
                    self.user = Users(result[0], result[1], result[2], result[3], result[4], result[5], result[6],
                                      result[7], result[8], result[9], result[10], result[11], result[12])

                    self.labelMesaj.setText("Giriş yapılıyor...")

                    DbQuery.girisLog(self.user.id, self.user.isim, self.user.statu, f"{self.tarih} - {self.saat}")

                    # KUllanıcı veya yönetici olarak log kayıtlarını listele
                    if self.user.statu == True:
                        self.logKayitlari = DbQuery.logKayitlari()  # Admin için

                    else:
                        self.logKayitlari = DbQuery.logKayitlariUser(
                            self.user.id)  # Kullanıcı için yalnızca kendi kayıtları

                    QTimer.singleShot(800, lambda: self.mainWindowOpen())


            else:
                print("parola sifrelenemdi")

        else:
            print("Secim kutunsundan kimse secilmedi")

    # ana pencereyi cağirma
    def mainWindowOpen(self):
        self.hide()
        self.mainWindowsFonk()

    # ana pencereyi cağırma
    def mainWindowsFonk(self):
        # print("user", self.user.isim)
        # print("kullanici listesi: ", self.kullaniciListesi)
        #   print("log kayitlari", self.logKayitlari)

        self.mainWin = MainWindow(self.user, self.kullaniciListesi, self.logKayitlari)
        self.mainWin.show()

    # Başlangıcta yönetici hesabı oluştur
    @pyqtSlot()
    def yeniKullaniciEkleFonk(self):
        id_ = None
        isim = self.isim.text()
        statu = True
        parola = self.parola.text()
        parola2 = self.parolaTekrar.text()

        # print(id_, isim, statu, parola, parola2)

        print("tc. doğru")
        if len(isim) >= 5:
            print("ad yeterli")
            if len(parola) >= 5 and len(parola2) >= 5:
                print("Parola uzunluğu yeterli")
                if parola == parola2:
                    print("Parolalr esit kayıt yapılabilir")
                    if self.parolaEnc(parola):
                        print("parola sifrelendi")
                        result = DbQuery.yeniKullaniciEkle(
                            user=Users(id_, isim, True, self.parolaEnc(parola), f"{self.tarih} - {self.saat}", True,
                                       True, True, True, True, True, True, True))
                        if result == 1:
                            self.ornek = MessageBOX("KAYIT BAŞARILI",
                                                    " Yönetici hesabınız başarılı bir şeklilde oluşturuldu.", 1)
                            self.stacked_widget.setCurrentIndex(0)
                            # yeni kullanici eklendikten sonra kullanici listesini yeniden döndürme
                            self.kullaniciListeleFonk()
                        else:
                            self.ornek = MessageBOX("KAYIT EKLENEMDİ",
                                                    " Bilinmeyen veritabanı ekleme hatası oluştu. Tekrar deneyiniz ya da geliştiriciden destek alınız.",
                                                    0)
                    else:
                        self.ornek = MessageBOX("ŞİFRELEME HATASI",
                                                " Parolanız şifrelenemedi. Lütfen tekrar deneyiniz.", 0)
                else:
                    self.ornek = MessageBOX("PAROLALAR UYUŞMUYOR",
                                            " Parolalarınız uyuşmuyor. Lütfen, parolaları kontrol ediniz!", 0)
            else:
                self.ornek = MessageBOX("EKSİK ALANLAR VAR", "Parolanız en az 5 karakterden oluşmalı", 0)
        else:
            self.ornek = MessageBOX("EKSİK ALANLAR VAR", " İsim en az 5 karakerden oluşmalı.", 0)

    def closeFonk(self):
        DbQuery.databaseClosed()
        self.close()


uyg = QApplication(sys.argv)

icon_path = os.path.join(os.path.dirname(__file__), "kuyosis.png")
uyg.setWindowIcon(QIcon(icon_path))

try:
    lang = gettext.translation('kuyosis', localedir='/usr/share/locale', languages=['tr'])
    if lang:
        # print("Dil dosyasına ulaşıldı")
        lang.install()
        _ = lang.gettext

        uyg.setApplicationName(_("kuyosis"))

        print("Uygulama adı : ", uyg.applicationName())

except FileNotFoundError:

    uyg.setApplicationName("Kuyosis")

pen = ParolaGirisPen()
pen.show()
uyg.exec()
