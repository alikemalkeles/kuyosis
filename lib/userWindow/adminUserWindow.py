
import base64
import sys

from PyQt6 import QtCore
from PyQt6.QtCore import Qt, pyqtSignal, QTimer, QDate, QDateTime, pyqtSlot
from PyQt6.QtWidgets import QPushButton, QFrame, QGridLayout, QLabel, QListWidget, QScrollArea, QLineEdit, QVBoxLayout, \
    QHBoxLayout, QSizePolicy, \
    QWidget, QListWidgetItem, \
    QAbstractItemView, QDialog, QSpacerItem

from customWidget.customWidgets import *
from query.dbQuery import *
from customWidget.messageBox import *
from models.model import Users
from customWidget.shadowEffect import BoxShadowEffect

with open("assets/stil/stil_user.qss") as file:
    stil = file.read()


class PasswordReplaceWindow(QDialog):
    def __init__(self, user: Users, screen_width, screen_height):
        super().__init__()

        self.user = user

        self.screen_width = screen_width
        self.screen_height = screen_height

        self.h1 = 38 if self.screen_height > 900 else 32 #  buton yüksekliği


        self.setContentsMargins(0,0,0,0)
        self.setStyleSheet(stil)
        self.setFixedWidth(int((self.screen_width / 5) * 3))
        #self.setFixedHeight(int())

         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)

      #  self.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self.parolaFrame = QFrame()
        self.parolaFrame.setObjectName("userFramePass")
      #  self.parolaFrame.setFixedWidth(self.screen_width)
        self.parolaFrame.setContentsMargins(15, 17,15,12)
 
        layout.addWidget(self.parolaFrame)

        self.parolaLayout = QGridLayout(self.parolaFrame)

        self.title = QLabel("PAROLA GÜNCELLEME")
        self.title1 = QLabel("Mevcut Parolanız")
        self.title2 = QLabel("Yeni Parolanız")
        self.title3 = QLabel("Yeni Parolanınızı Doğrulayın")
        self.title4 = QLabel("")

        self.lineEdit1 = QLineEdit()
        self.lineEdit2 = QLineEdit()
        self.lineEdit3 = QLineEdit()

        self.lineEdit1.setEchoMode(QLineEdit.EchoMode.Password)
        self.lineEdit2.setEchoMode(QLineEdit.EchoMode.Password)
        self.lineEdit3.setEchoMode(QLineEdit.EchoMode.Password)

        self.lineEdit1.setFixedHeight(self.h1)
        self.lineEdit2.setFixedHeight(self.h1)
        self.lineEdit3.setFixedHeight(self.h1)

        self.lineEdit1.setObjectName("passWord")
        self.lineEdit2.setObjectName("passWord")
        self.lineEdit3.setObjectName("passWord")        

        self.lineEdit1.setMaxLength(15)
        self.lineEdit2.setMaxLength(15)
        self.lineEdit3.setMaxLength(15)

        self.title4.setWordWrap(True)

        self.title.setObjectName("baslik")
        self.title1.setObjectName("genel")
        self.title2.setObjectName("genel")
        self.title3.setObjectName("genel")
        self.title4.setObjectName("genelMesaj")

        self.title.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.title1.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.title2.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.title3.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.title4.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)

        self.iptalBut = QPushButton("İptal")
        self.guncelleBut = QPushButton("Güncelle")

        self.guncelleBut.setObjectName("genel")
        self.iptalBut.setObjectName("iptalBut")

        self.iptalBut.setFixedHeight(self.h1)
        self.guncelleBut.setFixedHeight(self.h1)



        self.parolaLayout.addWidget(self.title,      0, 0, 1, 2)
        self.parolaLayout.addItem(QSpacerItem(10, 15, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 1, 0, 1, 2)

        self.parolaLayout.addWidget(self.title1,     2, 0, 1, 2)
        self.parolaLayout.addWidget(self.lineEdit1,  3, 0, 1, 2)
        self.parolaLayout.addItem(QSpacerItem(10, 12, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 4, 0, 1, 2)

        self.parolaLayout.addWidget(self.title2,     5, 0, 1, 2)
        self.parolaLayout.addWidget(self.lineEdit2,  6, 0, 1, 2)
        self.parolaLayout.addItem(QSpacerItem(10, 12, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 7, 0, 1, 2)

        self.parolaLayout.addWidget(self.title3,     8, 0, 1, 2)
        self.parolaLayout.addWidget(self.lineEdit3,  9, 0, 1, 2)
        self.parolaLayout.addItem(QSpacerItem(10, 12, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 10, 0, 1, 2)

        self.parolaLayout.addWidget(self.title4,     11, 0, 1, 2)
        self.parolaLayout.addItem(QSpacerItem(10, 12, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed), 12, 0, 1, 2)

        self.parolaLayout.addWidget(self.iptalBut,      13, 0, 1, 1)
        self.parolaLayout.addWidget(self.guncelleBut,   13, 1, 1, 1)


    # sinyaller
        self.iptalBut.clicked.connect(self.close)
        self.guncelleBut.clicked.connect(self.kullaniciParolaGuncelle)


    ## kullanıcı parola değiştirme fonksiyonu
    @pyqtSlot()
    def kullaniciParolaGuncelle(self):
        
        eskiParola = self.lineEdit1.text()
        yeniParola = self.lineEdit2.text()
        yeniParolaD = self.lineEdit3.text()
        ## tüm lanların doldurulması gerekiyor
        if eskiParola and yeniParola and yeniParolaD:
            eskiParolaEnc = self.parolaEnc(eskiParola)

            # öncelikle kullanıcının eski parolaysını doğru girip girmediğini kontrol etme
            result = DbQuery.girisKontrol(self.user.id, eskiParolaEnc)
            print("giriş kontrol sonucu", bool(result))
            if result:
                # yeni parolanın en az 5 karakter olması gerekiyor
                if len(yeniParola) >= 5:

                    ## yeni parolaların uyuşup uyuşmadığını kontrol etme
                    if yeniParola == yeniParolaD:
                        ## yeni parolayı sifrele
                        yeniParolaEnc = self.parolaEnc(yeniParola)

                        result = DbQuery.kullaniciParolaGuncelle(yeniParolaEnc, self.user.id)

                        if result == 1:
                            MessageBOX("BİLGİLENDİRME", " Parolanız güncellendi",  1)
                            self.close()

                        else:
                            MessageBOX("UYARI", " Parolanız güncellenemedi. (Veritabanı hatası)",  0)
                            QTimer.singleShot(3000, lambda: self.parolaAlaniniTemizle())

                    else:
                        self.title4.setText("Yeni parolanız birbiriyle uyuşmuyor.")
                        QTimer.singleShot(3000, lambda: self.parolaAlaniniTemizle())
                      #  MessageBOX("UYARI", " Yeni parolanız birbiriyle uyuşmuyor.",  0)

                else:
                    self.title4.setText("Yeni parolanız en az 5 karakterden oluşmalı")
                    QTimer.singleShot(3000, lambda: self.parolaAlaniniTemizle())
                   # MessageBOX("UYARI", " Yeni parolanız en az 5 karakterden oluşmalı",  0)

            else:
                self.title4.setText("Mevcut parolanız sistemde kayıtlı olan parolayla uyuşmuyor")
                QTimer.singleShot(3000, lambda: self.parolaAlaniniTemizle())
               # MessageBOX("UYARI", " Mevcut parolanız sistemde kayıtlı olan parolayla uyuşmuyor.",  0)

        else:
            self.title4.setText("Tüm alanları eksiksiz olarak doldurunuz. (Mevcut parolanız, yeni parolanız ve yeni parolayı doğrulayınız)")
            QTimer.singleShot(3000, lambda: self.parolaAlaniniTemizle())
          #  MessageBOX("UYARI", " Tüm alanları eksiksiz olarak doldurunuz. (Mevcut parolanız, yeni parolanız ve yeni parolayı doğrulayınız)",  0)


    ## parolayı base64 ile kodlama
    def parolaEnc(self, string):
        try:
            string_bytes = string.encode('utf-8')
            b64_bytes = base64.b64encode(string_bytes)
            b64_string = b64_bytes.decode('utf-8')
            print("Base64 Encoded String: ", b64_string)
            return b64_string
        except:
            return False

    def parolaAlaniniTemizle(self):
        self.lineEdit1.clear()
        self.lineEdit2.clear()
        self.lineEdit3.clear()

        self.title4.setText("")





## Kullnıcıyı düzenle Widget
class UserEditWindow(QDialog):
    sinyal = pyqtSignal(list)

    def __init__(self, user, screen_width, screen_height, window_type: bool):
        super().__init__()

        self.user = user

        self.screen_width = screen_width
        self.screen_height = screen_height

        self.window_type = window_type

        self.h1 = 38 if self.screen_height > 900 else 32 #  buton yüksekliği


        self.screen_width = int(screen_width / 3)

        self.edit_menu_width = int(self.screen_width / 2)
       

        layout = QVBoxLayout()
        layout.setContentsMargins(0,0,0,0)
        self.setLayout(layout)

        self.setContentsMargins(0,0,0,0)
        self.setStyleSheet(stil)
        self.setFixedWidth(int((self.screen_width / 3) * 1))
        #self.setFixedHeight(int())

         ## pencerey12i baslıksız yapma
        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint | QtCore.Qt.WindowType.WindowStaysOnTopHint)
        # seffaf pencere
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)


        self.userEditFrame = QFrame() # Arkaplan rengi için
        self.userEditFrame.setObjectName("userEditFrame")
        self.userEditFrame.setContentsMargins(0,0,0,0)

        layout.addWidget(self.userEditFrame)




        self.mainLayout = QHBoxLayout(self.userEditFrame)
        self.mainLayout.setContentsMargins(0,0,0,0)



        #Pencerenin sol tarafında yer alan kullnıcıya ait bilgiler
        self.userInfoEditFrame = QFrame()
        self.userInfoEditFrame.setContentsMargins(0,0,0,0)
        self.userInfoEditFrame.setFixedWidth(self.edit_menu_width)
        self.userInfoEditFrame.setObjectName("userInfoEditFrame")


        # Sağ tarafta yetkile yer alacak 
        self.userPermissionLayout = QVBoxLayout()
        self.userPermissionLayout.setContentsMargins(8,8,8,8)


        self.mainLayout.addWidget(self.userInfoEditFrame)
        self.mainLayout.addLayout(self.userPermissionLayout)



    # Yeni kullanıcı Ekleme bilgileri
        self.newUserEditLayout = QVBoxLayout(self.userInfoEditFrame)
        self.newUserEditLayout.setContentsMargins(20, 12, 20, 12)

        self.userIcon = QLabel()
        self.userNameEdit = QLineEdit()
        self.userStatuLabel = QLabel()
        self.userDateLabel = QLabel()
        self.passWordEdit1 = QLineEdit()
        self.passWordEdit2 = QLineEdit()
        self.userYeniGuncelleBut = QPushButton()
        self.userDeleteBut = QPushButton("     Kullanıcıyı Sil     ")

        self.userNameEdit.setMaxLength(20)
        self.passWordEdit1.setEchoMode(QLineEdit.EchoMode.Password)
        self.passWordEdit2.setEchoMode(QLineEdit.EchoMode.Password)
        self.passWordEdit1.setMaxLength(15)
        self.passWordEdit2.setMaxLength(15)


        self.userIcon.setFixedSize(110, 150)
        self.userNameEdit.setFixedHeight(self.h1)
        self.userStatuLabel.setFixedHeight(20)
        self.userDateLabel.setFixedHeight(self.h1)
        self.passWordEdit1.setFixedHeight(self.h1)
        self.passWordEdit2.setFixedHeight(self.h1)
        self.userYeniGuncelleBut.setFixedHeight(self.h1)
        self.userDeleteBut.setFixedHeight(self.h1)

        self.userIcon.setObjectName("userIcon")
        self.userNameEdit.setObjectName("userNameNew")
        self.userStatuLabel.setObjectName("userStatu")
        self.userDateLabel.setObjectName("userDate")
        self.passWordEdit1.setObjectName("passWordNew")
        self.passWordEdit2.setObjectName("passWordNew")
        self.userYeniGuncelleBut.setObjectName("genel")
        self.userDeleteBut.setObjectName("iptalBut")

        self.passWordEdit1.setPlaceholderText("Parola")
        self.passWordEdit2.setPlaceholderText("Parolayı doğrulayınız")


        self.newUserEditLayout.addSpacing(40)
        self.newUserEditLayout.addWidget(self.userIcon,                     alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        self.newUserEditLayout.addSpacing(12)
        self.newUserEditLayout.addWidget(self.userStatuLabel,               alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        self.newUserEditLayout.addSpacing(10)
        self.newUserEditLayout.addWidget(self.userNameEdit)
        self.newUserEditLayout.addSpacing(8)
        self.newUserEditLayout.addWidget(self.passWordEdit1)
        self.newUserEditLayout.addSpacing(8)
        self.newUserEditLayout.addWidget(self.passWordEdit2)
        self.newUserEditLayout.addSpacing(10)
        
        self.newUserEditLayout.addWidget(self.userDateLabel,                alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        
        self.newUserEditLayout.addSpacing(15)
        self.newUserEditLayout.addWidget(self.userYeniGuncelleBut)
        self.newUserEditLayout.addStretch()
        self.newUserEditLayout.addWidget(self.userDeleteBut)


    # Kullanıcı yetkileri ekleme ve düzenleme
        self.closeBut = QPushButton()
        self.closeBut.setFixedSize(24, 24)
        self.closeBut.setObjectName("closeBut")

        self.title5 = QLabel("  Kullanıcının Yetkileri")
        self.title5.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.title5.setFixedWidth(self.screen_width - 20)


        self.ekleme = ItemWidget(title_="EKLEME", subtitle_="Yeni kitap ekleme ve yeni üye ekleme")
        self.guncelleme = ItemWidget(title_="GÜNCELLEME", subtitle_="Üye güncelleme ve kitap güncelleme")
        self.sil = ItemWidget(title_="SİLME", subtitle_="Üye silme ve kitap silme")
        self.ayar = ItemWidget(title_="AYARLARI DEĞİŞTİREBİLME", subtitle_="Kurum bilgilerini değiştirme, sınıf yükseltme, yazar, yayınevi ve raf konumu güncelleme, yedekleme oluşturma ve geri yükleme, uygulamayı güncelleme")
        self.rapor = ItemWidget(title_="SONUÇLARI DIŞARI AKTARMA (XLSX OLARAK)", subtitle_="Kitap arama, üye arama, emanetteki kitaplar listesi, teslim süresi geçmiş kitaplar listesi ve üyenin okuduğu kitaplar listesini xlsx olarak dışarıya aktarma")
        self.istatistik = ItemWidget(title_="KÜTÜPHANE İSTATİSTİKLERİ", subtitle_="Kayıtlı kitap sayısı, kayıtlı üye sayısı, cinsiyete göre okuma oranları, aylara ve sınıflara göre okuma oranları, en çok okuyan üyeler ve en çok okunan kitapları listeleme")
        self.coklu_aktarım = ItemWidget(title_="TOPLU KİTAP VE ÜYE EKLEME", subtitle_="Toplu olarak kita ve üye ekleme")
        self.karekod_kimlik = ItemWidget(title_="KAREKOD VE KİMLİK OLUŞTURMA", subtitle_="Kitaplar için karekod oluşturma ve üye kimliği oluşturma")


        self.userPermissionLayout.addWidget(self.closeBut,  alignment=QtCore.Qt.AlignmentFlag.AlignRight | QtCore.Qt.AlignmentFlag.AlignTop)
        self.userPermissionLayout.addWidget(self.title5,  alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        self.userPermissionLayout.addSpacing(15)
        self.userPermissionLayout.addLayout(self.ekleme)
        self.userPermissionLayout.addLayout(self.guncelleme)
        self.userPermissionLayout.addLayout(self.sil)
        self.userPermissionLayout.addLayout(self.ayar)
        self.userPermissionLayout.addLayout(self.rapor)
        self.userPermissionLayout.addLayout(self.istatistik)
        self.userPermissionLayout.addLayout(self.coklu_aktarım)
        self.userPermissionLayout.addLayout(self.karekod_kimlik)

        


    # sinyaller
        self.closeBut.clicked.connect(self.close)
        self.userYeniGuncelleBut.clicked.connect(self.yeniKullaniciKaydetFonk)
        self.userNameEdit.textChanged.connect(lambda: self.userNameEdit.setText(self.karakterBuyut(self.userNameEdit.text())))
        self.userDeleteBut.clicked.connect(self.kullaniciSil)


        self.setFixedWidth(int((self.screen_width + self.edit_menu_width)))

        self.initialFonk()




    def initialFonk(self):
        if self.window_type == True: # Pencere tipi TRue ise yeni ekleme ögeleri aktif olacak
            self.userNameEdit.setPlaceholderText("Yeni kullanıcı adı ve soyadı")
            self.userStatuLabel.setText("Kullanıcı")

            date = QDate.currentDate()
            self.userDateLabel.setText(f"Eklenme tarihi  {str(date.toString('dd.MM.yyyy'))}")

            self.userYeniGuncelleBut.setText("          Kaydet          ")

            self.userNameEdit.setFocus()

            self.userDeleteBut.setHidden(True)

            # Switch butonlar varsayılan olarak başlangıcta secili değil

        else:
            self.userYeniGuncelleBut.setText("        Güncelle        ")
            self.passWordEdit1.setEnabled(False) # Güncelleme modunda şifre girme yeri görünsün ama değiştirilemesin yalnızca kullanıcı kendisi değiştirebilsin
            self.passWordEdit2.setHidden(True)
            self.passWordEdit1.setText("*******")

            self.userNameEdit.setText(f"{self.user.isim}")
            self.userDateLabel.setText(f"Eklenme tarihi  {self.user.eklenme_tarihi}")
            self.userStatuLabel.setText(f"""{"Yönetici" if self.user.statu == True else "Kullanıcı"}""")

            
            self.ekleme.setChecked_(self.user.ekleme_yetkisi)
            self.guncelleme.setChecked_(self.user.guncelleme_yetkisi)
            self.sil.setChecked_(self.user.silme_yetkisi)
            self.ayar.setChecked_(self.user.ayar_yetkisi)
            self.rapor.setChecked_(self.user.raporlama_yetkisi)
            self.istatistik.setChecked_(self.user.istatistik_yetkisi)
            self.coklu_aktarım.setChecked_(self.user.coklu_aktarim_yetkisi)
            self.karekod_kimlik.setChecked_(self.user.karekod_kimlik_yetkisi)


    # Yeni kullanıcı ekle
    @pyqtSlot()
    def yeniKullaniciKaydetFonk(self):

        id_ = None # otomatik verilir
        isim = self.userNameEdit.text()
        statu = False
        parola1 = self.passWordEdit1.text()
        parola2 = self.passWordEdit2.text()
        eklenme_tarihi = QDateTime.currentDateTime().toString("dd.MM.yyyy - hh.mm")

        ekleme = self.ekleme.isChecked_()
        guncelleme = self.guncelleme.isChecked_()
        silme = self.sil.isChecked_()
        ayar = self.ayar.isChecked_()

        rapor = self.rapor.isChecked_()
        istatistik = self.istatistik.isChecked_()
        coklu_aktarim = self.coklu_aktarım.isChecked_()
        karekod_kimlik = self.karekod_kimlik.isChecked_()



        if self.window_type == True:  # Window type True ise yeni kullanıcı ekle

            if len(isim) >= 5:
                if len(parola1) >= 5:
                    if parola1 == parola2:
                        print("ekleme yapılabilir")
                        if self.parolaEnc(parola1):

                            newUser = Users(id_, isim, statu, self.parolaEnc(parola1), eklenme_tarihi, 
                                            ekleme, guncelleme, silme, ayar, rapor, istatistik, coklu_aktarim, karekod_kimlik
                                            )
                            result = DbQuery.yeniKullaniciEkle(user=newUser)

                            if result == 1:
                                MessageBOX("BİLGİLENDİRME", """ Yeni kullanıcı hesabı başarılı bir şekilde oluşturuldu.""", 1)

                                # yeni kullanıcı eklenince sinyal yayıp kullanıcı listesini güncelle
                                kullanici_listesi = DbQuery.kullaniciListesi()

                                self.sinyal.emit(kullanici_listesi)

                            else:
                                MessageBOX("UYARI", """ Bilinmeyen bir veritabanı hatası oluştu. Tekrar deneyiniz ya da geliştiriciyle iletişim kurunuz""", 0)

                        else:
                            MessageBOX("UYARI", """ Şİfreleme hatası. Geliştiriciyle iletişim kurunuz""", 0)

                    else:
                        MessageBOX("UYARI", """ Parolalar birbiriyle uyuşmuyor""", 0)
                else:
                    MessageBOX("UYARI", """ Parola en az 5 karekter olmalı""", 0)
            else:
                MessageBOX("UYARI", """ Kullanıcı adınız en az 5 karekter olmalı""", 0)

        else:  # Güncelle

            if len(isim) >= 5:

                user = Users(self.user.id, isim, self.user.statu, self.parolaEnc(parola1), self.user.eklenme_tarihi,
                                 ekleme, guncelleme, silme, ayar, rapor, istatistik, coklu_aktarim, karekod_kimlik)
                result = DbQuery.kullaniciGuncelle(user)

                if result == 1:

                    MessageBOX("BİLGİLENDİRME", """ Kullanıcı başarılı bir şeklilde güncellendi.""", 1)

                    # Kullanıcı güncellenince sinyal yayıp kullanıcı listesini güncelle
                    kullanici_listesi = DbQuery.kullaniciListesi()

                    self.sinyal.emit(kullanici_listesi)

                else:
                    MessageBOX("UYARI", """ Bilinmeyen bir veritabanı hatası oluştu. Tekrar deneyiniz ya da geliştiriciyle iletişim kurunuz""", 0)

                       
            else:
                MessageBOX("UYARI", """ Kullanıcı adınız en az 5 karekter olmalı""", 0)          




    #KUllanıcı sil
    @pyqtSlot()
    def kullaniciSil(self):
        silinecek_kullanici = self.user.id

        msg = MessageBOXQuestion("UYARI", f" {self.user.isim} adlı kullanıcıyı silmek istediğinize emin misiniz?", 0)
        msg.okBut.clicked.connect(lambda: self.kullaniciSil2(silinecek_kullanici)) 

    def kullaniciSil2(self, silinecek_kullanici):
        result = DbQuery.kullaniciSil(silinecek_kullanici)
        if result == 1:
            MessageBOX("BİLGİLENDİRME", """ Seçilen kullanıcı silindi""", 1)

            # Kullanıcı güncellenince sinyal yayıp kullanıcı listesini güncelle
            kullanici_listesi = DbQuery.kullaniciListesi()

            self.sinyal.emit(kullanici_listesi)

            self.close()

        else:
            MessageBOX("UYARI", """ Bilinmeyen bir hata oluştu. Tekrar deneyiniz""", 0) 

     ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()


    ## parolayı base64 ile kodlama
    def parolaEnc(self, string):
        try:
            string_bytes = string.encode('utf-8')
            b64_bytes = base64.b64encode(string_bytes)
            b64_string = b64_bytes.decode('utf-8')
            print("Base64 Encoded String: ", b64_string)
            return b64_string
        except:
            return False


class AdminUserWindow(QFrame):

    sinyal = pyqtSignal(Users)

    def __init__(self, user, screen_width, screen_height, userList, loginRecords):
        super().__init__()

        self.setObjectName("userFrame")
        self.setStyleSheet(stil)

        self.setContentsMargins(0, 0, 0, 0) # 52 sol menu genişliği

        self.user = user           # aktif kullanıcı
        self.userList = userList   # KUllnıcılar listesi
        self.loginRecords = loginRecords


        self.screen_width = int(screen_width / 3)
        self.screen_height = screen_height

        self.h1 = 38 if self.screen_height > 900 else 32 #  buton yüksekliği
        self.h2 = 240 if self.screen_height > 900 else 180 # Kullınıcı giriş kayıtları en küçük yükseklik oranı

        self.scrollLayout = QVBoxLayout(self)
        self.scrollLayout.setContentsMargins(0,0,0,0)

        self.widget = QWidget()
        self.widget.setContentsMargins(0,0,0,0)
     #   self.widget.setSizePolicy(QSizePolicy.Policy.Preferred,  QSizePolicy.Policy.Fixed)


        self.scrollAreaWid = QScrollArea()
        self.scrollAreaWid.setWidgetResizable(True)
        self.scrollAreaWid.setLayoutDirection(Qt.LayoutDirection.LeftToRight)   #setWidget(self.mainFrame)
        self.scrollAreaWid.setWidget(self.widget)   #   setLayout(self.mainLayout)
        self.scrollAreaWid.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        self.scrollAreaWid.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)

        self.scrollLayout.addWidget(self.scrollAreaWid)



        # Kullanıcı cercevesi 
        self.userFrameInfo = QFrame() #başlangıcta tüm kullanıcılar için açık

        # Kayıtlı kullanıcılar
        self.userFrameUsers = QFrame() 
       

        # Yetkiler cercevesi (kullnıcı için)
        self.userFramePermission = QFrame()
        

        # Giriş kayıtları
        self.userFrameLoginRecords = QFrame()
        

    ## KUllanıcı cercevesi ortak
        self.userFrameInfo.setContentsMargins(12, 40, 12, 35)
        self.userFrameInfo.setGraphicsEffect(BoxShadowEffect.colorBlueDark())
        self.userFrameInfo.setObjectName("user")
        self.userFrameInfo.setFixedWidth(self.screen_width)
        

        self.userFrameLayout = QGridLayout(self.userFrameInfo)
        self.userFrameLayout.setContentsMargins(0,0,0,0)
        self.userFrameLayout.setVerticalSpacing(0)

        self.userIcon = QLabel()
        self.userNameEdit = QLineEdit()
        self.userEditBut = QPushButton()
        self.userStatuLabel = QLabel()
        self.userDateLabel = QLabel()
        self.userSessionClose = QPushButton(" Oturumu Kapat ")
        self.title1 = QLabel("Parola")
        self.title2 = QLabel("* * * * *")
        self.userPasswordEditBut = QPushButton()

        self.userIcon.setFixedHeight(int(self.h1 * 3) + 10)
        self.userNameEdit.setFixedHeight(self.h1)
        self.userEditBut.setFixedHeight(self.h1)
        self.userStatuLabel.setFixedHeight(20)
        self.userDateLabel.setFixedHeight(self.h1)
        self.userSessionClose.setFixedHeight(self.h1)
        self.title1.setFixedHeight(self.h1)
        self.title2.setFixedHeight(self.h1)
        self.userPasswordEditBut.setFixedHeight(self.h1)

        self.userNameEdit.setEnabled(False)
        self.userNameEdit.setMaxLength(20)

        self.title1.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.title2.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        

        self.userIcon.setObjectName("userIcon")
        self.userNameEdit.setObjectName("userName")
        self.userEditBut.setObjectName("userEdit")
        self.userStatuLabel.setObjectName("userStatu")
        self.userDateLabel.setObjectName("userDate")
        self.userSessionClose.setObjectName("genel")
        #self.title1.setObjectName("")
        self.title2.setObjectName("title2")
        self.userPasswordEditBut.setObjectName("userPasswordEdit")


        self.userFrameLayout.addWidget(self.userIcon,                1, 0, 5, 2)
        self.userFrameLayout.addWidget(self.userNameEdit,            1, 2, 1, 5)
        self.userFrameLayout.addWidget(self.userEditBut,             1, 8, 1, 2)
        self.userFrameLayout.addWidget(self.userStatuLabel,          2, 2, 1, 5)
        self.userFrameLayout.addWidget(self.userDateLabel,           3, 2, 1, 5)
        self.userFrameLayout.addWidget(self.userSessionClose,        3, 8, 1, 2)
        self.userFrameLayout.setRowStretch(6, 22)
        self.userFrameLayout.addWidget(self.title1,                  6, 0, 1, 2)
        self.userFrameLayout.addWidget(self.title2,                  6, 3, 1, 3)
        self.userFrameLayout.addWidget(self.userPasswordEditBut,     6, 8, 1, 2)


        # Yalnızca admin için aktif
        self.frameYeni = QFrame()
        self.frameYeni.setFixedWidth(self.screen_width) 
        

        layout1 = QHBoxLayout(self.frameYeni)
        layout1.setContentsMargins(0,0,6,0)

        self.title3 = QLabel("  Kullanıcılar")
        self.title3.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.title3.setFixedWidth(self.screen_width)

        self.yeni_kullanici = QPushButton()
        self.yeni_kullanici.setText("     Yeni Kullanıcı     ")

        self.yeni_kullanici.setFixedHeight(self.h1)
        self.yeni_kullanici.setObjectName("genel")

        layout1.addWidget(self.title3)
        layout1.addWidget(self.yeni_kullanici)



        self.title5 = QLabel("  Yetkilerim")
        self.title5.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.title5.setFixedWidth(self.screen_width)
        


        self.title4 = QLabel("  Giriş Kayıtları")
        self.title4.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.title4.setFixedWidth(self.screen_width)

        

        self.mainLayout = QVBoxLayout(self.widget)
        self.mainLayout.setContentsMargins(0,12,52,12)

        self.mainLayout.addWidget(self.userFrameInfo,         alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addSpacing(22)
        self.mainLayout.addWidget(self.frameYeni,             alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addSpacing(12)
        self.mainLayout.addWidget(self.userFrameUsers,        alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addSpacing(22)

        self.mainLayout.addWidget(self.title5,                alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addSpacing(2)
        self.mainLayout.addWidget(self.userFramePermission,   alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)

        self.mainLayout.addSpacing(22)
        self.mainLayout.addWidget(self.title4,                alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addSpacing(2)
        self.mainLayout.addWidget(self.userFrameLoginRecords, alignment=QtCore.Qt.AlignmentFlag.AlignCenter | QtCore.Qt.AlignmentFlag.AlignTop)
        self.mainLayout.addStretch()

        
        
    
    # Kayıtlı kullanıcılar
        self.userFrameUsers.setFixedWidth(self.screen_width)
        self.userFrameUsers.setContentsMargins(12, 12, 12, 12)
        self.userFrameUsers.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.userFrameUsers.setObjectName("userFrame")

        self.userFrameUsersLayout = QVBoxLayout(self.userFrameUsers)
        self.userFrameUsersLayout.setContentsMargins(0,0,0,0)

        self.usersListWidget = QListWidget()
        self.usersListWidget.setSpacing(0)
        self.usersListWidget.setContentsMargins(0,0,0,0)
        self.usersListWidget.setSelectionMode(QAbstractItemView.SelectionMode.NoSelection)
     #   self.usersListWidget.setSizeAdjustPolicy(QAbstractScrollArea.SizeAdjustPolicy.AdjustToContents
        self.userFrameUsersLayout.addWidget(self.usersListWidget)



    # Yetkiler (yalnızca kullanıcı için)
        self.userFramePermission.setFixedWidth(self.screen_width)
        self.userFramePermission.setContentsMargins(12, 12, 12, 12)
        self.userFramePermission.setMinimumHeight(self.h2)
        self.userFramePermission.setGraphicsEffect(BoxShadowEffect.colorBlue())
        self.userFramePermission.setObjectName("userFrame")

        self.userFramePermissonLayout = QVBoxLayout(self.userFramePermission)

        self.ekleme = ItemWidget(title_="EKLEME", subtitle_="Yeni kitap ekleme ve yeni üye ekleme")
        self.guncelleme = ItemWidget(title_="GÜNCELLEME", subtitle_="Üye güncelleme ve kitap güncelleme")
        self.sil = ItemWidget(title_="SİLME", subtitle_="Üye silme ve kitap silme")
        self.ayar = ItemWidget(title_="AYARLARI DEĞİŞTİREBİLME", subtitle_="Kurum bilgilerini değiştirme, sınıf yükseltme, yazar, yayınevi ve raf konumu güncelleme, yedekleme oluşturma ve geri yükleme, uygulamayı güncelleme")
        self.rapor = ItemWidget(title_="SONUÇLARI DIŞARI AKTARMA (XLSX OLARAK)", subtitle_="Kitap arama, üye arama, emanetteki kitaplar listesi, teslim süresi geçmiş kitaplar listesi ve üyenin okuduğu kitaplar listesini xlsx olarak dışarıya aktarma")
        self.istatistik = ItemWidget(title_="KÜTÜPHANE İSTATİSTİKLERİ", subtitle_="Kayıtlı kitap sayısı, kayıtlı üye sayısı, cinsiyete göre okuma oranları, aylara ve sınıflara göre okuma oranları, en çok okuyan üyeler ve en çok okunan kitapları listeleme")
        self.coklu_aktarım = ItemWidget(title_="TOPLU KİTAP VE ÜYE EKLEME", subtitle_="Toplu olarak kitap ve üye ekleme")
        self.karekod_kimlik = ItemWidget(title_="KAREKOD VE KİMLİK OLUŞTURMA", subtitle_="Kitaplar için karekod oluşturma ve üye kimliği oluşturma")


        
        self.userFramePermissonLayout.addLayout(self.ekleme)
        self.userFramePermissonLayout.addLayout(self.guncelleme)
        self.userFramePermissonLayout.addLayout(self.sil)
        self.userFramePermissonLayout.addLayout(self.ayar)
        self.userFramePermissonLayout.addLayout(self.rapor)
        self.userFramePermissonLayout.addLayout(self.istatistik)
        self.userFramePermissonLayout.addLayout(self.coklu_aktarım)
        self.userFramePermissonLayout.addLayout(self.karekod_kimlik)



    # Kullanıcı giriş kayıtları
        self.userFrameLoginRecords.setFixedWidth(self.screen_width)
        self.userFrameLoginRecords.setMinimumHeight(180)
        self.userFrameLoginRecords.setContentsMargins(1, 8, 1, 12)
      #  self.userFrameLoginRecords.setGraphicsEffect(BoxShadowEffect.colorBlueDark())
        self.userFrameLoginRecords.setObjectName("userFrame")

        loginLayouts = QVBoxLayout()
        self.userFrameLoginRecords.setLayout(loginLayouts)

        
        self.logList = QListWidget(self.userFrameLoginRecords)
        self.logList.setContentsMargins(0,0,0,0)

        loginLayouts.addWidget(self.logList)
    

    # Sinyal slotlar

        self.userNameEdit.textChanged.connect(lambda: self.userNameEdit.setText(self.karakterBuyut(self.userNameEdit.text())))
        self.userEditBut.clicked.connect(self.kullaniciAdiniGuncelle)
        self.userSessionClose.clicked.connect(self.restart)
        self.userPasswordEditBut.clicked.connect(self.parolaGuncelleFonk)
        self.yeni_kullanici.clicked.connect(lambda: self.kullaniciyiDuzenle(True))
    

        self.initialFonk()

        

    def initialFonk(self):

        # Ortak başlatma alanları (admin + kullanıcı)
        self.userNameEdit.setText(self.user.isim)
        self.userStatuLabel.setText(" Yönetici" if self.user.statu == True else " Kullanıcı")
        self.userDateLabel.setText(f" Hesap Tarihi  {self.user.eklenme_tarihi}")

        #list widgeti öncelikle temizle
        self.usersListWidget.clear()

        # Yönetici için görünecek widgetler
        if self.user.statu:

            self.userFrameUsers.setHidden(False) #kullanıcılar
            self.frameYeni.setHidden(False) # Yeni kullanıcı ekleme butonu çercevesi yalnızca yönetici için
            self.userFramePermission.setHidden(True) #yetkiler
            self.title5.setHidden(True) # Yetkilerim başlığı

            ## Kullanıcı listesini yazdırmadan önce yöneticiyi listede çıkarma
            kullaniciler_id = [i.id for i in self.userList]
            index = kullaniciler_id.index(self.user.id)
            print("kullanici id: ", kullaniciler_id, index)

            if self.user.id in kullaniciler_id:
                # Yönetici listede bulunduğu için kaldırıldı
                self.userList.remove(self.userList[index])

            #Kullanıcıları listele

            item_h = 50
            for kullaniciler in enumerate(self.userList, 0):

                item = kullaniciler[1].isim
                print("Kullanıcılar: ", kullaniciler[1].isim)

                itemWid = QWidget()
               # itemWid.setFixedHeight(40)
             #   itemWid.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Ignored)

                item = QListWidgetItem(self.usersListWidget)
                # item_widget = ItemListWidget(f"{kullaniciler[0].isim}",  f"Hesap tarihi {kullaniciler[0].eklenme_tarihi}")
                
                layout = QGridLayout()

                icon = QLabel()
                title = QLabel(f"{kullaniciler[1].isim}")
                subtitle = QLabel(f"Hesap tarihi {kullaniciler[1].eklenme_tarihi}")

                self.button = QPushButton()
                self.button.setObjectName(f"{kullaniciler[0]}")
                self.button.setFixedHeight(32)
                self.button.setStyleSheet(""" 
                        QPushButton {
                                background-image: url("./assets/icon/user-edit.png");
                                background-position: center center;
                                background-repeat: no-repeat;
                                background-color: transparent;
                                outline: 0px;
                                }
                        QPushButton::hover {
                            background-image: url("./assets/icon/user-edit-hover.png");
                            background-position: center center;
                            background-repeat: no-repeat;
                            background-color: transparent;
                            outline: 0px;
                        }
                """)
                self.button.clicked.connect(lambda: self.kullaniciyiDuzenle(False))


                icon.setObjectName("iconCustom")
                title.setObjectName("titleCustom")
                subtitle.setObjectName("subtitleCustom")
                

                layout.addWidget(icon,       0, 0, 2, 1)
                layout.addWidget(self.button,     0, 8, 2, 2)

                layout.addWidget(title,      0, 1, 1, 8)
                layout.addWidget(subtitle,   1, 1, 1, 8)


                itemWid.setLayout(layout)

                item.setSizeHint(itemWid.sizeHint())

                self.usersListWidget.setItemWidget(item, itemWid)

                item_h = itemWid.height()

            self.usersListWidget.setFixedHeight(int(item_h * len(self.userList)) + 32)
            print("item boyutu: ", int(item_h * len(self.userList)) + 32)

            ## LOg kayıtları
            for log in self.loginRecords:
               # print(log, log[2])
                self.logList.addItem(f"{'Yönetici' if log[2] == '1' else 'Kullanıcı'}  {log[3]}  {log[1]}")

        else:
            # KUllanıcı için widgetler
            self.userFrameUsers.setHidden(True) #kullanıcılar
            self.frameYeni.setHidden(True) # Yeni kullanıcı ekleme butonu çercevesi yalnızca yönetici için
            self.userFramePermission.setHidden(False) #yetkiler
            self.title5.setHidden(False) # Yetkilerim başlığı

            self.ekleme.setChecked_(self.user.ekleme_yetkisi)
            self.guncelleme.setChecked_(self.user.guncelleme_yetkisi)
            self.sil.setChecked_(self.user.silme_yetkisi)
            self.ayar.setChecked_(self.user.ayar_yetkisi)
            self.rapor.setChecked_(self.user.raporlama_yetkisi)
            self.istatistik.setChecked_(self.user.istatistik_yetkisi)
            self.coklu_aktarım.setChecked_(self.user.coklu_aktarim_yetkisi)
            self.karekod_kimlik.setChecked_(self.user.karekod_kimlik_yetkisi)

            self.userFramePermission.setEnabled(False)
            


            for i in self.loginRecords:
                self.logList.addItem(f"{i[3]}")


    # Kullanıcıyı düzenle
    @pyqtSlot()
    def kullaniciyiDuzenle(self, yeni: bool):
        if yeni:
            self.kullaniciyiDuzenleWin = UserEditWindow(self.user, int(self.screen_width*3), int(self.screen_height*3), True) # True ise yeni kullanıcı
            self.kullaniciyiDuzenleWin.sinyal.connect(self.guncellenenKullaniciListesi)
            self.kullaniciyiDuzenleWin.exec()

        else:
            self.sender_ = self.sender()
            print("basılan buton index:", self.sender_.objectName())

            if self.sender_:
                self.kullaniciyiDuzenleWin = UserEditWindow(self.userList[int(self.sender_.objectName())], int(self.screen_width*3), int(self.screen_height*3), False) # False ise düzenle
                self.kullaniciyiDuzenleWin.sinyal.connect(self.guncellenenKullaniciListesi)
                self.kullaniciyiDuzenleWin.exec()



    
    # Kullanıcı eklenince veya kullanıcı silince kullanıcı listesini db den cek
    def guncellenenKullaniciListesi(self, kullanici_listesi):
        print("Sinyal alındı:,", kullanici_listesi)
        if kullanici_listesi:
            self.userList = kullanici_listesi
            self.initialFonk()

            

    # PArola güncelleme ekranı
    @pyqtSlot()
    def parolaGuncelleFonk(self):
        print("pencere aktif")
        self.ornek = PasswordReplaceWindow(self.user, self.screen_width, self.screen_height)
        self.ornek.exec()


    # kullanıcı adını güncelleme alanını aktif etme
    @pyqtSlot()
    def kullaniciAdiniGuncelle(self):
        # isim alanını düzenlenebilir kıl
        if self.userNameEdit.isEnabled() == False:
            self.userNameEdit.setEnabled(True)
            self.userNameEdit.setFocus()
            self.userNameEdit.setStyleSheet(""" 
                        QLineEdit{  
                                border-bottom: 1px solid #00b9c1;
                                border-top: none;
                                border-left: none;
                                border-right: none;
                                background-color: transparent;
                                color: black;
                                font-size: 20px;
                                font-weight: 600;
                                outline: 0px;
                                border-radius: 0px;
                                }
                         """)

            self.userEditBut.setStyleSheet("""QPushButton {
                                            background-image: url("assets/icon/save.svg");
                                            background-position: center;
                                            background-repeat: no-repeat;
                                            background-color: transparent;
                                            outline: 0px;
                                            padding-left: 9px;
                                          }"""
                                        )
        else:

            if len(self.userNameEdit.text()) >= 3:
                result = DbQuery.kullaniciAdiGuncelle(str(self.userNameEdit.text()), self.user.id)
                if result == 1:
                    print("BAşarılı")

                    self.userNameEdit.setEnabled(False)
                    self.setFocus()
                    self.userNameEdit.setStyleSheet(""" 
                                QLineEdit {  
                                         border: none;
                                        background-color: transparent;
                                        font-weight: 800;
                                        outline: 0px;
                                        color: #5daff3;
                                        font-size: 22px;
                                        }
                                 """)
                    self.userEditBut.setStyleSheet("""QPushButton {
                                                    background-image: url("assets/icon/edit-3.svg");
                                                    background-position: center;
                                                    background-repeat: no-repeat;
                                                    background-color: transparent;
                                                    outline: 0px;
                                                    padding-left: 9px;
                                                  }"""
                                                )

                    self.sinyal.emit(Users(
                        self.user.id, 
                        self.userNameEdit.text(), 
                        self.user.statu, 
                        self.user.parola,
                        self.user.eklenme_tarihi,
                        self.user.ekleme_yetkisi,
                        self.user.guncelleme_yetkisi,
                        self.user.silme_yetkisi,
                        self.user.ayar_yetkisi,
                        self.user.raporlama_yetkisi,
                        self.user.istatistik_yetkisi,
                        self.user.coklu_aktarim_yetkisi,
                        self.user.karekod_kimlik_yetkisi
                        )
                    )

                else:
                    print("Başarısız")
                    MessageBOX("HATA", """ Kayıt, veritabanı hatasından dolayı güncellenmedi.""", 1)
            
            else:
                MessageBOX("UYARI", """ Kullanıcı adınız en az 3 karakter olmalı. """, 0)
                print("Kullanıcı adı en az 3 karakter olmalı")


    ## karekter büyütme fonksiyonu
    def karakterBuyut(self, girdi):
        return girdi.replace("i","İ").upper()
   


    ## uygulamayı yeniden başlatma (oturum kapatma)
    @pyqtSlot()
    def restart(self):
        QtCore.QCoreApplication.quit()
        status = QtCore.QProcess.startDetached(sys.executable, sys.argv)
        print(status)